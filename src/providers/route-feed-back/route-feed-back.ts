import { Injectable } from '@angular/core';
import { Platform} from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable()
export class RouteFeedBackProvider {
  userId:string = null;
  constructor(public platform: Platform, public afAuth: AngularFireAuth) {
    this.platform.ready().then(()=>{

        this.afAuth.authState.subscribe( user => {
          if(user){
            this.userId = user.uid;
          }

        });
      });
  }

  async uploadSuggestedGrade(gymId:string, mapsId:string, routeId:string, suggestedGrade:number){

  }

  getCommentsCollectionPath(gymId:string,mapsId:string, routeId:string){
    if(gymId && mapsId && routeId){
      return `/GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/FEEDBACK/`;
    }else{ throw new Error('route-reed-back prov, cannot get Feedback path: Bad input')};
  }
}
