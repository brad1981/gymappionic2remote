import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteEditorPage } from './route-editor';
//import { CameraComponent } from '../../components/camera/camera';
//import { SvgRouteComponent } from '../../components/svg-route/svg-route';
import { ComponentsModule } from '../../components/components.module';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

@NgModule({
  declarations: [
    RouteEditorPage,
    //ComponentsModule,
    //CameraComponent,
    //SvgRouteComponent,
  ],
  imports: [
    IonicPageModule.forChild(RouteEditorPage),
    ComponentsModule
  ],
  providers: [
    AndroidFullScreen,
  ]
})
export class RouteEditorPageModule {}
