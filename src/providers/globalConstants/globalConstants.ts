/* For globaly accesed constant variables only */

'use strict';
import {Location} from '../../providers/filtered-routes/filtered-routes';

// export const photoWidth = 320;
export const photoWidth = 270;
export const photoHeight = 480;
export const pAspectRatio = photoWidth / photoHeight;
export const lAspectRatio = photoHeight / photoWidth;
export const landscapeSuffix = "_L";
export const listDisplayWidth:number = 60;
export const listDisplayHeight:number = 120;

//The data available in firestore at USERS/{userId}
//TODO: switch all imports to get User interface from here, rathewr than user-details
export interface User{
  displayName: string,
  photoURL: string

}

//TODO: delete this inteface (starts with lowercase)
export interface holdObject {
  color: number;
  pointCor: {x: number, y:number};
  radius: number;
}

export interface HoldObject {
  color: number;
  pointCor: {x: number, y:number};
  radius: number;
}

export interface routeObject {
  pushKey:string; //for finding the comment and updating or removing it
  setBy:string; //'me' or 'setter' ...for distinguishing official routes from DIY routes
  color?:string;//only routes with setBy=='setter' should have a color
  topo: {photos: string[], holds: holdObject[]};
  name: string;
  authorName:string; //TODO: phase in userName for storage location rather than authorId
  authorMug?:string; //storage url (also used for local storage)
  authorId?: string;
  features: any;    //TODO implement interface for feature
  location: Location; // TODO: import location object from feed.ts to this file.
  mapId: string;
  floor?: number;
  imageUrl?:string;
  coverPhoto?:string;//path to image in firebase storage
  betaPhotos?:string[]; // TODO determine type
  grade: number;
  consGrade: number;
  consensusGrade?:number;
  wallAngle?:number;
  description?:string;
  markedForDelete?:firebase.firestore.Timestamp;
  markedUID?:string;
  markedName?:string;
  markedContest?: string[];
  markedAffirm?: string[];
}

export interface DeleteContest{
  date: firebase.firestore.Timestamp;
  deleteVoterIds: string[];
  contestVoterIds: string[];
}

export interface RouteObject {
  pushKey:string; //for finding the comment and updating or removing it
  setBy:string; //'me' or 'setter' ...for distinguishing official routes from DIY routes
  color?:string;//only routes with setBy=='setter' should have a color
  topo: {photos: string[],
         holds: holdObject[] ,
         holdsDims?: {width:number, height:number}[]//array of dimensions objects,
                                                    //specifying width and height of 
                                      //corresp photos. NOTE: confusing name!
        };
  name: string;
  authorName:string; //TODO: phase in userName for storage location rather than authorId
  authorMug?:string; //storage url (also used for local storage)
  authorId?: string;
  features: any;    //TODO implement interface for feature
  location: Location; // TODO: import location object from feed.ts to this file.
  mapId: string;
  floor?: number;
  imageUrl?:string;
  coverPhoto?:string;//path to image in firebase storage
  betaPhotos?:string[]; // TODO determine type
  grade: number;
  consGrade: number;
  consensusGrade?:number;
  wallAngle?:number;
  description?:string;


  markedForDelete?:firebase.firestore.Timestamp;//date first time a user voted to delete it
  deleteVoterIds?:string[]; //the ids of all users who think this should be deleted
  contestVoterIds?:string[];//ids of all users who contest and think it should NOT be deleted

  //TODO: remove this deprecated stuff... it is not in the deleteContest property
  markedUID?:string;
  markedName?:string;
  markedContest?: string[];
  markedAffirm?: string[];



}

export interface MapDimensions{
  height: number,
  width: number
}

export interface ContainerDimensions{
  x:number, y:number
}

export interface SvgMapObject {
  mode:string;
  mapDimensions:MapDimensions;
  mapContents:string;
  newRoute:Location;
  selectedRoute:routeObject;
  routes:routeObject[];
  containerDimensions:ContainerDimensions;
}

export interface ListObject {
  listType?: string;
  name: string;
  id?: string;
  permission?: string;
  floorNum: number;
  listArray: string[];
  mapId:string;
}
/************** Filters ***************************/
export interface RangeFilter{
  //for example: grades:{lower: 0, upper:6}
  lower:number;
  upper:number;
}

export interface RouteFilters {
  colors?:{
    [key:string]:boolean;//show all of these colors in the feed
  }
  setBy?:string;

  attributes?:{
    [key:string]:boolean;
  }

  rangeFilters:{//examples: grades, 'reachyness'
    [key:string]:RangeFilter
  }

}

export interface RouteEditorFeatures{

  attributes?:{
    [key:string]:boolean;
  }

  rangeFilterVal:{//Unlike RouteFilters, RouteEditor's have a SINGLE value for things like "grade" or "wallAngle"
    [key:string]: number;
  }
}

// export interface ExclusiveAttribute {
//   prompt:string,
//   choiceKeys: number[],
//   choices: {
//     [key:number]:{
//       label: string,
//       value: string
//     }
//   }
// }

export const filterCategoriesObject = {
  // exclusiveAttributes:{//a route can only have on trait from each category
  //   'official':{prompt: "Who set this route?",
  //               choiceKeys: [0,1],//for convenience looping through keys in templates
  //               choices: {
  //                 0: {label: "Me/Us",
  //                     value: "me" },
  //                 1:{label: "Route Setter(s)",
  //                    value: "setter"},
  //               }
  //             },
  //           },
  attributes:{
    'style': {label: 'Style',
              options: ['power','balancy','stem','dyno','mantle']},
    'holdTypes': {label: 'Hold Types',
                  options: ['crimpy','pockets', 'sloper','open-hand','pinchy','fingery']},
    'footWork': {label: 'Foot Work',
                options: ['heal-hook','toe-hook','edging','foot-swap']},
  },
  rangeFilterCategories:{
    'grade':{
      label:'Grades',
      min:{
        label: 'Minimum Grade',
        value: 0
      },
      max:{
        label: 'Maximum Grade',
        value: 14
      },
    },
    'wallAngle':{
      label:'Wall Angle',
      min:{
        value: 0
      },
      max:{
        value:5
      },
      valueMap: {//if valueMap is present, use it to declare labels
        0:{
          label: 'Low Angle Slab',
          explanation: 'The wall is less than 60 degrees.' },
        1: {
          label: 'Slab',
          explanation: 'The wall is 60 to 80 degrees.'
        },
        2: {
          label: 'Vertical',
          explanation: 'The wall is nearly vertial'
        },
        3: {
          label: 'Steep',
          explanation: 'The wall overhangs 15 to 30 degrees.'
        },
        4: {
          label: 'Very Steep',
          explanation: 'The wall overhangs by 30 to 60 degrees'
        },
        5: {
          label: 'Roof',
          explanation: 'The wall overhangs by more than 60 degrees'
        }
      }

    },

  },

}

export interface EditorOptions {
  features:{
    [key:string]:boolean,
  },
  grade:number,
  wallAngle:number
}

export const defaultFilters = {
        attributes:{
          slab:false,
          fingery:false,
          mantle:false,
          face:false,
          pumpy:false,
          sloper:false,
          overhang:false,
          balancy:false,
          pinchy: false,
        },

        rangeFilters: {
          grades: {lower:0, upper:6}
        }
      };
/************ File System ********************************/

//feedCache
export const feedCacheName = 'FeedCache';

/******************** Tools ****************************/
/*
usage: import { betterConsole } as console from '<pathToGlobalConstants>';
let console = betterConsole;

...now you can just type console.log("I'm telling the truth!") and it won't lie to you.
If you want it to lie to you, just do: console.lie("This message may contain lies")

*/
export const betterConsole = {
  log: (...args)=>{
    let msg = [];
    args.forEach(arg => {
      if(arg !== undefined){//do not try to parse/stringify undefined
        msg.push(JSON.parse(JSON.stringify(arg)));
      }else{//JSON.parse does not play nice with undefined
        msg.push("undefined");
      }
    });
    console.log(...msg);
  },
  lie: (...args)=>{
    console.log(...args);
  }
}

/******************* Color-coding routes by grade *********************/
//translate colors names to color codes

export const colorNamesToColorCodes = {

        yellow: "#e0d53e",
        red: "#b42222",
        green: "#4bb022",
        purple: "#37177a",
        orange: "#e8651c",
        black: "#050404",
        blue: "#0e3898",
        pink:"#ee7dd2",
        white: "#e2e6e5",
}
//
// export const colorNamesToGrades = {
//         yellow: "V0",
//         red: "V0 to V2",
//         green: "V1 t0 V3",
//         purple: "V2 to V4",
//         orange: "V3 to V5",
//         black: "V4 to V6",
//         blue: "V4 to V6",
//         pink:"V6 to V8",
//         white: "V8 to V10+",
// }
//
export const orderedColorNames:string[] =  ['yellow',
                                            'red',
                                            'green',
                                            'purple',
                                            'orange',
                                            'black',
                                            'blue',
                                            'pink',
                                            'white' ];

// export function convertGradeToColorName(grade:number){
//   if(grade === 0) {
//     return 'white';
//   }else if(grade  >= 1 && grade <= 2){
//     return 'yellow';
//   }else if(grade === 3){
//     return 'green';
//   } else if(grade === 4){
//     return 'red';
//   } else if(grade  === 5){
//     return 'blue';
//   } else if(grade  === 6){
//     return 'orange';
//   } else if(grade  >= 7 && grade <= 8){
//     return 'purple';
//   } else if(grade  >= 9){
//     return 'black';
//   }
//
// }
//
// export function convertGradeToColorCode(grade:number):string{
//   console.log('converting grade: ', grade);
//   console.log('name: ', convertGradeToColorName(grade));
//   console.log('code: ', colorNamesToColorCodes[convertGradeToColorName(grade)]);
//   return colorNamesToColorCodes[convertGradeToColorName(grade)];
// }
