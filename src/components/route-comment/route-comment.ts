import { Component, Input, ViewChild } from '@angular/core';
import {RouteDetails4Provider, Comment, Reply } from '../../providers/route-details4/route-details4';
import { AngularFireAuth } from '@angular/fire/auth';

import {List} from 'ionic-angular';
import {IonicImageCacheConfig} from 'ionic3-image-cache';

//use connProv to control comment and reply button behavior:
//if connected, write a comment/reply, else show pop up saying connect to internet
import{ ConnectionProvider } from '../../providers/connection/connection';
import { Platform } from 'ionic-angular';

import { betterConsole, User }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

@Component({
  selector: 'route-comment',
  templateUrl: 'route-comment.html'
})
export class RouteCommentComponent {
  @ViewChild('commentsList') ionCommentsList: List;

  @Input() gymId: string = null;
  @Input() mapsId: string = null;
  @Input() routeId:string = null;
  @Input() numComments: number = null;

  //used for template component inclusion to avoid async initialization errors
  commentsReady:boolean = false;//set true after comments have been loaded from the server
  platformReady:boolean = false;//set true after platform.ready() resolves

  comments: Comment[] = null;

  //template-accessible vars for managing new comments
  showCommentBox:boolean = false;//triggers new comment input field inclusion in template
  replyBoxVisible:boolean = false; //use to hide other reply buttons and post comment buttons
  showCommentLoadingSpinner:boolean = false;
  disableCommentBox:boolean = false;
  newCommentMessage: string= null;

  //template-accesible vars for managing new replies
  // showReplyBox:boolean = false; //triggers showing new reply input field
  // showReplyLoadingSpinner:boolean = false;
  // disableReplyBox:boolean = false;
  newReplyMessage: string = null;

  userId:string = null;
  user:User = null;

  // userComment



  constructor(public connProv: ConnectionProvider,
    public platform:Platform,
    public commentsProv: RouteDetails4Provider,
    public afAuth: AngularFireAuth,
    public ionicImageCacheConfig: IonicImageCacheConfig
  ) {
    ionicImageCacheConfig.setCacheDirectoryName("avatar");
    platform.ready().then(async ()=>{
     this.afAuth.authState.subscribe( user => {
        if(user){
          this.user = user;

          this.userId = user.uid;
        }

      });
      this.platformReady = true;
      await this.init();
    });

  }

  async init(){
    try{
      if( this.gymId && this.mapsId && this.routeId){
        this.comments = await this.commentsProv.getCommentsAndReplies(this.gymId, this.mapsId, this.routeId);
        if(this.comments){
          this.comments.forEach(comment=> {
            comment['showReplyBox'] = false;
            comment['showReplyButton'] = true;
          });

        }else{
          // console.log(`Setting this.comments to [] `);
          this.comments = [];
        }

        //console.log('route-comments component, init. got comments as:');
        //console.log(this.comments);

        this.commentsReady = true;
      }
    }
    catch(error){
      // console.log('route-comments component, error in init:');
      // console.lie(error);
    };
  }

  //TODO figure out how to get returned reply into correct place in this.comments
  async getMoreReplies(comment:Comment, i:number){
    try{
      // console.log('route-comment component, getMoreReplies fired. comment.comment is: ', comment.comment);
      // console.log('route-comment component, getMoreReplies fired. comment.lastReplySnap is: ', comment.lastReplySnap);
      // console.log('route-comment component, getMoreReplies fired. comment.lastReplySnap.data() is: ', comment.lastReplySnap.data());

      if( this.gymId && this.mapsId && this.routeId){
        // console.log('getMoreReplies: calling getMoreReplies');
        const newRepliesAndLastReplySnap = await this.commentsProv.getMoreReplies(this.gymId,this.mapsId,this.routeId,comment);
        // console.log('got newRepliesAndLastReplySnap.newReplies as');
        // console.log(newRepliesAndLastReplySnap.newReplies);
        this.comments[i].replies = this.comments[i].replies.concat(newRepliesAndLastReplySnap.newReplies);

        // console.log('rute-comment component, this.comments[i].replies is now:');
        // console.log(this.comments[i].replies);
        this.comments[i].lastReplySnap = newRepliesAndLastReplySnap.lastReplySnap;
        return true;
      }else{
        throw new Error('route-comment, getMoreReplies error: need gymId, mapsId and routeId');
      }
    }
    catch(error){
      // console.log('route-comments, getMoreReplies error: ');
      // console.lie(error);
    };

  }

  //TODO: trigger scroll to correct item. This seems to be a tedious, stultifying problem.
  async getMoreComments(){
    try{
      if( this.gymId && this.mapsId && this.routeId){
        const moreComments = await this.commentsProv.getMoreComments(this.gymId, this.mapsId, this.routeId);
        //console.log('route-comments component, getMoreComments. got moreComments as:');
        //console.log(moreComments);

        moreComments.forEach(comment=>{
          comment.showReplyBox = false;
          comment.showReplyButton = true;
        });

        this.comments.concat(moreComments);

      }
    }
    catch(error){
      console.log('route-comments component, error in init:');
      console.lie(error);
    };

  }

  async postComment(){
    if(!this.newCommentMessage || this.newCommentMessage === ""){
      return false;
    }
    try{
      this.showCommentLoadingSpinner = true;
      this.disableCommentBox = true;
      const newComment = await this.commentsProv.uploadComment(this.gymId, this.mapsId, this.routeId, this.newCommentMessage);

      this.disableCommentBox = false;
      this.showCommentLoadingSpinner = false;
      this.showCommentBox = false;
      this.newCommentMessage = null;

      //so user can see the new comment in the feed
      this.comments = !!this.comments ? this.comments : []; //if this.comments is undef, set it to empty array

      this.comments.unshift(newComment);

      //update the count of available comments
      this.numComments = this.numComments ? this.numComments + 1 : 1;
      if(this.numComments === 1){
        //just created 1st comment, so make sure they can see it...
        this.commentsReady = true;
      }
      // console.log('route-commment component, postComment finished executing. this.comments is now:');
      // console.log(this.comments);
    }
    catch(error){
      console.log('route-comments component, postComment. Error:');
      console.lie(error);
    };
  }

  async postReply(comment:Comment, i:number){
    // console.log('______postReply_______' );
    // console.log('this.newReplyMessage is:', this.newReplyMessage);
    try{
      if(!this.newReplyMessage || this.newReplyMessage === ""){ //don't let anyone try to post an empty comment
        // console.log('returning false!');
        return false;
      }

      // console.log('______postReply_______, after if.')
      this.comments[i]['showReplyLoadingSpinner'] = true;
      this.comments[i]['disableReplyBox'] = true;

      // console.log('postReply, attempting to upload');
      const replyAndSnap = await this.commentsProv.uploadReply(this.gymId, this.mapsId,
        this.routeId, comment.commentId, this.newReplyMessage );
       const newReply = replyAndSnap.reply;

      // console.log('postReply, finished uploading');
      this.comments[i]['disableReplyBox'] = false;
      this.comments[i]['showReplyLoadingSpinner'] = false;
      this.cancelReplyAndRevealAllReplyButtons(comment);
      this.newReplyMessage = null;
      this.replyBoxVisible = false;

      //so user can see the new reply in the feed
      if(!this.comments[i].hasOwnProperty('replies')){
        this.comments[i]['replies'] = [newReply];
      }else{
        this.comments[i].replies.push(newReply);
      }

      this.comments[i]['numReplies'] = this.comments[i].numReplies ? this.comments[i].numReplies + 1 : 1;

    }catch(error){

      this.comments[i]['disableReplyBox'] = false;
      this.comments[i]['showReplyLoadingSpinner'] = false;
      this.cancelReplyAndRevealAllReplyButtons(comment);
      this.newReplyMessage = null;
      this.replyBoxVisible = false;

      console.log('route-comments component, postReply. Error:');
      console.lie(error);
    };

  }

  //NOTE: used to check ownership of comments and replies!
  //if you change this method, make sure it still works with reply objects!
  isOwner(messageObject:{authorId:string}){
    if(!this.userId){
      //if you're logged in as a guest, you're not the owner of anything
      return false;
    }else{
      if(this.userId === messageObject.authorId){
        return true;
      }else{
        return false;
      }
    }
  }

  toggleCommentIsSelected(comment:Comment){
    comment['isSelected'] = comment.hasOwnProperty('isSelected') ? !comment.isSelected : true;
  }

  toggleReplyIsSelected(reply:Reply){
    reply['isSelected'] = reply.hasOwnProperty('isSelected') ? !reply.isSelected : true;
  }

  //hides this comment's reply box and reveals all reply buttons
  cancelReplyAndRevealAllReplyButtons(comment:Comment){
    comment.showReplyBox = false;
    this.comments.forEach(comment => comment['showReplyButton'] = true);
    this.replyBoxVisible = false;
  }

  hideReplyButtonsAndShowReplyBox(comment:Comment){
    // console.log('hideReplyButtonsAndShowReplyBox triggered!');
    this.comments.forEach(comment=> comment['showReplyButton'] = false);
    comment.showReplyBox = true;
    this.replyBoxVisible = true;

  }

  // toggleShowComment(){
  //
  // }

  //TODO: for now, template only shows delete icon if connected to the internet.
  //NOTE: a cloud function should delete all of this comment's replies
  async deleteComment(comment:Comment, i:number){
    // console.log('route-comments component, deleteComment triggered  ');
    try{
      await this.commentsProv.deleteComment(this.gymId, this.mapsId, this.routeId, comment.commentId);
      //remove the comment from the displayed comments and delete all of it's replies.
      this.numComments = this.numComments > 1 ? this.numComments - 1 : 0;
      this.comments.splice(i,1);
    }
    catch(error){

    };
  }

  async deleteReply(reply:Reply, comment:Comment, replyIndex:number, commentIndex: number){
    try{
      await this.commentsProv.deleteReply(this.gymId, this.mapsId,this.routeId, comment.commentId, reply.replyId );
      comment.numReplies = comment.numReplies - 1 >= 0 ? comment.numReplies - 1: 0;
      comment.replies.splice(replyIndex,1);
    }catch(error){
      console.log("route-comment component, deleteReply ERROR");
      throw error;
    };

  }

  cancelComment(){

  }

}
