import { Component,
         ViewChild } from '@angular/core';
import { IonicPage,
         NavController,
         ToastController,
         Platform,
         Loading,
         LoadingController,
         Alert,
         AlertController,
         Navbar,
         Events,
       } from 'ionic-angular';
//import { RoutesListPage } from '../routes-list/routes-list';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//TODO: maybe integrate AngularFireAuth stuff into the AuthProvider for more modular style
import { AuthProvider } from '../../providers/auth/auth';
import { AngularFireAuth } from '@angular/fire/auth';

import * as firebase from 'firebase/app';
import { Facebook } from '@ionic-native/facebook';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

   @ViewChild('navBar') navBar : Navbar ;
  public loginForm:FormGroup;
  public loading:Loading;
  private loggedIn = false;
  private loaded = false;
  private newUser = false;
  public backButtonPressedToExit = false
  public unregisterBackButtonAction : Function = null;

  prepareToExit: () => Promise<any> = null; //a reference to the 'exit:prepare' callback for unsubscribing purposes

  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public authProvider: AuthProvider,
              public formBuilder: FormBuilder,
              public alertCtrl: AlertController,
              public afAuth: AngularFireAuth,
              public toastCtrl: ToastController,
              public facebook: Facebook,
              public platform: Platform,
              public events: Events

  ) {

    console.log(' ***** LoginPage constructor **********' );
    this.loginForm = formBuilder.group({
      email: ['', Validators.required],
      //the minLength of 6 is a firebase rule
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])]
    });

  }

  //TODO: this doesn't publish the exit:prepare event, so it depends on arriving at the login page without having
  //to clean up subscriptions and such in other parts of the app.
  //Perform a code audit and make sure that this assumption is valid.
  private customHandleBackButton(): void {
    console.log("******** customHandleBackButton triggered ************* ");
    if(this.backButtonPressedToExit){
      console.log('login.ts (page), exiting app');

      this.platform.exitApp();
    } else{
          console.log('showing toast, setting backButtonPressedToExit=true....');
          this.showExitToast();
          this.backButtonPressedToExit = true;
          setTimeout(() => {
            this.backButtonPressedToExit =false;
          }, 2000);
        }
      }

  showExitToast() {
    let toast = this.toastCtrl.create({
      message: 'Press Again to exit',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }

  public initializeBackButtonCustomHandler(): void {
    console.log('login.ts (page), initializeBackButtonCustomHandler');
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
        this.customHandleBackButton();
    }, 10) as Function;
  }

  ionViewDidLoad(){

    console.log('login.ts (page), ionViewDidLoad');
    this.platform.ready().then(()=>{

    console.log('login.ts (page), ionViewDidLoad, platform.ready callback');


      //the prepareToExit callback is triggered by the exit:prepare event which is published
      //in app.component.ts (it is important in other pages, where prep work must be done
      // before exiting)
      if( ! this.unregisterBackButtonAction ){
        console.log('login.ts (page), paltform.ready callback: calling initializeBackButtonCustomHandler');
        this.initializeBackButtonCustomHandler();
      };

    });
  }


  goToResetPassword():void {
    this.newUser = true;
    this.navCtrl.push('ResetPasswordPage');
  }

  goToSignupPage(){
    this.newUser = true;
    this.navCtrl.push('SignupPage');
  }

  loginUser():Promise<any>{
   if (!this.loginForm.valid){
     // console.log(this.loginForm.value);
   } else {
     const email:string = this.loginForm.value.email;
     const password:string = this.loginForm.value.password;

     // this.loading = this.loadingCtrl.create();
     // this.loading.present();
     //
     // await this.authProvider.loginUser(email,password).catch(error=>{
     //     this.loading.dismiss().then( () => {
     //     const alert:Alert = this.alertCtrl.create({
     //       message: error.message,
     //       buttons: [
     //         {
     //           text: "Ok",
     //           role: 'cancel'
     //         }
     //       ]
     //     });
     //     alert.present();
     //   });
     // });
     //
     // this.loading.dismiss().then( () => {
     //
     //    this.loggedIn = true;
     //    this.navCtrl.pop();
     //  });

     this.loading = this.loadingCtrl.create();
     this.loading.present();

     return this.authProvider.loginUser(email, password).then( () => {
         this.loading.dismiss().then( () => {

           this.loggedIn = true;
           //changed from 'pop' because you can get here from SettingsPage
           //(counter-intuitive to click LogIn and be taken to settings if you are a returning user who has settings)
           //if settings are not defined, root page redirects to settings anyway
           //NOTE: in general, with the login/settings/filters, we should probably always explicitly say which page to pop to insetead of just using pop
           //since the pages are not consistently pushed in the same order
           this.navCtrl.popToRoot();
         });
     }, error => {
       this.loading.dismiss().then( () => {
         const alert:Alert = this.alertCtrl.create({
           message: error.message,
           buttons: [
             {
               text: "Ok",
               role: 'cancel'
             }
           ]
         });
         alert.present();
       });
     });


   }
 }


 //TODO: delete this stuff
 // loginFacebook() {
 //     this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
 //     .then((res) => console.log(res));
 //   }
 //
 async ionViewWillLeave():Promise<any>{
   if(this.unregisterBackButtonAction){
     this.unregisterBackButtonAction();
     this.unregisterBackButtonAction = null;
   }
   if(this.loggedIn){
     return true;
   }
   else if(this.newUser){
     return true;
   }
   else{
     try{
       await this.loginUser();
       return true;
     }
     catch(err){
       console.log("ionViewWillLeave loginUser throwing error:", err);
       return false;
     }
   }
 }


 async signInWithFacebook() {
   this.loading = this.loadingCtrl.create();
   this.loading.present();
   console.log('signInWithFacebook....');
   if (this.platform.is('cordova')) {
     console.log("...platform cordova ..." );
     return this.facebook.login(['email', 'public_profile']).then(async res => {
       const facebookCredential  = firebase.auth.FacebookAuthProvider.credential(res.authResponse.accessToken);
       console.log('got facebook credential as:');
       console.log(facebookCredential);
       const credential = await this.authProvider.signInFacebookUser(facebookCredential);
       if(credential){
         this.loggedIn = true;

         await this.navCtrl.popToRoot();
         this.loading.dismiss();
       }
     }).catch(error=>{
        this.loading.dismiss();
        console.log('could not log in to facebook. got error');
        console.lie(error);
     });
    }
    else { //this probably will not work (not sure)... I think it only works in a legit web browser
      return this.afAuth.auth
        .signInWithPopup(new firebase.auth.FacebookAuthProvider())
        .then(res => {
          this.loading.dismiss();
          console.log(res);
        });
    }
 }



 ionViewCanLeave():boolean{

   //console.log("ionViewCanLeave");
   if(!this.newUser){
     if(!this.loggedIn && this.loaded){

       const alert:Alert = this.alertCtrl.create({
         message: "Please log in to continue.",
         buttons: [
           {
             text: "Ok",
             role: 'cancel'
           }
         ]
       });
       alert.present();
     }
     return this.loggedIn;
   }else{
     this.newUser = false;
     return true;
   }
 }
}
