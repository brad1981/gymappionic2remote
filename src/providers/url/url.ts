import { Injectable } from '@angular/core';

@Injectable()
export class UrlProvider {
  localStorageModifiers: any = {
    'settings':'_set',//input: userId
    'gymId':'_g'//input: userId

  }

  constructor() {
  }

  getFireStore(){

  }

  getStorageUrL(base:string, urlType:string){
    if (this.localStorageModifiers.hasOwnProperty(urlType)){
      return base+this.localStorageModifiers[urlType];
    }
    else{
      return null;
    }
  }

  getRelatedStorageUrls(routeIds:Array<string>){
    // input: array of route ids for finding routes in local storage
    //output: array of all ids that derive from those ids:
    //example: input ['route1','route2'] --> output: ['c_route1','c_route2']
    //NOTE: does not search stored arrays and objects which might contain these ids
    //NOTE: if more related information is stored, update getIdFromRouteId
    let relatedIds = [];
    if (!(!routeIds) && routeIds.length > 0){
      //recreate or modify this loop to add more id types
      //loop for adding comment ids
      for (let id of routeIds){
        let related = this.getStorageUrL(id,'comments');
        if(related){
          relatedIds.push(related);
        }
      }
    }
    if(relatedIds.length > 0){
      // console.log('related storage urls are:');
      // console.log(relatedIds);
      return relatedIds;
    }
  }

}
