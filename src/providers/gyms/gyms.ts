import { Injectable } from '@angular/core';
import{ ConnectionProvider } from '../../providers/connection/connection';
import { AngularFireAuth } from '@angular/fire/auth';

import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

import { LocalStorageProvider } from '../../providers/local-storage/local-storage';
import { UrlProvider } from '../../providers/url/url';

/*
  Generated class for the GymsProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
export interface GymData {
  location: any;
  name: string;
  gymId?: string;
  id?:string;
}

@Injectable()
export class GymsProvider {
  gymData: GymData[];
  currentGym: GymData;

  constructor(public connection: ConnectionProvider, public afs: AngularFirestore,
  public localStorageProv: LocalStorageProvider, public urlProv: UrlProvider,
  public afAuth: AngularFireAuth) {
    // console.log('Hello GymsProvider Provider');
  }

  setCurrentGym(gymId){
    //Caution: does not work unless gymData have been loaded
    let gymData = this.findGymDataById(gymId);
    if(gymData){
      // console.log("1.) setting gymData as:");
      // console.log(gymData);
      this.currentGym = gymData;
    }else{
      return null;
    }
  }

  findGymDataById(gymId):GymData{
    //TODO: if can't find gymId in this.gymData, try fetching gymData from firestore
    //if not there and it's from offline mode, prompt user to connect to internet and try again

    // console.log("findGymDataById, BEFORE if");
    // console.log("this.gymData 'is' ");
    // console.log(this.gymData);
    if(this.gymData && this.gymData.hasOwnProperty('length')){
    // console.log("findGymDataById, inside if");
      for (let gym of this.gymData){
        if( gym.id = gymId){
          return gym;
        }
      }
      return null;
    }
  }

  setCurrentGymAsync(gymId:string):Promise<GymData>{
    return new Promise((res,rej)=>{
      this.findGymDataByIdAsync(gymId).then(( gym )=>{
        // console.log("2.) Setting currentGym as:");
        // console.log(gym);
        this.currentGym = gym;
        res(this.currentGym);
      }).catch((error)=>{
        rej(error);
      });
    });
  }

  findGymDataByIdAsync(gymId):Promise<GymData>{
  return new Promise((res,rej)=>{
    let gym = this.findGymDataById(gymId);
    if(gym){//the gymId was already in the list of available gyms
      res(gym);
    }else{ //might not have found because there were no/outdated gymData to search through
      this.setGymOptions().then(( gymData )=>{
        let gym = this.findGymDataById(gymId);
        if(gym){
          res(gym);
        }else{
          // console.log("Could not find gym with id: "+ gymId+ 'in the following options' );
          // console.log(this.gymData);
          rej(new Error('Could not find gym with id: '+ gymId ));
        }
      }).catch((error)=>{
        rej(error);
      });
    }
  });
  }

  setAndStoreCurrentGym(gymId):Promise<GymData>{
    //possible improvements: if not in current list of gymOptions, maybe query
    //firestore to see if that list has changed, then search again
    return new Promise((res,rej)=>{
    this.setCurrentGymAsync(gymId).then(( gymData )=>{
      if(gymData && this.afAuth.auth.currentUser){
        // console.log("Just setCurrentGymAsync, resolved. Saving to local storage");
        let url = this.urlProv.getStorageUrL(this.afAuth.auth.currentUser.uid, 'gymId');
        this.localStorageProv.saveLocalCopy(url,gymData).then(( result )=>{
          res(gymData);
        }).catch((error)=>{
          rej(error);
        });
      }else{
        rej(new Error('Could not save current gym, possibly because is not in list of gym options'));
      }
    }).catch((error)=>{
        //handle error
    });
      let gymData = this.findGymDataById(gymId);
      // console.log("Got gymData as:");
      // console.log(gymData);

    });
  }

  getCurrentGym():Promise<GymData>{
    return new Promise((res,rej)=>{
      // console.log("In getCurrentGym");
      if(this.currentGym){
        // console.log("0.) In getCurentGym, this.currentGym already defined as:");
        // console.log(this.currentGym);
        res(this.currentGym);
      }else{
        if(this.afAuth.auth.currentUser){
          let url = this.urlProv.getStorageUrL(this.afAuth.auth.currentUser.uid, 'gymId');
          this.localStorageProv.getData(url).then(( gymData )=>{
            // console.log("3.) Setting current gym as: ");
            // console.log(gymData);
            if(gymData){
              this.currentGym = gymData;
              res(gymData);
            }else{
              rej(new Error('Could not get gymData from local storage'));
            }
          }).catch((error)=>{
            rej(error);
          });
        }else{
          rej(new Error('Cannot get current gym from storage if user is not logged in'));
        }
      }
    });
  }

  setGymOptions():Promise<GymData[]>{
    return new Promise((res,rej)=>{
      this.gymData = [];
      // if (this.connection.hasFireConn){
        let db = firebase.firestore();
        db.collection('GYMS').get().then(( snap )=>{
          // console.log('*******snap is:**********');
          // console.log(snap);
          snap.forEach( (doc)=>{
            let data = doc.data() as GymData;
            data['gymId']=doc.id;

            // console.log("******Pushing: *******");
            // console.log(data);
            this.gymData.push(data);
          });
          res(this.gymData);
        }).catch((error)=>{
          rej(new Error('Could not set gymOptions'));
        });;
      // }
    });
  }

  //get an array of GymData for user to select their gym from.
  //if no network connection, try to get the options from the offline firestore cache
  //else, try to get the options from the server
  //if cannot get at least one GymData doc, throw an error
  async setGymOptions2():Promise<GymData[]>{
    this.gymData = [];

    let ref:firebase.firestore.CollectionReference = null;
    let db = this.afs.firestore;
    if(!this.connection.isConnected){
      //disableNetwork forces firestore to only return results from the cache (improving performance)
      await db.disableNetwork();
      ref = db.collection('GYMS');
      let docs = await ref.get();
      await db.enableNetwork();

      docs.forEach( (doc)=>{
        let data = doc.data() as GymData;
        data['gymId']=doc.id;

        // console.log("******Pushing: *******");
        // console.log(data);
        this.gymData.push(data);
      });
    }else{//try to get the gymOptions from the server (don't disable network)
      ref = db.collection('GYMS');
      let docs = await ref.get();
      docs.forEach( (doc)=>{
        let data = doc.data() as GymData;
        data['gymId']=doc.id;

        // console.log("******Pushing: *******");
        // console.log(data);
        this.gymData.push(data);
      });
    }
    if (this.gymData.length > 0){
      return this.gymData;
    }else{
      throw new Error('gyms.ts (provider), setGymOptions2, Could not fetch gymData');
    }
  }

}
