import { Component } from '@angular/core';
import { App, Platform, ToastController, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from '@angular/fire/auth';
import { RoutesFeedPage } from '../pages/routes-feed/routes-feed';
//import { RoutesListPage } from '../pages/routes-list/routes-list';
//import { SyncatronProvider } from '../providers/syncatron/syncatron';
//TODO delete local storage. only using it for Testing
import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { SettingsProvider } from '../providers/settings/settings';
import { UserDetailsProvider, User } from '../providers/user-details/user-details';
import { Events } from 'ionic-angular';

import { AppCenterAnalytics } from '@ionic-native/app-center-analytics';
import { AppCenterCrashes } from '@ionic-native/app-center-crashes';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //NOTE: Unsubscribes all of it's listeners.
  rootPage:any;
  backButtonPressedOnceToExit: boolean = false;
  user:User;

  constructor(app: App, platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    afAuth: AngularFireAuth,
    private toastCtrl: ToastController,
    //public syncatron: SyncatronProvider,
    public storage:LocalStorageProvider,
    public settingsProv: SettingsProvider,
    public events: Events,
    public userDetailsProv: UserDetailsProvider,
    private appCenterAnalytics: AppCenterAnalytics,
    private AppCenterCrashes: AppCenterCrashes,
    public menu: MenuController
    ) {

    // console.log("app.component.ts");
    platform.ready().then(() => {
      // console.log("platform ready from app.component.ts");

      this.appCenterAnalytics.setEnabled(true).then(() => {
        this.appCenterAnalytics.trackEvent('My Event', { TEST: 'HELLO_WORLD' }).then(() => {
          console.log('Custom event tracked');
        });
      });

      this.AppCenterCrashes.setEnabled(true).then(() => {
        this.AppCenterCrashes.lastSessionCrashReport().then(report => {
          console.log('Crash report', report);
        });
      });





      //this.userDetailsProv.testScope();
      //when back button is pressed 2x, app.component.ts
      //emits 'exit:prepare' event, feed-cache runs
      //shutdown promises .then(=> emits 'exit:ready' event)

      //initializing instabug.
      //
      // cordova.plugins.instabug.activate(
      //   {
      //       android: "77ef81dbf64380e002c2530d35ce60bf"
      //   },
      //   "shake",
      //   {
      //     commentRequired: true,
      //     colorTheme: "dark"
      //   },
      //   function () {
      //     console.log("Instabug initialized.");
      //   },
      //   function (error) {
      //     console.log("Instabug could not be initialized - " + error);
      //   }
      // );



      statusBar.styleDefault();
      splashScreen.hide();

      platform.registerBackButtonAction(() => {
        let nav = app.getActiveNavs()[0];

        if (this.backButtonPressedOnceToExit) {
          //TODO: call Promise.all(storage.ShutdownCallbacks).then(platform.exitApp())
          console.log('about to shutdown app');
          this.events.publish('exit:prepare');
          this.events.subscribe('exit:ready',()=>{
            platform.exitApp();
          });
        } else if (nav.canGoBack()) {
           nav.pop();
        } else {
          this.showToast();
          this.backButtonPressedOnceToExit = true;
          setTimeout(() => {
            this.backButtonPressedOnceToExit = false;
          },2000)
        }
      });
    });

    const authListener = afAuth.authState.subscribe(async user=>{
      if(user){
        this.user = await this.userDetailsProv.getUserDetails();
        console.log('app.component.ts...... calling settingsProv.init()');
        await this.settingsProv.init();
        console.log('app.component.ts...... settingsProv.init() finished');
        this.rootPage = 'RoutesFeedPage';
        authListener.unsubscribe();
      }else{
        console.log('app.component.ts, setting rootPage. NO USER (line 122)');
        this.rootPage = 'RoutesFeedPage';
        authListener.unsubscribe();
      }
    });


    // Set one root page.
    //this.rootPage = 'RoutesFeedPage';

    // console.log("from app.component, initializing settings...");
    // this.settingsProv.init().then(( settings )=>{
    //     console.log("settings initialized as:", settings);
    // }).catch((error)=>{
    //     console.log("some error occured in initializing settings", error);
    // });



/*
    const authListener = afAuth.authState.subscribe( user => {
      if (user){

        this.userDetailsProv.getUserDetails().then(function(details){
          //console.log("from app.component:");
          //console.log("details from page:", details);
        });

        //console.log("In app.component.ts in if clause (user is defined)");
        //this.settings.init().then(this.rootPage = 'RoutesFeedPage').catch( error => { pick gym page})
        this.settingsProv.init().then(( settings )=>{
          if(settings.gymId){
            this.rootPage = 'RoutesFeedPage';
            authListener.unsubscribe();
          }else{//settings.init resolved, but there's no gymId
            //TODO: send them to a page where all they do is pick their gym (not settings page)
            this.rootPage = 'SettingsPage';
            authListener.unsubscribe();
          }
        }).catch((error)=>{//settings.init failed to fetch settings or get default settings
          this.rootPage = 'SettingsPage';
          authListener.unsubscribe();
        });
        //reason for unsubscribe on p. 82 of firebase book
      }
      else {
        this.settingsProv.init().then(( settings )=>{
          this.rootPage = 'NotLoggedInPage';
        }).catch((error)=>{
          console.log("settingsProv.init rejected in app.component.ts trying to route to NotLoggedInPage");
        });
        authListener.unsubscribe();
      }
    });

*/



  }

  /***************** Menu Events *******************/
  emitSettingsClicked(){
    this.events.publish("goToSettings");
    this.menu.close();
  }

  emitFiltersClicked(){
    this.events.publish("goToFilters");
    this.menu.close();
  }

  emitRouteEditorClicked(){
    this.events.publish("goToRouteEditor");
    this.menu.close();
  }

  /************* END Menu Events *****************/

  showToast() {
    let toast = this.toastCtrl.create({
      message: 'Press Again to exit',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
}
