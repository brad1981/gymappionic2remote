import { NgModule } from '@angular/core';
import { CameraComponent } from './camera/camera';
import { SvgMapComponent } from './svg-map/svg-map';
import { SvgGestureComponent } from './svg-gesture/svg-gesture';
import { SvgRouteComponent } from './svg-route/svg-route';
import { IonicModule } from  'ionic-angular';
import { RouteDisplayComponent } from './route-display/route-display';
import { FeedRouteDisplayComponent } from './feed-route-display/feed-route-display';
import { FilterSelectorComponent } from './filter-selector/filter-selector';
import { RouteFeatures2Component } from './route-features2/route-features2';
import { RouteCommentComponent } from './route-comment/route-comment';
import { RouteFeedBackComponent } from './route-feed-back/route-feed-back';
import { ListViewComponent } from './list-view/list-view';

import {IonicImageCacheModule} from 'ionic3-image-cache';
import { GymAndFloorPickerComponent } from './gym-and-floor-picker/gym-and-floor-picker';
import { DeleteContestRouteComponent } from './delete-contest-route/delete-contest-route';
import { FloorToggleComponent } from './floor-toggle/floor-toggle';
import { RouteHeadingComponent } from './route-heading/route-heading';

@NgModule({
	declarations: [CameraComponent,
    SvgGestureComponent,
    SvgRouteComponent,
    SvgMapComponent,
    RouteDisplayComponent,
		FeedRouteDisplayComponent,
    FilterSelectorComponent,
    RouteFeatures2Component,
    RouteCommentComponent,
    RouteFeedBackComponent,
    ListViewComponent,
    GymAndFloorPickerComponent,
    DeleteContestRouteComponent,
    FloorToggleComponent,
    RouteHeadingComponent,
	],
	imports: [IonicModule,IonicImageCacheModule],
	exports: [CameraComponent,
    SvgGestureComponent,
    SvgRouteComponent,
    SvgMapComponent,
    RouteDisplayComponent,
		FeedRouteDisplayComponent,
    FilterSelectorComponent,
    RouteFeatures2Component,
    RouteCommentComponent,
    RouteFeedBackComponent,
    ListViewComponent,
    GymAndFloorPickerComponent,
    DeleteContestRouteComponent,
    FloorToggleComponent,
    RouteHeadingComponent,
	]
})
export class ComponentsModule {}
