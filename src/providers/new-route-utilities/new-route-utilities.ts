import { Injectable } from '@angular/core';
import { UserDetailsProvider } from '../../providers/user-details/user-details';
import { LocalStorageProvider } from '../local-storage/local-storage';
import * as firebase from 'firebase/app';//used for definig types
import  'firebase/storage';
import { ListObject } from '../globalConstants/globalConstants'
//TODO: phase out angularfire database
import { Alert, AlertController } from 'ionic-angular';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuth} from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { UrlProvider } from '../../providers/url/url';
import { ConnectionProvider } from '../../providers/connection/connection';
import { SettingsProvider } from '../../providers/settings/settings';
import { File } from '@ionic-native/file';
import { ListsProvider } from '../../providers/lists/lists';
import { AuthProvider } from '../../providers/auth/auth';
import { HoldObject, RouteObject } from '../../providers/globalConstants/globalConstants';
import 'rxjs/add/operator/map';
import { betterConsole }  from '../../providers/globalConstants/globalConstants';

let console = betterConsole;

@Injectable()
export class NewRouteUtilitiesProvider {

  currentImageProgress:number = null; //percent of current image that has uploaded
  onImage:number = null; //current image being uploaded (1 -> first, ie, zeroth image in array)
  numImages:number = null; //total number of images to be uploaded
  finished: boolean = false;
  routesToUpload = [];
  //routesBeingUploaded = [];
  routeCatcher = [];
  routesToDelete = [];
  gymDocId = null;



  constructor(
    public connectionProv: ConnectionProvider,
    public settingsProv: SettingsProvider,
    public localStorageProv: LocalStorageProvider,
    public authProv: AuthProvider,
    public listsProv: ListsProvider,
    public afDatabase: AngularFireDatabase,
    public file: File,
    public auth: AngularFireAuth,
    public alertCtrl: AlertController) {

  }

  // for testing, uncomment below & line 79 to indicate whether you want the route to be
  // uploaded (true), or rejected (false)
  //async handleNewRoute(route:routeObject, upload:boolean):Promise<string>{

  //Brad: QUESTION... is it ever possible that this function will be called with a route from a different gym than
  //this.settingsProv.getSettings().gymId ?

  async handleNewRoute(route:RouteObject):Promise<string>{
    // console.log("handle NewRoute called");
    let mapId = route.mapId;
    try{
      // get settings info
      let settings = await this.settingsProv.getSettings();
      // console.log("settings:", settings);
      this.gymDocId = settings.gymId;
      // console.log("gymDocId:", this.gymDocId);

      const db = firebase.firestore();
      let firestoreDocRef: firebase.firestore.DocumentReference = null;
      // console.log("db: ", this.db);
      // console.log("first collection call :", this.firestoreDocRef);

      if(!route.pushKey){
        firestoreDocRef = db.collection('GYMS').doc(this.gymDocId)
        .collection('MAPS').doc(mapId).collection('ROUTES').doc();
        route.pushKey = firestoreDocRef.id;
        // console.log("route.pushKey:", route.pushKey);
      }
      else{
        firestoreDocRef = db.collection('GYMS').doc(this.gymDocId)
        .collection('MAPS').doc(mapId).collection('ROUTES').doc(route.pushKey);
      }


      let message:string = '';

      // NOTE: uncomment below for test
      //if(upload){

      // NOTE: comment below for test
      if (this.connectionProv.isConnected){

        try{
          console.log("route before sent to uploadImagesThenRoute:");
          console.log(route);
          // try uploading images, then route.
          let imgRouteMsg = await this.uploadImagesThenRoute(route,mapId);
          return imgRouteMsg;
        }
        // on failure, save route locally, then push the key to array of routes to upload latter.
        catch(err1){
          console.log("err1:");
          // console.lie(err1);
          //Brad: QUESTION...did you mean to use await with saveLocalCopy here? Looks like 'savedRoute' is never used after being
          //assigned here (savedRoute is a promise literal/function literal, not the resolved value)
          //NOTE: I (brad) added a mapId field for local copies of routes so we can put them under the correct map
          //NOTE: (from dawson), in response to the question, I think it was left over from the process of changing the code to use await,
          //      ive commented it out for now as I will be testing through these functions and will determine if it is actually needed.


          route['mapId'] = mapId; //when upload, we need the map id to get route 'under' correct MAPS document
          //let savedRoute = this.localStorageProv.saveLocalCopy(this.firestoreDocRef.id, route);
          try{
            console.log('*********** trying to save local copy of route');
            let sRoute = await this.localStorageProv.saveLocalCopy(route.pushKey, route).catch(error=>{
              console.log('error saving local copy: ');
              console.lie(error);
              throw error;
            });
            // 888 this.routesToUpload.push(this.firestoreDocRef.id);
            await this.addToRouteCache(route.pushKey).catch(error=>{
            console.log('error in addToRouteCache');
            console.log(error);
          });
            let failedMsg = "Could not upload route... We will try again later!";
            return failedMsg;
          }catch(err2){
            //QUESTION: where does err2 get thrown? I'm not sure how this code block is reached
            //NOTE: (in repsonse to above), err1 gets thrown if the route can not be uploaded, this code is thrown if one of the immediately above
            //      promises fails for some reason (couldnt save a local copy or retrive local storage data, etc). The thinking was, for each
            //      try block, a corresponding catch block.
            console.log("oh no! we could not upload the route and a local copy could not be saved!");
            console.log("err2:");
            console.lie(err2);
            throw(err2);
          }
        }
      }

      // if there is no connection, do the same thing as when an upload fails.
      //Brad: prob should wrap this in another function since it is repeated above
      //...eventually. It's certainly not urgent.
      else{
        //let savedRoute = this.localStorageProv.saveLocalCopy(this.firestoreDocRef.id, route);
        try{

          let sRoute = await this.localStorageProv.saveLocalCopy(route.pushKey, route);
          let currentCache = await this.addToRouteCache(route.pushKey);
          //888 this.routesToUpload.push(this.firestoreDocRef.id);

          //console.log("currentCache:", currentCache);
          message = 'There is no connection at this time. We will try again later!';
          throw(message);
        }
        catch(err3){
          console.log("oh no! we could not upload the route and a local copy could not be saved!");
          console.log("error:", err3);
          throw(err3);
        }
      }
    }
    catch(err){
      console.log("could not retrieve settings or some other error occured");
      console.log("error message:", err);
      throw(err);
    }
  }

//NOTE below is for testing purposes only
/*
  async clearCache(){
    let emptyArray = [];

    try{
      let oldArray = await this.localStorageProv.getData("unsavedRoutes");
      console.log("unsavedRoutes:", oldArray);

      await this.localStorageProv.saveLocalCopy("unsavedRoutes", emptyArray);
      console.log("unsaved routes replaced as []");
      let tester = await this.localStorageProv.getData("unsavedRoutes");
      console.log("tester:", tester);
    }
    catch(err){
      console.log("some error in deleting em", err);
    }
  }
*/

  async checkForOfflineRoutes():Promise<boolean>{
    console.log('new-route-utilities, checkForOfflineRoutes');
    let routesBeingUploaded = [];

    try{
      //BUG: 1.) local storage returns null if it cannot find a record.
      routesBeingUploaded = await this.localStorageProv.getData('unsavedRoutes');
    }
    catch(err){//NOTE 99% sure local storage will not throw if there is no record
      console.log("oh no! we couldnt retrieve the unsaved routes!");
      console.log("err msg:", err);
    }

    if(routesBeingUploaded == null || routesBeingUploaded.length == 0){
      return false;
    }
    else if(routesBeingUploaded.length > 0){
      return true;
    }
  }

  async uploadOfflineRoutes():Promise<any>{
    console.log('new-route-utilities, uploadOfflineRoutes');
    let routesBeingUploaded = [];
    try{
      routesBeingUploaded = await this.localStorageProv.getData('unsavedRoutes');
      //console.log("routesToUPload:", routesBeingUploaded);
    }
    catch(err){
      console.log("oh no! we couldnt retrieve the unsaved routes!");
      console.log("err msg:", err);
    }
    if(routesBeingUploaded == null || routesBeingUploaded.length == 0){
      //console.log("routes to upload was empty!");
      return;
    }
    // if there are more than one routes to upload, this function is called recursively
    else if(routesBeingUploaded.length != 1){

      // handle the last route first and incrementally deconstruct the array to trigger
      // the exit case on the recursive function
      let currentRoute = routesBeingUploaded.pop();

      try {
        // update cache
        await this.removeFromRouteChache(currentRoute);
        // get the data to be uploaded out of storage
        let data = await this.localStorageProv.getData(currentRoute);
        //console.log("data:", data);
        //console.log("map ID:", data.mapID);


        // upload the route.
        // NOTE below version for testing.
        //let message = await this.handleNewRoute(data, true);
        let message = await this.handleNewRoute(data); //TODO: store mapId when saving a map for later upload

        this.routesToDelete.push(currentRoute);

        //mark upload progress in local storage
        await this.removeFromRouteChache(currentRoute);

        // call the function again for every other item left in the array.
        let recurse = await this.uploadOfflineRoutes();
        // only return once all recursively called uploads have finished.
        return recurse;
      }

      // catch the failed route so we can still attempt to upload it again later,
      // even if it failed to be retrieved from memory or uploaded.
      catch(err) {
        //console.log("This shoudl fire and the route should be cuaght. Error:", err.message);
        // if the route does not already add it to the array, add it. If not we
        // have to exit so that we do not get caught in a loop of adding an uploadable
        // route to an array infinite times. This should never happen though, as
        // the route is removed from the array before it is processed.
        if(!(this.routeCatcher.indexOf(currentRoute) >= 0)){
          // save the route for later.
          this.routeCatcher.push(currentRoute)
          //console.log("routeChatcher!", this.routeCatcher);
          try{
            // just because one failed doesnt mean the others shouldnt try.
            let recurse = await this.uploadOfflineRoutes();

            // return the results.
            return recurse;
          }
          catch(err){
            console.log("could not recurse! Error message:", err);
          }

        }
        else{
          // if we wind up down here I am genuinely confused. better break.
          console.log("unknown error reached.", err);
          throw err;
        }
      }
    }

    // if this is is the last element in the array, trigger the exit case and return back up through the rabbit hole.
    else{
      let currentRoute = routesBeingUploaded.pop();

      try {
        //mark upload progress in local storage
        await this.removeFromRouteChache(currentRoute);
        let data = await this.localStorageProv.getData(currentRoute);

        //NOTE below line for test.
        //let message = await this.handleNewRoute(data, true);
        let message = await this.handleNewRoute(data);
        //console.log("message from handle new route:", message);

        //console.log("state of routesBeingUploaded after last pop:", routesBeingUploaded);


        //HACK for testing only:

        // let currentStatus = await this.localStorageProv.getData('unsavedRoutes');
        // console.log("currentStatus of unsaved routes after last pop:", currentStatus);


        this.routesToDelete.push(currentRoute);
        this.localStorageProv.removeByKeys(this.routesToDelete).then(()=>{
          //console.log("all routes deleted");
          this.routesToDelete = [];
        }).catch((err)=>{
          console.log("could not delete routes from storage!");
          throw err;
        });

        // if the current upload is succesful, but there were previous failed attempts,
        // catch the routes and notify the user.
        if(this.routeCatcher.length >= 1){
          //console.log("some items were found in the route catcher!");
          for(let item in this.routeCatcher){
            //console.log("adding:", item);
            this.addToRouteCache(item);
          }
          this.routeCatcher = [];
          let partial = "some of the routes failed to upload, please find a better connection."
          return partial;
        }

        // otherwise, return success msg.
        return message;
      }

      // if this route did not succesfully get pulled from storage and uploaded;
      catch(err) {
        console.log("Error:", err.message);
        // catch it if its not already caught.
        if(!(this.routeCatcher.indexOf(currentRoute) >= 0)){
          this.routeCatcher.push(currentRoute);
        }

        // either way, (although im not sure how it could already be caught),
        // make sure that all the unsucessful routes are are put back in the
        // routesToUpload array, the catcher is cleared, and the exit case is triggered
        if(this.routeCatcher.length >= 1){
          //console.log("routeChatcher! (ie routes not uploaded)", this.routeCatcher);
          for(let item in this.routeCatcher){
            //console.log("adding:", item);
            this.addToRouteCache(item);
          }
          this.routeCatcher = [];
          this.localStorageProv.removeByKeys(this.routesToDelete).then(()=>{
            //console.log("all routes deleted");
            this.routesToDelete = [];
          }).catch((err)=>{
            console.log("could not delete routes from storage! error:", err);
            throw err;
          });

          let message = "not all the routes were uploaded, please find a better connection"
          return message;
        }

        // if there is an error but nothing in the routeCatcher, then something is wrong.
        // better throw an error.
        else{
          console.log("some uknown error occured in uploading offline routes");
          throw err;
        }
      }
    }
  }

  async uploadImagesThenRoute(route:RouteObject, mapId):Promise<string>{
    //console.log('new-route-utilities provider, uploadImagesThenRoute');
    try{
      let fbStoragePaths = await this.pushAllPhotosToFbStorage(route.topo.photos,route.pushKey,this.gymDocId)
      //HACK: thise 2 assignments will need to change (note from Dawson- it is
      // not immediately obvious why these 2 assignments need to change, or what they should be changed to.)
      //let holdArrayArray = this.getHoldArrayArray(route);
      // console.log('SUCCESS: pushAllPhotosToFbStorage without error');
      // console.log('fbStoragePaths is:');
      // console.log(fbStoragePaths);

      let betaPhotos = null;
      let fbRoute = this.getFirebaseRouteData(route,fbStoragePaths, betaPhotos);
      // console.log('SUCCESS: getFirebaseRouteData without error');
      let batchMsg = await this.saveRouteDataToFirestoreBatch(fbRoute, this.gymDocId, mapId);
      // console.log('SUCCESS: saveRouteDataToFirestoreBatch without error');
      return(batchMsg);
    }
    catch(err){
      console.log('FAIL: error in uploadImagesThenRoute');
      throw(err);
    }
  }
/*
  async deleteLocalImagesThenRoute():Promise<string>{

  }
*/

  async saveRouteDataToFirestoreBatch(route:RouteObject,  gymDocId:string, mapId:string):Promise<string>{

    // Polish up / assemble route object
    let holds = [];
    for (let holdInst of route.topo.holds){
      let holdObj: HoldObject = {
        pointCor: holdInst.pointCor,
        radius: holdInst.radius,
        color: holdInst.color
      }
      holds.push(holdObj);
    }
    route.coverPhoto = route.topo.photos[0]
    route.consGrade = route.grade;


    // Get references for bach writes
    const db = firebase.firestore();
    let batch = db.batch();
    let uid = this.auth.auth.currentUser.uid;

    // Details Ref
    let detailsRef = db.collection('GYMS').doc(gymDocId)
      .collection('MAPS').doc(mapId)
      .collection('ROUTES').doc(route.pushKey)
      .collection('DETAILS').doc('detailsDoc');

    // Doc Ref
    let docRef: firebase.firestore.DocumentReference = null;
    // if route has attempted to upload before, it will already have a push key. use this instead of generating a new one.
    if(route.hasOwnProperty('pushKey') && route.pushKey){
      docRef = db.collection('GYMS').doc(this.gymDocId)
        .collection('MAPS').doc(mapId).collection('ROUTES').doc(route.pushKey);
    }
    // if this does not exist, generate a new one.
    else{
      docRef = db.collection('GYMS').doc(this.gymDocId)
        .collection('MAPS').doc(mapId).collection('ROUTES').doc();
      route['pushKey'] = docRef.id;
    }

    // User's my route's list ref
    console.log("floor number:", route.floor);
    let listId: string = "My Routes" + this.gymDocId + route.floor;
    let listRef: firebase.firestore.DocumentReference = db.
      collection('USERS').doc(uid).
      collection('GYMLISTS').doc(gymDocId).
      collection('FLOORLISTS').doc(listId)
    let listArr: string[];
    let listData: any;
    try{
      let listSnapshot = await listRef.get();
      console.lie("listSnapshot", listSnapshot);
      if (listSnapshot.exists){
        console.lie("snapshot exists, existing data is:", listSnapshot.data());
        listData = listSnapshot.data()
        listData.listArray.push(route.pushKey);
      }
      else {
        listArr = [route.pushKey];

        let newListData:ListObject = {
          listType: "default",
          name: "My Routes",
          id: listId,
          permission: "private",
          floorNum: route.floor,
          listArray: listArr,
          mapId:mapId
        };
        listData = newListData;
      }
      batch.set(listRef, listData);
    }
    catch(err){
      console.lie(err);
      console.log("err getting listReference! ...perhaps list does not exist?");
    }

    batch.set(docRef, route);

    // console.lie("detailsRef: ", detailsRef);
    batch.set(detailsRef, {
      suggGrades: {[uid]:route.grade},
      //comments will go here when they are created
      //if there are many comments, they should be a subcollection or pruned by cloud functions
    });

    try{
      console.log("attempting to commit batch...");
      await batch.commit();
      await this.listsProv.updateAllListData();
      return("Your route has been succesfully uploaded.");
    }
    catch(err){
      console.log('error committing batch');
      console.lie(err);
      throw(err);
    }
  }

  async pushAllPhotosToFbStorage(imageFilePaths:string[],routePushKey:string, gym:string):Promise<string[]>{
      //resolve array of strings representing the firebase storage paths
      //of the uploaded images
      // console.log('new-route-utilities, pushAllPhotosToFbStorage');
      return new Promise<string[]>((res,rej)=>{
        // console.log('length 1');
        this.numImages= imageFilePaths.length; //total number images to upload

        let fbStorage = firebase.storage();
        let storageRef = fbStorage.ref();

        //collect storage url's here for putting into the database route info
        let fbStoragePaths = [];

        for (let i = 0; i< imageFilePaths.length; i++){
          this.onImage = i+1; //current image being uploaded (1 -> first, ie, zeroth image in array)

          let fbStoragePath = `${gym}/topos/${routePushKey}/`+i.toString()+'.jpg'; //<- firebase storage ref

          let fbPhotoRef = storageRef.child(fbStoragePath);

          this.getBlob(imageFilePaths).then((blob)=>{
            let uploadTask = fbPhotoRef.put(blob);

            uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot)=>{//monitor progress
              this.currentImageProgress= (uploadTask.snapshot.bytesTransferred / uploadTask.snapshot.totalBytes) * 100;

              // console.log('Upload is ' + this.currentImageProgress+ '% done');
              switch(uploadTask.snapshot.state){
                case firebase.storage.TaskState.PAUSED:
                  // console.log('**UPLOAD PAUSED**');
                  break;
                case firebase.storage.TaskState.RUNNING:
                  // console.log('upload running');
                  break;
              }
            },
            (error)=>{//handle various errors
              //we can get more info on the type of error but there's a type problem
              //just printing error for now
              console.log('error in pushAllPhotosToFbStorage');
              console.lie(error);
              rej(error);
            },
            ()=>{//Successful upload! Save storage path
              fbStoragePaths.push(fbStoragePath);
              if(fbStoragePaths.length = imageFilePaths.length){
                //all images succesfully uploaded
                this.numImages = null;
                this.onImage = null;
                this.currentImageProgress = null;

                res(fbStoragePaths);
              }
              return undefined;
            });
          });
        }
    });
  }

  getFirebaseRouteData(newRoute: RouteObject, topoPhotos:string[],
      betaPhotos:string[]):RouteObject{
        // console.log('new-route-utilities, getFiregbaseRouteData topotPhotos:');
        // console.log(topoPhotos);
        // console.log("newRoute:", newRoute);
      //
      // let fbRouteTemp:routeObject= {
      //
      //   pushKey:newRoute.pushKey, //for finding the comment and updating or removing it
      //   //the name of the route
      //   name: newRoute.name,
      //   topo:{photos:topoPhotos, holds: newRoute.topo.holds},
      //   betaPhotos:betaPhotos,
      //   authorName:newRoute.authorName, //TODO: phase in userName for storage location rather than authorId
      //   authorId:newRoute.authorId,
      //   grade: newRoute.grade,
      //   location:newRoute.location,
      //   mapId: newRoute.mapId,
      //   features:newRoute.features
      // };

      let fbRoute = newRoute;
      fbRoute.topo = {photos:topoPhotos,
        holds: newRoute.topo.holds,
        holdsDims:newRoute.topo.holdsDims
      };

      fbRoute.betaPhotos = betaPhotos;
      fbRoute.features = newRoute.features ? newRoute.features : {};


      //console.log("fbRoute:", fbRoute);
      return fbRoute;
  }

  getBlob(photoUrl):Promise<any>{
    return new Promise((res,rej)=>{
      let img =   <HTMLImageElement> new Image();
      img.src = photoUrl;
      // console.log('image.src is:');
      // console.log(img.src)

      img.onload=()=>{
        // console.log('Image Loaded!');
      };

      let canvas = <HTMLCanvasElement>document.createElement('CANVAS');
      var ctx = canvas.getContext('2d');
      var dataURL;

      img.onload = function() {
        //imgInst solves some type problems
        let imgInst = <HTMLImageElement>this;
          canvas.height = imgInst.height;
          canvas.width = imgInst.width;

          ctx.drawImage(imgInst, 0, 0);

          canvas.toBlob((blob)=>{
            canvas = null;
            res(blob);
        },'image/jpeg',1);

      };
    });
  }

  async addToRouteCache(routePushKey: string):Promise<string>{
    //console.log("###########################addToRouteCache called.");
    let newRouteKey = [routePushKey];
    //console.log("newRoute:");
    try{
      let currentCache = await this.localStorageProv.getData('unsavedRoutes');
      // console.log("currentCache:");
      // console.log(currentCache);
      // if cache exists & isnt empty

      if(currentCache !== null && currentCache.length > 0){
        // add the new route
        currentCache = currentCache.concat(newRouteKey);
        // make sure its not duplicate
        currentCache = currentCache.filter(function(item, pos, self){
          return self.indexOf(item) == pos;
        });
      }
      else {
        // otherwise just make the new cache
        currentCache = newRouteKey;
      }
      //console.log("##################################newRouteCache:", currentCache);
      await this.localStorageProv.saveLocalCopy('unsavedRoutes', currentCache);
      let confirmation = await this.localStorageProv.getData('unsavedRoutes');
      return confirmation;

    }catch(err){
      console.log("some err occured in updating cache:");
      console.lie(err);
      throw err;
    }
  }

  async removeFromRouteChache(route: string):Promise<string>{
    //console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@removeFromRouteChache called with:", route);
    try{
      let currentCache = await this.localStorageProv.getData('unsavedRoutes');
      //console.log("previous cache:", currentCache);
      if(currentCache !== null && currentCache.length > 0){
        let index = currentCache.indexOf(route);
        if(index !== -1){
          currentCache.splice(index, 1);
        }
      }
      else{
        currentCache = [];
      }
      await this.localStorageProv.saveLocalCopy('unsavedRoutes', currentCache);
      let confirmation = await this.localStorageProv.getData('unsavedRoutes');
      //console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@@cache is now:", confirmation);
      return confirmation;
    }
    catch(err){
      console.log("some error occured in removeing item from routeChache:", err);
      throw(err);
    }
  }

  // NOTE: the mapID & if then concerning mapID property is only for cleaning up old routes that dont have mapIDs.

  async deleteRoute(route, pushedMapId){
    let routeMapId = '';
    const db = firebase.firestore();
    // console.log("db: ", db);
    let settings = await this.settingsProv.getSettings();
    // console.log("settings:", settings);
    this.gymDocId = settings.gymId;
    // console.log("gymDocId:", this.gymDocId);
    // console.log("routeID:", route.pushKey);

    if(route.hasOwnProperty('mapId')){
      routeMapId = route.mapId;
    }
    else{
      routeMapId = pushedMapId;
    }

    db.collection('GYMS').doc(this.gymDocId)
    .collection('MAPS').doc(routeMapId)
    .collection('ROUTES').doc(route.pushKey).
    delete().then(function() {
      // console.log("Document successfully deleted!");
    }).catch(function(error) {
      console.log("Error removing document: ");
      console.lie(error);
    });
  }

  async markRouteForDeletion(route):Promise<number>{
    if(route.markedForDelete){
      console.log("!route.markedForDelete: ", route.markedForDelete);
      // route has already been marked for deletion.
      let err = "Route already marked for deletion";
      throw err;
      ;
    }
    else if(this.connectionProv.isConnected()){
      const db = firebase.firestore();
      let settings = await this.settingsProv.getSettings();
      const userInfo = this.authProv.getUser();
      this.gymDocId = settings.gymId;

      let deleteNum = Number(new Date()); //1479895361931


      try{
        await db.collection('GYMS').doc(this.gymDocId)
              .collection('MAPS').doc(route.mapId)
              .collection('ROUTES').doc(route.pushKey).update({
                markedForDelete: firebase.firestore.FieldValue.serverTimestamp(),
                markedUID: userInfo.uid,
                markedName: userInfo.displayName
              });

        //return deleteTime.toLocaleString();
        return deleteNum;
      }
      catch(err){
        console.log("error in marking for deletion");
        //catch
        throw err;
      }

    }
    else{
      let offlineAlert = this.alertCtrl.create({
        title: 'Offline Error',
        subTitle: 'You must be online to mark routes for deletion.',
        buttons: ['Dismiss']
      });
      offlineAlert.present();
    }
  }

  async unmarkRouteForDeletion(route){
    const db = firebase.firestore();
    let settings = await this.settingsProv.getSettings();
    this.gymDocId = settings.gymId;
    try{
      await db.collection('GYMS').doc(this.gymDocId)
            .collection('MAPS').doc(route.mapId)
            .collection('ROUTES').doc(route.pushKey).update({
              markedForDelete: firebase.firestore.FieldValue.delete(),
              markedUID: firebase.firestore.FieldValue.delete(),
              markedName: firebase.firestore.FieldValue.delete(),
              markedAffirm: firebase.firestore.FieldValue.delete(),
              markedContest: firebase.firestore.FieldValue.delete()
            });
    }
    catch(err){
      console.log("error in unmarking route for deletion");
      //catch
      throw err;
    }
  }

  async contestDelete(route){

    let userInfo = this.authProv.getUser();
    const db = firebase.firestore();
    let settings = await this.settingsProv.getSettings();
    this.gymDocId = settings.gymId;
    try{
      await db.collection('GYMS').doc(this.gymDocId)
            .collection('MAPS').doc(route.mapId)
            .collection('ROUTES').doc(route.pushKey).update({
              markedContest: firebase.firestore.FieldValue.arrayUnion(userInfo.uid)
            });
    }
    catch(err){
      console.log("error in unmarking route for deletion");
      //catch
      throw err;
    }
  }

  async affirmDelete(route){

    let userInfo = this.authProv.getUser();
    const db = firebase.firestore();
    let settings = await this.settingsProv.getSettings();
    this.gymDocId = settings.gymId;
    try{
      await db.collection('GYMS').doc(this.gymDocId)
            .collection('MAPS').doc(route.mapId)
            .collection('ROUTES').doc(route.pushKey).update({
              markedAffirm: firebase.firestore.FieldValue.arrayUnion(userInfo.uid)
            });
    }
    catch(err){
      console.log("error in unmarking route for deletion");
      //catch
      throw err;
    }
  }


  async changeMarkedVote(route, affirmed:boolean){
    let userInfo = this.authProv.getUser();
    let settings = await this.settingsProv.getSettings();
    this.gymDocId = settings.gymId;
    const db = firebase.firestore();
    let batch = db.batch();
    let routeRef = db.collection('GYMS').doc(this.gymDocId)
                     .collection('MAPS').doc(route.mapId)
                     .collection('ROUTES').doc(route.pushKey);

    if(affirmed){
      batch.update(routeRef,{markedAffirm: firebase.firestore.FieldValue.arrayRemove(userInfo.uid)});
      batch.update(routeRef,{markedContest: firebase.firestore.FieldValue.arrayUnion(userInfo.uid)});
    }
    else{
      batch.update(routeRef,{markedContest: firebase.firestore.FieldValue.arrayRemove(userInfo.uid)});
      batch.update(routeRef,{markedAffirm: firebase.firestore.FieldValue.arrayUnion(userInfo.uid)});
    }

    try{
      await batch.commit();
    }
    catch(err){
      console.log("error in unmarking route for deletion");
      //catch
      throw err;
    }
  }

  /*
  getHoldArrayArray(route)  {
    //HACK: while we're working on implementing multiple images, check for
    //holdArray of type Hold[] vs Hold[][]
    //I think we should eventually move this to the route-editor side of things.
    let holdArrayArray: holdObject[][] = [[]];
    let neededType = typeof(holdArrayArray);
    let haveType = typeof(route.topo.holdArrays);

    if (haveType !== neededType){
      // console.log('Types don NOT match!!!');
      let holdArrayArray: holdObject[][] = [route.topo.holdArrays];
      return holdArrayArray;
    }
    else{
      // console.log('ALERT: Somehow bypassed type enforcement efforts!');
      let holdArrayArray = route.topo.holdArrays;
      return holdArrayArray;
    }
  }
  */
}
