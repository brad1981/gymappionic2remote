import { Injectable } from '@angular/core';
import { Network } from "@ionic-native/network";
import { Platform, Events } from 'ionic-angular';
import {AngularFireDatabase} from '@angular/fire/database';


@Injectable()
export class ConnectionProvider {
  hasDataConn:boolean = false;
  hasFireConn:boolean;
  dataDisconnSubscription:any;//TODO: It's actually a subscription (or observable?)
  dataConnSubscription:any;
  activeRefs:any[];
  //systemPaused: boolean = false;

  constructor(public network: Network, public platform: Platform, public afDatabase: AngularFireDatabase,
  public events: Events) {
    this.activeRefs = [];

    this.platform.ready().then(()=>{
      // console.log('network.type has value: '+ this.network.type);

      this.hasDataConn = (this.network.type != 'undefined' &&  this.network.type != 'none' &&
                          this.network.type != null);
      // console.log("this.hasDataConn is:");
      // console.log(this.hasDataConn);

      // watch network for a disconnect
      this.dataDisconnSubscription = this.network.onDisconnect().subscribe(() => {
         // console.log('network was disconnected :-(');
        this.hasDataConn = false;
      });


      // watch network for a connection
      this.dataConnSubscription = this.network.onConnect().subscribe(() => {
         // console.log('network connected!');
        // We just got a connection but wessts as well.
        setTimeout(() => {
          if (this.network.type != 'undefined' &&  this.network.type != 'none' &&
                          this.network.type != null) {
            // console.log('we got a data connection, woohoo!');
            this.hasDataConn = true;
            console.log("about to publish connection event....");
            this.events.publish('connected:true');
            console.log("published....");
          }
        }, 3000);
      });
      //HACK??? this monitors connection to Realtime db. NOT firestore.
      //They are prob both connected or both disconnected, but this does feel hackish...
      //prob doesn't matter if we keep using RTdb for some things (i.e. settings)
      let connectedRef = this.afDatabase.database.ref('.info/connected');

      // console.log("in connection provider, connectedRef is: ");
      // console.log(connectedRef);

      connectedRef.on('value',(snap)=>{
        this.hasFireConn = snap.val();
        if(this.hasFireConn){
          // console.log('event!!  fbConnected:true');
          events.publish('fbConnected:true');
        }else{
          if(!this.hasFireConn){
            // console.log('event!!  fbConnected:false');
            events.publish('fbConnected:false');
          }
        }

         // console.log('fb conn changed. current value:' );
         // console.log(this.hasFireConn);
        this.activeRefs.push(connectedRef);
      });

    });//end platform.ready
  }

  isConnected():boolean{
    return this.hasDataConn; //&& this.hasFireConn;
  }

  //Misnomer ALERT: also calls .off() on all refs
  unscubscribeAllTheThings(){
    this.dataDisconnSubscription.unsubscribe();
    this.dataConnSubscription.unsubscribe();
    for (let ref of this.activeRefs){
      ref.off();
    }
  }

}
