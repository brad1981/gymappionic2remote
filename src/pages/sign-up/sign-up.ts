/*
Summary:
  constructor:
    1.) build array of gymData, save as this.gymData
    Problems with this:
      -gymData is a type defined in GymsProvider. It is *singular*, as in one gym's data, but it is
       used in this page.ts file to describe an array of data
      -setGymOptions is defined in this file AND in GymsProvider. This file should be calling setGymOptions
       in the GymsProvider, rather than doing the work in this file (seperation of concerns)
    2.) toggle optionsLoaded variable for inserting <form> htm into template
    3.) build signup form
  registerUser:
    1.)when the form is valid, attempt to sign up the user with afAuth.auth, .then=>
    2.)build a new doc for the user under the USERS collection: it's id is the new user's id
    3.) set default settings with gymId from the form .then =>
    4.) set RoutesFeedPage as root




  ~~~Note from Dawson ~Nov 12, 2018:
    Alot of this code was pretty old and had nested promises and was throwing confusing errors. I re-wrote most of it,
    but left your original notes and the general layout of things.
*/
import { Component } from '@angular/core';
import { IonicPage,
         NavController,
         Loading,
         LoadingController,
         Alert,
         AlertController,
         Platform} from 'ionic-angular';
import { SettingsProvider } from '../../providers/settings/settings';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth/auth';
import { ConnectionProvider } from '../../providers/connection/connection';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs/Observable';
import * as firebase from 'firebase/app';

@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'sign-up.html',
})
export class SignupPage {
  public signupForm:FormGroup;
  public gymData: any[] = null;

  //TODO: consider user experience for slow gym options loading. Maybe show form before all gyms have loaded
  public optionsLoaded: boolean = false; //only load <form> into html after the gyms have loaded

  constructor(
    public navCtrl:NavController,
    public loadingCtrl:LoadingController,
    public alertCtrl:AlertController,
    public formBuilder:FormBuilder,
    public afAuth: AngularFireAuth,
    public settings: SettingsProvider,
    public connection: ConnectionProvider,
    public platform: Platform,
    public afs: AngularFirestore){

      platform.ready().then(()=>{
        this.init().then().catch((error)=>{
            //handle error
            console.log("could not intialize:", error);
        });
      }).catch((error)=>{
        console.log("error from sign-up", error);
      });
  }

  async init(){
    try{
      await this.getGymOptions();
      this.optionsLoaded = true;
      this.signupForm = this.formBuilder.group({
        email: ['', Validators.compose([Validators.pattern('[\\w-]+@([\\w-]+\\.)+[\\w-]+'), Validators.required])],
        password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
        gym:['',Validators.required],
        displayName:['',Validators.required]
      });
    }
    catch(err){
      //catch
      console.log("init error:", err);
      throw err;
    }
  }

  async getGymOptions():Promise<any[]>{
    this.gymData = [];
    if(!this.connection.isConnected()){
      const noConnectionAlert:Alert = this.alertCtrl.create({
        message: "Please connect to the internet to continue.",
        buttons: [
          {
            text: "OK",
            handler: () => {
              this.navCtrl.pop();
            }
          }
        ]
      });
      noConnectionAlert.present();
    }
    else{
      const db = firebase.firestore();
      const collectionRef = db.collection('GYMS');
      try{
        let snap = await collectionRef.get();
        snap.forEach((doc) => {
          let data = doc.data();
          console.log("data:", data);
          data['gymId'] = doc.id;
          this.gymData.push(data);
        });
        console.log("returning gymData:", this.gymData);
        return this.gymData;
      }
      catch(err){
        //catch
        console.log("could not get GYMS collection");
        throw err;
      }
    }
  }

  async registerUser():Promise<void>{
    console.log("registerUser");
    if(!this.signupForm.valid){
      console.log('INVALID SIGNUP FORM!');
      const invalidFormAlert:Alert = this.alertCtrl.create({
        message: "Please complete all fields to continue.",
        buttons: [
          {
            text: "OK",
            role: 'cancel'
          }
        ]
      });
      invalidFormAlert.present();
    }
    else{
      const email: string = this.signupForm.value.email;
      const password: string = this.signupForm.value.password;
      const gymName: string = this.signupForm.value.gym;
      const displayName:string = this.signupForm.value.displayName;

      let loading = this.loadingCtrl.create();
      loading.present();
      try{
        const newUser = await this.afAuth.auth.createUserWithEmailAndPassword(email,password);
        const userDocRef = this.afs.collection('USERS').doc(newUser.user.uid);
        await userDocRef.set({'displayName':displayName});
        await this.afAuth.auth.currentUser.updateProfile({displayName: displayName, photoURL: null});
        await this.settings.setDefaults(gymName);
        loading.dismiss();
        this.navCtrl.setRoot('RoutesFeedPage');
      }
      catch(err){
        loading.dismiss();
        console.log("error creating new user w/ email and password:", err);
        throw err;
      }
    }
  }
}
