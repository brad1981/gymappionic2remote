import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Alert, AlertController, Platform,Events } from 'ionic-angular';
import { SettingsProvider, NotificationsSettings,SettingsObject  } from '../../providers/settings/settings';
import { ConnectionProvider } from '../../providers/connection/connection';
import { MapsProvider, Maps } from '../../providers/maps/maps';
import { AuthProvider } from '../../providers/auth/auth';
import { ListsProvider } from '../../providers/lists/lists';


import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  settings: SettingsObject= null;
  gymId:string;
  currentFloor:number;
  currentDisplayName:string = "";
  mapsObj:Maps = null;
  visibleMode: boolean;
  cacheRouteComments:boolean;
  cacheRouteDetails:boolean;
  onComment:boolean;
  onRouteDeleted:boolean;
  onNewSend:boolean;
  onNewAttempt:boolean;
  onNewRouteByFollowed:boolean;
  // canLeave:boolean = false;
  isPoppingToRoot = false;
  isLoggingOut:boolean = false;

  selectedGym:any = null;
  gymData: any[] = null;
  optionsLoaded:boolean = false;
  // loggedIn: boolean = true;

  Object: any //for exposing 'Object to the template';


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public platform: Platform,
    public settingsProv: SettingsProvider,
    public connection: ConnectionProvider,
    public listsProv: ListsProvider,
    public mapsProv: MapsProvider,
    public authProv: AuthProvider,
    public alertCtrl: AlertController,
    public events: Events,
    ) {
      console.log('in Settings page constructor' );
      //SUPER IMPORTANT: cannot use Object.keys in template without this! THERE WILL BE BUGS if you delete it.
      this.Object = Object;
  }


  //Trying to use DidEnter instead of did load, so gymOptions are set every time
  ionViewDidEnter() {
    console.log('settingsPage: ionViewDidEnter');
    this.platform.ready().then(()=>{
      const userInfo = this.authProv.getUser();
      this.currentDisplayName = userInfo.displayName;
      //TODO: set this as an onconnect callback (currently require leave return after connect)
      this.settingsProv.setGymOptions().then( ( gymData )=>{
        this.gymData = gymData;
        console.log('settingsPage: this.gymData:' );
        console.log(this.gymData);

        this.optionsLoaded = true;
      }).catch((error)=>{
        //TODO: prompt user to connect to internet and set an onConnect listener to setGymOptions
        console.log("Could Not Load Gym Options (In Settings) TODO: prompt user to connect to internet, etc");
      });


      this.settingsProv.getSettings().then(async (result)=>{
        if(result){
          this.settings = result;
          console.log("@@## settingsPage: current settings:", this.settings);

          if(this.settings.gymId){
            console.log('settingsPage: about to getMaps and this.settings.gymId is:');
            console.log(this.settings.gymId);
            try{
              let maps = await this.mapsProv.getMaps(this.settings.gymId);
              console.log('settingsPage: ionViewDidEnter ...got maps as:');
              console.log(maps);

              if(maps && maps.hasOwnProperty('mapByFloor')){
                console.log('setting this.mapsObj as');
                console.log(maps);
                this.mapsObj = maps;
              }
              console.log('settingsPage: got mapsObj as:');
              console.log(this.mapsObj);
            }
            catch(error){
              console.log('mapsPage ionViewDidEnter could not get mapsObj. Got Error:');
              console.lie(error);
            };
          }
          this.unpackSettings();
        }
      }).catch((error)=>{
        console.log(error);
      });
    });
  }

  unpackSettings():Promise<any>{
    //console.log("unpackSettings called");
    return new Promise((res,rej)=>{
      if (this.settings){

        if(this.settings.gymId){
          this.gymId=this.settings.gymId;
        }
        if(this.settings.currentFloor){
          this.currentFloor = this.settings.currentFloor;
        }
        // this.visibleMode=this.settings.visibleMode;
        // this.onComment=this.settings.notificationSettings.onComment;
        // this.onRouteDeleted=this.settings.notificationSettings.onRouteDeleted;
        // this.onNewSend = this.settings.notificationSettings.onNewSend;
        // this.onNewAttempt= this.settings.notificationSettings.onNewAttempt;
        // this.onNewRouteByFollowed=this.settings.notificationSettings.onNewRouteByFollowed;
        res();
      }else{
        let error = new Error('Cannot unpack settings. Settings are: '+ this.settings);
        //rej(error);
      }
    });
  }


  repackSettings():Promise<any>{
    console.log("repackSettings");
    return new Promise((res)=>{
      //HACK: hack alert:
      if(!this.settings){
        this.settings = {
          gymId: null,
          currentFloor:null
        };
      }
      this.settings['gymId'] = this.gymId;
      this.settings['currentFloor'] = this.currentFloor;
      /*
      this.settings['visibleMode'] = this.visibleMode;
      this.settings.notificationSettings['onComment']=this.onComment;
      this.settings.notificationSettings['onRouteDeleted']= this.onRouteDeleted;
      this.settings.notificationSettings['onNewSend'] = this.onNewSend;
      this.settings.notificationSettings['onNewAttempt']= this.onNewAttempt;
      this.settings.notificationSettings['onNewRouteByFollowed'] = this.onNewRouteByFollowed;
      */
      console.log('repackSettings: about to resolve');
      res();
    });
  }

  async updateGymId(gym){
    //console.log('******UPDATE GYM ...and gym is:****** ', gym);

    this.gymId = gym.gymId;
    // this.settings.gymId = gym.gymId;
    await this.settingsProv.updateGymId(this.gymId);

    if(!this.settings.hasOwnProperty('currentFloor') || !Number.isInteger(this.settings.currentFloor)){
      this.settings.currentFloor = 0;
    }

    if(this.gymId){
      try{
        console.log('settingsPage: updateGymId. Trying to set this.mapsObj and this.gymId is:');
        console.log(this.gymId);
        let maps = await this.mapsProv.getMaps(this.gymId);
        console.log('settingsPage: updateGymId...got maps as:');
        console.log(maps);
        if(maps && maps.hasOwnProperty('mapByFloor')){
          console.log('settingsPage: updateGymId... settings this.mapsObj to:');
          console.log(maps);
          this.mapsObj = maps;
        }
      }
      catch(error){
        //this.mapsObj = null;
        console.log('settingsPage: Could not get maps object');
        console.log(error);
      };

      await this.settingsProv.updateGymId(gym.gymId);
    }
    return(null);

  }

  async updateFloor(floor){
    console.log('updateFloor: type of floor is:');
    console.log(typeof floor);
    this.currentFloor = floor;
    this.settings.currentFloor = floor;
    this.settingsProv.updateCurrentFloor(floor);
  }

  async logOutUser(){
    console.log('settings.ts (page), logOutUser Triggered');
    try{
      this.listsProv.wipeLists();

      //Important: since the login page has the ability to shut down the app,
      //it is critical that the 'exit:ready' event is received before pushing to the LoginPage
      //This event signifies that the feed-page has cleaned up it's various subscriptions
      this.events.subscribe('logout:ready', async ()=>{
        await this.authProv.logoutUser();
        this.isLoggingOut = true;
        await this.navCtrl.push('LoginPage');
      });
      //
      // await this.authProv.logoutUser();
      // this.isLoggingOut = true; //if false, settings are not saved
      // await this.navCtrl.push('LoginPage');

      this.events.publish('logout:prepare');
      console.log('settings.ts (page), logOutUser published logout:prepare');

    }
    catch(err){
      console.log("could not log out user...", err);
    }

  }

  returnToFeed(){
    //BUG: disable the button after it is pressed once (double tap tries to pop all pages from stack including root page)
    this.isPoppingToRoot = true;
    console.log('settingsPage: returnToFeed... about to popToRoot()');
    this.navCtrl.popToRoot();
    // this.navCtrl.pop();
  }

//
// ionViewCanLeave2():Promise<any>{
//   console.log('ionViewCanLeave');
//     return new Promise((res, rej)=>{
//       this.repackSettings().then(()=>{
//         console.log('ionViewCanLeave: calling saveSettings and this.settings is:');
//         console.log(this.settings);
//         this.settingsProv.saveSettings(this.settings).then(()=>{
//           if(this.settings && this.settings.gymId){
//             console.log("settings saved, exiting");
//             res();
//           }else{
//             //if settings are undef or null OR if gymId is not set, cannot proceed to any
//             //other content
//             console.log("settings not saved, rejecting");
//             rej();
//           }
//         }).catch(error=>{
//           console.log("ionViewCanLeave: error trying to save settings");
//           console.lie(error);
//         });
//       }).catch(error=>{
//         console.log("ionViewCanLeave: error trying to repack settings");
//         console.lie(error);
//       });
//     });
//   }

  async ionViewCanLeave():Promise<any>{
    if(this.isLoggingOut){
      return true;
    }
    else{
      try{
        await this.repackSettings();
        console.log('ionViewCanLeave: settings is:');
        console.log(this.settings);

        if(this.settings && this.settings.gymId){
          //NOTE: saveSettings calles <some_firestore_ref>.set which does not resolve when offline. For some reason, this should still pass the navGuard

          console.log('ionViewCanLeave: about to save settings');
          return this.settingsProv.saveSettings(this.settings);
        }else{
          console.log('ionViewCanLeave: in else');
          //TODO: display a message in the template: could not save settings because:
          //...maybe have them connect to internet (if could not get a gymId... etc)
          return false;
        }
      }catch(error){
        //TODO: set messages helping user fix the problem
        console.log('ionViewCanLeave: catching error:');
        console.lie(error);
        return false;
      };
    }
  }

  // //QUESTION: only saves settings when navigate away from page.
  // //is this good enough?
  // async ionViewCanLeave():Promise<boolean>{
  //   if(this.authProv.getUser()){
  //     //console.log("ionViewCanLeave called");
  //     try{
  //       await this.repackSettings();
  //       //console.log("settings repacked...");
  //     }
  //     catch(repackErr){
  //       console.log("could not repackSettings... Error:", repackErr);
  //     }
  //
  //     try{
  //       //console.log("this.settings:", this.settings);
  //       //console.log("this.settings.gymId:", this.settings.gymId);
  //       await this.settingsProv.saveSettings(this.settings);
  //       //console.log("settings saved...", this.settings);
  //       //console.log("gymid::", this.settings.gymId);
  //       if(this.settings && this.settings.gymId ){
  //         //console.log("resolving true...");
  //         return true;
  //       }
  //       else{
  //
  //         const alert:Alert = this.alertCtrl.create({
  //          message: "Please select a gym and floor number to continue.",
  //          buttons: [
  //            {
  //              text: "Ok",
  //              role: 'cancel'
  //            }
  //          ]
  //         });
  //         alert.present();
  //         return false;
  //       }
  //     }
  //     catch(saveErr){
  //       console.log("could not save settings... Error:", saveErr);
  //       const alert:Alert = this.alertCtrl.create({
  //        message: "Please select a gym and floor number to continue.",
  //        buttons: [
  //          {
  //            text: "Ok",
  //            role: 'cancel'
  //          }
  //        ]
  //       });
  //       alert.present();
  //       return false;
  //     }
  //   }
  //   else{
  //     return true;
  //   }
  // }


}
