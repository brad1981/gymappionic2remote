import { Component, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MapsProvider, Maps } from '../../providers/maps/maps';//for accessing maps.colors.<stuff you need>

import { RouteEditorFeatures, RangeFilter, filterCategoriesObject,RouteFilters,
         EditorOptions } from '../../providers/globalConstants/globalConstants';

import { FilterSelectorComponent } from '../../components/filter-selector/filter-selector';
import { Platform, Alert, AlertController } from 'ionic-angular';

@Component({
  selector: 'route-features2',
  templateUrl: 'route-features2.html'
})

export class RouteFeatures2Component {

  @ViewChild('filterSelector') filterSelector: FilterSelectorComponent ;
    //TODO: get proper type for existingRoute
  @Input() existingRouteName: string = null;
  @Input() existingRouteGrade: number = null;
  // @Input() existingRouteFloor: any = null;
  @Input() existingRouteFeatures: string[]= null;
  @Input() existingWallAngle: number = null;
  @Input() existingDescription: string = null;
  @Input() color: string = null;
  @Input() setBy: string = null;

  @Output() gotFeatures = new EventEmitter();

  mode : string = 'edit'; //use the filters component in 'edit' mode (disables dualKnobs)
  // name:string = null;
  // grade:number = null;
  variablesReadyForFilterSelector:boolean = false;


  featuresForm: FormGroup;
  routeEditorFeatures:RouteEditorFeatures = null;
  orderedColorNames:string[] = null;
  colorNamesToColorCodes:{[key:string]:string} = null;
  platformReady:boolean = false;


  constructor(
    public platform: Platform,
    public mapsProv: MapsProvider,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
  ) {
    console.log('route-featueres2 constructor');

    this.featuresForm = formBuilder.group({
      routeName:['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z0-9\' ]*'), Validators.required])],
      // routeGrade:['', Validators.compose([Validators.pattern('^(1[0-7]|[1-9])$'), Validators.required])],
      routeDescription:['', Validators.compose([Validators.pattern('[a-zA-Z0-9.,\'!? ]*'), Validators.required])]
    })

    platform.ready().then(async ()=>{

      const mapsObj = await this.mapsProv.getMaps();
      if(mapsObj.hasOwnProperty('colors')){
        this.orderedColorNames = mapsObj.colors.orderedColors;
        this.colorNamesToColorCodes = mapsObj.colors.colorNamesToColorCodes;
      }

      console.log('************** route-features2, constructor, orderedColorNames: ***************');
      console.log(this.orderedColorNames);

      console.log('************** route-features2, constructor, colorNamesToColorCodes: ***************');
      console.log(this.colorNamesToColorCodes);

      this.platformReady = true;
    });
  }

  ngAfterContentInit(){
    console.log('calling getRouteEditorInputsFromTemplateValues');
    this.routeEditorFeatures = this.getRouteEditorInputsFromTemplateValues();
    this.variablesReadyForFilterSelector = true;
  }

  //build this.routeEditorFeatures as RouteEditorFeatures from existing inputs, if possible
  //If any relevant @Input's are falsely (see the if below)...
  //return whatever value this.routeEditorFeatures already has (should be null)
  getRouteEditorInputsFromTemplateValues():RouteEditorFeatures{

    if(this.existingRouteName && this.existingRouteGrade && this.existingRouteFeatures
       && this.existingWallAngle){
      this.routeEditorFeatures = { attributes:{ },
                                 rangeFilterVal:{}
                               };
    //create attributes: {<feature-name>:true} for each feature that has been set
    this.existingRouteFeatures.forEach(feature => {

      this.routeEditorFeatures.attributes[feature]=true;
    });

    this.routeEditorFeatures.rangeFilterVal['grade'] = this.existingRouteGrade;
    this.routeEditorFeatures.rangeFilterVal['wallAngle'] = this.existingWallAngle;
    }

    console.log('route-features2.ts, getRouteEditorInputsFromTemplateValues returning this.routeEditorFeatures');
    console.log(this.routeEditorFeatures);
    return this.routeEditorFeatures;
  }

  // toggleExclusiveCategories(exclusiveAttributesObject:ExclusiveAttribute,key:number){
  //   // console.log('********* filter-selector, toggleExclusiveCategories, key is: ', key, ' and exclusiveAttributesObject is: ***********');
  //   // console.log(exclusiveAttributesObject);
  //   //if this.filters does not have attributes property, itnit attributes property with
  //   const featureToKeep = exclusiveAttributesObject.choices[key].value;
  //   // console.log('featureToKeep is: ', featureToKeep);
  //
  //   //build array of features from the exclusiveAttributesObject
  //   let exclusiveFeatures:string[] = [];
  //   exclusiveAttributesObject.choiceKeys.forEach(key => {
  //     exclusiveFeatures.push(exclusiveAttributesObject.choices[key].value);
  //   });
  //
  //   console.log('exclusiveFeatures array is:');
  //   console.log('exclusiveFeatures');
  //
  //   //remove all of those features from the filters object (if it's not present, delete just returns true... does not error)
  //   exclusiveFeatures.forEach(exFeat => {
  //     delete this.filters.attributes[exFeat];
  //   });
  //
  //   console.log('after deleting exclusiveFeatures from this.filters.attributes, this.filters.attributes is:');
  //   console.log(this.filters.attributes);
  //
  //   //set the filter that we want to keep
  //   this.filters.attributes[featureToKeep] = true;
  //
  //   console.log('afterAddidng featureToKeep to this.filters.attributes, this.filters.attributes is:');
  //   console.log(this.filters.attributes);
  //
  //   console.log('block outline boolean test: ', !this.filters.attributes.hasOwnProperty(this.filterCategoryObject.exclusiveAttributes.official.choices[key].value)
  //               || !this.filters.attributes[this.filterCategoryObject.exclusiveAttributes.official.choices[key].value]);
  //
  // }



  tbDoneSetting(){
    console.log("description:", this.featuresForm.value.routeDescription);
    if(!this.featuresForm.valid){
      console.log("featuresForm:", this.featuresForm.value.routeName);
      const formAlert:Alert = this.alertCtrl.create({
        message: "Notice, route names can only be 30 characters long and consist of numbers and letter only. Route descriptions can not have special characters. Both route name and description are required.",
        buttons: [
          {
            text: "OK",
            role: 'cancel'
          }
        ]
      });
      formAlert.present();
    }

    else{
      const routeData = this.filterSelector.getFilters() as RouteEditorFeatures;

      //build array containing only the feature names that correspond to a value of true in RouteData.attributes
      const filters:string[] = Object.keys(routeData.attributes).filter(filterName => {return routeData.attributes[filterName]});

      let routeInfo = { name: this.featuresForm.value.routeName,
                          grade: routeData.rangeFilterVal['grade'],
                          setBy: this.setBy ,
                          wallAngle: routeData.rangeFilterVal['wallAngle'],
                          features: filters,//the filter-selector component scrubs out false values
                          description:this.featuresForm.value.routeDescription
                        }
      //color should only be set in some cases: iff gym has a color scheme on the database
      if(this.color){
        routeInfo['color'] = this.color;
      }

      console.log('route-features2.ts, tbDoneSetting, emitting routeInfo:');
      console.log(routeInfo);
      this.gotFeatures.emit(routeInfo);
    }

  }

}
