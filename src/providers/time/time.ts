import { Injectable } from '@angular/core';
import {ConnectionProvider} from '../../providers/connection/connection';

import * as firebase from 'firebase/app';//used for definig types

@Injectable()
export class TimeProvider {

  constructor(public connectionProv: ConnectionProvider ) {
  }


  //
  //if connected to the internet, return a timestamp that will be executed on the server
  //if not connected to the internat, use device time to approximate the
  //WARNING: the client's clock could be way off, resulting in workouts that happen in the future,
  // or messing up querry orders. Be sure you want to use this method.
  //TODO: consider using a cloud function to check that client-generated timestamps are not egregiously
  //inaccurate
  getTimestampForFirestore(): Date | firebase.firestore.FieldValue{
   if(this.connectionProv.isConnected()){
     return firebase.firestore.FieldValue.serverTimestamp();
   }else{
     return new Date();
   }

  }

}
