import { Component, Input, Output, EventEmitter } from '@angular/core';

import { SettingsProvider } from '../../providers/settings/settings';

import { Maps } from '../../providers/maps/maps';

@Component({
  selector: 'floor-toggle',
  templateUrl: 'floor-toggle.html'
})
export class FloorToggleComponent {

  @Input() currentFloor:number = 0;
  @Input() mapsObj:Maps;

  @Input() changingFloorNow = false;
  @Output() newFloor = new EventEmitter;


  constructor(public settingsProv:SettingsProvider) {
  }

  async toggleFloor(){

    // this.changingFloorNow = true;
    const numFloors = Object.keys(this.mapsObj.mapByFloor).length;
    this.currentFloor = (this.currentFloor + 1) % numFloors;
    this.settingsProv.updateCurrentFloor(this.currentFloor);
    //TODO: settingsProv should probably handle updating the settings record in the database 
    await this.settingsProv.updateFloorOnFirestore(this.currentFloor);

    this.newFloor.emit(this.currentFloor);

  }

}
