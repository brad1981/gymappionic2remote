import { Component,
         ElementRef,
         } from '@angular/core';
import { IonicPage,
         NavController,
         NavParams,
         Platform ,
         Events,
         LoadingController,
         ActionSheetController,
         Alert,
         AlertController,
         } from 'ionic-angular';
// import { RouteDetailsProvider } from '../../providers/route-details/route-details';

// import { Observable } from 'rxjs/Observable';
import { RouteDetails4Provider } from '../../providers/route-details4/route-details4';
import { FeedCache4Provider } from '../../providers/feed-cache4/feed-cache4';

import{ ConnectionProvider } from '../../providers/connection/connection';
import{ routeObject,
        SvgMapObject } from '../../providers/globalConstants/globalConstants';
import{ NewRouteUtilitiesProvider } from '../../providers/new-route-utilities/new-route-utilities';
import{ ListsProvider } from '../../providers/lists/lists';
import{ AuthProvider } from '../../providers/auth/auth';

import { SendProvider, Send } from '../../providers/send/send';

import {  Maps, MapDimensions } from '../../providers/maps/maps';

import { RouteCommentComponent } from '../../components/route-comment/route-comment';
import { RouteFeedBackComponent} from '../../components/route-feed-back/route-feed-back';
import { DeleteContestRouteComponent } from  '../../components/delete-contest-route/delete-contest-route';

import { ContainerDimensions } from '../../providers/globalConstants/globalConstants';



@IonicPage()
@Component({
  selector: 'page-route-detail2',
  templateUrl: 'route-detail2.html',
  providers: [RouteDetails4Provider]//unique instance of provider for each details view
})

export class RouteDetail2Page {

  //GENERAL OBJS & MISC. DOM
  route: routeObject = null;
  gymName: string;//this is really the gymId
  routeId: string;
  // imgCache: string;
  feedCache:FeedCache4Provider;
  routeName: string = "loading";
  routeGrade: number = 0;
  send:Send = null; //use to display info about whether/when this route has been sent
  routeLoaded: boolean = false;
  alreadySent: boolean = null;
  fStoreRouteDocPath: string = null
  showRouteFeedbackComponent:boolean = false;
  routeDetailWidth: number = 0;
  routeDetailHeight: number = 0;

  svgRouteContainerDimensions: ContainerDimensions = null;
  mapContainerDimensions:ContainerDimensions = null;
  mapWidthFractionOfWindow: number = 0.75;
  mapHeightFractionOfWindow: number = 0.5;

  //LISTS
  existsInLists:string[] = null;
  inLists: any[] = null;
  floorLists: string[] = [];
  doesNotExistInLists: boolean = true;
  userDoesNotHaveFloorLists: boolean = true;
  addToListButton: boolean = false;
  selectedOpenList: string;
  // localAdd: string[] = [];
  editList: boolean = false;
  // listsLoaded: boolean = false;

  // ROUTE DELETE-CONTEST
  userId:string;
  showDeleteContestComponent:boolean = false;

  // willBeDeleted: boolean = false;
  // dateForDelete: string = "";
  // markedByCurrentUser: boolean = false;
  // markedByUserName: string = "";
  // thisUserVoted: boolean = false;
  // thisUserAffirmed: boolean = false;
  // thisUserContested: boolean = false;
  // votesAffirm: number = 1;
  // votesContest: number = 0;
  //
  // MAP
  svgMapObj:SvgMapObject;
  //TODO: maybe pass in a few nearby routes to help with finding this route
  mapsObj: Maps = null;

  //NOTE: below is temp for beta
  mapId: string;
  commentsUrl:string;
  connectedCallback:()=>void;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public routeDetailsProv: RouteDetails4Provider,
    public loadingCtrl: LoadingController,
    public platform: Platform,
    public events: Events,
    public connectionProv: ConnectionProvider,
    public actionSheetCtl: ActionSheetController,
    public alertCtrl: AlertController,
    public nruProv: NewRouteUtilitiesProvider,
    public listsProv: ListsProvider,
    public authProv: AuthProvider,
    public sendProv: SendProvider,
    private el: ElementRef,
    ) {
  }
  async ionViewDidLoad(){
    this.platform.ready().then(()=>{
      // console.log('route-details2 (page), ionViewDidLoad{}, el.nativeElement:');
      // console.log(this.el.nativeElement);
      // console.log(`offsetHeight: ${this.el.nativeElement.offsetHeight}, offsetWidth: ${this.el.nativeElement.offsetWidth}`);

      // console.log('height---' + this.el.nativeElement.offsetHeight);
      // console.log('width---' + this.el.nativeElement.offsetWidth);
      //TODO: Move width and height setting logic into the RouteDisplayComponent,
      // then build in a few style settings
      //to be passed as flags, like "half-platform-height" and "full-platform-width"

      this.routeDetailHeight = this.el.nativeElement.offsetHeight;
      this.routeDetailWidth = this.el.nativeElement.offsetWidth;


    this.svgRouteContainerDimensions = {
      x: this.platform.width(),
      y: this.platform.height()
    };

    //TODO: fix the width adjust so it is a better match for the gutter
    this.mapContainerDimensions = {
      x: this.platform.width() * this.mapWidthFractionOfWindow ,
      y: this.platform.height() * this.mapHeightFractionOfWindow
    };

      this.gymName = this.navParams.get('gymName');
      this.route = this.navParams.get('routeSummary');
      this.send = this.navParams.get('send') ? this.navParams.get('send') : null;
      this.mapsObj = this.navParams.get('mapsObj');


      //mode:'navigate'
      this.svgMapObj = {
        mode:'view' ,
        mapDimensions: this.mapsObj.mapByFloor[this.route.floor].dimensions,
        mapContents: this.mapsObj.mapByFloor[this.route.floor].mapContents,
        routes: [this.route],
        selectedRoute: this.route,
        newRoute:this.route.location ,
        containerDimensions: null
      }

      // console.log('route-details, ionViewDidLoad{}, this.svgMapObj:');
      // console.log(this.svgMapObj);

      this.userId = this.authProv.getUser().uid;

      // console.log("### svgMapObj:", this.svgMapObj);
      // console.log("HHH got route as:", this.route);
      this.routeId = this.navParams.get('routeSummary')['pushKey'];
      //TODO: phase out imgCache, replace with cache... delete imgCache entirely.
      // this.imgCache = this.navParams.get('cacheInfo');
      this.feedCache = this.navParams.get('feedCache');
      //// NOTE: below is temp.
      this.mapId = this.mapsObj.mapId;

      // let userInfo = this.authProv.getUser();
      this.fStoreRouteDocPath= `/GYMS/${this.gymName}/MAPS/${this.mapId}/ROUTES/${this.routeId}/`

      if(this.route){
        this.routeLoaded = true;
        this.routeName = this.route.name;
        this.routeGrade = this.route.grade;
      }
    }).catch((err) => {
      console.log("platform ready  ", err);
      throw err;
    });//end platform.ready
  }

  handleDeleteContestAction(action:string){
    switch(action){
      case 'delete':

        this.showDeleteContestComponent = false;
        // console.log(`route-detail2.ts, handleDeleteContestAction, action:${action}`)
        this.navCtrl.pop();
        break;
      case 'delete-vote':

        this.showDeleteContestComponent = false;
        // console.log(`route-detail2.ts, handleDeleteContestAction, action:${action}`)
        this.navCtrl.pop();
        break;
      case 'contest':

        this.showDeleteContestComponent = false;
        // console.log(`route-detail2.ts, handleDeleteContestAction, action:${action}`)
        break;
      case 'cancel':
        this.showDeleteContestComponent = false;
        // console.log(`route-detail2.ts, handleDeleteContestAction, action:${action}`)
        break;
    }
  }

  sendRoute(){
    // console.log('route-detail2 page, sendRoute triggered');
    // console.log('this.send:');
    // console.log(this.send);
    try{
      if(this.send && this.send.numSends > 0){
        const sendActionSheet = this.actionSheetCtl.create({
        title: `You sent this route ${this.send.numSends} previous time(s). `,
        buttons: [
          {
            text: 'I sent again!',
            handler: () => {
              // console.log('route-detail2 page, sendRoute, submit handler callback triggered');
              this.sendProv.submitSend(this.gymName, this.mapId, this.route).then(send=>{
                //IMPORTANT: update current send!
                this.send = send;
              });
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              // console.log('***route-detail2 page, sendRoute: sendActionSheet .... Cancel clicked');
            }
          }
        ]
      });
      sendActionSheet.present();
    }else{ //they haven't sent this route yet
      this.sendProv.submitSend(this.gymName,this.mapId, this.route).then(send=>{
        // console.log('***route-detail2 page, sendRoute: sendActionSheet submitt SUCCESS!!!... send is:');
        // console.log(send);
        //IMPORTANT: update current send.
        this.send=send;
      });
    }
    }
    catch(error){
      // console.log('route-detail2 prov, sendRoute: error')
      throw error;
    };
  }



  ionViewWillLeave(){
    this.listsProv.cleanLists();
  }
}


  // async listChecks(){
  //   // console.log("listChecks");
  //
  //   try{
  //     let detailsListObj = await this.listsProv.returnRouteDetailsListObject(this.routeId);
  //     this.existsInLists = detailsListObj.existsInLists;
  //     let uniq = this.existsInLists.filter((v, i, a) => a.indexOf(v) === i);
  //     let count = Array(uniq.length).fill(0);
  //     uniq.forEach((v, i) => {
  //       this.existsInLists.forEach( v2 => {
  //         if(v === v2){
  //           count[i] += 1;
  //         }
  //       });
  //     });
  //
  //     this.inLists = [];
  //     uniq.forEach((v, i) => {
  //       this.inLists.push({list: v, count: count[i]});
  //     });
  //     // console.log("inLists:", this.inLists);
  //
  //     this.floorLists = detailsListObj.floorLists;
  //     if(this.existsInLists.length > 0){
  //       this.doesNotExistInLists = false;
  //     }
  //     else{
  //       this.doesNotExistInLists = true;
  //       this.editList = false;
  //     }
  //     if(this.floorLists.length > 0){
  //       this.userDoesNotHaveFloorLists = false;
  //     }
  //     else{
  //       this.userDoesNotHaveFloorLists = true;
  //     }
  //   }
  //
  //   catch(err){
  //     console.log("route-details2 could not checkIfRouteIsInLists listProv");
  //     throw err;
  //   }
  //
  //
  // }


  // createNewCustomList(){
  //   let newListAlert = this.alertCtrl.create({
  //     title: 'Create New List',
  //     message: 'Enter a title for this new list',
  //     inputs: [
  //       {
  //         name: 'title',
  //         placeholder: 'New List'
  //       },
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Save',
  //         handler: (data)  => {
  //           console.log('Saved clicked, name is:  ', data.title);
  //           this.triggerAsyncNewList(data.title, this.routeId);
  //         }
  //       }
  //     ]
  //   });
  //
  //   newListAlert.present();
  // }

  // async triggerAsyncNewList(title:string, pushKey:string){
  //   // Async methods for creating a new list that could not be called in the alertCtrl.
  //   let loading = this.loadingCtrl.create();
  //   loading.present();
  //   try{
  //     let pushKeyArr = [pushKey]
  //     let result = await this.listsProv.addRouteToList(title, pushKeyArr, true);
  //     if(result){
  //       this.listChecks();
  //     }
  //     else{
  //       let listFailAlert = this.alertCtrl.create({
  //         title: 'Error',
  //         subTitle: 'There is already a list with this name, please choose a different one.',
  //         buttons: ['Dismiss']
  //       });
  //       listFailAlert.present();
  //     }
  //   }
  //   catch(err){
  //     console.log("could not call the createCustomList function from listProv in route-details2");
  //     throw err;
  //   }
  //   loading.dismiss();
  // }
  //
  // toggleEditList(){
  //   this.editList = !this.editList;
  // }
  //
  // async removeFromList(listName){
  //   let loading = this.loadingCtrl.create();
  //   try{
  //     loading.present();
  //     await this.listsProv.removeRouteFromList(this.routeId, listName);
  //     this.listChecks();
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     loading.dismiss();
  //     console.log("err in calling removeRouteFromList");
  //     throw err;
  //   }
  // }
  //

  //
  // unmarkDeleteAlert(){
  //   if(this.markedByCurrentUser){
  //     const unmarkDeleteAlert:Alert = this.alertCtrl.create({
  //       message: "Are you sure you want to unmark this route from deletion?",
  //       buttons: [
  //         {
  //           text: "YES",
  //           handler: () => {
  //             this.unmarkDeleteHandler();
  //           }
  //         },
  //         {
  //           text: "Cancel",
  //           role: 'cancel'
  //         }
  //       ]
  //     });
  //     unmarkDeleteAlert.present()
  //   }
  //   console.log("unMarkDelete");
  // }
  //
  // async unmarkDeleteHandler(){
  //   let loading = this.loadingCtrl.create({});
  //   loading.present();
  //   try{
  //     await this.nruProv.unmarkRouteForDeletion(this.route);
  //     this.willBeDeleted = false;
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     loading.dismiss();
  //     //catch
  //     throw err;
  //   }
  // }
  //
  // async contestDelete(){
  //   console.log("contestDelete");
  //   let loading = this.loadingCtrl.create({});
  //   loading.present();
  //   try{
  //     await this.nruProv.contestDelete(this.route);
  //     this.thisUserContested = true;
  //     this.votesContest += 1;
  //     this.thisUserVoted = true;
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     loading.dismiss();
  //     console.log("error conttesting delete");
  //     throw err;
  //   }
  // }
  //
  // async affirmDelete(){
  //   console.log("affirmDelete");
  //   let loading = this.loadingCtrl.create({});
  //   loading.present();
  //   try{
  //     await this.nruProv.affirmDelete(this.route);
  //     this.thisUserAffirmed = true;
  //     this.votesAffirm += 1;
  //     this.thisUserVoted = true;
  //
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     loading.dismiss();
  //     console.log("error conttesting delete");
  //     throw err;
  //   }
  // }
  //
  // confirmDelete(){
  //   let connected = this.connectionProv.isConnected();
  //   if(connected){
  //     const deleteAlert:Alert = this.alertCtrl.create({
  //       message: "Warning, this will permanently delete the route for all users",
  //       buttons: [
  //         {
  //           text: "DELETE",
  //           handler: () => {
  //             let loading = this.loadingCtrl.create({});
  //             loading.present();
  //             this.deleteRoute().then( () =>{
  //               loading.dismiss();
  //             }).catch((error)=>{
  //                 //handle error
  //                 console.log("error in deleting route", error);
  //             });;
  //
  //           }
  //         },
  //         {
  //           text: "Cancel",
  //           role: 'cancel'
  //         }
  //       ]
  //     });
  //     deleteAlert.present();
  //   }
  //   else{
  //     const connectAlert:Alert = this.alertCtrl.create({
  //       message: "Please go online to delete routes.",
  //       buttons: [
  //         {
  //           text: "Dismiss",
  //           role: 'cancel'
  //         }
  //       ]
  //     });
  //     connectAlert.present()
  //   }
  // }
  //
  // async changeUserVote(){
  //   let loading = this.loadingCtrl.create({});
  //   loading.present();
  //   console.log("changeUserVote");
  //   if(this.thisUserAffirmed){
  //     try{
  //       await this.nruProv.changeMarkedVote(this.route, true);
  //       this.votesAffirm -= 1;
  //       this.votesContest +=1;
  //       this.thisUserContested = true;
  //       this.thisUserAffirmed = false;
  //       loading.dismiss();
  //     }
  //     catch(err){
  //       loading.dismiss();
  //       //catch
  //       throw err;
  //     }
  //   }
  //
  //   else if(this.thisUserContested){
  //     try{
  //       await this.nruProv.changeMarkedVote(this.route, false);
  //       this.votesAffirm += 1;
  //       this.votesContest -=1;
  //       this.thisUserAffirmed = true;
  //       this.thisUserContested = false;
  //       loading.dismiss();
  //     }
  //     catch(err){
  //       loading.dismiss();
  //       //catch
  //       throw err;
  //     }
  //   }
  // }
  //
  // async deleteRoute(){
  //   try{
  //     await this.nruProv.deleteRoute(this.route, this.mapId);
  //     this.navCtrl.pop();
  //   }
  //   catch(error){
  //     console.log("could not delete route", error);
  //   }
  // }
  //
  // toggleAddToListButton(){
  //   this.addToListButton = !this.addToListButton;
  // }
  //
  // async addToList(listName){
  //   let loading = this.loadingCtrl.create();
  //   let duplicate:boolean = false;
  //   for(let i=0; i<this.existsInLists.length; i++){
  //     if(this.existsInLists[i] == listName){
  //       duplicate = true;
  //     }
  //   }
  //   if(duplicate){
  //     let duplicateListAlert = this.alertCtrl.create({
  //       title: 'Duplicate in List',
  //       message: 'This route already exists in this list. Are you sure you want to add it another time?',
  //       buttons: [
  //         {
  //           text: 'Cancel',
  //           handler: () => {
  //             console.log('Cancel clicked... addAgain will be false.');
  //           }
  //         },
  //         {
  //           text: 'Add Again',
  //           handler: async ()  => {
  //             console.log('adding a second time...');
  //             try{
  //               let pushKeyArr = [this.route.pushKey];
  //               loading.present();
  //               let result = await this.listsProv.addRouteToList(listName, pushKeyArr, false);
  //               loading.dismiss();
  //               if(result){
  //                 this.toggleAddToListButton();
  //                 this.listChecks();
  //                 console.log("list added a second time...");
  //               }
  //               else{
  //                 let listFailAlert = this.alertCtrl.create({
  //                   title: 'Error',
  //                   subTitle: 'There was an error.',
  //                   buttons: ['Dismiss']
  //                 });
  //                 listFailAlert.present();
  //               }
  //               this.selectedOpenList = "";
  //             }
  //             catch(err){
  //               loading.dismiss();
  //               console.log("could not add route to list!");
  //               throw err;
  //             }
  //           }
  //         }
  //       ]
  //     });
  //     duplicateListAlert.present();
  //   }
  //   else{
  //     try{
  //       let pushKeyArr = [this.route.pushKey];
  //       loading.present();
  //       let result = await this.listsProv.addRouteToList(listName, pushKeyArr, false);
  //       loading.dismiss();
  //       if(result){
  //         this.toggleAddToListButton();
  //         this.listChecks()
  //       }
  //       else{
  //         let listFailAlert = this.alertCtrl.create({
  //           title: 'Error',
  //           subTitle: 'There is already a list with this name, please choose a different one.',
  //           buttons: ['Dismiss']
  //         });
  //         listFailAlert.present();
  //       }
  //       this.selectedOpenList = "";
  //     }
  //     catch(err){
  //       loading.dismiss();
  //       console.log("could not add route to list!");
  //       throw err;
  //     }
  //   }
  // }
