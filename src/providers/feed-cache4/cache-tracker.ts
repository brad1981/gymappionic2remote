
import { FileTransferObject } from '@ionic-native/file-transfer';
import { betterConsole }  from '../../providers/globalConstants/globalConstants';
import {routeObject} from '../globalConstants/globalConstants';
let console = betterConsole;

/*
*/

//represent an image in the phone's file system
export interface LoadedObject {
  name:string;//name of the image loaded in the directory
  fullPath:string;//the complete nativeURL of the image
}

export interface LoadingObject {
  name:string; //name of the image loaded in the directory
  transferObject: FileTransferObject;//start,  abort or monitor the image download
  //QUESTION: remove this property? It is currently used downloadImage without accessing from expectedSize property
  expectedSize:number; //get from image metadata. use to detect/delete incomplete downloads
}

//describe the CacheDirectory's contents
//track loadingObjects
export interface CacheState{
  idToLoaded: {//keys are routeIds
    [key:string]: LoadedObject
  };

  idToLoading?:{//keys are routeIds
    [key:string]:LoadingObject;
  };

  //represent image priority order (these routes are the most likely to be viewed next)
  //routeId[] of form [highest priority routeId ... lowest priority routeId]
  priorityStack?:string[];

  //track which images are currently being removed (aborted and delteted or just deleted)
  pendingRemovals:{
    [key:string]: boolean;
  }

  //when settings/filters, etc trigger changes in routes$, there will be images in the FeedCache directory
  //that do not belong to any routeObject[] snapshots available to the cacheManager$. These images are 'orphans'
  //orphans are the highest priority for removal from the cache when making space.
  orphans?:{
    [key:string]:string;//key-> route id, value: file name
  }

}

export const emptyCacheState:CacheState = {
      idToLoaded:{},
      idToLoading:{},
      pendingRemovals: {},
      orphans:{},
      priorityStack:[]
    }

export class CacheTracker{
  /* maintain an accurate CacheState object
     provide getters, setters and delete/remove methods for CacheState fields
     DOES NOT handle cache maintenence logic (when to delete images, which to delete, etc)
  */

  cacheState:CacheState = null;

  constructor(cacheState?: CacheState){
    //wherever this is instantiated, make sure that if cacheState.corrupted == true, you reset the cache
    //BEFORE instantiating this!
    //removed from if: && cacheState.hasOwnProperty('corrupted') && !cacheState.corrupted
    if(cacheState ){
      this.cacheState = cacheState;
      //TODO: if there's any way that a cacheState can get saved with pending removals or file transfer
      //objects that have not finished loading, deal with that stuff here.

    }else{
      this.cacheState = emptyCacheState;
    }
  }

//************************ getters **********************************/
/*
all getters with optional routeId parameter (getLoaded, getLoading, etc) return
the entire requested object if routeId is omitted.
*/
  getEmptyCacheState():CacheState{
    return emptyCacheState;
  }

  //if routeId is provided, return the LoadedObject for that routeId xor null if DNE
  //if routeId is not provided, return the current idToLoaded object (it may be an empty object)
  getLoaded(routeId:string = null):{[key:string]:LoadedObject}|LoadedObject{
    if(this.cacheState && this.cacheState.hasOwnProperty('idToLoaded')){
      if(routeId){
        return this.cacheState.idToLoaded[routeId] as LoadedObject;
      }else{
        return this.cacheState.idToLoaded as {[key:string]:LoadedObject};
      }
    }else{
      return null;
    }
  }

  //same as getLoaded, but for the LoadingObject
  getLoading(routeId:string = null):{[key:string]:LoadingObject}|LoadingObject{
    if(this.cacheState && this.cacheState.hasOwnProperty('idToLoading')){
      if(routeId){
        return this.cacheState.idToLoading[routeId] as LoadingObject;
      }else{
        return this.cacheState.idToLoading as {[key:string]:LoadingObject};
      }
    }else{
      return null;
    }
  }

  //return priorityStack if it exists (it might be an empty array)
  //if DNE, return null
  getPriorityStack(): string[]{
    if(this.cacheState && this.cacheState.hasOwnProperty('priorityStack')){
      return this.cacheState.priorityStack;
    }else{
      return null;
    }
  }

  //if requesting entire pendingRemovals object and it is not present, null is
  //returned. if it is present and empty, the entire, empty object is returned.
  //if requesting entry for a specific routeId, boolean is always returned (not null)
  getPendingRemovals(routeId:string = null):{[key:string]:boolean}|boolean{
    if(this.cacheState && this.cacheState.hasOwnProperty('pendingRemovals')){
      if(routeId){
        return !!this.cacheState.pendingRemovals[routeId] as boolean; //could be undefined, hence use !! as type-cast
      }else{
        return this.cacheState.pendingRemovals as {[key:string]:boolean};
      }
    }
  }

  //Same as getLoaded but for the orphans array
  getOrphans(routeId:string = null):{[key:string]:string}|string{
    if(this.cacheState && this.cacheState.hasOwnProperty('orphans')){
      if(routeId){
        return this.cacheState.orphans[routeId] as string;
      }else{
        return this.cacheState.orphans as {[key:string]:string};
      }
    }else{
      return null;
    }

  }

  //how many complete images are in the FeedCache directory?
  //(does not count images that are in the process of loading)
  private getNumLoaded():number{
    const loaded = this.getLoaded();
    const numLoaded = !!loaded ? Object.keys(loaded).length : 0;
    return numLoaded;
  }

  //how many images are currently loading
  getNumLoading():number{
    const loading = this.getLoading();
    const numLoading = !!loading ? Object.keys(loading).length : 0;
    return numLoading;
  }

  //how many orphaned images are in the FeedCache directlry?
  //see the CacheState interface for the definition of an orphan
  getNumOrphans():number{
    const orphans = this.getOrphans();
    const numOrphans = !!orphans ? Object.keys(orphans).length : 0;
    return numOrphans;
  }

  //use to compute how 'full' cache is (since loading stuff will eventually take space)
  getNumLoadedOrLoading(){
    const numLoaded = this.getNumLoaded();
    const numLoading = this.getNumLoading();
    const numOrphans = this.getNumOrphans();

    // console.log(`numCached: ${numCached}, numLoading: ${numLoading}, getNumCachedOrLoading: ${numCached + numLoading}`);
    return numLoaded + numLoading + numOrphans;
  }

  getCacheState(){
    if(this.cacheState){
      return this.cacheState;
    }else{
      return this.getEmptyCacheState();
    }
  }

  /********************* end getters section *********************************/

  /************************* setters ****************************************/
  setOrphan(routeId:string, fileName:string){
    if(!this.cacheState.hasOwnProperty('orphans')){
      this.cacheState.orphans = {};
    }
    this.cacheState.orphans[routeId]=fileName;
    return true;
  }

  //return null if removal is already pending
  //return true if successfully added routeId to pending removals object
  setPendingRemoval(routeId:string){
    if(!this.cacheState.hasOwnProperty('pendingRemovals')){
      this.cacheState.pendingRemovals = {};
    }
    if(this.cacheState.pendingRemovals.hasOwnProperty(routeId)){
      return null;
    }else{
      this.cacheState.pendingRemovals[routeId] = true;
      return true;
    }
  }

  //return null if it is already loading.
  //return the LoadingObject for that routeId if it was successfully added to the cacheState
  setLoading(routeId:string, fileName:string,
    transferObject:FileTransferObject, expectedSize:number){
    if(!this.cacheState.hasOwnProperty('idToLoading')){
      this.cacheState.idToLoading = {};
    }
    if(this.cacheState.idToLoading.hasOwnProperty(routeId)){
      return null;
    }else{
      this.cacheState.idToLoading[routeId]={
        name:fileName,
        transferObject:transferObject,
        expectedSize:expectedSize};
      return this.cacheState.idToLoading[routeId]; ;
    }
  }

  setLoaded(routeId:string, fileName: string, fullPath:string){
    if(!this.cacheState.hasOwnProperty('idToLoaded')){
      this.cacheState.idToLoaded = {};
    }
    this.cacheState.idToLoaded[routeId] = {name:fileName, fullPath:fullPath};
  }
  /********************* end setters section *********************************/

  /********************** updaters ************************************/
  //QUESTION: should this return a value to guarentee synchronous code execution?
  //...is that a thing?
  deleteLoaded(routeId:string){
    // console.log('trying to delete: ', routeId);
    if(this.cacheState && this.cacheState.hasOwnProperty('idToLoaded')){
      // console.log('actually deleting: ', routeId);
      delete this.cacheState.idToLoaded[routeId];
    }
  }

  //return true if it is successfully deleted
  //return false if the routeId was not present
  deleteLoading(routeId:string){
    if(this.cacheState && this.cacheState.hasOwnProperty('idToLoading')
    && this.cacheState.idToLoading.hasOwnProperty(routeId)){
      delete this.cacheState.idToLoading[routeId];
      return true;
    }else{
      return false;
    }
  }

  //return true if delete is successful, return false otherwise
  deletePendingRemoval(routeId:string){
    if(this.cacheState && this.cacheState.hasOwnProperty('pendingRemovals')
    && this.cacheState.pendingRemovals.hasOwnProperty(routeId)){
      delete this.cacheState.pendingRemovals[routeId];
      return true;
    }else{
      return false;
    }
  }

  //compute which currently loaded files are orphans
  //add those records to the this.orphans object
  //delete those records from idToFileName object
  updateOrphans(routes:routeObject[]){
    const loadedIds = Object.keys( this.getLoaded() ) as string[];
    // console.log('updateOrphans: before update, orphans is:');
    // console.log(this.cacheState.orphans);
    // console.log('updating orphans and loadedIds is:');
    // console.log(loadedIds);
    // console.log('routes:');
    // console.log(routes);

    this.cacheState.orphans = {};

    if(loadedIds && loadedIds.length > 0){

      for (let candId  of loadedIds ){
        let isOrphan= true;
        for (let route of routes){
          if (candId === route.pushKey){
            isOrphan = false;
            break;
          }
        }
        if(isOrphan){
          this.setOrphan(candId, this.cacheState.idToLoaded[candId].name);
          this.deleteLoaded(candId);
        }
      }
      // console.log('finished computing orphans:');
      // console.log(this.cacheState.orphans);
      return true;
    }


  }

  //remove routeId from orphan object
  removeOrphan(orphanId:string){
    //TODO: is throwing useful here? Maybe should just return null or false
    if(!this.cacheState.orphans){
      throw new Error(`Cannot remove orphan: ${orphanId} ...orphans not defined`);
    }

    if(this.cacheState && this.cacheState.hasOwnProperty('orphans')
    && this.cacheState.orphans.hasOwnProperty(orphanId) ){
      delete this.cacheState.orphans[orphanId];
      return true;
    }
    else {
      return false;
    }
  }
  /********************** end updaters *******************************/

  /***** update helpers ************/
  //returns null if there are no current downloads to abort
  //returns true if at least one download was aborted
  //DELETES idToLoading[routeId] for any routeId that is currently loading an image
  //does NOT clean up the file system, so make sure you do that somewhere else!
  abortAllTransfers():string[]{
  // console.log('running abort all transfers!')
    if(!this.cacheState
      || !this.cacheState.hasOwnProperty('idToLoading')
      || Object.keys(this.cacheState.idToLoading).length < 1){
        // console.log('abortAllTransfers returning null (no idToLoading)');
      return null;
    }else{
      let routeIds = Object.keys(this.cacheState.idToLoading);
      // console.log('abortAllTransfers, routeIds:');
      // console.log(routeIds);
      routeIds.forEach(pushKey=>{
              let record = this.cacheState.idToLoading[pushKey] as LoadingObject;
              if (record.hasOwnProperty('transferObject')){
                record.transferObject.abort();
                this.deleteLoading(pushKey);
              }
            });
      return routeIds;
    }
  }

/****************** Debugging Helpers *****************************/
  reportLoading(){
    if(!this.cacheState || !this.cacheState.hasOwnProperty('idToLoading')
    || Object.keys(this.cacheState.idToLoading).length < 1){
      // console.log('% Nothing loading %');
    }else{
      // console.log('% ......Loading the following: ...........%');
      // console.log(Object.keys(this.cacheState.idToLoading));
      // console.log('% ......... end loading message ............')
    }
  }
}
