import { Component,
  Input,
  Output,
  EventEmitter } from '@angular/core';

import { AlertController,
         Alert, AlertButton, ToastController, Toast  } from 'ionic-angular';

import{ AuthProvider } from '../../providers/auth/auth';
import{ ConnectionProvider } from '../../providers/connection/connection';

import { DeleteContestProvider } from '../../providers/delete-contest/delete-contest';

import { betterConsole,
         // convertGradeToColorCode,
         // colorNamesToColorCodes,
         RouteObject  }  from '../../providers/globalConstants/globalConstants';

let console = betterConsole;
//TODO: Hide or deactivate the delete or contest button whenever they are clicked ...maybe show a loading spinner
/************** DeleteContestRouteComponent Responsibilities **********************
****  WHEN SHOULD I HIDE THIS COMPONENT? ****
-@Output deleteContestAction emits either 'delete','delete-vote','contest' or 'cancel'
...the component ONLY emits when the user has finished their delete/contest task
*********************************************

-present option to Delete a route
DO NOT ACTUALLY DELETE ROUTES!, instead:
  -if uid is NOT already in the DeleteContest object for this route, add it
  -similar to above for contesting
  -if it is already in the DeleteContest object, inform them they've already voted and emit 'cancel'

  -if there has been at least one delete vote already, present option to contest
  -cancel vote option: emit the 'cancel' event, so parent hides this component (or does whatever we want it to do)

  -switch vote from confirm to contest or vice-versa if appropriate



This component DOES update the database
This component DOES NOT hide it's self (emit 'cancel' and handle in parent)

IMPORTANT architectual info: Google Cloud Platform emits a pub event as part of a cron job.
This pub event is subscribed to by Cloud Functions.

The actual deleting (TODO: switch to archving...another can of worms) of route data is handled
by a cloud function (triggered by a 48hr cron event) that is triggered every 24 hours.
*/

@Component({
  selector: 'delete-contest-route',
  templateUrl: 'delete-contest-route.html'
})
export class DeleteContestRouteComponent {

  //Only one instance of alert should ever exist. Every handler should should dismiss
  //this.alert whether or not the alert's work is successful
  alert:Alert;
  isMarkedForDelete:boolean = true;

  //gymId and mapsId for the route the user is trying to delete or contest
  @Input() gymId:string;
  @Input() mapsId:string;
  //The route the user is trying to delete or contest (the deletion of)
  @Input() route: RouteObject;
  //the curent userId
  @Input() userId: string;

  //emit the action. Options are: 'delete', 'contest' or 'cancel'
  @Output() deleteContestAction =  new EventEmitter();
  constructor( public authProv: AuthProvider, public connectionProv:ConnectionProvider,
              public delContProv:DeleteContestProvider, public toastCtrl:ToastController,
              public alertCtrl:AlertController ) {
  }

  ngAfterViewInit(){
    if(this.route && this.route.hasOwnProperty('deleteVoterIds')){
      this.isMarkedForDelete = this.route.deleteVoterIds.length > 0;
    }else{
      this.isMarkedForDelete = false;
    }
  }

  //Create alert popups that ask for cofirmation of the delete attempt
  //Handle various scenarios: user is/isn't route author, there is/isn't a connection
  //Generate toasts that summarize results for the user
  handleDeleteAttempt(){
    try{
      // console.log('********* handleDeleteAttempt triggered **********');

      const cancelButton = {
            text: "Cancel",
            role: "cancel",
            handler: ()=>{
              if(this.alert){
                this.alert.dismiss();
                this.deleteContestAction.emit('cancel');
              }

            }
          }

      if(!this.connectionProv.isConnected()){
        const title = "Connect to internet before delting routes."
        const confirmButton: AlertButton = {
                                text: 'Ok',
                                role: 'cancel',
                                handler: ()=>{
                                  this.alert.dismiss();
                                  this.deleteContestAction.emit('cancel')}
                                }
        this.generateAlert(title,[confirmButton]);
        return
      }else{
        if(this.route.authorId == this.authProv.getUser().uid){
          //connected to internet and this user IS the author
          const title = "Are you sure you want to permanently delete this route?"
          const delButton:AlertButton = {

            text: 'Delete Route',
            handler: ()=>{
              this.delContProv.deleteRoute(this.gymId,this.mapsId,this.route.pushKey).then(()=>{
                this.alert.dismiss();
                this.deleteContestAction.emit('delete')
                this.toastCtrl.create({
                  message:"Route deleted. It may take a few minutes for the feed to reflect this change.",
                  duration:3000,
                  position:'top'
                }).present();
              }).catch((error)=>{
                this.alert.dismiss();
                this.deleteContestAction.emit('cancel');
                this.toastCtrl.create({
                  message:"We could not delete your route. Check your internet connection.",
                  duration:3000,
                  position:'top'
                }).present();
              throw(error);
              });
            }
          }

          // this.alert = this.alertCtrl.create({title:title, buttons:[delButton,cancelButton] });
          // this.alert.present();
          this.generateAlert(title, [delButton,cancelButton]);

        }else{
          //connected to internet and this user is NOT the author
          const title = "Are you sure this route does not exist?"
          const delButton:AlertButton = {
            text: 'Delete Route',
            handler: ()=>{
              // console.log('Calling handleVote with:');
              // console.log(this.gymId,this.mapsId,this.route.pushKey,
                // this.userId, 'delete', this.route.deleteVoterIds,
                // this.route.contestVoterIds);
                this.delContProv.handleVote(this.gymId,this.mapsId,this.route.pushKey,
                this.userId, 'delete', this.route.deleteVoterIds,
                this.route.contestVoterIds).then(async (res)=>{
                  this.alert.dismiss();
                  if(res){
                    //res is truthy, so the vote was successfully uploaded
                    this.deleteContestAction.emit('delete-vote');
                    this.toastCtrl.create({
                      message:"Thanks for helping keep our records up to date! The route will be removed soon.",
                      duration:3000,
                      position:'top'
                    }).present();
                  }else{
                    //res was falsey (but an error was NOT thrown=> the user already voted)
                    this.deleteContestAction.emit('cancel');
                    this.toastCtrl.create({
                      message:"It looks like you already told us about this. We'll remove it after the community has had time to double-check that it is missing.",
                      duration:4000,
                      position:'top'
                    }).present();
                  }
              }).catch(error=>{
                this.alert.dismiss();
                this.toastCtrl.create({
                  message:"We could not delete the route. Check your internet connection.",
                  duration:3000,
                  position:'top'
                }).present();
                throw(error);
              });
            }
          }

          // this.alert = this.alertCtrl.create({title:title, buttons:[delButton,cancelButton] });
          // this.alert.present();
          this.generateAlert(title,[delButton,cancelButton]);
        }
      }
    }catch(error){
     throw(error);
    };
  }

  //Create alert popups that ask for cofirmation of the delete attempt
  //Generate toasts that summarize results for the user
  handleContestAttempt(){
    const cancelButton = {
      text: "Cancel",
      role: "cancel",
      handler: ()=>{
        this.alert.dismiss();
        this.deleteContestAction.emit('cancel')
      }
    }
//This is their first attempt to contest. Make a popup to confirm.
const title = "Are you sure the route is still there?";
const contestButton: AlertButton = {
      text: 'The route is still here!',
      handler: ()=>{
          this.delContProv.handleVote(this.gymId,this.mapsId,this.route.pushKey,
          this.userId, 'contest', this.route.deleteVoterIds,
          this.route.contestVoterIds).then((res)=>{
            this.alert.dismiss();
            if(res){
              this.deleteContestAction.emit('contest');
              this.toastCtrl.create({
                message:"Thanks for helping keep our records up to date!",
                duration:3000,
                position:'top'
              }).present();
            }else{
              //res was falsey (but an error was NOT thrown=> the user already voted)
              this.deleteContestAction.emit('cancel');
              this.toastCtrl.create({
                message:"It looks like you already told us about this. We're getting confirmation from the community. Thanks for the help!",
                duration:4000,
                position:'top'
              }).present();
            }
        }).catch(error=>{
          this.alert.dismiss();
          this.toastCtrl.create({
            message:"We could not process your feedback. Check your internet connection.",
            duration:3000,
            position:'top'
          }).present();
          throw(error);
        });
      }
    }
    this.generateAlert(title,[contestButton, cancelButton]);
  }

  //Create alert popups that ask for cofirmation of the delete attempt
  //Generate toasts that summarize results for the user
  // handleContestAttempt(){
  //
  //   const cancelButton = {
  //     text: "Cancel",
  //     role: "cancel",
  //     handler: ()=>{
  //       this.alert.dismiss();
  //       this.deleteContestAction.emit('cancel')
  //     }
  //   }
  //
  //   const alreadyVotedContest = this.route.hasOwnProperty('contestVoterIds') ?
  //                           this.route.contestVoterIds.indexOf(this.userId) >=0 : false;
  //   //if they have not already contested:
  //   if(!alreadyVotedContest){
  //     //This is their first attempt to contest. Make a popup to confirm.
  //     const title = "Are you sure the route is still there?";
  //     const contestButton: AlertButton = {
  //           text: 'The route is still here!',
  //           handler: ()=>{
  //               this.delContProv.handleVote(this.gymId,this.mapsId,this.route.pushKey,
  //               this.userId, 'contest', this.route.deleteVoterIds,
  //               this.route.contestVoterIds).then(()=>{
  //                 this.alert.dismiss();
  //                 this.deleteContestAction.emit('contest');
  //                 this.toastCtrl.create({
  //                   message:"Thanks for helping keep our records up to date!",
  //                   duration:3000,
  //                   position:'top'
  //                 });
  //             }).catch(error=>{
  //               this.alert.dismiss();
  //               this.toastCtrl.create({
  //                 message:"We could not process your feedback. Check your internet connection.",
  //                 duration:3000,
  //                 position:'top'
  //               });
  //               throw(error);
  //             });
  //           }
  //         }
  //         this.generateAlert(title,[contestButton, cancelButton]);
  //   }else{
  //     //They already voted to contest this delete.
  //     //Do not generate an alert. Just show toast thanking for help.
  //     this.toastCtrl.create({
  //       message:"It looks like you already told us about this. Thanks for the help!",
  //       duration:3000,
  //       position:'top'
  //     });
  //   }
  // }

  generateAlert(title:string, buttons:AlertButton[] ){
    console.log('***GENERATE ALERT***');
    this.alert = this.alertCtrl.create({title:title, buttons:buttons});
    this.alert.present();
  }

  //trigger the emit
  emitAction(action:string){
    this.deleteContestAction.emit(action);
  }
}
