import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Platform, Events } from 'ionic-angular';
import * as firebase from 'firebase/app';//used for definig types
//import{ ConnectionProvider } from '../connection/connection';
import { LocalStorageProvider } from '../local-storage/local-storage';
import { UserDetailsProvider, User } from '../../providers/user-details/user-details';
//import { SettingsProvider } from '../settings/settings';

import { UrlProvider } from '../../providers/url/url';


const commentsBatchSize = 2;
const repliesBatchSize = 2;

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

/* Related cloud functions:

onDeleteRoute: trigger deletetion of all of the route's comment documents and all of their reply documents
onDeleteComment: trigger the deletion of the comment's reply thread if any exist.

eventually, might want to send the reply author a copy of their deleted reply
and give them the option to edit it into a stand-alone comment. Maybe not though.

*/

export interface Reply{
  date?: Date | any; //could not find type returned by firebase.firestore.FieldValue.serverTimestamp()
  authorName?: string;
  authorId: string;
  authorMug?:string;
  comment: string;
  replyId?: string;
  isSelected?:boolean;
}

export interface Comment{
  date: Date | any;
  authorId:string;
  authorMug?: string; //url to the author's avatar image
  authorName:string;
  comment: string;
  /*The stuff below is only used in the client, not uploaded to firestore.*/
  replies?:Reply[];
  numReplies?: number;
  lastReplySnap?: firebase.firestore.DocumentSnapshot;//used for paginating replies. Update when upload another reply, or fetch another batch
  commentId?:string; //add commentId when load comments so can build replyis under correct supply subcollection
  isSelected?:boolean; //only for use in the template. DO NOT upload this property
  showReplyBox?:boolean;
  showReplyButton?:boolean;

}


@Injectable()
export class RouteDetails4Provider {
  gymName: string= null;
  userId:string = null;

  lastCommentSnap: firebase.firestore.DocumentSnapshot;

  comments: Comment[] = null;

  constructor(public platform: Platform, public events: Events,
    public urlProv:UrlProvider ,
    public userDetailsProv: UserDetailsProvider, public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    ) {
      this.platform.ready().then(()=>{

        this.afAuth.authState.subscribe( user => {
          if(user){
            this.userId = user.uid;
          }
        });
      });
    }

    //returns array of Comment objects. Builds Reply[] for each Comment and assigns it to property replies.
    //if no comments were available, return null
      async getCommentsAndReplies(gymId:string, mapsId:string, routeId:string): Promise<Comment[]>{
      try{
        this.comments = [];

        const commentsColPath = this.getCommentsCollectionPath(gymId,mapsId,routeId);
        const commentsSnap = await this.afs.firestore.collection(commentsColPath)
                         .orderBy('date','desc').limit(commentsBatchSize).get();

        for (let commentSnap of commentsSnap.docs){
          if(commentSnap.exists){
            let comment =  commentSnap.data() as Comment;
            comment.commentId = commentSnap.id;



            //TODO: consider using route.numComments to determine whether to run this code
              //get this comment's reply thread (if there are any)
              const repliesCollectionPath = this.getRepliesCollectionPath(gymId, mapsId, routeId, commentSnap.id);
              const repliesColRef = this.afs.firestore.collection(repliesCollectionPath);
              //order replies in normal order, so first reply appears right under the comment that it's replying to
              const repliesSnap = await repliesColRef
                              .orderBy('date').limit(repliesBatchSize).get();
                    let replies: Reply[] = [];
                    if(repliesSnap.docs.length > 0){
                      repliesSnap.docs.forEach(snap  => {
                        if(snap.exists){
                          let replyData = snap.data() as Reply ;
                          replyData['replyId']=snap.id;
                          replies.push(  replyData );
                        }
                      });
                    }


              comment['replies'] = replies;
              comment['lastReplySnap'] = repliesSnap.docs[repliesSnap.docs.length-1];

              // comment['lastReplySnap'] = repliesSnap.docs[0];

            //replies come in reverse order
            //console.log('route-details4, getCommentsAndReplies, pushing comment:');
            //console.log(comment);
            this.comments.push(comment);
          }
        }
        if(this.comments.length > 0){
          this.lastCommentSnap = commentsSnap.docs[commentsSnap.docs.length -1 ];
          return this.comments;
        }else{
          return null;
        }
      }catch(error){
        console.log('route-details provider, error:');
        console.lie(error);
        throw error;
      };
    }

    //use for pagination. call this function when user clicks 'more' under the comments thread
    //uses the last comment snapshot
    async getMoreComments(gymId:string, mapsId:string, routeId:string):Promise<Comment[]>{
      if(!this.lastCommentSnap){
        throw new Error('route-details4, getMoreComments: Error: no this.lastCommentSnap');
      }else{
        // console.log('route-details4, getMoreComments called');
        let commentBatch = [];

        const commentsColPath = this.getCommentsCollectionPath(gymId,mapsId,routeId);
        const commentsSnap = await this.afs.firestore.collection(commentsColPath)
                         .orderBy('date','desc').startAfter(this.lastCommentSnap).limit(commentsBatchSize).get();

        for (let commentSnap of commentsSnap.docs){
          if(commentSnap.exists){
            let comment =  commentSnap.data() as Comment;
            // console.log('got commentData as:');
            // console.log(comment);
            comment.commentId = commentSnap.id;

            if(comment.hasOwnProperty('numReplies') && !!comment.numReplies && comment.numReplies > 0){
              //get this comment's reply thread (if there are any)
              const repliesCollectionPath = this.getRepliesCollectionPath(gymId, mapsId, routeId, commentSnap.id);
              const repliesColRef = this.afs.firestore.collection(repliesCollectionPath);
              //order replies in normal order, so first reply appears right under the comment that it's replying to
              const repliesSnap = await repliesColRef
                              .orderBy('date').limit(repliesBatchSize).get();
                    let replies: Reply[] = [];
                    repliesSnap.docs.forEach(snap  => {
                      if(snap.exists){
                        let replyData = snap.data() as Reply;
                        replyData['replyId'] = snap.id;
                        replies.push(replyData );
                      }
                    });

              // console.log('route-detail4, getMoreComments got replies as: ');
              // console.log(replies);
              comment['replies'] = replies;
              comment['lastReplySnap'] = repliesSnap.docs[repliesSnap.docs.length-1];
            }
            //replies come in reverse order
            this.comments.push(comment);
          }
        }
        if(this.comments.length > 0){
          this.lastCommentSnap = commentsSnap.docs[commentsSnap.docs.length -1 ];
          return this.comments;
        }else{
          return null;
        }
      }
    }

    //if this comment has no lastReplySnap property, then throw error: can't get more replies if none were already fetched
    //the returned object contains Repies[] and firebase.firestore.DocumentSnapshot. The DocumentSnapshot is for supplying in
    //the next REPLIES querry as the startAfter property.
    async getMoreReplies(gymId:string, mapsId:string, routeId:string, comment:Comment):Promise<{newReplies:Reply[], lastReplySnap: firebase.firestore.DocumentSnapshot}>{
      try{
        // console.log('route-details4 prov, getMoreReplies got comment as:');
        // console.log(comment);
        if(!comment.hasOwnProperty('lastReplySnap')){
          console.log('BUG ALERT: route-details4, getMoreReplies, lastReplySnap MISSING!')
          throw new Error('route-details4 provider, getMoreReplies. no lastReplySnap error.');
        }else{
          // let replyBatch=[];
          const repliesCollectionPath = this.getRepliesCollectionPath(gymId, mapsId, routeId, comment.commentId);
          // console.log('repliesCollectionPath is:');
          // console.log(repliesCollectionPath);
            const repliesColRef = this.afs.firestore.collection(repliesCollectionPath);
            //order replies in normal order, so first reply appears right under the comment that it's replying to
            const repliesSnap = await repliesColRef
                            .orderBy('date').startAfter(comment.lastReplySnap).limit(repliesBatchSize).get();
                  let replies: Reply[] = [];
                  repliesSnap.docs.forEach( snap  => {
                        if(snap.exists){
                          let replyData = snap.data() as Reply ;
                          replyData['replyId']=snap.id;
                          replies.push(  replyData );
                        }
                      });
            return {newReplies: replies,
                    lastReplySnap: repliesSnap.docs[repliesSnap.docs.length-1]};
        }
      }
      catch(error){
      };
    }

    async uploadComment(gymId:string, mapsId:string, routeId:string, newComment: string):Promise<Comment>{
      try{
        const commentsColPath = this.getCommentsCollectionPath(gymId,mapsId,routeId);
        const commentsCol = this.afs.firestore.collection(commentsColPath);
        const commentDoc = commentsCol.doc();

        //We do not upload the parts of the client that we just added for the Client's use (such as document snapshots)
        const user = await this.userDetailsProv.getUserDetails();

        let fsComment: Comment = {
            comment: newComment,
            date: firebase.firestore.FieldValue.serverTimestamp() as any,
            authorId: this.userId,
            authorMug: user.photoURL,
            authorName: user.displayName
          }

        await commentDoc.set(fsComment);

        let clientComment = fsComment;
        //for the client, generate a Date object.
        clientComment['date'] = new Date();
        clientComment['commentId'] = commentDoc.id;

        return clientComment;

        }catch(error){
          console.log('error in route-details4 provider, uploadComment:');
          console.lie(error);
        };
    }

    //returns a reply object that was sucessfuly sent to the server and a DocumentSnapshot pointing to this reply
    //use the DocumentSnapshot as the new value of the parent comment's lastReplySnap property (for pagination)
    async uploadReply(gymId:string, mapsId:string, routeId:string, commentId:string, newReply: string) :
    Promise<{reply: Reply, snap:firebase.firestore.DocumentSnapshot}>{
      try{
        // console.log('route-details4 prov, uploadReply, got gymId, mapsId, routeId, commentId as:');
        // console.log(gymId,', ',mapsId,', ',routeId,', ',commentId);
        const repliesColPath = this.getRepliesCollectionPath(gymId,mapsId,routeId, commentId);
        const repliesCol = this.afs.firestore.collection(repliesColPath);
        const repliesDoc = repliesCol.doc();

        const user = await this.userDetailsProv.getUserDetails();

        let fsReply: Reply = {
          comment: newReply,
          authorId:this.userId,
          authorMug:user.photoURL,
          authorName: user.displayName,
          date: firebase.firestore.FieldValue.serverTimestamp()
        }

        // console.log('route-details provider, uploadReply, about to .set fsReply as:');
        // console.log(fsReply);
      let printableComment = this.comments[0];


        await repliesDoc.set(fsReply);
        const newReplySnap = await repliesDoc.get();

        await repliesDoc.set(fsReply);

        let clientReply = fsReply;

        clientReply.date = new Date();

        return {reply:clientReply, snap: newReplySnap};
      }
      catch(error){
        console.log('error in route-details4 provider, uploadReply');
        console.lie(error);
      };
    }

    //just delete the comment. Cloud functions will delete the reply thread.
    async deleteComment(gymId:string, mapsId:string, routeId:string, commentId: string){
      // console.log('route-details4 prov, deleteComment');
      try{
        const commentsColPath = this.getCommentsCollectionPath(gymId,mapsId,routeId);
        //need path to comment doc, not just collection
        const commentDoc = this.afs.firestore.doc(commentsColPath+`${commentId}`);
        await commentDoc.delete();
        return true;

        }catch(error){
          console.log('error in route-details4 provider, deleteComment:');
          console.lie(error);
        };
    }

    //use this method to delete a reply
    async deleteReply(gymId:string, mapsId:string, routeId:string, commentId:string, replyId: string){
      const repliesColPath = this.getRepliesCollectionPath(gymId,mapsId,routeId, commentId);
      // console.log('route-details4 provider, deleteReply and replyPath is: ');
      // console.log(repliesColPath + `${replyId}`);
      const replyDoc = this.afs.firestore.doc(repliesColPath + `${replyId}`);

      try{
        await replyDoc.delete();
        return true;
      }
      catch(error){
        throw error;
      };
    }

    getRepliesCollectionPath(gymId:string, mapsId:string, routeId:string, commentsId:string){
      if(gymId && mapsId && routeId && commentsId){
        return `/GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/COMMENTS/${commentsId}/REPLIES/`;
      }else{ throw new Error('route-details prov, cannot get replies collection path: Bad input')}
    }

    getCommentsCollectionPath(gymId:string,mapsId:string, routeId:string){
      if(gymId && mapsId && routeId){
        return `/GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/COMMENTS/`;
      }else{ throw new Error('route-details prov, cannot get comments collection path: Bad input')};
    }

}
