import { Component, Input, Output, EventEmitter, ElementRef,
  ViewChild,OnChanges, SimpleChanges } from '@angular/core';
import { Platform, Events, Gesture, AlertController} from 'ionic-angular';
import { SvgGestureComponent } from '../svg-gesture/svg-gesture';
import { getImageDimensions } from '../../providers/image/image';
import * as globalConstants from '../../providers/globalConstants/globalConstants';

@Component({
  selector: 'SvgRouteComponent',
  templateUrl: 'svg-route.html'
})
export class SvgRouteComponent extends SvgGestureComponent {
  @ViewChild('svgRouteContainer') topoContainer: ElementRef;

  @Input() routeSrc: any;
  @Input() containerDimensions: globalConstants.ContainerDimensions = null;
  @Output() gotHolds = new EventEmitter();
  @Input() footerHeight;
  // @Output() finishedLoading = new EventEmitter()

  //public routeSrc: string;    // temp default src for development
  public svgWidth: number;
  public svgHeight: number;
  public svgViewBox: string;

  // variables for holds
  @Input() holdArray: any[];                   // array of holds drawn on the route
  private colorState: State = State.Start;      // variable to set hold color, determined by ionic Event, published in route-creator.ts
  private in_existing: boolean                  // for selecting existing holds
  private holdSelectedBool: boolean = false;    // indicate whether the user has currently selected a hold
  private holdSelectedHold: any = null;         // the hold object which has been selected
  private holdSelectedIndex: number = null;     // index for referencing selected hold's location in array for deletion
  private oldPinchDistance: number = null;      // used for resizing holds
  private pinchTime: number = 0;                // timestamp for determining when the pinch is finished
  private isPinching: boolean = false;          // bool used to prevent panning at the end of pinch events.
  private scaleHoldsBy: number = 1;             // used to offset the difference between image dimensions (source of truth / what things are stored as)
                                                // and platform dimensiont
  private isLandscape: boolean = false;
  private doCheckDraw: boolean = false;         // indicates to wait for panZoomGroup to initialize in re-drawing holds if returning to component from
                                                // another component in the route editor.

  constructor(
    public platform: Platform,
    public events: Events,
    private alertCtrl: AlertController,
  ) {
    super(
      platform,
      events,
    );

    // initialize variables
    this.routeSrc = "./assets/img/wall.jpg";

    if(this.platformWidth > this.platformHeight){
      // if the platform dimensions were taken in landscape mode, swap to correct
      [this.platformWidth, this.platformHeight] = [this.platformHeight, this.platformWidth];
    }

  }

  ngAfterContentInit(){
    if(this.holdArray.length !== 0){
      this.doCheckDraw = true;
    }

    //Use the photo src dimensions to set the size of the svg
    // getImageDimensions(this.routeSrc).then((photoDim)=>{
    //   this.svgWidth = photoDim.width;
    //   this.svgHeight = photoDim.height;
    //   this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;
    //   this.scaleHoldsBy = (globalConstants.photoHeight / this.svgWidth);
    // }).catch((error)=>{
    //   throw error;
    // });

    let sfx = this.routeSrc.slice(this.routeSrc.length - 6, this.routeSrc.length -4);
    // if(sfx === globalConstants.landscapeSuffix){
    //   this.svgWidth = this.platformWidth;
    //   this.svgHeight = this.svgWidth / globalConstants.lAspectRatio;
    //   this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;
    //   this.scaleHoldsBy = (globalConstants.photoHeight / this.svgWidth);
    // }
    // else{
    //   this.svgWidth = this.platformWidth;
    //   this.svgHeight = this.svgWidth / globalConstants.pAspectRatio;
    //   this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;
    //   this.scaleHoldsBy = (globalConstants.photoWidth / this.svgWidth);
    // }


  }


  ngAfterViewInit(){
    //Use the photo src dimensions to set the size of the svg
    getImageDimensions(this.routeSrc).then((photoDim)=>{
      this.svgWidth = photoDim.width; //this.platform.width();
      this.svgHeight = photoDim.height;
      console.log(`******** svg-route (component), ngAfterViewInit{} svgWidth/Height:${this.svgWidth}, ${this.svgHeight}`);
      this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;

      this.scaleHoldsBy = (photoDim.height / this.svgWidth);

      // const svgDimensions = {width: this.svgWidth, height:this.svgHeight};

      const svgDimensions = {width: this.svgWidth, height:this.svgHeight};
      const topoContDivBox: {width:number, height:number } = this.topoContainer.nativeElement.getBoundingClientRect();
      // this.svgViewBox = "0 0 " + topoContDivBox.width + " " + topoContDivBox.height;
      //
      this.containerDimensions= {
        x: topoContDivBox.width,
        y: topoContDivBox.height
      };
      // this.containerDimensions= {
      //   x: this.platform.width(),
      //   y: this.platform.height() - this.footerHeight
      // };

      //
      // this.containerDimensions= {
      //   x: this.platform.width(),
      //   y: this.platform.height()
      // };

      //
      // this.initAsync(this.containerDimensions, svgDimensions).then(( result )=>{
      //   this.drawHoldArray();
      // }).catch((error)=>{
      //   throw error;
      // });

      if(this.svgWidth){
        const svgDimensions = {width: this.svgWidth, height:this.svgHeight};
        // const svgDimensions = {width: this.containerDimensions.x,
        //   height:this.containerDimensions.y};

        this.init(this.containerDimensions, svgDimensions);
        this.drawHoldArray();
      }else{
        throw new Error("svg-route component, ngAfterViewInit{}: NEED svgWidth. race condition with content init?");
      }

    }).catch((error)=>{
      throw error;
    });
    //


  }

  ngDoCheck(){
    if(this.doCheckDraw && this.panZoomGroup !== null){
      for (let i = 0; i < this.holdArray.length; i++){
        this.holdArray[i].pointCor.x = this.holdArray[i].pointCor.x / this.scaleHoldsBy;
        this.holdArray[i].pointCor.y = this.holdArray[i].pointCor.y / this.scaleHoldsBy;
        this.holdArray[i].Radius = this.holdArray[i].Radius / this.scaleHoldsBy;
      }

      this.drawHoldArray();
      this.doCheckDraw = false;
    }
  }

  setChildClassHandlers():Promise<any>{

    // Restrict zoom
    // this.svgPanZoom.setMinZoom(1);

    return new Promise((res)=>{
      this.gesture.on('tap',(ev)=>{
          this.drawHoldArray();
          let point = this.convertCoordinatesToSvg(ev.center.x, ev.center.y);
          // if nothing is selected, determine whether to place or select hold
          if(this.holdSelectedBool === false){

            //if the holdArray is empty, new hold!
            if(this.holdArray.length === 0){

              let newHold = new hold(point, 15, this.colorState);

              // add the new hold to the arry of holds
              this.holdArray.push(newHold);
              this.drawHoldArray();
            }

            // if the hold array is not empty, then:
            else{
              // (re)initialize var for determing if hold exists
              this.in_existing = false;
              // for each hold in the array,
              for(let i = 0; i < this.holdArray.length; i++){
                // determine if the touch center is within radius
                if(getDistance(point.x, this.holdArray[i].pointCor.x, point.y, this.holdArray[i].pointCor.y) < this.holdArray[i].Radius){

                  //if so, select the hold
                  this.in_existing = true;
                  this.holdSelectedBool = true;
                  //TODO: determine if the two bools above can be consolidated
                  this.holdSelectedHold = this.holdArray[i];
                  this.holdSelectedHold.Selected = true;
                  this.holdSelectedIndex = i;
                  this.drawHoldArray();

                  //disable Zooming
                  this.gesture.off('pinch');
                  this.gesture.on('pinch', (ev)=>{
                    console.log("ev:", ev);
                    // boolean used for preventing tap event from firing when pinch is ending.
                    this.isPinching = true;

                    // determine if it is a new pinch and distance needs to be reset
                    if((ev.timeStamp - this.pinchTime) > 100) {
                      this.oldPinchDistance = null;   // if so reset distance
                      this.pinchTime = ev.timeStamp;  // set timeStamp

                    }

                    // if it is a new pinch:
                    if(this.oldPinchDistance === null){

                      let x1 = ev.pointers[0].clientX;
                      let y1 = ev.pointers[0].clientY;
                      let x2 = ev.pointers[1].clientX;
                      let y2 = ev.pointers[1].clientY;
                      this.oldPinchDistance = (getDistance(x1, x2, y1, y2));  // set distance to the distance between two fingers on first pinch
                      this.pinchTime = ev.timeStamp;                          // set timeStamp
                    }

                    else{
                      let oldRad = this.holdSelectedHold.Radius;
                      let x1 = ev.pointers[0].clientX;
                      let y1 = ev.pointers[0].clientY;
                      let x2 = ev.pointers[1].clientX;
                      let y2 = ev.pointers[1].clientY;
                      let newPinchDistance = (getDistance(x1, x2, y1, y2));
                      let newRad = ((oldRad / this.oldPinchDistance) * newPinchDistance); // set the new radius to be equal to the change in distance betwen pinchmovements

                      if(newRad < 100){                                       // restrict size to remain on the page
                        this.holdSelectedHold.Radius = newRad;
                      }

                      this.oldPinchDistance = newPinchDistance;
                      this.pinchTime = ev.timeStamp;

                    }

                    this.drawHoldArray();

                  });

                  this.gesture.off('panstart');
                  this.gesture.off('panmove');

                  this.gesture.on('pan', (ev)=>{
                    console.log("gesture:", this.gesture);
                    console.log("svgPanZoom:", this.svgPanZoom);
                    console.log("getPan:", this.svgPanZoom.getPan());
                    console.log("getZoom:", this.svgPanZoom.getZoom());
                    console.log("getSizes:", this.svgPanZoom.getSizes());
                    console.log("ev Center:", ev.center.x, ", ", ev.center.y);
                    // make sure that the panning is not part of a pinch event
                    if((ev.timeStamp - this.pinchTime) > 100) {
                      this.isPinching = false;
                    }

                    if(this.isPinching === false){

                      this.holdSelectedHold.pointCor = this.convertCoordinatesToSvg(ev.center.x, ev.center.y);
                      this.drawHoldArray();
                    }

                  });
                }
              }

              // if the event is not withing an existing hold, create a new hold
              // add it to the array, and draw.
              if(this.in_existing === false){
                let newHold = new hold(point, 15, this.colorState);
                this.holdArray.push(newHold);
                this.drawHoldArray();
              }

            }


        }

        // if something is selected, determine if it should be un-selected
        else {
          if(getDistance(point.x, this.holdSelectedHold.pointCor.x, point.y, this.holdSelectedHold.pointCor.y) > this.holdSelectedHold.Radius){
            // then reset indicating vars
            this.holdSelectedBool = false;
            this.holdSelectedHold.Selected = false;
            this.holdSelectedHold = null;
            this.holdSelectedIndex = null;

            //re-enable Zooming
            this.gesture.off('pinch');
            this.setDefaultPinch(this.gesture);
            this.gesture.off('pan');
            this.gesture.off('panstart');
            this.gesture.off('panmove');
            this.setDefaultPan(this.gesture);

            this.drawHoldArray();
          }
        }


      });
      res();

    });
  }

  tbChangeState = () => {
    if(this.colorState === State.Start){
      this.colorState = State.Middle;
      if(this.holdSelectedBool === true){
        this.holdSelectedHold.Color = this.colorState;
        this.drawHoldArray();
      };
    }

    else if(this.colorState === State.Middle){
      this.colorState = State.Finish;
      if(this.holdSelectedBool === true){
        this.holdSelectedHold.Color = this.colorState;
        this.drawHoldArray();
      };
    }

    else{
      this.colorState = State.Start;
      if(this.holdSelectedBool === true){
        this.holdSelectedHold.Color = this.colorState;
        this.drawHoldArray();
      }
    }
  }

  tbUndo = () => {
    if(this.holdArray.length < 1){
      return;
    }
    else{
      this.holdArray.pop();
      this.drawHoldArray();
    }
  }

  tbDeleteHold = () => {
    if(this.holdSelectedIndex === null){
      return;
    }
    else{
      this.holdArray.splice(this.holdSelectedIndex, 1);
      this.holdSelectedBool = false;
      this.holdSelectedHold = null;
      this.holdSelectedIndex = null;
      this.drawHoldArray();
    }
  }

  tbDoneSetting = () => {
    //go through and make sure all holds are unselected
    if (this.holdSelectedBool === true){
      this.holdSelectedBool = false;
      this.holdSelectedHold.Selected = false;
      this.holdSelectedHold = null;
      this.holdSelectedIndex = null;
    }

    //loop through the array, ensuring that at least one start and finish hold were placed
    let startH, finishH = false;

    for (let i = 0; i < this.holdArray.length; i++){
      if (this.holdArray[i].Color === State.Start) startH = true;
      if (this.holdArray[i].Color === State.Finish) finishH = true;
    }

    if (startH && finishH){
      //loop through and scale all holds, then emit them.
      for (let i = 0; i < this.holdArray.length; i++){
        this.holdArray[i].pointCor.x = this.holdArray[i].pointCor.x * this.scaleHoldsBy;
        this.holdArray[i].pointCor.y = this.holdArray[i].pointCor.y * this.scaleHoldsBy;
        this.holdArray[i].Radius = this.holdArray[i].Radius * this.scaleHoldsBy;
      }

      this.gotHolds.emit(this.holdArray);
    }

    else if(!startH){
      let alert = this.alertCtrl.create({
        title: 'No start hold',
        subTitle: 'Please place at least one green hold to indicate where to start the route.',
        buttons: ['Dismiss']
      });

      alert.present();
    }

    else if(!finishH){
      let alert = this.alertCtrl.create({
        title: 'No finish hold',
        subTitle: 'Please place at least one red hold to indicate where the route ends.',
        buttons: ['Dismiss']
      });

      alert.present();
    }


  }

  clearSVG = () => {
    // TODO rewrite so that it clears the svg by using class, not ID (refer to clearCircles function in svg-map.ts)
    //NOTE: access the svg with this.panZoomGroup, rather tan the 'document' object
    let oldHolds = document.getElementById("hold");
    if(oldHolds != undefined){
      oldHolds.parentNode.removeChild(oldHolds);
      this.clearSVG();
    }
    else{
      return;
    }
  }

  drawHoldArray = () => {
    this.clearSVG();

    for (let i = 0; i < this.holdArray.length; i++){
      let color = "green";
      if(this.holdArray[i].Color === State.Middle){
        color = "yellow";
      }
      else if(this.holdArray[i].Color === State.Finish){
        color = "red";
      }

      let cx = this.holdArray[i].pointCor.x;
      let cy = this.holdArray[i].pointCor.y;
      let cr = this.holdArray[i].Radius;

      let svgNamespace = "http://www.w3.org/2000/svg";
      let circle = document.createElementNS(svgNamespace,'circle');

      circle.setAttributeNS(null, 'cx', cx.toString());
      circle.setAttributeNS(null, 'cy', cy.toString());
      circle.setAttributeNS(null, 'r', cr.toString());
      circle.setAttributeNS(null, 'style', 'fill: none; stroke: ' + color + '; stroke-width: 3px;');
      circle.setAttributeNS(null, 'class', 'CIRCLE');
      circle.setAttributeNS(null, 'id', 'hold');

       if(this.panZoomGroup != null){
         this.panZoomGroup.appendChild(circle);
       }
       else{
       }

      if(this.holdArray[i].Selected === true){
        let hr = cr + 2;
        let halo = document.createElementNS(svgNamespace, 'circle');

        halo.setAttributeNS(null, 'cx', cx.toString());
        halo.setAttributeNS(null, 'cy', cy.toString());
        halo.setAttributeNS(null, 'r', hr.toString());
        halo.setAttributeNS(null, 'style', 'fill: none; stroke: white; stroke-width: 3px;');
        halo.setAttributeNS(null, 'class', 'CIRCLE');
        // TODO when you change the clear function, fix this shit
        halo.setAttributeNS(null, 'id', 'hold');

        if(this.panZoomGroup != null){
          this.panZoomGroup.appendChild(halo);
        }
      }
    }
  }

}

function getDistance(x1, x2, y1, y2): number{
  let a = (x1 - x2);
  let b = (y1 - y2);
  return Math.sqrt ( a*a + b*b );
};

enum State{
  Start = 1,
  Middle,
  Finish
}

class hold {
  public pointCor: any;
  public Radius: number;
  public Color: State;
  public Selected: boolean;

  constructor(pc:any, r:number, c:State){
    this.pointCor = pc;
    this.Radius = r;
    this.Color = c;
    this.Selected = false;

  }
/*
NOTE: I think this method is now obsolete
  getHoldObject(){
    //for storing in firestore (can't upload a class instance)
    //omitting selected because doesn't need to be stored on db
    return {
      p: this.pointCor,
      r:this.Radius,
      c:this.Color,
    };
  }
  */
}
