import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
//import { AngularFireDatabase } from 'angularfire2/database';

import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';

import { UrlProvider } from '../../providers/url/url';

import { Facebook } from '@ionic-native/facebook';

@Injectable()
export class AuthProvider {


  constructor(public afAuth: AngularFireAuth, public afs: AngularFirestore,
  public url:UrlProvider,
  public facebook: Facebook ) {
    // console.log('Hello AuthProvider Provider');
  }

  getUser(): firebase.User{
    console.log('authProvider, getUser: currentUser is:');
    console.log(this.afAuth.auth.currentUser);
    return this.afAuth.auth.currentUser;
  }

  loginUser(newEmail:string, newPassword:string):Promise<any>{
    return this.afAuth.auth.signInWithEmailAndPassword(newEmail, newPassword);
  }

  anonymousLogin():Promise<any> {
    return this.afAuth.auth.signInAnonymously();
    }

  async signInFacebookUser(facebookCredential:firebase.auth.AuthCredential){
    //not exists, create a USERS record for them
    try{
      const firebaseCredential = await this.afAuth.auth.signInWithCredential(facebookCredential);
      //check if user already exists
      const userDocRef = this.afs.firestore.doc(`USERS/${firebaseCredential.uid}`);
      const userSnap = await userDocRef.get();

      if(userSnap.exists){
        return firebaseCredential;
      }else{//need to make a USERS doc for this user (who is probably signing in for first time)
        let userDoc = {'displayName':firebaseCredential.displayName};
        if(firebaseCredential.hasOwnProperty('photoURL') && firebaseCredential.photoURL){
          userDoc['photoURL'] = firebaseCredential.photoURL;
        }
        await userDocRef.set(userDoc);
        return firebaseCredential;
      }
    }
    catch(error){
        //handle error
    };


  }

  //TODO: make sure another user can not register using the same email or some such  (prob handled by firebase)
  registerUser(email:string, password: string, displayName: string):Promise<any>{
    //registers user with email and password, then sets their display name (called name)
    return new Promise((res,rej)=>{
      this.afAuth.auth.createUserWithEmailAndPassword(email,password).then((newUser)=>{
        //set user display name
        let userDocRef = this.afs.collection('USERS').doc(newUser.user.uid);
        userDocRef.set({'displayName':displayName});
          res(newUser);
      }).catch(error=>{
        rej(error);
      });
    });
  }

  //TODO: link account after they invest some effort, say by building a list
  //...or setting a rout (heads up, we switched to firestore so afDatabase needs to be changed)
  // linkAccount(email:string, password:string):Promise<any> {
  //   const credential = firebase.auth.EmailAuthProvider.credential(email, password);
  //   return this.afAuth.auth.currentUser.linkWithCredential(credential).then( user => {
  //     this.afDatabase.object(`/userProfile/${user.uid}/email`).update(email);
  //   }, error => {
  //     console.log("There was an error linking the account", error);
  //   });
  // }

  resetPassword(email: string):Promise<any> {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }

  async logoutUser():Promise<void> {
    await this.facebook.logout();
    return this.afAuth.auth.signOut();
  }

}
