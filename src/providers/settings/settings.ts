import { Injectable } from '@angular/core';
import { Platform, Events} from 'ionic-angular';

import{ ConnectionProvider } from '../../providers/connection/connection';
import { UrlProvider } from '../../providers/url/url';
import { GymsProvider, GymData } from '../../providers/gyms/gyms';

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';


import { AngularFireAuth } from '@angular/fire/auth';

import * as firebase from 'firebase/app';//used for definig types

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;


export interface NotificationsSettings{
    onComment:boolean, //notify this user if a route they posted gets a new comment
    onRouteDeleted: boolean, //notify this user if a route they posted is deleted
    onNewAttempt:boolean, //notify this user if someone attempts their route
    onNewSend:boolean, //notify this user every time a person sends their route (give option to send them knucks)
    onNewRouteByFollowed: boolean//notify this user when someone they follow posts a new route
}

export interface SettingsObject{
  gymId:string;
  mapId?:string;
  currentFloor?: number;
  notificationSettings?:NotificationsSettings
  visibleMode?: boolean;
}

@Injectable()
/* HOW TO USE THIS PROVIDER:
call settingsProv.ready.then((result) =>{
  if(result && result.gymId){
    settings initialized properly, so do whatever you want with them
    }
  if( result && !result.gymId){
    redirect user to the settings page and tell them to puick a gym
  }
  if( !result ){
    SOMETHINGS GONE WRONG,this should only be true in the .catch
  }

}).catch( (error)={
  ..something is broken AF. this code is only reached if firestore throws error (probably?)
 });
*/

export class SettingsProvider{
  public settings: SettingsObject=null;
  public userId: string = null;
  public isGuest: boolean = false;
  public gotUserId: boolean = false;

  public gymData:GymData[];

  settingsDocRef:any = null;
  onAuthCallback: ()=>any;

  ready:Promise<any>;

  constructor(
    public afAuth: AngularFireAuth,
    public connection: ConnectionProvider,
    public url:UrlProvider,
    public platform: Platform,
    public events:Events,
    public afs: AngularFirestore,
    public afDatabase: AngularFireDatabase,
    public gymsProv: GymsProvider) {

    this.platform.ready().then(()=>{
      const authListener = afAuth.authState.subscribe((user)=>{
        if (user){
          this.userId = user.uid;
          authListener.unsubscribe();
        }
      })
    });
  }

  async init():Promise<SettingsObject>{
    // console.log('settings (provider), init: execution started');
    try{
      const settingsDoc = this.getSettingsDoc();//could return null, if not a registered user
      if(settingsDoc){


        // console.log('settings (provider), init: got settingsDoc, inside if');
        const settingsSnap = await settingsDoc.ref.get();
        if (settingsSnap.exists){

          // console.log('settings (provider), init: settingsSnap.exists!');
          let settings = settingsSnap.data() as SettingsObject;
          // console.log('settings (provider), init: got settings as:');
          // console.log(settings);
          if(! settings.hasOwnProperty('gymId') || !settings.gymId){
            const gym = await this.gymsProv.getCurrentGym().catch(); //still want to resolve, even if can't get gym

            if(gym && gym.hasOwnProperty('gymId') && gym.gymId){
              settings['gymId'] = gym.gymId;
            }
            //resolve settings, even if they don't have a gymId (probably should deal with this later)
            //there was a comment saying to do this, but not sure why now
            this.settings = settings;
            return this.settings;
          }else{
            this.settings=settings;
            return this.settings;
          }
        }
      }//if these lines execute, it is because we could not build and return settings
      const gym = await this.gymsProv.getCurrentGym().catch();//still want to resolve if can't get gym
      if(gym && gym.hasOwnProperty('gymId') && gym.gymId){
        return await this.setDefaults(gym.gymId);
      }else{
        return await this.setDefaults();
      }
      }catch(error){
        console.log('settings.ts (provider), init, error:');
        console.lie(error);
      };
  }
  //
  // init():Promise<SettingsObject>{
  //   console.log("settings init called");
  //   //call this method before setting root page from app.component.ts
  //   console.log("Running settings.init");
  //   return new Promise((res,rej)=>{
  //     //try to get settings from firestore
  //     let ref = this.getSettingsDocRef();
  //     // console.log("Got ref as:");
  //     // console.log(ref);
  //     if(ref){
  //       this.settingsDocRef = ref;
  //       this.settingsDocRef.get().then(( settings )=>{
  //         if(settings.exists){
  //           //No matter what, we'll resolve these settings. Try to verify or get gymId, first
  //           // console.log("In settings.init, got settings.data() as:");
  //           // console.log(settings.data());
  //
  //           this.settings = settings.data();
  //           // if(!this.settings || !this.settings.hasOwnProperty('gymId') || !this.settings.gymId){
  //           //   this.settings = null;
  //           //
  //           // }
  //
  //           if(this.settings && this.settings.hasOwnProperty('gymId') && this.settings.hasOwnProperty('currentFloor')){
  //
  //             res(this.settings);
  //
  //           }else{//gymId did not exist or was null
  //             //try to get gymId from storage
  //             this.gymsProv.getCurrentGym().then((currentGym)=>{
  //             // console.log("got currentGym from gymProvider as:");
  //             // console.log(currentGym);
  //
  //               if(currentGym && currentGym.gymId){
  //                 this.settings['gymId']=currentGym.gymId;
  //               }
  //
  //               res(this.settings);
  //
  //             }).catch((error)=>{//gmProv could not get current gym. resolve anyway
  //               res(this.settings);
  //             });
  //           }
  //         }else{//settings DNE in firestore
  //           // console.log("Settings from firestore DIN NOT EXIST!!!");
  //           this.gymsProv.getCurrentGym().then(( currentGym )=>{
  //             if(currentGym && currentGym.gymId){
  //               res(this.setDefaults(currentGym.gymId));
  //             }else{
  //               res(this.setDefaults());
  //             }
  //           }).catch((error)=>{
  //             res(this.setDefaults());
  //           });
  //         }
  //       }).catch(error=>{
  //         console.log('Error getting settingsDocRef in settings provider:');
  //         console.log(error);
  //       });
  //     }else{//could not get ref, so user is not logged in
  //       this.setDefaults().then(( settings )=>{
  //         res(settings);
  //       }).catch((error)=>{
  //         rej(error);
  //       });
  //
  //     }
  //   });
  //
  // }
  //
  getSettings():Promise<SettingsObject>{
    return new Promise((res,rej)=>{
      if(this.settings){
        res(this.settings)
      }else{
        res(this.init());
      }

    });
  }

  setSettings(gymName=null):Promise<SettingsObject>{
    //CAUTION: might return settings with settings.gymId == null. Handle this in page
    return new Promise((res,rej)=>{
      if (this.settings && this.settings.hasOwnProperty('gymId') && this.settings.gymId){
        res(this.settings);
      }else{
        //'gymId property was null or undef'
        if(this.settings && this.settings.hasOwnProperty('gymId')){
          this.getGymId().then(( gymId )=>{
            this.settings['gymId'] = gymId;
            res(this.settings);
          }).catch((error)=>{
            // console.log("Returning settings even though gym id is null or undef and settings are:");
            // console.log(this.settings);

            res(this.settings);
          });
        }
      }
    });
  }

  setDefaultFloor(floorNum:number = 0):number{
    if(this.settings){
      this.settings.currentFloor = floorNum;
      return this.settings.currentFloor;
    }else{
      throw new Error('Could not set default floor');
    }
  }

  getSettingsFromFirestore(firestoreDocRef):Promise<any>{
    //if settings exist in firestore, resolve with them
    //if !settings exists in firestore, set defaults and resolve with those
    return new Promise((res,rej)=>{
      firestoreDocRef.get().then(( settings )=>{
        if(settings.exists){
          res(settings.data());
        }else{
          this.setDefaults().then(( settings )=>{
            res(settings);
          }).catch((error)=>{
            rej(error);
          });
        }
      }).catch((error)=>{
        // console.log("ERROR in getSettingsFromFirestore. querried with firestoreRef: ");
        // console.log(firestoreDocRef);
        // console.log("and error was:");
        // console.log(error);
        rej(error);
      });

    });
  }

  setDefaults(gymName=null):Promise<SettingsObject>{
    //sets defaults to firestore.
    //TODO: if gymName is null, direct them to enable wifi/data and pick a gym

    return new Promise((res,rej)=>{
      // console.log("Attempting to set defaults and gymName is");
      // console.log(gymName);

      let notificationsSettings = {
        onComment:true, //notify this user if a route they posted gets a new comment
        onRouteDeleted: true, //notify this user if a route they posted is deleted
        onNewAttempt:true, //notify this user if someone attempts their route
        onNewSend:true, //notify this user every time a person sends their route (give option to send them knucks)
        onNewRouteByFollowed: true //notify this user when someone they follow posts a new route
      }

      let settings = {
        //TODO: set default gym to null, then have user use picker to set initial gymName

        gymId: gymName,
        currentFloor:0,
        notificationSettings:notificationsSettings,
        visibleMode: true,

      }
      this.settings = settings;
      if(this.afAuth.auth.currentUser && this.settings.hasOwnProperty('gymId')
        && this.settings.gymId){//they're logged in
        // console.log("currentUser!!! saving settings");
        // console.log(this.settings);
        // console.log('settingsProvider calling saveSettings and settings is:');
        // console.log(this.settings);
        this.saveSettings(this.settings).then((ref)=>{//throws error if !settings.gymId
          res(this.settings);
        }).catch((error)=>{
          //If they ARE logged in and we can NOT save their settings it might be their first time
          //so they're picking their gymId:
          if(!this.settings.gymId){
            res(this.settings);
          }else{
            //something is broken
            // console.log("setDefaults could not resolve with settings");
            rej(error);
          }
          rej(error);
        });
      }else{//if they're not logged in, do not save their settings
        res(this.settings);
      }

    });
  }

  getDefaultSettings():SettingsObject{
    if(this.settings){
      return this.settings;
    }else{
      this.setDefaults().then(()=>{
        return this.settings;
      }).catch((error)=>{
        // console.log(error);
        return null;
      })
    }
  }

  setGymOptions():Promise<GymData[]>{
    return new Promise((res,rej)=>{
      this.gymsProv.setGymOptions().then(( gymData )=>{
        this.gymData = gymData;
        res(this.gymData);
      }).catch((error)=>{
        rej(error);
      });
    });
  }

  async updateGymId(gymId):Promise<string>{
    try{
      await this.gymsProv.setAndStoreCurrentGym(gymId);
      this.settings.gymId = gymId;
      return this.settings.gymId;
    }
    catch(error){
      // console.log(error);
      throw error;
    };
  }

  async updateFloorOnFirestore(floorNum:number):Promise<any>{
    const docRef = this.getSettingsDoc();
    await docRef.update({currentFloor:floorNum});
    return floorNum;
  }

  updateCurrentFloor(floor:number){
    this.settings.currentFloor = floor;
    return null;
  }

  getGymId():Promise<string>{
    return new Promise((res,rej)=>{
      // console.log("getGymId called.");
      this.gymsProv.getCurrentGym().then(( gymData )=>{
        // console.log("Got gymData from gymsProv as:");
        // console.log(gymData);
        if(gymData.gymId){
          // console.log("resolving gym id as:", gymData.gymId);
          res(gymData.gymId);
        }else{
          rej(new Error('Could not get gymId in settings.getGymId'));
        }

      }).catch((error)=>{
        rej(error);
      });
    });
  }

  getSettingsDoc():AngularFirestoreDocument{
    if (this.afAuth.auth.currentUser){
      const doc = this.afs.doc(`USERS/${this.afAuth.auth.currentUser.uid}/SETTINGS/settings`);
      return doc;
    }else{
      return null;
    }
  }

  async saveSettings(settings:SettingsObject):Promise<any>{
    if(!this.afAuth.auth.currentUser.uid ){
      // console.log('saveSettings: 1');
      //TODO: trigger offline mode for firestore and only save the settings on the offline version of firestore
      return true;
    }
    if(!settings){
      // console.log('saveSettings: 2');
      throw new Error('can not save settings with value: '+ this.settings);
    }

    if(!settings.gymId){
      // console.log('saveSettings: 3');
      throw new Error('can not save settings since gymId is: '+ this.settings.gymId);
    }

    this.settings = settings;

    const settingsDoc= this.getSettingsDoc();
    const settingsRef = settingsDoc.ref;
    if(this.connection.isConnected()){
      try{
        await settingsRef.set(settings);
        return settingsRef;
      }
      catch(error){
        this.settings = settings;
        //TODO: trigger offline firestore mode and save a copy of settings to offline firestore.
        //Do not wait for offline firestore to resolve.
        settingsRef.set(settings);
        return true;
      };
    }else{//no internet connection
      //await this.afs.firestore.disableNetwork();
      //NOTE: ref.set method will NOT resolve when offline, so DO NOT await result!
      settingsRef.set(settings);

      //await this.afs.firestore.enableNetwork();
      return true;
    }
  }

  saveSettingsLegacy(settings:SettingsObject):Promise<any>{
    //Does not save settings if:
    // the user is not logged in, this.settings is null or undef, this.settings.gymId is null or undef
    //resolves with the ref used to save the settings

    return new Promise((res,rej)=>{
      // console.log('saveSettings: 0');
      if(!this.afAuth.auth.currentUser.uid ){
        // console.log('saveSettings: 1');
        rej(new Error('can not save settings: user not logged in'));
      }
      if(!settings){

        // console.log('saveSettings: 2');
        rej(new Error('can not save settings with value: '+ this.settings));

      }
      if(!settings.gymId){

        // console.log('saveSettings: 3');
        rej(new Error('can not save settings since gymId is: '+ this.settings.gymId));
      }

      // console.log('saveSettings: 4');
      const settingsDoc = this.getSettingsDoc();
      const  settingsRef = settingsDoc.ref;
      // console.log("saveSettings: 4.5 settingsRef:");
      // console.log(settingsRef);



      // console.log('saveSettings: 5');
      settingsRef.set(settings).then(( )=>{

        // console.log('saveSettings: 6');
        this.gymsProv.setAndStoreCurrentGym(settings.gymId);
        // console.log("SAVED SETTINGS AS:");
        // console.log(settings);

        // console.log('saveSettings: 7');
        res(settingsRef);
      }).catch((error)=>{

        console.log('saveSettings: 8');
        rej(error);
      });;
    });
  }

}
