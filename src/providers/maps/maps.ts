import { Injectable } from '@angular/core';

import * as firebase from 'firebase/app';//used for definig types

import { AngularFirestore } from '@angular/fire/firestore';

import { ImageProvider } from '../../providers/image/image';
import { FileUtilitiesProvider } from '../../providers/file-utilities/file-utilities';

import { Entry } from '@ionic-native/file';


/*
Maps interface provides metadata describing "mapContents"
mapContents is read as text and then inserted into a template containing this: <svg #svgElement class='empty-map'> </svg>
(class 'map-component' is also used. see components/svg-map.html for detailed description of usage)
This dom manipulation is done by svg-map.ts (the map component)
*/

export interface MapDimensions{
  height: number,
  width: number
}

export interface Maps {
  gymId:string;
  mapId:string;
  current?:boolean; //if current, should be missing property 'created'
  created?:Date;
  deprecated?:Date;
  urlByFloor?:{//TODO phase out urlByFloor
    [key:number]:string;//each entire floor is depicted in an svg stored at this url
  }
  mapByFloor: {
    [key: number]: {//<--the floor that this map represents TODO: decide on convention. ex: 0->ground floor, -1: basement?
      mapContents?:string,
      dimensions: MapDimensions  //used by the svg-map component for proper scaling TODO: template binding in component for using this
    }
  }
  colors?:{
    colorNamesToColorCodes:{
      [key:string]: string;
    }
    orderedColors:string[];
  }
}


@Injectable()
export class MapsProvider {
  maps:Maps=null;
  constructor(public afs:AngularFirestore, public imageProv: ImageProvider,
  public fileUtilsProv: FileUtilitiesProvider)  {
  }

  /*
  TODO: complete query for map accroding to date functionality
  */
  async getMaps(gymId:string = null, forceReload:boolean = false):Promise<Maps>{
    //if they don't just need the current map. For example if trying to display
    //a route's position that was uploaded before the most recent map was created...

    //if no gymId is requested and this.maps is already definded, return this.maps
    //used by filters and route editor to access maps.colors
    if (this.maps && !gymId && !forceReload){
      return this.maps;
    }
    if(this.maps && this.maps.hasOwnProperty('gymId') && this.maps.gymId === gymId && !forceReload){
      //TODO: this approach will almost never update client maps, even if a new map has 'current==true'
      //FIX this! Maybe forget this.maps when the app is paused or shutdown.
      return this.maps;
    }else{//get map with property 'current' == true
      try{
        if(!gymId){
          throw new Error('Maps provider: maps not initialized and no gymId provided to fetch maps.');
        }

        const mapCollection = this.afs.firestore.collection(`GYMS/${gymId}/MAPS`);
        const maps2Snap = await mapCollection.where('test', '==', true).get();
        // console.log('*** got maps2Snap ***' );
        //there should only ever be one maps doc with property 'current':true;
        const maps = maps2Snap.docs[0].data() as Maps;
        this.maps = maps;
        // console.log("*********** GOT MAPS FROM FIRESTORE AS **********");
        // console.log(maps);
        // console.log("***...and got text as:");
        // console.log(maps.mapByFloor[1].mapContents);
        return maps;

      }catch(error){
        throw(error);
      }
    }
  }


  //TODO finish this!
  async getMapsForDate(date:Date):Promise<Maps>{
    try{
      throw new Error('getMapsForDate in maps.ts (a provider): Method not implemented ERROR');
    }
    catch(error){
      throw error;
    };
  }

/* NO LONGER USING THIS APPROACH (maps stored in file system... keeping code for reference because might need to switch back to this system)
  if (maps are loaded in local directory) return maps object pointing to the svg files for the maps
  else try to load maps into file system from firebase/firestore then return maps object pointing to maps
  finally, if can't get maps loaded, rej with error
  TODO, IMPORTANT: if connected to internet, verify the 'current' maps directory
  contains the most recent version of the maps (on the db.) If not, redownload and
  move old version to appropriate file system location (or maybe just delete it?)
*/
// async getMaps2(gymId:string, routeDate:Date = null){
//
//   try{
//     let mapFileEntries = [];
//     const basePath = await this.fileUtilsProv.getBasePath('cache');
//     //NOTE: for testing, uncomment the following line to force redownload of maps
//     // await this.fileUtilsProv.file.removeRecursively(basePath +`maps/${gymId}`, 'current');
//     //build 'current' maps directory under correct gymId if does not exist
//     const fullPath = await this.fileUtilsProv.checkOrBuildPath(`maps/${gymId}/current/`,'cache' );
//     // console.log(`getMaps2, got fullPath as: ${ fullPath }. SHOULD MATCH in mapObject `);
//     mapFileEntries = await this.fileUtilsProv.file.listDir(basePath +`maps/${gymId}`, 'current');
//     // console.log(`Map file entries: `);
//
//     // console.log(mapFileEntries);
//     //if directory is empty, load the maps.
//     if(mapFileEntries.length < 1){
//     console.log(`***&&&*** Calling loadMaps2 (...no maps in maps directory)`);
//       mapFileEntries = await this.loadMaps2(gymId, basePath);
//       // console.log(mapFileEntries);
//     }
//
//     if( mapFileEntries.length > 0){//maps are loaded
//
//       //for debugging...
//       //await this.displayMapFileEntriesAndText(mapFileEntries, gymId, basePath);
//       let mapsText = [];
//       let mapsTextPromises = [];
//
//       console.log(`About to loop through mapFileEntries`);
//       for (let i = 0; i<mapFileEntries.length; i++){
//         let entry = mapFileEntries[i];
//         console.log(`Entry number ${ i } :`);
//         console.log(entry);
//         console.log('...about to await text the next line should be a slice of map text:');
//         let text = await this.fileUtilsProv.file.readAsText(basePath +`maps/${gymId}/current`, entry.name);
//         console.log('...after awaiting text and text is:')
//         console.log(text.slice(0,50));
//         mapsText.push(text);
//       }
//       const mapObject = this.buildMapObject(gymId, mapFileEntries.map(fileEntry=>fileEntry.nativeURL), mapsText);
//       // console.log(`**** mapObject:  `);
//       // console.log(mapObject);
//       console.log('about to return mapObject')
//       return mapObject;
//     }else{
//       throw( new Error('in getMaps2 Could not load maps!'));
//     }
//   }catch(error){
//       throw(error);
//   };
// }

//This method was (temporarily?) abandoned...
// async getMapText(nativePathToParentDir, fileName){
//   return await this.fileUtilsProv.file.readAsText(nativePathToParentDir, fileName);
//
// }

//NOTE: currently only displays contents of the first map. Usedd for debugging
async displayMapFileEntriesAndText(mapFileEntries, gymId, basePath){
      const fileEntries = await this.fileUtilsProv.file.listDir(basePath +`maps/${gymId}`, 'current');
      // console.log(`checked maps/${gymId}/current and got contents as:`);
      // console.log(JSON.parse(JSON.stringify(fileEntries)));

      const fileNames = fileEntries.map(entry => entry.name);

      const text = await this.fileUtilsProv.file.readAsText(`${basePath}/maps/${gymId}/current`, fileNames[0]);
      // console.log(`******* GOT MAP TEXT AS :`);
      // console.log(text);
      return null;

}


// async loadMaps2(gymId:string, basePath: string, date:Date=null):Promise<Entry[]> {
//   //returns an array consisting the FileEntry objects of the loaded svgs
//   //or throws error
//   try{
//     const db = firebase.firestore();
//     const mapsRef = db.collection(`GYMS/${gymId}/MAPS`);
//     const query  = mapsRef.where('current', '==', true);
//     const mapSnap = await query.get();
//     //there should only ever be one maps doc with property 'current':true;
//     const urlByFloor = mapSnap.docs[0].data().urlByFloor;
//     let downloadPromises = [];
//
//     for (let floorNum of Object.keys(urlByFloor)){
//       //grab storgeUrl
//       const storageUrl = urlByFloor[floorNum];
//       const fileName = this.fileUtilsProv.getPathAndFileName(storageUrl)[1];
//       //build transfer object for that svg
//       let result  = await this.imageProv.getTransferObject(storageUrl,
//           `maps/${gymId}/current/`,'cache',fileName);
//       //start the download and push it to the downloadPromises
//       downloadPromises.push(result.trans.download(result.link,result.destination));
//     }
//     await Promise.all(downloadPromises);
//
//     const fileEntries = await this.fileUtilsProv.file.listDir(basePath +`maps/${gymId}`, 'current');
//     console.log(`in loadMaps2. Loaded Maps Successfully! returning Entries:`);
//     console.log(JSON.parse(JSON.stringify(fileEntries)));
//     return fileEntries;
//   }
//   catch(error){
//     // console.log(`Error in loadMaps2 : ${ error } `);
//     throw(error);
//   };
// }
//

  /*load maps object that was in use during the time period including parameter 'date'
    check file system for requested maps
      if map svg's in file system, build maps:Maps pointing to them and res(maps)
      else, try to download maps svg to filesystem, build maps:Maps pointing to them and res(maps)
      else: can not get requested maps svgs at all, reject(new Error())
  */
  // loadMaps(gymId:string, date:Date=null):Promise<Maps>{
  //   //TODO: cloud function: adds 'deprecated' to map with 'current'==true whenever a new map is uploaded
  //   return new Promise((res,rej)=>{
  //     //grab correct maps from firestore
  //     let db = firebase.firestore();
  //     let mapsRef = db.collection(`GYMS/${gymId}/MAPS`);
  //     let query = null;
  //
  //     //build the querry, depending on if they are constrained by date
  //     if(!date){
  //       // console.log("No Date Provided");
  //       query = mapsRef.where('current', '==', true);
  //     }else{
  //       //TODO can 'created' and 'deprecated' be compared to date w/o new Date() or something? i.e. does this work?
  //       query = mapsRef.where('created','<', date)
  //       .where('deprecated', '>', date);
  //     }
  //
  //     //the query has been built
  //     if(mapsRef){
  //
  //       query.get().then(( fstoreMaps )=>{
  //         //console.log(fstoreMaps);
  //         if(fstoreMaps.empty){
  //           let err = new Error(`No maps documents matched the querry...gymId:${gymId}, date: ${date}`);
  //           // console.log(err);
  //           rej(err);
  //         }else{
  //           let mapsArr = [];
  //           fstoreMaps.forEach((docSnap)=>{
  //             if(docSnap.data()){
  //               mapsArr.push(docSnap.data());
  //             }
  //           });
  //
            //BUG: this is running too soon. Should run after the above .get, .then block completes
            // let mapsObj = mapsArr[0];//there should only ever be one snapshot (enforced by cloud function), bu just in case...
            // let path = '';
            // let fileName = '';
            // let fbMapUrls = [];
            // let fbMapNames = [];
            // mapsObj['urlByFloor'].forEach(url=>{
            //   [path,fileName] = this.fileUtilsProv.getPathAndFileName(url);
            //   fbMapUrls.push(path);
            //   fbMapNames.push(fileName);
            // });

            //have files already been downloaded? yes-> resolve, no-> try to download them
            // for(let i = 0; i<fbMapUrls.length; i++){
            //   this.fileUtilsProv.file.checkDir()
            // }

  //         }
  //       }).then(()=>{//the snapShots are done loading
  //
  //       })
  //       .catch((error)=>{
  //
  //         rej(error);
  //         // console.log(error);
  //       });
  //     }
  //   });
  // }
  //

  isDateMatch(maps:Maps, routeGymId:string, routeDate:Date):boolean{
    //PURPOSE: was 'maps' used to pick the location of routes created on 'routeDate'?
    //maps: the maps object we're checking for compatibility (from this.maps or firestore)
    //routeGymId: the gymId of the route created on 'routeDate'
    //routeDate: the date that the route in question was created

    //are these the current maps?
    if(!maps.deprecated){
      //only check maps.created date
      return maps.created < routeDate;
    }else{//this map has been deprecated
      // time: maps.created --> routeDate --> maps.deprecated --> now
      return maps.created < routeDate && routeDate < maps.deprecated;
    }
  }
  //
  // buildMapObject(gymId:string, filePaths: string[], mapsText:string[]):Maps{
  //   //paths look like:
  //   //<basepath>/maps/<gymId>/current/mapDocPushKey_<nunmber>.svg
  //   let mapsObj: Maps = {
  //                  gymId:gymId,
  //                  mapByFloor:{}
  //                 };
  //
  //   let i = 0;
  //   filePaths.forEach((path)=>{
  //     const floorNum = parseInt(path.slice(path.lastIndexOf('_')+1).split('.')[0]);
  //     mapsObj['mapByFloor'][floorNum] = mapsText[i];
  //     i++;
  //   });
  //   console.log("*** Returning mapsObj as: ****");
  //   console.log(JSON.parse(JSON.stringify(mapsObj)));
  //   return mapsObj;
  // }

  // buildFirebaseStorageUrl(gymId:string, floor:number, mapsId:string = null ){
  //   //no calls to firestore: must pass in all correct prarmeters
  //   //QUESTION: is this really useful? eventually it will be (when uploading user-generated maps),
  //   //...but for now, maps are uploaded manually and the maps object in firestore should have these urls
  //   //..in it.
  //   if( !gymId || !floor || !mapsId) {
  //      // console.log(`Cannot get storageUrl with gymId: ${gymId}, floor: ${floor} and mapsId: ${mapsId}.`);
  //      return null;
  //   }else{
  //     return `maps/${gymId}/${mapsId}/${mapsId}_${floor}.svg`;
  //   }
  // }

  // buildFileSysUrl(fStorageUrl:string){
  //   //TODO: finish this!
  //
  // }

}
