import { Injectable } from '@angular/core';
import { Platform, Events } from 'ionic-angular';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFireDatabase } from '@angular/fire/database';

import { RouteFilters, RangeFilter, routeObject} from '../../providers/globalConstants/globalConstants';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/take';

import { filter } from 'rxjs/operators/filter';
//import { mergeMap} from 'rxjs/operators/mergeMap';
//import { switchMap } from 'rxjs/operators/switchMap';
import { map } from 'rxjs/operators/map';

import * as firebase from 'firebase/app';//used for definig types
import  'firebase/storage';

import { LocalStorageProvider } from '../local-storage/local-storage';
import { UserDetailsProvider } from '../../providers/user-details/user-details';
//import { SettingsProvider } from '../settings/settings';
import { UrlProvider } from '../../providers/url/url';
import{ ConnectionProvider } from '../../providers/connection/connection';
import { FiltersProvider } from '../../providers/filters/filters';
import { SettingsProvider } from '../../providers/settings/settings';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

//TODO: single source of truth: use global constants import: routeObject (actually should be RouteObject)
interface RouteData{
  pushKey:string; //for finding the comment and updating or removing it
  name: string;
  imageUrl:string;

  authorName:string; //QUESTION: phase in userName for storage location rather than authorId?
  authorMug?:string; //storage url (also used for local storage)
  grade: string;
  consensusGrade?:number;
  features?:{[key:string]:boolean}
}

export interface Location{
  point:{x:number, y:number};
  t:number;
  wallCode:string;
  floor?:number;
  dist?:string;
}

interface Hold{
  Color:number;
  Radius: number;
  Selected?: boolean;
  pointCor:{x:number, y:number};

}

// export interface FirestoreRouteData{
//   pushKey:string; //for finding the comment and updating or removing it
//   //the name of the route
//   name: string;
//   coverPhoto:string;//url in storage (QUESTION: download url or storage url?  )
//   topo?: {photos: string[], //download Urls (NOT STORAGE IDS)
//         //TODO (HACK): using <any> to check firestore functionality
//          holdArrays: any//Hold[][]
//        };
//
//   betaPhotos?:string[];
//
//   authorName:string; //TODO: phase in userName for storage location rather than authorId
//   //HACK: made authorId optional so I could add it later
//   authorId?:string;
//   authorMug?:string; //storage url (also used for local storage)
//
//   grade: number;
//   consensusGrade?:number;
//   location:Location;
//   features?:string[];
// }

export class FireStoreObservableRoute{

  routesColRef: AngularFirestoreCollection<routeObject>;
  queryDescrip:any = null;
  gymId:string;
  route$:Observable<routeObject[]> = null;
  filteredRoute$:Observable<routeObject[]> = null;

  behaviorSubjRoute$:BehaviorSubject<routeObject[]> = new BehaviorSubject(null);
  behaviorSubjFilteredRoute$:BehaviorSubject<routeObject[]> = new BehaviorSubject(null);
  routeStateChanges$:Observable<any> = null;
  // afs:AngularFirestore;
  // urlProv:UrlProvider;
  // filtersProv:FiltersProvider;
  // settingsProv:SettingsProvider;

  ready:any;

  constructor(public afs: AngularFirestore, public urlProv:UrlProvider,
    public filtersProv: FiltersProvider, public settingsProv: SettingsProvider, public mapId:string){

    this.ready = new Promise((res,rej)=>{
      //TODO: fix this error: getSettings?
      this.settingsProv.getSettings().then(()=>{
        // console.log("settingsProvider.getSettings RESOLVED!!!!!!!!!");
        this.init(mapId).then(async ()=>{
          // console.log("firestore observable route.ready resolving ");
          // await this.initFilteredRoutes();
          // res(undefined);
          this.initFilteredRoutes().then(()=>{
            res(undefined);
          }).catch(error=>{
            rej(error);
          })
        }).catch((error)=>{
          console.log(error);
        //TODO: reroute to settings page. They need to pick a gymId
        //handle error
          rej(error);
        });
      }).catch((error)=>{
        console.log("In filteredRoutesProv. settings.ready.catch: THREW ERROR");
        console.log(error);
        //TODO: reroute to settings page. They need to pick a gymId
        //handle error
        console.log('Trying to instantiate FireStoreObservableRoute, but could not get settings.');
        rej(new Error('Trying to instantiate FireStoreObservableRoute: Could not get settings'));
      });
    });
}
    //
  //   this.ready = new Promise(async (res,rej) => {
  //     console.log('***** ready called ********');
  //     try{
  //       await this.settingsProv.getSettings().catch(error => {throw error});
  //       await this.init(mapId);
  //       await this.initFilteredRoutes().catch(error => {throw error});;
  //       // res(undefined);
  //       return undefined;
  //
  //     }catch(error){
  //       console.log('filtered-routes (provider), FireStoreObservableRoute constructor, this.ready: ERROR ');
  //       console.lie(error);
  //       throw error;
  //     };
  //   });
  // }
  //
  // public async ready():Promise<boolean>{
  //   console.log('filtered-routes (provider), FireStoreObservableRoute.ready() triggered');
  //   try{
  //     await this.settingsProv.getSettings();
  //     await this.init(this.mapId);
  //     await this.initFilteredRoutes();
  //     console.log('filtered-routes (provider), about to resolve');
  //     return true;
  //   }
  //   catch(error){
  //     console.log('filtered-routes (provider, FireStoreObservableRoute.ready() throwing ERROR!)');
  //     throw error;
  //   };
  //
  // }

  /*
  assumptions:
    --settings provider has been initialized
  */
  init(mapId:string):Promise<any>{
    return new Promise((res,rej)=>{
      // console.log("fsObsRoute.init");
      if(!this.gymId){//this.gymId has not been initialized
        // console.log("No this.gymId, so trying settingsProv.getGymId");

      this.settingsProv.getGymId().then(async ( gymId )=>{
        // console.log("got gymId as: ", gymId);
          this.gymId = gymId;
          this.getRouteObsRef(mapId).then(async ( routesColRef )=>{
            this.routesColRef = routesColRef;

            //this.routeStateChanges$ = this.routesColRef.stateChanges() as Observable<any>;
            //valueChanges returns an observable that emits the current
            //state of the routes array defined by the query (which was built in getRouteObsRef)

            // console.log(` ***** ****** ONE ***** Setting this.route$`);

            this.route$ = this.routesColRef
            .valueChanges();

            this.route$.subscribe(routes => this.behaviorSubjRoute$.next(routes));

            console.log('***** this.route$: *******');
            console.log(this.route$);

            console.log(` ***** ****** TWO ***** Calling initFilteredRoutes`);

            await this.initFilteredRoutes();

            console.log(` ***** ******  FOUR  ***** calling res`);

            res();
          }).catch((error)=>{//caleld getRoutesObsRef, but this.gymId was falsey (null?)
            console.log(error);
          });
      }).catch((error)=>{
        console.log(error);
      });
    }else{//gymId was already initialized
      console.log("filtered-routes (provider),init this.gymId was defined");
      this.getRouteObsRef(mapId).then(async ( routesColRef )=>{
      console.log("filtered-routes (provider),init Setting Firestore Observables (in else)");
        this.routesColRef = routesColRef;

        //TODO: get correct type for Observable<correct-type-here> on next line
        this.routeStateChanges$ = this.routesColRef.stateChanges() as Observable<any>;

        // console.log("*****Setting stateChanges callback in else");
        // this.routeStateChanges$ = this.routesColRef.stateChanges().do(actions=>{
        //     actions.forEach(action => { console.log("Action was:");
        //                                 console.log(action.payload.doc.data());
        //                               });
        //     });//.subscribe();
        this.route$ = this.routesColRef.valueChanges();
        this.route$.subscribe(routes => this.behaviorSubjRoute$.next(routes));

        //THE FOLLOWING WAS PRESENT, BUT SEEMS REDUNDANT!
        // await this.initFilteredRoutes();

        res();
      }).catch((error)=>{//called getRoutsObsRef, but this.gymId was falsey (null?)
        // console.log(error);
      });
    }
    });

  }

  checkFilters(route:routeObject, filters: RouteFilters):boolean{
    /************* setBy and color check *****************************/
    //if it does not have 'setBy' property, it is ill-formatted, return false
    let passSetBy = false;
    let passColor = false;
    if(!route.hasOwnProperty('setBy')){
      // console.log('route did not have property setBy route:');
      // console.log(route);
      return false;
    }else{

      //console.log('checking filters.setBy: ', filters.setBy);
      switch (filters.setBy){
        case 'anyone':
          // console.log('case anyone');
          //if route has a color property, it must pass the color test
          //only fails color test if filters.colors actually specifies colors to filter on
          if(route.setBy == 'setter' //the route should have a color property
             && filters.hasOwnProperty('colors') && Object.keys(filters.colors).length > 0//the user cares about route colors (for official routes)
             && !filters.colors[route.color]){ //this route is the kind that should have it's color checked
            return false;
          }

          //all routes pass the setBy test
          passSetBy = true;
          //it did not have a color property or it did and it had the right color
          passColor = true;
          break;

        case 'me':
          // console.log('case me, route.setBy: ', route.setBy);
          if(route.setBy !== 'me'){
            // console.log('route:');
            // console.log(route);
            // console.log('case me returning false!!!!!!');
            return false;
          }
          passSetBy = true;
          // console.log('switch case me. passSetBy:', passSetBy);
          //DIY route's don't have the color property, but we're not filtering
          //on that property because we're only looking for DIY routes
          passColor = true;
          break;

        case 'setter':
          // console.log('case setter. ');
          if(filters.hasOwnProperty('colors') && Object.keys(filters.colors).length > 0//the wants to filter based on color
          && !filters.colors[route.color]){//apply color filters
            return false;
          }
          passSetBy = route.setBy === 'setter';
          passColor = true;
          break;
      }
    }
    if(!passSetBy || !passColor){
      // console.log('pass setBy: ', passSetBy);
      // console.log('returning false;')
      return false;
    }

    /**************** end setBy check *********************************/

    return this.checkAttributesAndRanges(route,filters);
  }


  checkAttributesAndRanges(route:routeObject, filters:RouteFilters):boolean{

    const attrKeys: string[] = Object.keys(filters.attributes);
    if(filters.hasOwnProperty('attributes') && attrKeys.length > 0){
      //test the route's 'features'
      for (let attr of attrKeys){
        // console.log('**--**__** Checking filter, ', attr);
        if(!route.hasOwnProperty('features') || !route.features.hasOwnProperty(attr)
           || !route.features[attr] ){
          return false;
        }
      }
    }

    // test the route's rangeFilters
    if(filters.hasOwnProperty('rangeFilters') && Object.keys(filters.rangeFilters).length > 0){
      const rangeFilterKeys = Object.keys(filters.rangeFilters);

      for(let rangeKey of rangeFilterKeys){
        // console.log('*****--**__** Checking rangeFilter, ', rangeKey, ' **--**__*****');
        // console.log('...route[RangeKey]: ', route[rangeKey]);
        // console.log('...filters.rangeFilters[rangeKey]:');
        // console.log(filters.rangeFilters[rangeKey]);

        if(!route.hasOwnProperty(rangeKey) ||
        route[rangeKey] < filters.rangeFilters[rangeKey].lower ||
        route[rangeKey] > filters.rangeFilters[rangeKey].upper    ){
          // console.log('RETURNING FALSE');
          // console.log('***********************************');
          return false;
        }
      }
    }

    //the route passed all of the tests

    // console.log('RETURNING TRUE for route:');
    // console.log(route);
    // console.log('***********************************');
    return true;
  }

  async initFilteredRoutes(){
    try{
      const filters = await this.filtersProv.getFilters().catch(error=>{
        // console.log('filtered-routes, initFilteredRoutes: error from getFilters:');
        // console.lie(error);
        return null; //so filters becomes null if can't get filters
      });

      // console.log('filtered-routers provider, initFilteredRoutes, filters: ');
      // console.log(filters);

      // if(filters && this.route$ && this.route$.hasOwnProperty('pipe') ){

      if(filters && this.route$){
        // console.log('*** setting this.filteredRout$!!!! *****');
        this.filteredRoute$ = this.route$.pipe(
        map(routes => routes.filter(route=>this.checkFilters(route, filters)))
      );
      this.behaviorSubjFilteredRoute$ = new BehaviorSubject(null);
      this.filteredRoute$.subscribe(routes=>this.behaviorSubjFilteredRoute$.next(routes));

    }else{//filters or this.route$ was not ready (probably this.route$... figure out why!)
      if(!filters){
        throw new Error('filtered-routers provider, initFilteredRoutes, could not get filters!');
      }else{
        // if(!this.route$ || !this.route$.hasOwnProperty('pipe')){

        if(!this.route$ ){
          console.log('filtered-routers provider, initFilteredRoutes: this.route$ is falsey ');
          throw new Error('filtered-routers provider, initFilteredRoutes, this.route$ is falsey!');
        }
      }
    }

    // console.log('returning this.filteredRoute$ as:');
    // console.log(this.filteredRoute$);
    return this.filteredRoute$;

    } catch(error){
      console.log('filtered-routes.ts (provider), initFilteredRoutes: Error');
      console.lie(error);
    };

      }

  getRouteObsRef(mapId:string):Promise<AngularFirestoreCollection<routeObject>>{
    //Contract: this.gymId is defined before this method is called
    //build a reference to the collection of documents defined by a query and resolve it
    return new Promise(async (res,rej)=>{
      // console.log('filtered-routes (provider), getRouteObsRef: calling settingsProv.getSettings');
      const settings = await this.settingsProv.getSettings();

      // console.log('filtered-routes (provider), getRouteObsRef: got settings as:');
      // console.log(settings);
      let currentFloor = 0;

      if(settings.hasOwnProperty('currentFloor')){
        // console.log(`settings had property currentFloor as :${ currentFloor } `);
        currentFloor = settings.currentFloor;
      }else{
        // console.log(`Setting currnent floor to fefault `);
        currentFloor = this.settingsProv.setDefaultFloor(0);
      }

      if(!this.gymId){
        rej(new Error('called getRouteObsRef but gymId was: '+ this.gymId));
      }

      this.queryDescrip = await this.filtersProv.getFiltersAndOrderBy();

      this.routesColRef = this.afs.collection(`GYMS/${this.gymId}/MAPS/${mapId}/ROUTES`, ref=>{
                    return ref.where('location.floor', '==', Number(currentFloor)).orderBy(this.queryDescrip.orderBy);
                     }
                    );


      res(this.routesColRef);
    });
  }
}

/********************** FILTERED ROUTES PROVIDER ***************************/
@Injectable()
export class FilteredRoutesProvider {
  fstoreRoutes:any;
  fstoreObs:FireStoreObservableRoute;
  // fstoreObsReady = false;

  constructor(public platform: Platform, public events: Events, public storage:LocalStorageProvider,
    public urlProv:UrlProvider, public afDatabase: AngularFireDatabase ,
    public userDetailsProv: UserDetailsProvider, public afAuth: AngularFireAuth,
    public connection: ConnectionProvider, public filtersProv: FiltersProvider,
    public settingsProv: SettingsProvider, public afStore: AngularFirestore) {
  }

  init(mapId:string):Promise<FireStoreObservableRoute>{
    return new Promise((res,rej)=>{
      this.platform.ready().then(async ()=>{
        //TODO: change to set pause and resume subscriptions
        // console.log('filtered-routes.ts (provider) about to await this.initFirestoreObsRoutes');
        this.fstoreObs = await this.initFirestoreObsRoutes(mapId);
        //this ".ready" is redundant... initFirestoreObsRoutes awaits the .ready method
        //todo... get rid of the .ready stuff below
        // console.log('filtered-routes.ts (provider) about to return this.fstoreObs');
        res(this.fstoreObs);

      }).catch((error)=>{
        console.log("THROWING AT II.)");
        rej(error);
      });
    });
  }

  async initFirestoreObsRoutes(mapId:string):Promise<FireStoreObservableRoute>{
    try{

      //FireStoreObservableRoute constructs asynchronously! make sure to
      //await instance.ready() before returning!
      this.fstoreObs = new FireStoreObservableRoute(this.afStore, this.urlProv,
      this.filtersProv,this.settingsProv, mapId);
      // console.log('filtered-routes (provider), initFirestoreObsRoutes, got this.fstoreObs as:');
      // console.lie(this.fstoreObs);

      //super important! wait for the fstoreObs instance to finish constructing before returning
      // console.log('************ this.fstoreObs has property ready?: ', this.fstoreObs.hasOwnProperty('ready'));

      await this.fstoreObs.ready;
      return this.fstoreObs;
      // this.fstoreObs.ready.then(()=>{
      //   return this.fstoreObs;
      // });
      //
    }catch(error){
      console.log('filtered-routes.ts (provider), error trying to build this.fstoreObs:');
      console.lie(error);
    }
  }

  // initFirestoreObsRoutes(mapId:string):Promise<FireStoreObservableRoute>{
  //   return new Promise((res,rej)=>{
  //
  //     this.fstoreObs = new FireStoreObservableRoute(this.afStore, this.urlProv,
  //     this.filtersProv,this.settingsProv, mapId);
  //     console.log('filtered-routes (provider), initFirestoreObsRoutes, got this.fstoreObs as:');
  //     console.lie(this.fstoreObs);
  //
  //     //super important! wait for the fstoreObs instance to finish constructing before returning
  //     console.log('************ this.fstoreObs has property ready?: ', this.fstoreObs.hasOwnProperty('ready'));
  //
  //     // await this.fstoreObs.ready;
  //     // return this.fstoreObs;
  //     console.log('filtered-routes.ts (provider), about to call this.fstoreObs.ready.then()');
  //     this.fstoreObs.ready.then(()=>{
  //       console.log('filtered-routes.ts (provider), about to resolve this.fstoreObs');
  //       res(this.fstoreObs);
  //     }).catch(error=>{
  //       console.lie(error);
  //       rej(error);
  //     });
  //
  //   });
  //
  // }
}
