import { Component, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Platform } from 'ionic-angular';

import { MapsProvider, Maps } from '../../providers/maps/maps';//for accessing maps.colors.<stuff you need>
import { FiltersProvider } from '../../providers/filters/filters';
//TODO: remove RouteFilters... it is never used (or figure out why you imported it/ SHOULD use it for something)
import { RouteFilters } from '../../providers/globalConstants/globalConstants';
import { AuthProvider } from '../../providers/auth/auth';

import { FilterSelectorComponent } from '../../components/filter-selector/filter-selector';
import { GymAndFloorPickerComponent } from '../../components/gym-and-floor-picker/gym-and-floor-picker';



import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;



@IonicPage()
@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html',
})
export class FiltersPage {
  //TODO: maybe pull minGrade and maxGrade from the server?

  mode: string = 'filter';

  user = null;

  filters: RouteFilters = null;
  orderedColorNames:string[] = null;
  colorNamesToColorCodes:{[key:string]:string} = null;

  variablesReady: boolean = false;

  message: string = null;

  @ViewChild('filterSelector') filterSelector: FilterSelectorComponent ;

  constructor(public navCtrl: NavController,
    public filtersProv: FiltersProvider,
    public platform:Platform,
    public authProv: AuthProvider,
    public mapsProv: MapsProvider
  ){
    this.platform.ready().then(async ()=>{

        //NOTE: we assume mapsProv has already initialized its maps property and it is the correct maps object
        const mapsObj = await this.mapsProv.getMaps();
        if(mapsObj.hasOwnProperty('colors')){
          this.orderedColorNames = mapsObj.colors.orderedColors;
          this.colorNamesToColorCodes = mapsObj.colors.colorNamesToColorCodes;
        }


      this.filters = await this.filtersProv.getFilters();
      //only build a colors property if this gym uses color-coding
      if(!this.filters.hasOwnProperty('colors') && this.orderedColorNames){
        this.filters.colors = {};
      }
      if(!this.filters.hasOwnProperty('setBy')){
        this.filters.setBy="anyone";
      }
      //set variablesReady=true when all page-level variables and properties needed in the
      //template (for example filters.colors) are initialized
      this.variablesReady = true;
    }).catch((error)=>{
      console.log('filters.ts (page), constructor, error: ');
      console.lie(error);
      throw(error);
    });
  }

  toggleColor(color:string){
    if(this.filters.colors[color]){
      delete this.filters.colors[color];
    }else{
      this.filters.colors[color]=true;
    }
  }

  //take any filters generated on this page (for example: colors, setBy) and merge
  //them into filtersWithAttributes before saving filters with filtersProv
  ionViewCanLeave(){
    // console.log(`** filters.ts (page) ionViewCanLeave called. Attempting to save filters as:`);
    // console.log(this.filters);
    // return this.filtersProv.saveFilters(this.filters);

    let filtersWithAttributes = this.filterSelector.getFilters() as RouteFilters;
    if(this.filters.hasOwnProperty('colors') && Object.keys(this.filters.colors).length>0){ //user has selected at least one color

    }
    // console.log('*** got filters from filterSelector as:');
    // console.log(filters);
    //if they're a logged in user, save their filters
    this.platform.ready().then(async ()=>{

  //TODO/QUESTION: authenticate before save stuff into filter provider? Does this make sense? Maybe don't always want to authenticate before save filters?
  // ...this must depend on how guest sessions are handled in firestore. Can we delete their preferences when they end their session (with cloud functions)?
      this.user = this.authProv.getUser();
      if(this.user && this.user.uid != null && this.user.hasOwnProperty('isAnonymous')
       && !this.user.isAnonymous){
        console.log('** in filters.ts (page) and calling filtersProv.saveFilters ***');
        return this.filtersProv.saveFilters(filtersWithAttributes) ;
      }else{//Display a message for a couple of seconds, saying that the filters could not be saved.
        this.message = 'Could not save filters!!!'
        setTimeout(()=>{
          return true;
        }, 2000);
      }
    });
  }
}
