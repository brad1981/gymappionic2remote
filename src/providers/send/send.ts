import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';

import * as firebase from 'firebase/app';//used for definig types

import { routeObject } from '../../providers/globalConstants/globalConstants';

import { TimeProvider } from '../../providers/time/time';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

/*    ****** Date considerations ********
TODO:

Add time zone property to each gym (Dawson's excellent idea)
Use gym's timezone to translate server timestamps into the time displayed for sends
...this is really more relevant to the template/view code which has to deal with translating
timestamps into user-readable times.

In offline mode, FieldValue.serverTimestamp returns null (if my memory serves me).
We'll need to check for online/offline and if offline, use a client-generated timestamp.
TODO: Think through why this might cause buggy behavior. Try to design everything so that
this only fucks up THAT user's maps and such. Lastly, give the user a way to go back and edit
send date info... eventually.
*/

//used to check whether send provider should be reinitialized, do to a change in gym, maps or user
interface Config{
  userId:string;
  gymId:string;
  mapsId: string;
}

//FUTURE WORK:
//Eventually, we can let people keep track of their tries as they attempt a route.
//Record every send in an array (timesToTries... see descrip below )
export interface Send{
  first: any, //actually a firebase.firestore.FieldValue.serverTimestamp() instance, but can't find type
  last: any; //actually a firebase.firestore.FieldValue.serverTimestamp() instance, but can't find type


  // timesToTries:{
  //   [key:number]:number; //key: the timestamp of the send. value: number of tries needed to send
  // };
  numSends: number;
  grade: number;

  setBy: string; //for querying official vs diy

  floor: number;
  color?: string;

  pushKey?:string;
}

@Injectable()
export class SendProvider {
  config:Config = null;
  userId:string = null;
  sends$:Observable<Send> = null;
  routeIdToSend$:Observable<{[key:string]:Send}> = null;

  constructor(public platform: Platform,
    public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    public timeProv: TimeProvider) {
      this.platform.ready().then(()=>{
        this.afAuth.authState.subscribe( async user => {
          if(user){
            this.userId = user.uid;
          }
        });
      });
  }

  //initialize routeIdToSend$
  //if config has changed, reset it (after routeIdToSend$ successfully initializes)
  //if user is not provided, attempts to use this.userId
  async initSends(gymId:string, mapsId:string, userId:string=null):Promise<Observable<{[key:string]:Send}>>{
    // console.log('******** sendsProvider: initSends *********');
    if(!userId){
      try{
        if(this.userId){
          userId = this.userId;
        }else{
          throw new Error('send provider, cannot init sends... No User!');
        }
      }
      catch(error){
        throw error;
      };
    }
    const configOk = !this.checkConfig(userId,mapsId, gymId);
    // console.log('configOk : ',configOk );
    // console.log('routeIdToSend$: ',this.routeIdToSend$);
    //if !configOk, then we might have the wrong routeIdToSends loaded
    //import {   } from '' !this.routeIdToSend$, then we need to initialize that object
    try{
      if(!this.routeIdToSend$ || !configOk){
        // console.log('sendProvider, trying building routeIdToSend$')
        const userSendsCollectionPath = this.getUserSendsPath2(userId,gymId, mapsId);
        // console.log('sendProvider, userSendsCollection path: ');
        // console.log(userSendsCollectionPath);
        const userSendsCollection = this.afs.collection(userSendsCollectionPath);
        //
        // this.sends$ = userSendsCollection.snapshotChanges().map(actions => {
        //   let sends = {} as Send;
        //   actions.forEach(a=>{
        //     const send = a.payload.doc.data() as Send;
        //     send['pushKey'] = a.payload.doc.id;
        //   });
        //   return sends;
        //
        // })
        //
        // userSendsCollection.get().forEach(send=>{
        //   send.docs.forEach(doc=>{
        //
        //     console.log('got send as:');
        //     console.log(doc.data() );
        //
        //   });
        // });
        //

        //...I don't really understand this. I was posessed by our dark lord when I wrote it!
        this.routeIdToSend$ = userSendsCollection.snapshotChanges().map(actions => {
          let routeToId = {};

          actions.map(a=>{
            const send = a.payload.doc.data() as Send;
            // console.log('******** sendsProvider: initSends  and send is:*********');
            // console.log(send);
            const sendId = a.payload.doc.id;
            routeToId[sendId]=send;
          });
          return routeToId;
        });

        if(this.routeIdToSend$){
          if(!configOk){
            this.setConfig(userId,mapsId, gymId);
          }
          return this.routeIdToSend$;
        }else{
          throw new Error('sendProvider, initSends: ERROR... could not build routeIdToSend$');
        }
      }
    }
    catch(error){
      console.log('send provider, error:');
      console.lie(error);
      throw error;
    };
  }


  // get most recent send data for the specified route
  //use this to check if the route was already sent, so you can verify that the
  //user sent again
  // async getSend(gymId:string, routeId: string){
  //   //only try to retrieve sends for logged in users
  //   try{
  //     if(!gymId || !routeId){
  //       throw new Error('Send provider, submitSend: bad gymId or routeId');
  //     }
  //
  //     if(this.userId){
  //       const userSendsPath = this.getUserSendsPath(this.userId, gymId, routeId);
  //       const sendDoc = await this.afs.doc(userSendsPath);
  //       const sendSnap = await sendDoc.ref.get();
  //
  //       if(sendSnap.exists){
  //         return sendSnap.data() as Send;
  //       }else{
  //         return null;
  //       }
  //     }
  //   }
  //   catch(error){
  //     console.lie(error);
  //     throw error;
  //   };
  // }

  //Usage NOTE: before calling submitSend, verify that they are either sending for the first time,
  //or they are actually repeating a previous send by calling getMostRecentSendDate
  //and asking for verification if it does not return null
  //
  //submitSend:
  //  -check if document exists in db for this send
  //  -if it does exist, fetch it, modify num sends, last send and timesToTries then use update to rewrite it
  //  TODO: rewrite this method to use
  //
  //
  //Cloud Functions must:
  //  -create corresponding record under the route
  //  -push new timestamp to the send.timestampHistory array (could do from client after
  //   updating the firestore node package)
  async submitSend(gymId:string, mapsId:string,
  route: routeObject){
    // console.log('send provider, submitSend, route is:');
    // console.log(route);
    try{
      if(!gymId || !route){
        throw new Error('Send provider, submitSend: bad gymId or routeId');
      }

      if(!route.hasOwnProperty('setBy') || typeof route.setBy != typeof 'hello' ||
         !route.hasOwnProperty('grade') || typeof route.grade != typeof 1 ||
         !route.hasOwnProperty('floor') || typeof route.floor != typeof "1" //why is it a string? wtf? (it's correct... for now)
       ){
         throw new Error("Send provider, submitSend: route has bad 'setBy', 'grade', or 'floor'");
       }

      const userSendsPath = this.getUserSendsPath2(this.userId, gymId,mapsId, route.pushKey);
      const sendsDoc= this.afs.firestore.doc(userSendsPath);
      const sendsSnap = await sendsDoc.get();


      const now = this.timeProv.getTimestampForFirestore();
      // console.log('send prov, about to check if sendsSnap exists...');

      if(sendsSnap.exists){
        // console.log('send prov submitSend, send already existed... updating');
        let send = sendsSnap.data() as Send;

        // console.log('sendProvider, submit send, got send.data() as:');
        // console.log(send);

        await sendsDoc.update({
          numSends: send.numSends + 1,
          last: now,
        });

        //TODO:
        //send finished uploading, now modify it with timestamps for the client
        send.numSends += 1;
        return send;

      }else{ //the send doc DNE, so create a new one
        // console.log('sendsProvider, submitSend, send did not exist.');
        const send = {
          first: now,
          last: now,
          numSends: 1,
          grade: route.grade,
          setBy: route.setBy,
          floor: route.floor
        }

        if(route.hasOwnProperty('color')){
          if(typeof route.color != typeof 'a string' ){
            throw new Error("Send provider, submitSend: route has bad 'color'");
          }
          send['color']=route.color;
        }

        await sendsDoc.set(send);

        return send;
      }
    }

    catch(error){
      console.lie(error);
      throw error;
    };
  }

  //userId, gymId guarantee that we're only getting doc(s) relevant to this user, gym, etc
  //if routeId is provided, a ***document*** path to that routeId is returned
  //if routeId is NOT provided, a ***collection*** path for all specified send documents is returned
  // getUserSendsPath(userId: string, gymId:string, routeId:string = null):string{
  //   if(routeId){
  //     return  `USERS/${userId}/GYM/${gymId}/ROUTE/${routeId}`;
  //   }else{
  //     return  `USERS/${userId}/GYM/${gymId}/ROUTE`;
  //   }
  // }

  //NOTE: this method returns a document path XOR a collection path, depending on
  //how whether you provide a routeId argument.
  getUserSendsPath2(userId: string, gymId:string, mapsId:string, routeId:string = null):string{
    if(routeId){
      return  `USERS/${userId}/SENDSBYGYM/${gymId}/SENDSBYMAPS/${mapsId}/SENDSBYROUTE/${routeId}`;
    }else{
      return  `USERS/${userId}/SENDSBYGYM/${gymId}/SENDSBYMAPS/${mapsId}/SENDSBYROUTE`;
    }
  }

  //get the path to the sends document UNDER THE ROUTE document
  //this document points providers a reference to every userId that should have
  //a corresponding send record at /USERS/userId/GYM/gymId/MAPS/mapsId/SENDSBYROUTE/routeId
  getRouteSendsPath(userId: string, mapsId:string, gymId:string, routeId:string):string{
    return `GYMS/${gymId}/MAPS/${mapsId}/ROUTES/${routeId}/SENDS/${userId}`;
  }

  //update the config object
  setConfig(userId: string, gymId:string, mapsId:string){
    if(!userId || !gymId || !mapsId){
      throw new Error('send provider, setConfig. Bad input');
    }else{
      this.config = {
        userId:userId,
        gymId:gymId,
        mapsId: mapsId
      }
    }
  }

  checkConfig(userId: string, gymId:string, mapsId:string):boolean{
    if(!this.config || ! this.config.hasOwnProperty('userId') ||
      !this.config.hasOwnProperty('gymId') ||
      !this.config.hasOwnProperty('mapsId') ||
      this.config.userId != userId ||
      this.config.mapsId != mapsId  ||
      this.config.gymId != gymId ){
        return false;
      }else{
        return true;
      }
  }

}
