import { Injectable } from '@angular/core';
import { UserDetailsProvider } from '../../providers/user-details/user-details';
import * as firebase from 'firebase/app';//used for definig types
import { Observable } from 'rxjs/Observable';
//import 'rxjs/add/observable/from';
import  'firebase/storage';
import { AngularFireAuth} from '@angular/fire/auth';
import { SettingsProvider } from '../../providers/settings/settings';
import { betterConsole }  from '../../providers/globalConstants/globalConstants';
import { ListObject } from '../globalConstants/globalConstants';
import { Alert, AlertController } from 'ionic-angular';

let console = betterConsole;

//TODO: firestore floorlist naming conventions: seems to allow accidental
//overwrites (what if someone tries to creat two lists with the same name?)

@Injectable()
export class ListsProvider {

  private settings: any = null;
  private allListData: ListObject[] = null;
  public listNameArr: string[] = [];
  public listInfoArr: any[] = [];
  public listArrArr: [string[]] = [[]];
  public existsInLists: any[] = [];
  //public openLists: any[] = [];
  public customListArr: any[] = [];
  private uId = null;

  constructor(
    public settingsProv: SettingsProvider,
    public auth: AngularFireAuth,
    public alertCtrl: AlertController) {

      // console.log("AAA list constructor called");
      // console.log("AAA existing allListData:", this.allListData);
      // console.log("setting allListData to null.");
      this.allListData = null;
      // this.getAllListData().then(() => {
      //   console.log("list constructor finished, all list data is: ", this.allListData);
      // });
  }

  cleanLists(){
    // console.log("===cleaning lists===");
    this.listNameArr, this.listInfoArr, this.existsInLists, this.customListArr = [];
  }

  wipeLists(){
    this.allListData = null;
    this.cleanLists();
  }

  async getAllListData(): Promise<any[]>{
    // Returns an array of every list object the user has for every floor for the gym.
    if(this.allListData === null){
      let placeHolder = {
        name: "All Routes",
        listArray: [],
        type: "default",
        floorNum: null,
        mapId:null
      }

      try{
        const settings = await this.settingsProv.getSettings();
        const gymId = settings.gymId;

        const db = firebase.firestore();
        this.uId = this.auth.auth.currentUser.uid;
        // let listRef = db.
        // collection('USERS').doc(this.uId).
        // collection('GYMLISTS').doc(gymId).
        // collection('FLOORLISTS');
        const listRef = await this.getListCollectionRef();

        let snapshot = await listRef.get();

        this.allListData = [placeHolder];
        if(snapshot.size > 0){
          let snapshotArray = snapshot.docs;
          // console.lie("snapshotArray:", snapshotArray);
          for(let i = 0; i < snapshotArray.length; i++){
            let listData = snapshotArray[i].data() as ListObject;
            // console.log("pushing ", listData.name, " to allListData");
            this.allListData.push(listData)
          }
        }
        // console.log("111allListData: ", this.allListData);
        return this.allListData;
      }
      catch(err){
        // console.log("111could not getAllListData!");
        throw(err);
      }
    } else{
      return this.allListData;
    }
  }

  async updateAllListData(){
    this.allListData = null;
    try{
      await this.getAllListData();

    }
    catch(err){
      //catch
      throw err;
    }
  }

  async returnListsInfoArray(): Promise<any[]>{
    let tmpLIA = [];

    // Returns an array of objects that contains list name, list type, and array of push keys
    // for each list that is relevant to current users gym & floor.
    // if(this.allListData.length > 0){
    // return this.allListData;
    // }
    // else{
      try{
        await this.getAllListData();
        this.settings = await this.settingsProv.getSettings();

        for(let i=0; i < this.allListData.length; i++){
          if(i == 0 || this.allListData[i].floorNum === this.settings.currentFloor){
            let listObj = {
              name: this.allListData[i].name,
              routeArray: this.allListData[i].listArray,
              type: this.allListData[i].listType
            }
            tmpLIA.push(listObj);
          }
        }
        this.listInfoArr = tmpLIA;
        return this.listInfoArr;
      }
      catch(err){
        console.log("error in returning list name!");
        throw err;
      }
  }

  async returnListNameArr(): Promise<string[]>{
    // returns an array of just the list names for the current users floor
    let lNA = [];
    try{
      this.listInfoArr = await this.returnListsInfoArray();
      for(let i=0; i < this.listInfoArr.length; i++){
        lNA.push(this.listInfoArr[i].name);
      }
      this.listNameArr = lNA;
      return this.listNameArr;
    }
    catch(err){
      console.log("333could not resturnListNameArr");
      throw err;
    }
  }

  async returnListArrArr(): Promise<[any[]]>{
    // returns an array of routeId arrays that corelates to the list name array.
    // structure is listArrArr = [[listName1, routeIdA, routeIdB], [listName2, routeIdA, routeIdC]]
    this.listArrArr = [[]];
    try{
      await this.returnListNameArr();
      for(let i=0; i<this.listInfoArr.length; i++){
        let listArr= [this.listNameArr[i]];
        let routeArr = this.listInfoArr[i].routeArray;
        for(let i2=0; i2<routeArr.length; i2++){
          listArr.push(routeArr[i2]);
        }
        this.listArrArr.push(listArr);
      }
      return this.listArrArr;
    }
    catch(err){
      console.log("444error in returningListArrArr");
      throw err;
    }
  }

  async returnListArr(listName:string): Promise<any[]>{
    try{
      await this.returnListArrArr();
      for(let i=0; i<this.listArrArr.length; i++){
        let listArr = this.listArrArr[i];
        if(listName == listArr.shift()){
          return listArr;
        }
      }
    }

    catch(err){
      console.log("555error in returnListArr");
      throw err;
    }
  }

  async checkIfRouteIsInLists(pushKey:string): Promise<string[]>{
    // returns an array of list names that the given pushkey belongs in.

    try{
      this.listInfoArr = await this.returnListsInfoArray();
      let listNames =[];
      for(let i=0; i<this.listInfoArr.length; i++){
        for(let i2=0; i2<this.listInfoArr[i].routeArray.length; i2++){
          if(pushKey == this.listInfoArr[i].routeArray[i2]){
            listNames.push(this.listInfoArr[i].name)
          }
        }
      }
      return listNames;
    }

    catch(err){
      console.log("666could not returnListsInfoArray");
      throw err;
    }
  }

  async returnCustomListArr(): Promise<string[]>{
    // returns an array of all custom list names for the curretnt floor
    this.customListArr = [];
    try{
      this.listInfoArr = await this.returnListsInfoArray();
      for(let i=0; i<this.listInfoArr.length; i++){
        if(this.listInfoArr[i].type == "custom"){
          this.customListArr.push(this.listInfoArr[i].name);
        }
      }
      return this.customListArr;
    }

    catch(err){
      console.log("777 could not returnCustomListArr");
      throw err;
    }
  }

  async returnOpenLists(routeId:string):Promise<string[]>{
    // takes a route pushKey and returns all custom lists for
    // floor number that the route is not a part of,
    // also initializes an array of all lists the route is a part of (existsInLists)
    let openLists = [];

    try{
      openLists = await this.returnCustomListArr();
      this.existsInLists = await this.checkIfRouteIsInLists(routeId);
      for(let i=0; i<openLists.length; i++){
        for(let i2=0; i2<this.existsInLists.length; i2++){
          if(openLists[i] == this.existsInLists[i2]){
            let poop= openLists.splice(i, 1);
          }
        }
      }
      return openLists;
    }
    catch(err){
      throw err;
    }
  }

  async returnRouteDetailsListObject(routeId:string){
    // returns an object w/ existsInLists array and openListsArray
    try{
      await this.returnOpenLists(routeId);
      await this.returnCustomListArr();

      let detailsListObj = {
        existsInLists: this.existsInLists,
        floorLists: this.customListArr
      }
      return detailsListObj;
    }
    catch(err){
      console.log("999 could not returnRouteDetailsListObject");
      throw err;
    }
  }

  async addRouteToList(listName:string,mapId:string, routes:string[], newList:boolean): Promise<boolean>{
    // if newList true then creates a new list w/ listName and selectedRoute as first route in list, as long
    // as list doesnt already exist. If newList false, add selectedRoute to existing listName.
    // console.log("createCustomList called with listName: ", listName, " and firstRoute: ", selectedRoute);
    let settings, gymId, floor, uId, listId, listData = null;
    let listRef: firebase.firestore.DocumentReference = null;
    const db = firebase.firestore();

    try{
      settings = await this.settingsProv.getSettings();
      gymId = settings.gymId;
      floor = settings.currentFloor;
      uId = this.auth.auth.currentUser.uid;
      listId = listName + gymId + floor;


      listRef = await this.getListDocumentRef(listId);

      try{
        let listSnapshot = await listRef.get();
        if(listSnapshot.exists){
          if(newList){
            // return false because the user is trying to create a new list with
            // the name of an existing list.
            return false;
          }
          else{
            listData = listSnapshot.data();
            // console.log("got List data from existing list with selected route as:", listData);
            listData.listArray = listData.listArray.concat(routes);
            // console.log("new listarray is", listData.listArray);
          }
        }

        if(newList){
          let newListData:ListObject = {
            listType: "custom",
            name: listName,
            id: listId,
            permission: "private",
            floorNum: floor,
            listArray: routes,
            mapId:mapId
          };
          listData = newListData;
        }

        try{
          await listRef.set(listData);
          // reflect the change locally
          if(newList){
            this.allListData.push(listData)
          }
          else{
            for(let i=0; i<this.allListData.length; i++){
              if(this.allListData[i].id == listId){
                this.allListData[i].listArray = this.allListData[i].listArray.concat(routes);
              }
            }

          }
          return true;
        }
        catch(err){
          console.log("err writting new list to firebase!");
          throw err
        }
      }
      catch(err){
        console.log("error in checking if list exists!");
        throw err;
      }
    }
    catch(err){
      console.log("error in getting settings");
      throw err;
    }

  }

  // async removeRouteFromList(routeId:string, listName:string, indx?:number){
  //   // Removes given routePushkey from given listname.
  //   // if no index is given, it deletes the first instance of the pushkey
  //   // otherwise, it deletes pushKey @ given index.
  //   // if it is the last route in the list it deletes the route.
  //   try{
  //     let settings = await this.settingsProv.getSettings();
  //     let gymId = settings.gymId;
  //
  //     const db = firebase.firestore();
  //     let listId = "";
  //     let routeArr = [];
  //     for(let i=0; i<this.allListData.length; i++){
  //       if(this.allListData[i].name == listName){
  //         listId = this.allListData[i].id;
  //         routeArr = this.allListData[i].listArray;
  //       }
  //     }
  //     if(indx !== undefined){
  //       // console.log("removeRouteFromList called w/ index:", indx);
  //       if(routeArr[indx] === routeId){
  //         routeArr.splice(indx, 1);
  //       }
  //     }
  //     else{
  //       let once = false;
  //       for(let i=0; i< routeArr.length; i++){
  //         if(routeArr[i] === routeId){
  //           if(!once){
  //             routeArr.splice(i, 1);
  //             once = true;
  //           }
  //         }
  //       }
  //     }
  //
  //     if(routeArr.length > 0){
  //       try{
  //         const listRef = await this.getListDocumentRef(listId);
  //         await listRef.update({listArray: routeArr});
  //
  //         for(let i=0; i<this.allListData.length; i++){
  //           if(this.allListData[i].name == listName){
  //             listId = this.allListData[i].id;
  //             this.allListData[i].listArray = routeArr;
  //           }
  //         }
  //       }
  //       catch(err){
  //         console.log("could not update routeArray from removeRouteFromList, listProv");
  //         throw err;
  //       }
  //     }
  //     else{
  //       try{
  //         await this.deleteList(listName);
  //       }
  //       catch(err){
  //         console.log("could not delete entire list from removeRouteFromList, listProv");
  //         throw err;
  //       }
  //     }
  //   }
  //   catch(err){
  //     throw err;
  //   }
  // }
  //
  // async renameList(oldName:string,mapId, newName:string){
  //   let routeArray, oldListId = null;
  //   for(let i=0; i<this.allListData.length; i++){
  //     if(this.allListData[i].name == oldName){
  //       routeArray = this.allListData[i].listArray;
  //       oldListId = this.allListData[i].id;
  //     }
  //   }
  //
  //   try{
  //     let settings = await this.settingsProv.getSettings();
  //     let gymId = settings.gymId;
  //     let uploadBool = await this.addRouteToList(newName,mapId, routeArray, true);
  //     if(uploadBool){
  //       //
  //       // const db = firebase.firestore();
  //       // await db.collection('USERS').doc(this.uId).
  //       //          collection('GYMLISTS').doc(gymId).
  //       //          collection('FLOORLISTS').doc(oldListId).delete();
  //
  //       const oldListDocRef = await this.getListDocumentRef(oldListId);
  //       await oldListDocRef.delete();
  //       this.allListData = null;
  //       await this.getAllListData()
  //     }
  //
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }
  //
  isListCustom(listName:string):boolean{
    console.log('isListCustom: STARTED');
    for(let i=0; i<this.allListData.length; i++){
      if(this.allListData[i].name == listName){
        if(this.allListData[i].listType == "custom"){

          console.log('isListCustom: returning true');
          return true;
        }
        else{

          console.log('isListCustom: returning false');
          return false;
        }
      }
    }
  }

  // async deleteList(listName:string){
  //   let listId = null;
  //   for(let i=0; i<this.allListData.length; i++){
  //     if(this.allListData[i].name == listName){
  //       listId = this.allListData[i].id;
  //       // console.log("got listId as: ", listId);
  //       break;
  //     }
  //   }
  //   try{
  //     // let settings = await this.settingsProv.getSettings();
  //     // let gymId = settings.gymId;
  //     // const db = firebase.firestore();
  //     // await db.collection('USERS').doc(this.uId).
  //     //          collection('GYMLISTS').doc(gymId).
  //     //          collection('FLOORLISTS').doc(listId).delete();
  //     const listDocRef = await this.getListDocumentRef(listId);
  //     await listDocRef.delete();
  //     // reflect local changes...
  //     for(let i=0; i<this.allListData.length; i++){
  //       if(this.allListData[i].name == listName){
  //         this.allListData.splice(i, 1);
  //       }
  //     }
  //     // console.log("list deleted!");
  //     // console.log("allListData:", this.allListData);
  //   }
  //   catch(err){
  //     console.log("error in deleting route!");
  //     throw err;
  //   }
  // }

  // async localReorderList(listName:string, newArray:string[]){
  //   // Reorders list, acts as a stageing area until the user is done reordering
  //   this.settingsProv.getSettings().then(( settings )=>{
  //     let gymId = settings.gymId;
  //     let floor = settings.currentFloor;
  //     let listId = listName + gymId + floor;
  //     this.allListData.forEach((e) => {
  //       if(e.id === listId){
  //         // console.log("in ", listId, " replacing: ", e.listArr, "with: ", newArray);
  //         e.listArray = newArray;
  //       }
  //     })
  //   }).catch((error)=>{
  //       //handle error
  //   });;
  //   // console.log("settings: ", settings);
  //
  // }

  // async setReorder(listName:string){
  //   // Once the user confirms that they are done reordering, this reflects the
  //   // changes in the db.
  //   try{
  //     let settings = await this.settingsProv.getSettings();
  //     // let gymId = settings.gymId;
  //     // let floor = settings.currentFloor;
  //     let listId = listName + settings.gymId + settings.currentFloor;
  //     let localArr = null;
  //
  //     //TODO:
  //     //CHANGE: querry database for list with name listName (rather than search array)
  //     //NOTE: listId is used to update the remote document below, so grab it from the ref/snap/whatever
  //     this.allListData.forEach((e) => {
  //       if(e.id === listId){
  //         localArr = e.listArray;
  //       }
  //     })
  //     // const db = firebase.firestore();
  //     // await db.collection('USERS').doc(this.uId).
  //     //          collection('GYMLISTS').doc(gymId).
  //     //          collection('FLOORLISTS').doc(listId).update({
  //     //            listArray: localArr
  //     //          });
  //     const listRef = await this.getListDocumentRef(listId);
  //     await listRef.update({listArray: localArr});
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }

  async getListDocumentRef(listId:string):Promise<firebase.firestore.DocumentReference>{
    try{
      const settings = await this.settingsProv.getSettings();
      const db = firebase.firestore();
      //TODO: commit and test using the old database structure, when working, uncomment code below on new branch
      // return db.collection('USERS').doc(this.uId).
      //          collection('GYMLISTS').doc(settings.gymId).
      //          collection('MAP').doc(settings.mapId).
      //          collection('FLOORLISTS').doc(listId);
      return db.collection('USERS').doc(this.uId).
               collection('GYMLISTS').doc(settings.gymId).
               collection('FLOORLISTS').doc(listId);
    }
    catch(error){
      console.log('lists.ts (provider), getListDocumentRef, ERROR:');
      throw(error);
    };
  }

  async getListCollectionRef():Promise<firebase.firestore.CollectionReference>{
    try{
      const settings = await this.settingsProv.getSettings();
      const db = firebase.firestore();

      //TODO: commit and test using the old database structure, when working, uncomment code below on new branch
      // return db.collection('USERS').doc(this.uId).
      //          collection('GYMLISTS').doc(settings.gymId).
      //          collection('MAP').doc(settings.mapId).
      //          collection('FLOORLISTS');
      return db.collection('USERS').doc(this.uId).
               collection('GYMLISTS').doc(settings.gymId).
               collection('FLOORLISTS');
    }
    catch(error){
      console.log('lists.ts (provider), getListCollectionRef, ERROR:');
      throw(error);
    };
  }


}
