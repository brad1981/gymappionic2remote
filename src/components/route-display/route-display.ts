import { Component, Input, ElementRef,

         ViewChild, SimpleChanges } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import { HoldObject,
         SvgMapObject,
         ContainerDimensions,
         MapDimensions,
         pAspectRatio,
         lAspectRatio,
         filterCategoriesObject,
         landscapeSuffix,
         photoHeight,
         photoWidth,
         routeObject,
         betterConsole

       } from '../../providers/globalConstants/globalConstants';
import { RouteHeadingComponent } from '../../components/route-heading/route-heading';
import { getImageDimensions } from '../../providers/image/image';
// import { getImageDimensions } from '../../providers/image/image';

let console = betterConsole;
// import { RangeFilter,
//          filterCategoriesObject,
//        } from '../../providers/globalConstants/globalConstants';
//QUESTION does the display component need the FeedCache2Provider? I think all it needs is the native file url, which is passed in as 'cache'
//from parent page
// import { FeedCache2Provider } from '../../providers/feed-cache2/feed-cache2';
import { SvgMapComponent } from '../../components/svg-map/svg-map';
import { SvgGestureComponent } from '../svg-gesture/svg-gesture';

import { RouteObject } from '../../providers/globalConstants/globalConstants';

@Component({
  selector: 'RouteDisplayComponent',
  templateUrl: 'route-display.html'
})
export class RouteDisplayComponent extends SvgGestureComponent {
  @ViewChild('routeDisplayTopoContainer') topoContainer:ElementRef;
  @ViewChild(SvgMapComponent) map : SvgMapObject;
  // @ViewChild('routeDisplayMapContainer') mapContainer: ElementRef;

  // Inputs from parent page indicating the size of the route-display component
  @Input() imgURL: string;
  @Input() divWidth: number;
  @Input() divHeight: number;
  @Input() routeHeadingStyleFlag: string; //'detail-view' or 'feed-view-map' or 'editor'
  //@Input() aspectRatio: number;
  @Input() completeRoute: RouteObject;
  @Input() displayMode: string; //Accepts "basic", "complete", and "complete-map"

  //WARNING: mapObj has a property called newRoute. it is not a Route. It is a Location!!!
  //it is used by the map to set the location in 'view' mode.
  @Input() mapObj:SvgMapObject=null; // <svg> mapContents --> <path> ... </path> <--mapContents </svg> (see map component for details)

  //SvgMapComponent element for calling child functions:
//  @ViewChild(SvgMapComponent) private svgMap: SvgMapComponent;
  // @Input('topoContainerDimensions') topoContainerDimensions: ContainerDimensions

  //TODO: remove 'mapContainerDimensions': it does not appear to be used
  @Input('mapContainerDimensions') mapContainerDimensions: ContainerDimensions;

  topoDimensions:MapDimensions;//use MapDimensions interface because super-class's init function expects this interface for svg's
  topoContainerDimensions: ContainerDimensions = null;


  // SVG DOM element

  public svgWidth: number;
  public svgHeight: number;
  public svgViewBox: string;

  public authorName: string;
  public authorMug: string;
  public routeName: string;
  public grade: number;
  public consensusGrade: number;

  public photoSrc: any;
  public holdArray: any;
  public location: any;
  public features: string[];
  public pushKey: any;
  public divID: any;
  public description: any;

  public svgDiv: any;
  public showComplete: boolean = false;

  public hideRoute: boolean = false; // used for showing the route picture, or the Details
  public showMap: boolean = false; // show the map in the details section or not
  public mapDimensions: MapDimensions = null;
  // public mapContainerDimensions: ContainerDimensions = {x: 350, y:350};
  public mapMode: any;
  public constantWidth: number;
  public scaleHoldsBy: number;
  public wallAngle: number;
  public filterCategoryObject = filterCategoriesObject;

  public currentRoute$: BehaviorSubject<routeObject> = new BehaviorSubject(null);

  constructor(
    // public feedCache: FeedCache2Provider,
    public platform: Platform,
    public events: Events,

  ) {
    super(
      platform,
      events,
    );
    this.features = [];
    //this.mapDimensions = {x:300, y:300}; // <-- HACK (temp)
    // this.mapMode = "view";
    this.hideRoute = false;
    platform.ready().then(()=>{
      this.mapDimensions = this.mapObj.mapDimensions;
    });
  }

  ngOnInit(){
    // console.log(this.topoContainerDimensions)
    // console.log(this.topoDimensions);
  }

ngAfterContentInit(){

   // console.log(`&&&&&& %%%%%%% &&&&&&&&& route-display, ngAfterContentInit &&&&&& %%%%%%% &&&&&&&&& `);
    // console.log(`divWidth: ${this.divWidth}`);
    // console.log(`divHeight: ${this.divHeight}`);
    // console.log(`mapCountainerDimensions:`);
    // console.log(this.mapContainerDimensions);
    // console.log(`this.displayMode: ${this.displayMode}` );
    // console.log(`styleFlag: ${this.routeHeadingStyleFlag}`);

    //if topo dimensions were passed in as part of the completeRoute object, use that
    //TODO: Create multiple initConstuction mode maybe?

    if(this.completeRoute && this.completeRoute.topo && this.completeRoute.topo.holdsDims){
      this.topoDimensions = this.completeRoute.topo.holdsDims[0];
    }else{ //assume the dimensions are just the size of the photo
      //BUG: Race condition.... make sure this resolves before doing other stuff
      getImageDimensions(this.photoSrc).then((photoDim)=>{
        this.topoDimensions = photoDim;
    });

    }

    this.svgWidth = this.topoDimensions.width;

    this.svgHeight =this.topoDimensions.height;


    //initialize the value of currentRoute$ for consumption by the map component
    // this.currentRoute$.next(this.completeRoute);

    // console.log(` *************************routeDisplay, ngAfterContentInit ******************************** `);
    // console.log('routeHeadingStyleFlag:');
    // console.log(this.routeHeadingStyleFlag);
    // console.log(`svgHeight, svgWidth :${this.svgHeight}, ${this.svgWidth} `);
    // console.log("topoContainerDimensions:");
    // console.log(this.topoContainerDimensions)
    // console.log("topoDimensions:");
    // console.log(this.topoDimensions);
    // console.log(` *************************routeDisplay, ngAfterContentInit ******************************** `);
    // console.log(`this.svgMapObj :`);
    // console.log(this.mapObj);

    this.holdArray = this.completeRoute.topo.holds;
    this.wallAngle = this.completeRoute.wallAngle;


    if(this.completeRoute.pushKey == null){
      this.pushKey = "routeInConstruction";
      this.divID = this.pushKey;
    }
    else{
      this.pushKey = this.completeRoute.pushKey;
      this.divID = this.pushKey;
      // console.log("divID:", this.divID);

      if(this.displayMode === "complete"){
        this.divID += 'details';
        // console.log("divID ammended:", this.divID);
      }
    }

    //for some use cases (route editor) imgURL is undefined, so gets appropriate image from completeRoute object
    if(this.imgURL !== undefined){
      this.photoSrc = this.imgURL;
    }else{
      //QUESTION: what is this.completeRoute.topo.photos[0]?
      this.photoSrc = this.completeRoute.topo.photos[0];
    }
    // TODO get consensus grade

    if(this.displayMode === "complete" || this.displayMode === "complete-map"){
      // console.log("display mode is:", this.displayMode);
      this.showComplete = true;
      // this.authorName = this.authorName;
      this.authorName = this.completeRoute.authorName;
      this.authorMug = this.completeRoute.hasOwnProperty('authorMug') ?
                       this.completeRoute.authorMug : null;
      this.routeName = this.completeRoute.name;
      this.grade = this.completeRoute.grade;
      this.consensusGrade = this.grade // <-- HACK (temp)
      this.location = this.completeRoute.location;
      this.description = this.completeRoute.description;

      //if(typeof this.completeRoute.features === )
      // console.log("typeof features:", typeof this.completeRoute.features);
      // console.log("features is array:", Array.isArray(this.completeRoute.features));
      if(Array.isArray(this.completeRoute.features)){
        for(let feature of this.completeRoute.features){
          // console.log("feature:", feature);
          this.features.push(feature);
        }
      }
      else{
        for (var property in this.completeRoute.features) {
          if (this.completeRoute.features.hasOwnProperty(property)) {//pretty sure this always evaluates to true.
            this.features.push(property);
          }
        }
      }

      if(this.displayMode === "complete-map"){
        // console.log('************* route-display (component), ngAfterContentInit, this.mapObj is:  ');
        // console.log(this.mapObj);


        // console.log('*********** route-display (component), ngAfterContentInit, setting showMap = true:  ');

        this.hideRoute = false;
        this.showMap = true;
      }
    }

    // determine the aspect ratio of the div to fill the space with
    // the largest size image possible while retaining the proper aspect ratio.
    let divAspect = this.divWidth / this.divHeight;


    let sfx = this.photoSrc.slice(this.photoSrc.length - 6, this.photoSrc.length -4);

    if(sfx === landscapeSuffix){
      // if the div aspect is less than the constant aspect,
      // then it is taller than it is wide, so fill the width, scale the height.
      if(divAspect < lAspectRatio){
        console.log("*&*&*&*&*&*&*&*&*&* IF &*&*&*&*&*&*&*&*&*&*&*&");
        this.svgWidth = this.divWidth;
        this.svgHeight = this.svgWidth / lAspectRatio;
      }

      // other wise, it is wider than tall, so fill the height, scale the width.
      else{
        console.log("*&*&*&*&*&*&*&*&*&* ELSE &*&*&*&*&*&*&*&*&*&*&*&");
        this.svgHeight = this.divHeight;
        this.svgWidth = this.svgHeight * lAspectRatio;
      }

      // because it is in landscape, the constant height is the width-
      // scale is determined from dividing the div's width w/ the photo's.
      this.scaleHoldsBy = this.svgWidth / photoHeight;
    }

    // same logic as above, but for portrait.
    //TODO: set pAspectRatio using image dimensions (look at global constants to
     // ...see how it is calculated)
    else{
      if(divAspect < pAspectRatio){
        this.svgWidth = this.divWidth;
        this.svgHeight = this.svgWidth / pAspectRatio;
      }
      else{
        this.svgHeight = this.divHeight;
        this.svgWidth = this.svgHeight * pAspectRatio;
      }

      this.scaleHoldsBy = (this.svgWidth / photoWidth);
    }


    // this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;

  }

  ngAfterViewInit(){
    this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;
    // console.log('******** route-display, topoContainer.nativeElement.getBoundingClientRect(): ***********');
    // console.log(this.topoContainer.nativeElement.getBoundingClientRect());

    const topoContDivBox: {width:number, height:number } = this.topoContainer.nativeElement.getBoundingClientRect();
      this.topoContainerDimensions= {
        x: topoContDivBox.width,
        y: topoContDivBox.height
      };

    //thie map didn't know how big it's container would be. Now it does.
    this.mapObj.containerDimensions = this.topoContainerDimensions;

    this.drawHoldArray();
    // this.setClassVariables();
    // console.log(`route-display(component), ngAfterViewInit{}, this.topoDimensions  : `);
    // console.log( this.topoDimensions);
    this.init(this.topoContainerDimensions,this.topoDimensions);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.drawHoldArray();
  }

  toggleMapView(){
    //TODO: figure out how to fire "focusOnPoint" from the map component
    console.log('TOGGLE MAP VIEW!!!');
    this.hideRoute = !this.hideRoute;
    if(this.currentRoute$ && this.completeRoute){
      this.currentRoute$.next(this.completeRoute);
    }

  }

  flipCard(){
    this.hideRoute = !this.hideRoute;
  }

  drawHoldArray = () => {
    //this.clearSVG(this.divID);
    this.svgDiv = document.getElementById(this.divID);
    if(this.svgDiv && this.holdArray && this.holdArray.hasOwnProperty('length')){

      for (let i = 0; i < this.holdArray.length; i++){
        let color = "green";
        if(this.holdArray[i].color == State.Middle){
          color = "yellow";
        }
        else if(this.holdArray[i].color == State.Finish){
          color = "red";
        }
        // let cx = this.holdArray[i].pointCor.x;
        // let cy = this.holdArray[i].pointCor.y;
        // let cr = this.holdArray[i].radius;

        this.scaleHoldsBy = this.topoContainerDimensions.x / this.svgWidth;
        // const xScale = this.topoContainerDimensions.x / this.svgWidth;
        // const yScale = this.topoContainerDimensions.y / this.svgHeight;

        // let cr = this.holdArray[i].radius * this.scaleHoldsBy;

        // let cx = this.holdArray[i].pointCor.x - ((320 - 270)/2.0)*this.scaleHoldsBy;
        let cx = this.holdArray[i].pointCor.x;
        let cy = this.holdArray[i].pointCor.y;
        let cr = this.holdArray[i].radius


        // //
        // let cx = this.holdArray[i].pointCor.x * this.scaleHoldsBy;
        // let cy = this.holdArray[i].pointCor.y * this.scaleHoldsBy;
        // let cr = this.holdArray[i].radius * this.scaleHoldsBy;

        let svgNamespace = "http://www.w3.org/2000/svg";
        let circle = document.createElementNS(svgNamespace,'circle');

        circle.setAttributeNS(null, 'cx', cx.toString());
        circle.setAttributeNS(null, 'cy', cy.toString());
        circle.setAttributeNS(null, 'r', cr.toString());
        circle.setAttributeNS(null, 'style', 'fill: none; stroke: ' + color + '; stroke-width: 2px;');
        circle.setAttributeNS(null, 'class', 'CIRCLE');
        circle.setAttributeNS(null, 'id', 'hold');

         if(this.svgDiv != null){
           this.svgDiv.appendChild(circle);
         }

         else{

           // console.log("svgDiv is null!");
         }
       }
    }
   }

   clearSVG = (routeID: string) => {

     let rid = routeID;
     let oldHolds = document.getElementById("hold");
     if(oldHolds != undefined && oldHolds.parentElement.id == rid){
       oldHolds.parentNode.removeChild(oldHolds);
       this.clearSVG(rid);
     }
     else{
       return;
     }
   }



}


enum State{
  Start = 1,
  Middle,
  Finish
}
