import { Component, ViewChild, ContentChildren, ElementRef, QueryList} from '@angular/core';
import { Platform,
  Content,
  IonicPage,
  NavController,
  NavParams,
  Events,
  Alert,
  AlertController,
  Loading,
  LoadingController,
  ToastController,
  Footer
 } from 'ionic-angular';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';
//import { AngularFireAuth } from 'angularfire2/auth';
import { AuthProvider } from '../../providers/auth/auth';
import { UserDetailsProvider } from '../../providers/user-details/user-details';
import { NewRouteUtilitiesProvider } from '../../providers/new-route-utilities/new-route-utilities';
import { Location } from '../../providers/filtered-routes/filtered-routes';
import { HoldObject,
  RouteObject,
  SvgMapObject,
  ContainerDimensions,
  MapDimensions } from '../../providers/globalConstants/globalConstants';

import { SvgRouteComponent } from '../../components/svg-route/svg-route';
import { MapsProvider, Maps } from '../../providers/maps/maps';
import { getImageDimensions } from '../../providers/image/image';
import { SvgMapComponent } from '../../components/svg-map/svg-map';

import { User } from 'firebase/app';

import { RouteFeatures2Component } from '../../components/route-features2/route-features2';


@IonicPage()
@Component({
  selector: 'RouteEditorPage',
  templateUrl: 'route-editor.html',
})

export class RouteEditorPage {

  //@ViewChild('Content') content;
  @ViewChild(SvgRouteComponent) private svgRoute: SvgRouteComponent;
  @ViewChild(SvgMapComponent) private svgMap: SvgMapComponent;
  @ViewChild(RouteFeatures2Component) private routeFeatures: RouteFeatures2Component;
  // @ViewChild("footerContainer") public footerContainer: ElementRef;

  // @ContentChildren("content") private content: Content;

  // switches for the components
  private showCamera: boolean = true; // do we need to take a picture?
  private showEditor: boolean = false; // show editor bar?
  private showMap: boolean = false; // show map component
  private floorNum: number;
  private showFeatures: boolean = false; // show features component
  private showDisplay: boolean = false;

  //when buttons are pressed once (or confirmation popups are displayed) disable
  //appropriate button
  private disableBackButton: boolean = false;
  private disableForwardButton: boolean = false;

  //map stuff
  public svgMapObj:SvgMapObject;
  public mapDimensions:MapDimensions = null;
  public mapContainerDimensions:ContainerDimensions = null;
  public mapContents:string = null;

  //SvgRouteComponent stuff
  public svgRouteContainerDimensions: ContainerDimensions = null;
  public footerHeight:number = null;
  public footerHeightToPlatformHeightRatio = 1.2/10.0;


  // bools for the progress bar
  private atMap: boolean = false;
  private atFeatures: boolean = false;
  private atDisplay: boolean = false;
  private useDefault: boolean; // temp, for testting
  private svgState: string = "secondary" // used for toolbar indicating what state the svg-route component is in
  public photoSrc: any;

  //declared Hold interface and imported it from NewRouteUtilitiesProvider
  //if you decide to change the hold data structure, make sure you update that interface
  private rHoldArray: any[]=[];
  private mapMode: string = "edit";
  private routeLocation: Location = null;
  public features: any;
  public color: string;
  public setBy: string;
  public routeName: any = null;
  public routeGrade: any = null;
  public wallAngle: number = null;
  public routeDescription: string = null;
  // public routeFloor: any = null;
  private authorName: string;
  // note need ot integrate user provider and figure out if we are going to use the user id in the route object
  public user: User = null;
  //heads up: i declared an interface (NewRouteData) in NewRouteUtilitiesProvider
  //and imported it
  public routeData: RouteObject;
  public routeDisplayMode: string = "complete-map";

  //NOTE temp vars for testing only
  //These are for displayin the route topo... don't think they're used by the map
  public displayWidth: number = 360; //<--these match the values programatically 
  public displayHeight: number = 615; //...generated in the detail page

  public loading:Loading;
  public unregisterBackButtonAction: any;
  public unregisterBackButtonActionCounter: number =0; //testing only: how many times is it registered?

  constructor(
    public newRouteUtils: NewRouteUtilitiesProvider,
    public authProv: AuthProvider,
    public userDetailsProv: UserDetailsProvider,
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    private androidFullScreen: AndroidFullScreen,
    public platform: Platform,
    private alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
    public mapsProvider: MapsProvider) {
      platform.ready().then(( )=>{
        this.user = this.authProv.getUser();
        this.floorNum = this.navParams.get('floorNum');
        this.mapsObj = this.navParams.get('mapsObj') as Maps;

        this.footerHeight = platform.height() * this.footerHeightToPlatformHeightRatio;

        this.mapContainerDimensions = {
          x: this.platform.width(),
          y: this.platform.height() - this.footerHeight
        }


        let newSvgMapObj:SvgMapObject = {
          mode: this.mapMode,
          mapDimensions: this.mapsObj.mapByFloor[this.floorNum].dimensions,
          mapContents: this.mapsObj.mapByFloor[this.floorNum].mapContents,
          routes: null,
          selectedRoute: this.navParams.get('routeSummary'),
          newRoute: null,
          containerDimensions: null
        }

        this.svgMapObj = newSvgMapObj;
        this.mapDimensions = this.svgMapObj.mapDimensions;

        this.mapContents = this.svgMapObj.mapContents;

      }).catch((error)=>{
          //handle error
          // console.log('*********route-editor constructor error ***************');
          // console.log(error);
          // throw(error);
      });
    }
    //TODO: What the hell is this doing down here (public mapsObj:Map):
    public mapsObj: Maps;






  passPhotoSrc(photo){
    // console.log('route-editor (page), passPhotoSrc: setting showCamera=false');
    this.photoSrc = photo;

    this.svgRouteContainerDimensions = {
      x: this.platform.width(),
      y: this.platform.height() - this.footerHeight
    };

    this.showCamera = false;
    this.showEditor = true;
  }

  // addingFooterToDom(){
  //   if(!this.showCamera){
  //     console.log('************* addingFooterToDom *****************');
  //     return true;
  //   }
  //
  // }

  //
  // updateFooter():Promise<boolean>{
  //   console.log('************* Footer Ready ****************');
  //   return new Promise((res,rej)=>{
  //   });
  // }

  passHoldInfo(holds:HoldObject[]){
    this.rHoldArray = holds;
    this.showEditor = false;
    this.showFeatures = true;
  }



  passFeatureInfo(featureInfo){
    // console.log("rHoldArray", this.rHoldArray);
    let newHoldArray:HoldObject[] = [];
    for( let i = 0; i < this.rHoldArray.length; i++){
      let newHoldObject = {
        color: this.rHoldArray[i].Color,
        pointCor: this.rHoldArray[i].pointCor,
        radius: this.rHoldArray[i].Radius
      }
      newHoldArray.push(newHoldObject);

    }
    // console.log("newHoldArray", newHoldArray);

    this.routeName = featureInfo['name'];
    this.routeGrade = featureInfo['grade'];
    this.wallAngle = featureInfo['wallAngle'];
    this.routeDescription = featureInfo ['description'];
    this.setBy = featureInfo.setBy;

    if(featureInfo.color){
      this.color = featureInfo.color;
    }

    //console.log("featurinfo:", featureInfo);
    this.features = featureInfo['features'];

    //this.routeName = this.features[0].routeName;
    //this.routeGrade = this.features[0].routeGrade;
/*
    console.log('this.features before splice');
    console.log(this.features);
    //this.features.splice(0, 1);
    console.log('this.features after splice');
    console.log(this.features);
*/


    this.routeData  = {
      pushKey: null,
      setBy: this.setBy,
      //the name of the route
      name: this.routeName,
      // NOTE Hold Arrays is just one array of holds(not an array of arrays)
      topo: {photos: [this.photoSrc], holds: newHoldArray},
      authorName: this.authorName,
      authorId: this.user.uid,
      grade: this.routeGrade,
      consGrade: this.routeGrade,
      location: null,
      mapId: null,
      features: featureInfo['features'],
      floor: this.floorNum,//this.routeFloor,
      wallAngle: this.wallAngle,
      description: this.routeDescription,
    }

    if(this.setBy==="setter"){
      this.routeData['color']=this.color;
    }

    this.showFeatures = false;

    //TODO: remove this and test (this is now done in platform.ready().then)
    this.mapContainerDimensions = {
      x: this.platform.width(),
      y: this.platform.height() - this.footerHeight
    }

    this.showMap = true;
    this.showMapInstructions();

    // console.log('route-editor.ts, passFeatureInfo, this.routeData is:');
    // console.log(this.routeData);
    //
    // console.log('route-editor.ts, passFeatureInfo, this.mapContents is:');
    // console.log(this.mapContents);
    //

  }

  showMapInstructions(){
    let toast = this.toastCtrl.create({
          message: 'Tap Map To Set Location',
          duration: 3000,
          position: 'middle',
          showCloseButton:true
        });

        toast.onDidDismiss(() => {
          // console.log('Dismissed toast');
        });

        toast.present();
      }

  passLocationInfo(mapData: Location){
    this.routeLocation = mapData;
    this.routeData.location = this.routeLocation;
    this.svgMapObj.newRoute = this.routeLocation;
    // this.svgMapObj.routes = [this.routeData]
    // this.svgMapObj.selectedRoute = this.routeData
    this.showMap = false;

    // console.log("PPPPPPPPP route-editor, passLocationInfo PPPPPPPPPPPPPPPP");
    // console.log('svgRouteContainerDimensions')
    // console.log(this.svgRouteContainerDimensions);
    // console.log('mapContainerDimensions');
    // console.log(this.mapContainerDimensions);

    //HACK: this probably should not happen here.
    //The route-display component depends on routeData.topo.holdsDims
    //this was (and still is) happening in tbFinished, but that's too late
    getImageDimensions(this.photoSrc).then((photoDim)=>{
      this.routeData.topo.holdsDims = [photoDim];
      this.showDisplay = true;
    });

  }


  defaultImg(){
    this.useDefault = true;
    this.showCamera = false;
    this.showEditor = true;
    this.events.publish('defaultImg', this.useDefault);
  }

  ionViewDidLoad() {

    this.userDetailsProv.getUserDetails().then((details) => {
      this.authorName = details.displayName;

    });

    // if full screen is supporte

  /*
    this.androidFullScreen.isImmersiveModeSupported()
    // then go full screen
    .then(() => this.androidFullScreen.immersiveMode())
    .catch((error: any) => console.log(error));
    */
  }

  ionViewDidEnter() {
    this.initializeBackButtonCustomHandler();
  }

  ionViewWillLeave(){
    // exit full screen
    this.androidFullScreen.showSystemUI();
    this.unregisterBackButtonAction && this.unregisterBackButtonAction();
  }

  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonActionCounter += 1;
    // console.log("*************** route-editor (page) registerBackButtonAction called {this.unregisterBackButtonActionCounter} times ***************** ");
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
        this.customHandleBackButton();
    }, 10);
  }

  private customHandleBackButton(): void {
    const exitEditorAlert:Alert = this.alertCtrl.create({
      message: "Are you sure you want to exit the route editor? Your route will be discarded...",
      buttons: [
        {
          text: "Exit",
          handler: () => {
            this.navCtrl.pop();
          },
        },
        {
          text: "Cancel",
          role: 'cancel',
        }
      ]
    });
    // if()
    exitEditorAlert.present();
  }

  tbGoBack(){
    //TODO: Remove this. Just for testing.
    // this.disableBackButton = true;
    if(this.showEditor){
      this.showEditor = false;
      this.showCamera = true;
    }
    else if(this.showFeatures){
      this.showFeatures = false;
      this.showEditor = true;
    }
    else if(this.showMap){
      this.showMap = false;
      this.showFeatures = true;
    }
    else{
      this.showDisplay = false;
      this.showMap = true;
    }

    this.updateProgress();

  }

  tbGoForward(){
    if(this.showEditor){
        this.disableForwardButton = true;
        this.disableForwardButton = !!this.svgRoute.tbDoneSetting();
    }
    else if(this.showFeatures){

      this.disableForwardButton = true;
      this.disableForwardButton = !!this.routeFeatures.tbDoneSetting();
    }
    else if(this.showMap){

      //CAUTION: slectLocation leads to the map component emitting a location,
      //so this may be setting up a race condition (though it probably doesn't matter)
      // this.disableForwardButton = true;
      // this.disableForwardButton = !!this.svgMap.selectLocation();
      this.svgMap.selectLocation();

    }
    this.updateProgress();
  }

  tbSVGUndo(){
    this.svgRoute.tbUndo();
  }

  tbSVGDelete(){
    this.svgRoute.tbDeleteHold();
  }

  tbSVGColor(){
    this.svgRoute.tbChangeState();

    if(this.svgState === "secondary"){
      this.svgState = "yellow";
    }
    else if(this.svgState === "yellow"){
      this.svgState = "danger";
    }
    else{
      this.svgState = "secondary";
    }
  }

  async tbFinished(){
    //if user has an account and is logged in...
    if(this.user && this.user.uid != null && this.user.hasOwnProperty('isAnonymous')
       && !this.user.isAnonymous){


      //the user has an account and is signed in
      //send route data to new route utilities for uploading or storing
      /*
      console.log('In tbFinished and mapsObj is:');
      console.log(this.mapsObj);
      */
      // console.log('route-editor.ts,tbFinished,  this.routeData is:');
      // console.log(this.routeData);

      //the database needs features to be loaded as key-value pairs such as : {'pumpy':true}
      let featuresAsObject: {[key:string]:boolean} = null;

      if(this.routeData.hasOwnProperty('features') && this.routeData.features.length > 0){

        featuresAsObject = {};
        this.routeData.features.forEach(attribute => {
          featuresAsObject[attribute] = true;
        });
      }

      this.routeData['features'] = featuresAsObject;

      this.routeData.authorId = this.user.uid;
      this.routeData.mapId = this.mapsObj.mapId;
      this.routeData.setBy = this.setBy;

      //if it's an official route by the setter team, assign it it's color
      if(this.setBy === 'setter'){
        this.routeData.color = this.color;
      }

      const photoDim = await getImageDimensions(this.photoSrc);
      this.routeData.topo.holdsDims = [photoDim];

      let finished = false;
      this.loading = this.loadingCtrl.create();
      this.loading.present();

      // the below is an alert suggesting that the user turn on wifi if the route has not
      // uploaded after 5 seconds...
      //TODO: routes can fail to upload for a lot of reasons (improper format, incompatibility with rules on firestore, etc...)
      //This logic should actually check the connection provider before telling the user to connect to the internet.
      //maybe if there's no connection, set an event subription that listens for the connection event and then tries to upload the route.
      //if there's still no connection as they're leaving the page, unsubscribe that callback and then pass the route to the offline routes system.
      //(I'm mentioning this because I'm trying to debug why route's don't upload for facebook login... the message on the screen says there's no
      // internet connection ...from the alert below, it was really confusing because there's def wifi turned on. It's just poping up because
      // that's what happens when the route doesn't upload for any reason... I'm still not sure what the reason is.)
      setTimeout(() => {
        if(!finished){
          // console.log("timeout!");
          let alert = this.alertCtrl.create({
            title: 'Connection Issues...',
            subTitle: "We are having trouble uploading your route... Please try connecting to wifi or ensuring you have access to the internet.",
            buttons: [{
              text: 'Dismiss',
            }]
          });
          alert.present();
        }
      }, 20000);

      // NOTE: below line for testing.
      //this.newRouteUtils.handleNewRoute(this.routeData, false).then((message) => {
      this.newRouteUtils.handleNewRoute(this.routeData).then((message) => {
        this.loading.dismiss();
        finished = true;
        let alert = this.alertCtrl.create({
          title: 'Finished',
          subTitle: message,
          buttons: [{
            text: 'Dismiss',
            handler: () => { this.navCtrl.pop();}
          }]
        });
        alert.present();
      }).catch((error) => {
        this.loading.dismiss();
        finished = true;
        let alert = this.alertCtrl.create({
          title: 'Finished',
          subTitle: error,
          buttons: [{
            text: 'Dismiss',
            handler: () => { this.navCtrl.pop();}
          }]
        });
        alert.present();
      });
      //this.navCtrl.pop();

    }
    else{
      // console.log("eror w/ user");
      console.log(this.user);
      //hold their new route hostage until they sign up or sign in
    }
  }

  updateProgress(){
    if(this.showFeatures){
      this.atFeatures = true;
      this.atMap = false;
      this.atDisplay = false;
    }
    else if(this.showMap){
      this.atFeatures = true;
      this.atMap = true;
      this.atDisplay = false;
    }
    else if(this.showDisplay){
      this.atFeatures = true;
      this.atMap = true;
      this.atDisplay = true;
    }
    else{
      this.atFeatures = false;
      this.atMap = false;
      this.atDisplay = false;
    }
  }
}
