import { Injectable } from '@angular/core';
import { AuthProvider } from '../../providers/auth/auth';
import * as firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';

//The data available in firestore at USERS/{userId}
//TODO: Switch all imports to get User interface from globalConstants, rather than here.
export interface User{
  displayName: string,
  photoURL: string

}

@Injectable()
export class UserDetailsProvider {

  userDetails: any = null;
  activeRefs: any[];
  userDocData: User;

  constructor(public authProv: AuthProvider, public afs:AngularFirestore ) {}


  getUserDetails = (uid:string = null):Promise<User> => {
    return new Promise((res,rej)=>{
      let currentUid = uid? uid: this.authProv.getUser().uid;

      //current details exist and are for the correct user?
      if (this.userDetails  && currentUid === this.userDetails.uid ){
        res(this.userDetails);
      }

      else{
        if(currentUid){//there's a user Id, so try to get their details
          let db = firebase.firestore();
          let docRef = db.collection("USERS").doc(`${currentUid}`);
          docRef.get().then((doc) => {
            if(doc.exists){
              let userDoc = doc.data() as User;
              res(userDoc) ;
            }
          }).catch((error) => {
            console.log("Error getting document:", error);
            rej(error);
          })
        }else{//there's no currentUser
         rej(new Error("user details do not exist for "+ currentUid));
        }


      }
    });
  }

  // fillDetails = (id:string, data:User): uDetails => {
  //   let uD = { uid: id, dispName: data.dispName};
  //   return uD;
  // }


  killAllRefs = () => {
    if (this.activeRefs !== null && this.activeRefs.length > 0){
      for ( let ref of this.activeRefs){
        ref.off();
      }
    }
  }
}

interface uDetails{
  uid:string;
  displayName:string;
}
