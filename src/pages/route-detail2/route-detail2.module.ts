import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RouteDetail2Page } from './route-detail2';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    RouteDetail2Page,
  ],
  imports: [
    IonicPageModule.forChild(RouteDetail2Page),
    ComponentsModule
  ],
})
export class RouteDetail2PageModule {}
