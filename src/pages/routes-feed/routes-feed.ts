// Platform and External imports
import { Component,
         ViewChild,
         ElementRef } from '@angular/core';
import { IonicPage,
         Platform,
         NavController,
         NavParams,
         Events,
         Slides,
         Alert,
         AlertController,
         ToastController,
         LoadingController,
         Content,
         MenuController,
         // Header,
         reorderArray } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { AngularFireDatabase } from '@angular/fire/database';
//TODO: use firebase import (commetned below) instead of AngularFireDatabase, remove AF database
//import * as firebase from 'firebase/app';
//IMPORTANT: DO NOT import observable from rxJs/Rx
import { Observable } from 'rxjs/Observable';
import { from } from 'rxjs'
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
// import { map } from 'rxjs/operators/map';
import { combineLatest } from 'rxjs/operators/combineLatest';
import * as firebase from 'firebase/app';

// Component Imports
import { SvgMapComponent } from '../../components/svg-map/svg-map';
import {ListViewComponent, RouteSelectedData } from '../../components/list-view/list-view';
import { FloorToggleComponent } from '../../components/floor-toggle/floor-toggle';

// Provider imports
import { SettingsProvider } from '../../providers/settings/settings';
import { UserDetailsProvider } from '../../providers/user-details/user-details';
import { ConnectionProvider } from '../../providers/connection/connection';
// import { FeedProvider, RouteData } from '../../providers/feed/feed';
import { SendProvider, Send} from '../../providers/send/send';

import { MapsProvider, Maps  } from '../../providers/maps/maps';
import { AuthProvider } from '../../providers/auth/auth';
import { GymsProvider } from '../../providers/gyms/gyms';
import { FilteredRoutesProvider, FireStoreObservableRoute } from '../../providers/filtered-routes/filtered-routes';
import { FeedCache4Provider } from '../../providers/feed-cache4/feed-cache4';
import { NewRouteUtilitiesProvider } from '../../providers/new-route-utilities/new-route-utilities';
import { ListsProvider } from '../../providers/lists/lists';
import { betterConsole,
         // convertGradeToColorCode,
         // colorNamesToColorCodes,
         ContainerDimensions,
         MapDimensions,
         listDisplayWidth,
         listDisplayHeight,
         routeObject }  from '../../providers/globalConstants/globalConstants';

let console = betterConsole;


@IonicPage()
@Component({
  selector: 'page-routes-feed',
  templateUrl: 'routes-feed.html',
})

export class RoutesFeedPage {
  // @ViewChild('header') header:Header;
  @ViewChild('topHalf') topHalfDiv:ElementRef;

  //TODO: delete following line (svg map component... do not appear to use it)
   @ViewChild('svgMapComponent') SvgMapComponent: SvgMapComponent;

   @ViewChild(Slides) slides: Slides;
   //TODO: content does not appear to be used! Comment out, test, then delete.
   @ViewChild('content') content: Content;
  // connected:boolean;

  //TODO: delete initCounter
  // initCounter: number = 0; //use for debugging init process. Init is being called too many times, causing weird page reload bugs
  // setListCounter: number = 0; //similar to above
  listsInitCounter:number = 0;// similar to above

  numRoutes:Subscription = null; //any = null;
  index$: BehaviorSubject<number> = new BehaviorSubject(0);
  selectedRoute$:Observable<routeObject>

  routeIdToSend$: Observable<{[key:string]:Send}> = null;
  routeIdToSends: {[key:string]:Send};
  routeIdToSendsSubscrip: Subscription ;

  lastRouteViewed: routeObject  = null;

  notifications: any[];
  gymName:string = null;
  rate:number = 3;

  mapMode:string = 'navigate';
  mapDimensions: MapDimensions;
  mapContainerDimensions:ContainerDimensions;

  listDisplayDimensions:{height:number,width:number}; //passed to list-view component for setting size of imbedded display component

  mapContents: string = null; //The inner-svg tags of the map: <svg ...attributes> mapContents </svg>

  mapDepReady: boolean = false; //toggle to true when all of the map's dependencies are ready

  selectedRoute:any;//the data of the route to be highlighted on the map
  // selectedRouteImage:string;

  headerHeight = null;

  fstoreObsReady:boolean = false;//toggle inclusion of fstoreObs-dependant elements in template


  mapsObj: Maps;
  currentFloor:number;
  setStorageRoutes: ()=>void;
  routesReady: boolean = false;
  testData: any = null;
  platformLoaded: boolean = false;
  offlineRoutesExist: boolean = false;
  listView: boolean = false;

  // currentRoutes: routeObject[];
  currentFilteredRoutes:routeObject[];
  currentUnfilteredRoutes:routeObject[];

  public unregisterBackButtonAction: Function = null;
  private backButtonPressedToExit: boolean = false;
  public selectedList: string = "All Routes"
  public listNameArr: string[] = [];
  public listArr$:Observable<routeObject[]> = null;
  public currentListPushKeyArr: string[] = [];
  // private initialized:boolean = false;
  public listOptionsToggle: boolean = false;// only show list optioins if they are editing a list.
  public showListOptions: boolean = false; //only show ability to show list options if it is a custom list.
  public hasReordered: boolean = false;
  public localListDisplayWidth: number = 0;
  public localListDisplayHeight: number = 0;
  public slideDisplayWidth: number = 0;
  public slideDisplayHeight: number = 0;
  //public list$: any = null;
  passedNavGuard: boolean = false;
  colorNamesToColorCodes: {[key:string]:string};
  // convertGradeToColorCode: (grade:number) => string;

  reloadRoutesOnDidEnter: boolean = false; //HACK: toggle to true when push settingsPage or filtersPage onto nav stack.
                                          //purpose: if user goes to settingsPage of filetersPage, they may have changed the
                                          //gyms or filters, so need to update (reload) the routes in the feed.
  public onConnectCallback: ()=>void;//if have not set routesList from fb (no listeners set up), do so.

  //Cleanup callbacks
  prepareToExit: () => Promise<any> = null; //the 'exit:prepare' callback for unsubscribing purposes
  cleanUpBeforeLogout: ()=> Promise<any> = null; //the 'cleanup:prepare' callback (Unsubscribes from stuff)

  fsObs:FireStoreObservableRoute = null;

  routes$:BehaviorSubject<routeObject[]> = null;
  scrollTop$:BehaviorSubject<number> = null;

  // mode:string = 'slider';
  sKey:string = 'slide';
  menuEventsSubscriptions:{[key:string]: any} = null; //key:name of event to subscribe to, value: the callback that is triggered by the subscriptio n

  changingFloorNow:Boolean=false;

  //TODO: delete AngularFireDatabase import: it's not used in this page

  constructor(
    // Platform / External imports
    public navCtrl: NavController,
    public menu: MenuController,
    public navParams: NavParams,
    public afDatabase: AngularFireDatabase,
    public platform: Platform,
    public events: Events,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,

    // Custom Providers
    // public routesProv: FeedProvider,
    public settingsProv: SettingsProvider,
    public sendProv: SendProvider,
    public authProv: AuthProvider,
    public connection:ConnectionProvider,
    public filteredRoutesProv: FilteredRoutesProvider,
    public maps: MapsProvider,
    public gymsProv: GymsProvider,
    public feedCache: FeedCache4Provider,
    public nruProv: NewRouteUtilitiesProvider,
    public listsProv: ListsProvider,
    private el: ElementRef
    ) {
      this.localListDisplayWidth = listDisplayWidth;
      this.localListDisplayHeight = listDisplayHeight;
      // console.log("routes-feed constructor");
      // console.log("platformLoaded:", this.platformLoaded);
  }

  //check that user is logged in (authProv.getUser) and has a valid gym set (settingsProv.getGymId)
  //if not,
  async rerouteIfNecessary():Promise<boolean>{
    // console.log('rerouteIfNecessary');
    let user = null;
    let gymId = null;
    // console.log('waiting for platform.ready');
    try{
      await this.platform.ready();
      user = this.authProv.getUser() as firebase.User;
      // console.log('routes-feed.ts, rerouteIfNecessary got user as:');
      // console.log(user);
      gymId = await this.settingsProv.getGymId();
      // console.log('routes-feed.ts, rerouteIfNecessary got gymId as:');
      // console.log(gymId);

      if(!gymId){
        throw new Error('rerouteIfNecessary: got gymId as: ' + gymId + '...throwing error to trigger reroute to settings page');
      }

      if(!user){
        throw new Error('rerouteIfNecessary: user was falsey!!!!! ...throwing error to trigger reroute to login page');

      }

      //this return value (true) is assigned to the local variable, finishedRerouting, after this function finishes executing
      //this happens in ionViewDidLoad and ionViewDidEnter
      if(user && gymId){
        return true;
      }

    }catch(error){
      //erro came from either: getting user or getting gymId, so reroute to appropriate page
      console.log('error in rerouteIfNecessary:');
      console.lie(error);
      if(!user){
        this.navCtrl.push('LoginPage').then(()=> {return false});
      }
      if(!gymId){
        this.reloadRoutesOnDidEnter = true;
        this.navCtrl.push('SettingsPage').then(()=> {return false});
      }
      else{
        // a catch for unknown errors, such as being offline, etc
        return true;
      }
    };
  }

  async ionViewDidLoad(){
    //this fires once, when the routes-feed page first loads (not when navigating back to this page)
    // await this.rerouteIfNecessary();

    console.log('routes-feed (page), ionViewDidLoad, topHalfDiv height, width:')
    console.log(this.topHalfDiv.nativeElement.offsetHeight, this.topHalfDiv.nativeElement.offsetWidth);

    console.log('routes-feed (page), ionViewDidLoad, topHalfDiv getBoundingClientRect:')
    console.log(this.topHalfDiv.nativeElement.getBoundingClientRect());
    console.log("****************************************************************");


    this.slideDisplayWidth = this.el.nativeElement.offsetWidth; // /3

    this.slideDisplayHeight = this.el.nativeElement.offsetHeight // /4;

    this.listDisplayDimensions = {
      height:this.el.nativeElement.offsetHeight,
      width:this.el.nativeElement.offsetWidth };

    let finishedRerouting = false;
    finishedRerouting = await this.rerouteIfNecessary().catch(error=>{
      console.log('************routes-feed, ionViewDidLoad: is this code unreachable???? ***********');
      finishedRerouting = false;
    }) as boolean;

    if(finishedRerouting){
      //unregisterBackButtonAction is only truthy after the custom back button handler is set
      if(!this.unregisterBackButtonAction){
        this.initializeBackButtonCustomHandler();
      }
      console.log('ionViewDidLoad: this.fsObs:');
      console.lie(this.fsObs);
      // console.log(this.routeArr$);
      if(!this.fsObs){
        console.log('***** routes-feed, ionViewDidLoad: calling this.init ');
        await this.init();

        this.scrollTop$ = new BehaviorSubject(0);

        this.reloadRoutesOnDidEnter = false;
        return null;
      }else{
        return null;
      }
    }
  }

  onPageScroll(e){
    this.scrollTop$.next(e.scrollTop);
  }

  async ionViewDidEnter(){
    console.log("ionViewDidEnter");

    //this fires when returning to this page
    //TODO: optimize this code so that it only runs the stuff in init() that has to be run
    // ...if mapsObject and floor did not change, then don't need to reload the routes (fro example)
    // await this.rerouteIfNecessary();

    let finishedRerouting = false;
    finishedRerouting = await this.rerouteIfNecessary().catch(error=>{

      console.log('************routes-feed, ionViewDidEnter: is this code unreachable???? ***********');
      finishedRerouting = false;
    }) as boolean;

    if(finishedRerouting){//=> user and gymId are set in their respective providers
      if(this.reloadRoutesOnDidEnter){//=> need to reload the routes in the feed because gymId or filters may have changed
        console.log("reloadRoutesOnDidEnter");

        //unregisterBackButtonAction is only truthy after the custom back button handler is set
        if(!this.unregisterBackButtonAction){
          this.initializeBackButtonCustomHandler();
        }

        // console.log(`IIIIIIIII route-feed (page), , ionViewDidEnter: calling this.init for the ${this.initCounter}th time IIIIIIIIII`);
        // this.initCounter += 1;
        /** QUESTION: should we call this.setList BEFORE calling this.init ?**/
        await this.init();


        // await this.setList(this.selectedList, true);

        // this.listArr$ = this.routeArr$;

        this.reloadRoutesOnDidEnter = false;
      }

      try{
        this.offlineRoutesExist = await this.nruProv.checkForOfflineRoutes();
      }
      catch(err){
        //catch
        throw err;
      }
      //TODO: consider triggering this checkForOfflineRoutes manually from another page where users review their cached routes
      if(this.connection.isConnected()){
        //TODO: get a reference to this callback and unsubscribe from it on app shutdown
        try{
          if(this.offlineRoutesExist){
            try{
              await this.nruProv.uploadOfflineRoutes().catch((err) => {
              });
              this.offlineRoutesExist = await this.nruProv.checkForOfflineRoutes();
            }
            catch(err){
              //catch
              throw err;
            }
          }
        }
        catch(err){
          //catch
          console.log("nruProv.checkForOfflineRoutes returned an error: ", err);
          throw err;
        }

      }else{
        //TODO: get a reference to this callback and unsubscribe from it on app shutdown
        this.events.subscribe( 'connected:true', async ()=> {
          try{
            await this.nruProv.uploadOfflineRoutes().catch((err) => {
              console.log("uploading offline routes returned an error:", err);
            });
            this.offlineRoutesExist = await this.nruProv.checkForOfflineRoutes();
          }
          catch(err){
            //catch
            throw err;
          }

        });
      }


      try{
        //
        // console.log(`LLLLLLLLL route-feed (page), , ionViewDidEnter (try): calling this.listsInit for the ${this.listsInitCounter}th time LLLLLLL`);
        // this.listsInitCounter += 1;
        // await this.listsInit();
      }
      catch(err){
        //catch
        console.log("problem w/ listsInit...", err);
        throw err;
      }
    }
  }
  //
  // async init2(){
  //   try{
  //     const gymId = await this.settingsProv.getGymId();
  //     this.mapsObj = await this.maps.getMaps(gymId);
  //
  //     this.colorNamesToColorCodes = this.mapsObj.hasOwnProperty('colors') ?
  //                 this.mapsObj.colors.colorNamesToColorCodes : null;
  //
  //     const settings = await this.settingsProv.getSettings();
  //     if (settings && settings.currentFloor
  //         && this.mapsObj.hasOwnProperty('mapByFloor')
  //         //TODO: reset to default floor if selected gym changed
  //         //for now, make sure that the current gym has the desired floor
  //         && this.mapsObj.mapByFloor.hasOwnProperty(settings.currentFloor)){
  //       this.currentFloor = settings.currentFloor;
  //     }else{
  //       this.currentFloor = this.settingsProv.setDefaultFloor(0); //use zeroth floor as default if cannot get currentFloor from settings
  //     }
  //
  //     this.mapContents = this.mapsObj.mapByFloor[this.currentFloor].mapContents;
  //     console.log("***********routes-feed (page), init2, this.mapContents: **********" );
  //     console.log(this.mapContents);
  //
  //     let fsObs = await this.filteredRoutesProv.init(this.mapsObj.mapId);
  //
  //     // this.routeArr$ = fsObs.filteredRoute$;
  //     // this.routeArr$ = fsObs.behaviorSubjFilteredRoute$;
  //   }
  //   catch(error){
  //   };
  // }


  /*
  responsibilities:
   -fetch gymId (settingsProv.gymId will be set if not already set)
   -set this.mapsObj and this.colorNamesToColorCodes
   -set this.currentFloor
   -set this.mapContents
   -set this.fsObs
   -set this.routeArr$
   -set this.numRoutes
   -set this.numRoutes
   -call feedCache.init()

  */
  async init(){
    try{
      const gymId = await this.settingsProv.getGymId();
      // console.log('feed page init: got gymId');
      this.mapsObj = await this.maps.getMaps(gymId);

      if(this.mapsObj.hasOwnProperty('colors')){
        this.colorNamesToColorCodes = this.mapsObj.colors.colorNamesToColorCodes;
      }

      // console.log('feed page init: got mapsObj');
      // console.log('route-feed (page), init: calling this.settingsProv.getSettings');

      let settings = await this.settingsProv.getSettings();

      // console.log('feed page init: got settings as ');
      // console.log(settings);

      if (settings && settings.currentFloor
          && this.mapsObj.hasOwnProperty('mapByFloor')
          //TODO: reset to default floor if selected gym changed
          //for now, make sure that the current gym has the desired floor
          && this.mapsObj.mapByFloor.hasOwnProperty(settings.currentFloor)){
        this.currentFloor = settings.currentFloor;
        // console.log('CORRECT SETTINGS!!!!!!!!!!!!!!!')
      }else{
        this.currentFloor = this.settingsProv.setDefaultFloor(0); //use zeroth floor as default if cannot get currentFloor from settings
        // console.log('WRONG SETTINGS!!!!!!!!!!!!!!!')
      }

      this.mapContents = this.mapsObj.mapByFloor[this.currentFloor].mapContents;

      // console.log('PPPPPPPPPPPPPP routes-feed (page), about to await this.filteredRoutesProv.init(this.mapsObj.mapId)');

      this.fsObs = await this.filteredRoutesProv.init(this.mapsObj.mapId);
      //
      // if (this.fsObs.hasOwnProperty('route$')){
      //    this.fsObs.route$.subscribe((r_list)=>{
      //      this.currentUnfilteredRoutes = r_list;
      //    } );
      // }
      //
      // if(this.fsObs.hasOwnProperty('filteredRoute$')){
      //   this.fsObs.filteredRoute$.subscribe((r_list)=>{
      //     this.currentFilteredRoutes = r_list;
      //   })
      // }
      //
      // TODO phase out this.routeArr$. Use this.fsObs.behaciorSubjectRoute$ instead
      // this.routeArr$ = this.fsObs.filteredRoute$;

      if(this.numRoutes){
        this.numRoutes.unsubscribe();
        this.numRoutes = this.fsObs.behaviorSubjFilteredRoute$.subscribe(r_list => r_list.length);
      }
      //
      // if(this.fsObs){
      //   // this.currentRoutes.unsubscribe();
      //   this.fsObs.behaviorSubjFilteredRoute$.subscribe((data)=> {
      //     //TODO: delete currentRoutes (using currentFilteredRoutes now)
      //     this.currentFilteredRoutes = data;
      //   });
      //
      // //TODO FIXME: this is wrong... should not always skip to start of feed.
      // this.index$.next(0);
      // //init is called when routeArr$ may have changed=> tell the feedCache which routes to work with
      //
      // }
      // console.log('*** routes feed page, about to call feedCache.init()');
      // console.log('in routesFeed page and this.feedCache.cacheTracker.cacheState.idToLoaded is:');
      // console.log(this.feedCache.cacheTracker.cacheState.idToLoaded );
      // console.log(' **********************')

      // console.log('feed page: subscribing to exit event');

      await this.feedCache.init(this.fsObs.behaviorSubjFilteredRoute$,this.index$);
      if(!this.cleanUpBeforeLogout){
        //the settings page will publish 'logout:prepare', triggering this cleanup function
        //All subscriptions should be unsubscribed from here
        this.cleanUpBeforeLogout = async ()=>{
          try{
            console.log('this.cleanUpBeforeLogout TRIGGERED!');
            /********* Cleanup Before Logging Out (put code here)*************/
            if(this.feedCache){
              await this.feedCache.shutDown();
            }
            if(this.routeIdToSendsSubscrip){
              this.routeIdToSendsSubscrip.unsubscribe();
              this.routeIdToSendsSubscrip = null;
            }

            if(this.menuEventsSubscriptions){
              Object.keys(this.menuEventsSubscriptions).forEach(event => {
                this.events.unsubscribe(event, this.menuEventsSubscriptions[event]);
              });
            }
            /********* END: Cleanup Before Logging Out   *********************/
            this.events.publish('logout:ready');
          }catch(error){
            throw(error);
          }
        }
        this.events.subscribe('logout:prepare', this.cleanUpBeforeLogout);
      }

      if(!this.prepareToExit){
        this.prepareToExit =  async ()=>{
          //console.log(`ABOUT TO UNLOAD!!! `);
          try{
            console.log('this.prepareToExit TRIGGERED!');
            if(this.feedCache){
              await this.feedCache.shutDown();
              // this.events.publish('exit:ready');
            }
            if(this.routeIdToSendsSubscrip){
              this.routeIdToSendsSubscrip.unsubscribe();
              this.routeIdToSendsSubscrip = null;
            }

            //unsubscribe from menu events
            if(this.menuEventsSubscriptions){
              Object.keys(this.menuEventsSubscriptions).forEach(event => {
                this.events.unsubscribe(event, this.menuEventsSubscriptions[event]);
              });
            }
            //TODO: move this.events.publish('exit:ready') down here?
            this.events.publish('exit:ready');
          }catch(error){
            throw(error);
          }
        }
        this.events.subscribe('exit:prepare',this.prepareToExit);
      }

      //See app.component.ts, "Menu Events" section for the events to subscribe to
      if(!this.menuEventsSubscriptions){
        //WARNING: the callbacks MUST use arrow functions so they do not lose context!!!
        const pushSettingsPageCallback = ()=>{this.goToSettings()};
        const pushFiltersPageCallback = ()=>{this.goToFilters()};
        const pushRouteEditorCallback = ()=>{this.goToRouteEditor()};


        this.events.subscribe('goToSettings', pushSettingsPageCallback);
        this.events.subscribe('goToFilters', pushFiltersPageCallback);
        this.events.subscribe("goToRouteEditor", pushRouteEditorCallback);


        //collect the subscription names and  callbacks here so the exit code can unsubscribe from them
        this.menuEventsSubscriptions = {
          'goToSettings': pushSettingsPageCallback,
          'goToFilters':pushFiltersPageCallback,
          'goToRouteEditor':pushRouteEditorCallback
        };

      }


      // if(this.routeArr$ && this.routeArr$.hasOwnProperty('pipe')){

      if(this.fsObs){
        // console.log('routes-feed (page), setting this.selectedRoute$');

      this.selectedRoute$ = <Observable<routeObject>>this.fsObs.behaviorSubjFilteredRoute$.pipe(
        combineLatest(this.index$, (array,i:number)=>{
          // console.log('CCCCC combine latest for selectedRoute$ CCCCCCC');
          if(array && array.hasOwnProperty('length') && i < array.length){
            this.selectedRoute = array[i];

            if(!this.mapDepReady){
              this.mapDepReady = true;
            }

            // console.log('emitting NEW SELECTED ROUTE');
            return array[i];
          }
      }));

    }
      //initialize idToSend$ observable
      // console.log('routes-feed, init. trying calling sendProv.initSends')
      this.routeIdToSend$ = await this.sendProv.initSends(gymId, this.mapsObj.mapId);

      if(this.routeIdToSend$){
        //unsubscribe before subscribing again (as might be necessary if gym, user or map changes)
        if(this.routeIdToSendsSubscrip){
          this.routeIdToSendsSubscrip.unsubscribe();
        }

        this.routeIdToSendsSubscrip = this.routeIdToSend$.subscribe(sends => {
          this.routeIdToSends = sends;
        });
      }

      // console.log("*&*&*&*&*&*& routes-feed init. setting fsroreObsReady = true;")
      // this.convertGradeToColorCode= convertGradeToColorCode;
      // this.testPrintFromRoutesObs(this.fsObs.filteredRoute$);
      // this.routes$ = new BehaviorSubject(this.currentFilteredRoutes);
      // this.routeArr$.subscribe(routes => this.routes$.next(routes));
      //
      // console.log('RRRRRRRRRRRRRRRRRRR Setting fstoreObsReady = true RRRRRRRRRRRRRRR')
      this.fstoreObsReady = true;


      let container = document.getElementById('map-container');
      let mapBox = container.getBoundingClientRect();

      //redundant? define this with @viewchild
      const slider = document.getElementById('slides-container');
      const sliderBox = slider.getBoundingClientRect();

      this.mapContainerDimensions = {
        x: this.platform.width(),
        y: sliderBox.top-mapBox.top
      };

      this.mapDimensions = this.mapsObj.mapByFloor[this.currentFloor].dimensions

      this.selectedRoute$.subscribe((route) => {
        if(route && !this.mapDepReady){
          this.mapDepReady = true;
        }
      });


    } catch(error){
      console.log('routes-feed init: ERROR:');
      console.lie(error);
      throw(error);
    }

    // this.initialized = true;

  }

  trackByPushKey(index, route){
    return route.pushKey;
  }

  showTimesSentAlert(send:Send){
    const sendsAlert:Alert = this.alertCtrl.create({
    message: `You sent this route ${send.numSends} time(s)!`,
      buttons: [
        {
          text: "OK",
          role: 'cancel'
        }
      ]
    });
    sendsAlert.present();
  }

  showMarkedAlert(){
    const markedAlert:Alert = this.alertCtrl.create({
      message: "This route has been marked for deletion. Unless the community objects, it will be deleted soon.",
      buttons: [
        {
          text: "OK",
          role: 'cancel'
        }
      ]
    });
    markedAlert.present();
  }

  //ASSUMPTION: all lists viewable in list-view component are ONLY relevant to the current gym,
  //so when we push the detail page, that page can load the current map of the current gym for
  //the floor of the route that is passed in
  listRouteSelected(data: RouteSelectedData){
    this.goToRouteDetailsPage2(data.route, this.feedCache )
  }

  // goToRouteDetailsPage(route:routeObject, imgCache:string){
  //   //To prevent a bug where clicks on the listView are triggering subsequent clicks on cards,
  //   //check to see if a timeout from the touch registered from the list click has been cleared.
  //     // console.log("mapsObj:", this.mapsObj);
  //     this.settingsProv.getSettings().then((settings)=>{
  //       //QUESTION Do we need to push the gym name? Is this avoidable?
  //       //// NOTE: pushing the mapID is temp.
  //       let currentSend:Send = null;
  //       if(this.routeIdToSends && this.routeIdToSends.hasOwnProperty(route.pushKey)){
  //         currentSend = this.routeIdToSends[route.pushKey] ? this.routeIdToSends[route.pushKey] : null;
  //       }
  //       // console.log('route-feed page, goToRouteDetailsPage: got current send as');
  //       // console.log(currentSend);
  //       this.navCtrl.push('RouteDetail2Page',{gymName: settings.gymId,
  //         routeSummary: route, cacheInfo:imgCache,
  //         mapId:this.mapsObj.mapId, send: currentSend})
  //     });
  // }
  //
  goToRouteDetailsPage2(route:routeObject, cache: FeedCache4Provider){
    //To prevent a bug where clicks on the listView are triggering subsequent clicks on cards,
    //check to see if a timeout from the touch registered from the list click has been cleared.
      // console.log("mapsObj:", this.mapsObj);
      this.settingsProv.getSettings().then((settings)=>{
        //QUESTION Do we need to push the gym name? Is this avoidable?
        //// NOTE: pushing the mapID is temp.
        let currentSend:Send = null;
        if(this.routeIdToSends && this.routeIdToSends.hasOwnProperty(route.pushKey)){
          currentSend = this.routeIdToSends[route.pushKey] ? this.routeIdToSends[route.pushKey] : null;
        }
        // console.log('route-feed page, goToRouteDetailsPage: got current send as');
        // console.log(currentSend);
        this.navCtrl.push('RouteDetail2Page',{gymName: settings.gymId,
          routeSummary: route, feedCache: cache,
           send: currentSend, mapsObj:this.mapsObj})
      });
  }

  goToSettings(){
    this.reloadRoutesOnDidEnter = true;
    this.fstoreObsReady = false;
    // this.menu.close();
    this.navCtrl.push('SettingsPage');
  }

  goToFilters(){
    this.reloadRoutesOnDidEnter = true;
    this.navCtrl.push('FiltersPage');
  }



  offlineRoutesNotice(){
    const offlineRoutesAlert:Alert = this.alertCtrl.create({
      message: "Notice, there are routes that have not been uploaded. Please connect to internet so we can save your routes!",
      buttons: [
        {
          text: "OK",
          role: 'cancel'
        }
      ]
    });
    offlineRoutesAlert.present();
  }

  async goToRouteEditor(){
    let floorNum = 0;
    const settings = await this.settingsProv.getSettings();
    if (settings && settings.hasOwnProperty('currentFloor')){
      floorNum = settings.currentFloor;
    }

    if(this.offlineRoutesExist){
      const offlineRoutesAlert:Alert = this.alertCtrl.create({
        message: "Notice, there are routes that have not been uploaded. If you do not see a route you just created, this is why. Please connect to internet so we can save your routes!",
        buttons: [
          {
            text: "Continue To Route Editor",
            handler: () => {
              this.navCtrl.push('RouteEditorPage', {mapsObj: this.mapsObj, floorNum: floorNum});
            }
          },
          {
            text: "Cancel",
            role: 'cancel'
          }
        ]
      });
      offlineRoutesAlert.present();
    }
    else{
      this.navCtrl.push('RouteEditorPage', {mapsObj: this.mapsObj, floorNum: floorNum});
    }
  }

/******* SLIDES **************/
  handleSliderChange(){
    //update index$ stream
    // const activeIndex = this.slides.getActiveIndex();
    // if(activeIndex >= 0 || activeIndex < )
    this.index$.next( this.slides.getActiveIndex() );
  }


  scrollToRoute(i){
    if(this.fsObs && this.fsObs.hasOwnProperty('behaviorSubjFilteredRoute$')
      && this.fsObs.behaviorSubjFilteredRoute$.getValue().length > 0){
      this.slides.slideTo(i);
    }
  }

/***** END SLIDES **********/


/********  LISTS *********/

  async toggleListView(){
    try{

      if(this.listView){//switching from list-view to map/slider view
        await this.prepareStateForMapSliderView();
      }else{//switching from map/slider view to list view
        await this.prepareStateForListView();
      }
      this.listView = !this.listView;
    }
    catch(error){
    };
  }

  async prepareStateForMapSliderView(){
    this.fstoreObsReady = false;
    // await this.resetFstoreObsForMapSliderView();
    // this.resetIndex$ForMapSliderView();

    this.emitRouteIndexForMapSlidesView();
    await this.feedCache.init(this.fsObs.behaviorSubjFilteredRoute$,this.index$);
    this.fstoreObsReady = true;
  }

  async prepareStateForListView(){
    //filteredRoutes might change while in list-view, so need to save the route and search for it later, rather than just saving it's index
    this.lastRouteViewed = this.fsObs.behaviorSubjFilteredRoute$.getValue()[this.index$.getValue()];
  }

  //try to emit the index of the last route that was viewed in the Map/Slides view.
  //if DNE or can't find it in the current filteredRoute$, just emit 0;
  emitRouteIndexForMapSlidesView(){
    //Do not emit a value if there are no routes aviailable.
    if(!this.fsObs.behaviorSubjFilteredRoute$.getValue().hasOwnProperty('length')
       || this.fsObs.behaviorSubjFilteredRoute$.getValue().length === 0 ){
         return null;
       }

    if(this.lastRouteViewed){
      const currentRoutes:routeObject[] = this.fsObs.behaviorSubjFilteredRoute$.getValue();

      //TODO: could optimize by only searching if behaviorSubjFilteredRoute$ actually emitted while in list-view
      //by saving index and route, and emitting the index if the routes array has not emitted
      let currentRouteFound = false;
      for(let i = 0; i< currentRoutes.length; i++ ){
        if(currentRoutes[i].pushKey === this.lastRouteViewed.pushKey){
          currentRouteFound = true;
          this.index$.next(i);
          break;
        }
      }
      if(!currentRouteFound){
        this.index$.next(0);
      }
    }else{
      this.index$.next(0);
    }
  }

  //
  async toggleFloor(newFloor){
    this.changingFloorNow = true;
    console.log('routes-feed (page), toggleFloor triggered, newFloor is:');
    console.log(newFloor);
    this.currentFloor = newFloor;
    await this.init();
    this.changingFloorNow = false;
  }

  ionViewWillLeave(){
    // exit full screen
    //TODO: why is this here?
    if(this.unregisterBackButtonAction){
      this.unregisterBackButtonAction();
    }
  }


  public initializeBackButtonCustomHandler(): void {
    this.unregisterBackButtonAction = this.platform.registerBackButtonAction(() => {
        this.customHandleBackButton();
    }, 10) as Function;
  }

  private customHandleBackButton(): void {
    // console.log("listView b4:", this.listView);
    if(this.listView){
      this.listView = !this.listView;
      // console.log("listViewAfter:", this.listView);
    }
    else if(this.backButtonPressedToExit){
      this.events.publish('exit:prepare');
      this.events.subscribe('exit:ready',()=>{
        console.log('routes-feed.ts (page), ****** platform.exit! *******');
        this.platform.exitApp();
      });
    }
    else if(this.navCtrl.canGoBack()){
      this.navCtrl.pop();
    }
    else{
      this.showExitToast();
      this.backButtonPressedToExit = true;
      setTimeout(() => {
        this.backButtonPressedToExit =false;
      }, 2000);
    }
  }



  showExitToast() {
    let toast = this.toastCtrl.create({
      message: 'Press Again to exit',
      duration: 2000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }



  ionViewWillUnload(){
    //console.log('routesFeed: &&&&&& ionViewWillUnload &&&&&&&' );
  }

/*
  ionViewDidLeave(){
    console.log("ionViewDidLeave");
    this.events.unsubscribe('connected:true')
    console.log("ionViewDidLeave finished, connections unsubscribed");
  }
*/

  // async prepareToExit(){
  //   //console.log('********************** prepareToExit ********************* ');
  //     try{
  //       if(this.feedCache){
  //         await this.feedCache.shutDown();
  //       }
  //       if(this.routeIdToSend$){
  //
  //       }
  //       if(this.routeIdToSendsSubscrip){
  //         this.routeIdToSendsSubscrip.unsubscribe();
  //       }
  //       //console.log('************ publishing exit:ready ********** ');
  //       this.events.publish('exit:ready');
  //     }catch(error){
  //       throw(error);
  //     }
  // }

  async testFunction(){
    console.log("testFunction called");
    let userInfo = this.authProv.getUser();
    console.log("userInfo:", userInfo);
    // try{
    // }
    // catch(err){
    //   console.log("error in test function:", err);
    // }
  }

  ngOnDestroy(){
    // this.currentRoutes.unsubscribe();
  }

  }
