import { Component, Input, ViewChild, SimpleChanges, ElementRef, OnInit } from '@angular/core';
import { holdObject } from '../../providers/globalConstants/globalConstants';
import * as globalConstants from '../../providers/globalConstants/globalConstants';
//QUESTION does the display component need the FeedCache2Provider? I think all it needs is the native file url, which is passed in as 'cache'
//from parent page
// import { FeedCache2Provider } from '../../providers/feed-cache2/feed-cache2';
import { SvgMapComponent } from '../../components/svg-map/svg-map';

const console = globalConstants.betterConsole

@Component({
  selector: 'FeedRouteDisplayComponent',
  templateUrl: 'feed-route-display.html'
})
export class FeedRouteDisplayComponent {

  // Inputs from parent page indicating the size of the route-display component
  @Input() imgURL: string;
  @Input() divWidth: number;
  @Input() divHeight: number;
  //@Input() aspectRatio: number;
  @Input() completeRoute: any;
  @Input() mode: string;


  // SVG DOM element

  public svgWidth: number;
  public svgHeight: number;
  public svgViewBox: string;
  public photoSrc: any;
  public holdArray: any;
  public pushKey: any;
  public divID: any;
  public divMode: string;

  public svgDiv: any;
  public showComplete: boolean = false;
  // public hideRoute: boolean = false; // used for showing the route picture, or the Details
  public showMap: boolean = false; // show the map in the details section or not
  public mapDimensions: any = {x:300, y:300}; // <-- HACK (temp);
  public mapMode: any;
  public constantWidth: number;
  public scaleHoldsBy: number;
  public that:any; //for passing context into callbacks

  constructor(
    // public feedCache: FeedCache2Provider,
  ) {
    // console.log("********feed-route-display: >> constructor << START *******************");
    // this.hideRoute = false;
    // console.log("this is:");
    // console.lie(this);

    // console.log("********feed-route-display: >> constructor << END *******************");

  }

  ngOnChanges(changes: SimpleChanges) {

    // console.log("********feed-route-display: >> ngOnChanges<< START*******************");
    // console.log(`===> ===> feed-route-display.ts (component) ngOnChanges, changes is : <=== <=== `);
    // console.log(changes);
    // console.log("feed-route-display.ts, (component) ngOnChanges, about to call drawHoldArray() ");

    if(this.svgDiv){
      this.drawHoldArray();
    }


    // console.log("********feed-route-display: >> ngOnChanges<< END*******************");
  }

  ngOnInit(){

    // console.log("********feed-route-display: >> ngOnInit << START *******************");

    this.initializePropertiesForTemplate();

    // console.log("********feed-route-display: >> ngOnInit << END*******************");

  }

  // ngAfterContentInit(){
  //   // TODO figure out a way to have the component determine its
  //   // own dimenstions (not use @input for div).
  //
  //   console.log("********feed-route-display: >> ngAfterContentInit << START*******************");
  //   console.log("********feed-route-display: >> ngAfterContentInit << END *******************");
  // }

  //
  // ngAfterViewInit(){
  //   console.log("ngAfterViewInit, this.divID is:, ", this.divID)
  //   console.log("********feed-route-display: >> ngAfterViewInit << END*******************");
  // }
  //
  ngAfterViewChecked(){


      if(!this.svgDiv){
        // console.log("********feed-route-display: >> ngAfterViewChecked << START *******************");
        this.svgDiv = document.getElementById(this.divID);
        console.lie(this.svgDiv );
        this.drawHoldArray();
        // console.log("********feed-route-display: >> ngAfterViewChecked << END *******************");
      }
  }



  initializePropertiesForTemplate(){
    // console.log('III initializing properties for template III');

    this.holdArray = this.completeRoute.topo.holds;

    // console.log(`feed-route-display.ts (component) ngAfterContentInit, this.mode is: ${this.mode  } `);

    if(this.mode){
      // console.log(`feed-route-display.ts (component) ngAfterContentInit, INSIDE if(this.mode) `);
      this.divMode = this.mode;
    }
    else{
      this.divMode = "placeHolder";
    }

    if(this.completeRoute.pushKey == null){
      this.pushKey = "routeInConstruction";
      this.divID = this.pushKey;
    }
    else{
      this.pushKey = this.completeRoute.pushKey;
      this.divID = this.pushKey+this.divMode;
      // console.log("feed-route-display.ts (component) ngAfterContentInit, divID:", this.divID);

    }

    //for some use cases (route editor) imgURL is undefined, so gets appropriate image from completeRoute object
    if(this.imgURL){
      this.photoSrc = this.imgURL;
    }else{
      this.photoSrc = this.completeRoute.topo.photos[0];
    }

    // TODO get consensus grade


    // determine the aspect ratio of the div to fill the space with
    // the largest size image possible while retaining the proper aspect ratio.
    const divAspect = this.divWidth / this.divHeight;
    let sfx = this.photoSrc.slice(this.photoSrc.length - 6, this.photoSrc.length -4);

    if(sfx === globalConstants.landscapeSuffix){
      // if the div aspect is less than the constant aspect,
      // then it is taller than it is wide, so fill the width, scale the height.
      if(divAspect < globalConstants.lAspectRatio){
        this.svgWidth = this.divWidth;
        this.svgHeight = this.svgWidth / globalConstants.lAspectRatio;
      }

      // other wise, it is wider than tall, so fill the height, scale the width.
      else{
        this.svgHeight = this.divHeight;
        this.svgWidth = this.svgHeight * globalConstants.lAspectRatio;
      }

      // because it is in landscape, the constant height is the width-
      // scale is determined from dividing the div's width w/ the photo's.
      this.scaleHoldsBy = this.svgWidth / globalConstants.photoHeight;
    }

    // same logic as above, but for portrait.
    else{
      if(divAspect < globalConstants.pAspectRatio){
        this.svgWidth = this.divWidth;
        this.svgHeight = this.svgWidth / globalConstants.pAspectRatio;
      }
      else{
        this.svgHeight = this.divHeight;
        this.svgWidth = this.svgHeight * globalConstants.pAspectRatio;
      }

      this.scaleHoldsBy = (this.svgWidth / globalConstants.photoWidth);
    }


    this.svgViewBox = "0 0 " + this.svgWidth + " " + this.svgHeight;

  }




  drawHoldArray() {
    //console.log("frd, drawHoldArray");
    //this.clearSVG(this.divID);
    this.svgDiv = document.getElementById(this.divID);
  //   if(!this.svgDiv){
  //
  //     setTimeout(()=>{
  //       // console.log("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
  //       // console.log('feed-route-display.ts (component), drawHoldArray, this.divID is:', this.divID);
  //       // console.log('feed-route-display.ts (component), drawHoldArray, got this.svgDiv as:');
  //       // console.log(this.svgDiv);
  //
  //       this.svgDiv = document.getElementById(this.divID);
  //         if(this.holdArray && this.holdArray.hasOwnProperty('length')){
  //   //console.log("holdArray:", this.holdArray);
  //
  //   for (let i = 0; i < this.holdArray.length; i++){
  //     let color = "green";
  //     if(this.holdArray[i].color == State.Middle){
  //       color = "yellow";
  //     }
  //     else if(this.holdArray[i].color == State.Finish){
  //       color = "red";
  //     }
  //
  //     let cx = this.holdArray[i].pointCor.x * this.scaleHoldsBy;
  //     let cy = this.holdArray[i].pointCor.y * this.scaleHoldsBy;
  //     let cr = this.holdArray[i].radius * this.scaleHoldsBy;
  //
  //     let svgNamespace = "http://www.w3.org/2000/svg";
  //     let circle = document.createElementNS(svgNamespace,'circle');
  //
  //     circle.setAttributeNS(null, 'cx', cx.toString());
  //     circle.setAttributeNS(null, 'cy', cy.toString());
  //     circle.setAttributeNS(null, 'r', cr.toString());
  //     circle.setAttributeNS(null, 'style', 'fill: none; stroke: ' + color + '; stroke-width: 2px;');
  //     circle.setAttributeNS(null, 'class', 'CIRCLE');
  //     circle.setAttributeNS(null, 'id', 'hold');
  //
  //      if(this.svgDiv != null){
  //        //console.log("frd svgDiv:", this.svgDiv);
  //        this.svgDiv.appendChild(circle);
  //        //console.log("frd Appended");
  //      }
  //
  //      else{
  //
  //        // console.log("svgDiv is null!");
  //      }
  //    }
  // }
  //     }, 5000);
  //
  //     // setTimeout(()=>{
  //     //   console.log("SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS");
  //     //   console.log('feed-route-display.ts (component), drawHoldArray, this.divID is:', this.divID);
  //     //   console.log('feed-route-display.ts (component), drawHoldArray, got this.svgDiv as:');
  //     //   console.log(this.svgDiv);
  //     //   this.drawHoldsHelper();
  //     // }, 5000);
  //   }else{
  //
  //     this.drawHoldsHelper();
  //   }
  this.drawHoldsHelper();
 }



   clearSVG(routeID: string){

     let rid = routeID;
     let oldHolds = document.getElementById("hold");
     if(oldHolds != undefined && oldHolds.parentElement.id == rid){
       oldHolds.parentNode.removeChild(oldHolds);
       this.clearSVG(rid);
     }
     else{
       return;
     }
   }

  drawHoldsHelper(){
  if(this.holdArray && this.holdArray.hasOwnProperty('length')){
    //console.log("holdArray:", this.holdArray);

    for (let i = 0; i < this.holdArray.length; i++){
      let color = "green";
      if(this.holdArray[i].color == State.Middle){
        color = "yellow";
      }
      else if(this.holdArray[i].color == State.Finish){
        color = "red";
      }

      let cx = this.holdArray[i].pointCor.x * this.scaleHoldsBy;
      let cy = this.holdArray[i].pointCor.y * this.scaleHoldsBy;
      let cr = this.holdArray[i].radius * this.scaleHoldsBy;

      let svgNamespace = "http://www.w3.org/2000/svg";
      let circle = document.createElementNS(svgNamespace,'circle');

      circle.setAttributeNS(null, 'cx', cx.toString());
      circle.setAttributeNS(null, 'cy', cy.toString());
      circle.setAttributeNS(null, 'r', cr.toString());
      circle.setAttributeNS(null, 'style', 'fill: none; stroke: ' + color + '; stroke-width: 2px;');
      circle.setAttributeNS(null, 'class', 'CIRCLE');
      circle.setAttributeNS(null, 'id', 'hold');

       if(this.svgDiv != null){
         //console.log("frd svgDiv:", this.svgDiv);
         this.svgDiv.appendChild(circle);
         //console.log("frd Appended");
       }

       else{

         // console.log("svgDiv is null!");
       }
     }
  }
 }

}


enum State{
  Start = 1,
  Middle,
  Finish
}
