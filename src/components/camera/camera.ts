import { Component, Output, EventEmitter } from '@angular/core';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { Events } from 'ionic-angular';
import * as globalConstants from '../../providers/globalConstants/globalConstants';

/**
 * Generated class for the CameraComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'CameraComponent',
  templateUrl: 'camera.html',
  providers: [[Camera]]
})
export class CameraComponent {

  @Output() gotPicture = new EventEmitter();
  //@Output() gotAspect = new EventEmitter();

  options: any;
  cameraCalled: boolean;
  captureImgURI: string;
  picWidth: number;
  picHeight: number;
  //aspectRatio: number;
  imgDim: any;
  isLandscape: boolean = false;
  htmlImg: any;


  constructor(private camera: Camera, private file: File, public events: Events) {
    this.cameraCalled =false;
    this.picWidth = globalConstants.photoWidth;
    this.picHeight = globalConstants.photoHeight;
    //this.aspectRatio = this.picWidth / this.picHeight;
  }


  public capture(){
    this.cameraCalled = true;
    // TODO test if the below call is being used anywhere
    this.options = {
      quality: 50,
      targetWidth: this.picWidth,
      targetHeight: this.picHeight,
      sourceType: this.camera.PictureSourceType.CAMERA,
      saveToPhotoAlbum: true,
      correctOrientation: true,
       destinationType: this.camera.DestinationType.FILE_URI,
      //destinationType: this.camera.DestinationType.DATA_URL,
      mediaType: this.camera.MediaType.PICTURE,
      encodingType: this.camera.EncodingType.JPEG
    }
    this.camera.getPicture(this.options).then((imageURI)=>{
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.captureImgURI = imageURI;


    }, (err) => {
      // Handle error
      console.log("camera Err:", err);
    })
    .then(() => {
      this.determineOrientation().then(result => this.proceedToAmend(result))
    });

  }

  public defaultImg(){
    let stockPhoto = "./assets/img/wall.jpg";
    this.gotPicture.emit(stockPhoto);
    //this.gotAspect.emit(this.aspectRatio);
  }

  determineOrientation():Promise<boolean>{
    return new Promise((res,rej)=>{
      //do stuff, resolve when ready
      // determine if the image was taken in landscape mode
      let img = <HTMLImageElement> new Image();
      img.src = this.captureImgURI;
      img.onload = () => {
        let width = img.naturalWidth;
        let height = img.naturalHeight;
        // console.log("width:", width);
        // console.log("height:", height);
        if (width > height){
          console.log("it was taken in landscape!");
          //this.isLandscape = true;
          res(true);
          // TODO determine if you actually use the scope of isLandscape anywhere, or if you can tighten the scope of the resolution
        }
        else{
          //this.isLandscape = false;
          res(false);
        }

      }

    });
  }

  private proceedToAmend = (proceed:boolean) =>{
    if(proceed){

      let uri = this.captureImgURI;
      let lastSlash = 0;

      for(let i = 0; i < uri.length; i++){
        if(this.captureImgURI[i] === "/"){
          lastSlash = i+1;
        }
      }

      let path = uri.slice(0, lastSlash);
      let oldFileName = uri.slice(lastSlash, uri.length);
      let extension = oldFileName.slice(oldFileName.indexOf('.'), oldFileName.length);
      let newFileName = oldFileName;

      newFileName = newFileName.slice(0, newFileName.indexOf('.'));
      newFileName += globalConstants.landscapeSuffix + extension;
      uri = path + newFileName;

      this.file.moveFile(path, oldFileName, path, newFileName).then(() => {
        this.gotPicture.emit(uri);
      }).catch((error) => console.log("moveFile Error", error));

    }

    else{
      this.gotPicture.emit(this.captureImgURI);
      //this.gotAspect.emit(this.aspectRatio); // TODO determine if this is still being used.
    }
  };


}
