import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireDatabase } from '@angular/fire/database';
import { Platform, Events } from 'ionic-angular';

@Injectable()
export class NotificationsProvider {
  userId:string;
  notificationsRef: any;
  notifications: any[];
  messageDecoder:any;

  constructor(public afAuth: AngularFireAuth, public afDb: AngularFireDatabase,
     public platform:Platform, public events:Events) {
    this.platform.ready().then(()=>{
      this.init();
    });
  }

  init():Promise<any>{
    return new Promise((res)=>{

    // console.log('Hello NotificationsProvider Provider');
    this.afAuth.authState.subscribe( user => {
      if(user){
        this.userId = user.uid;
      }
    });

    this.messageDecoder = {
      'new_comment': "Someone commented on your route!",
      'sandbagger': "Someone thinks you're a sandbagger!"
    }
      this.notifications=[];
      this.notificationsRef = this.afDb.database.ref(`userProfile/${this.userId}/notifications`);
      this.notificationsRef.on('child_added',(snap)=>{
        this.notifications.unshift(snap.val());
        this.events.publish('new_notification');
        res(this.notifications);
    });
  });
}

  getNotifications():Promise<any>{
    return new Promise((res)=>{
      this.notifications = [];
      this.notificationsRef = this.afDb.database.ref(`userProfile/${this.userId}/notifications`);
      this.notificationsRef.on('child_added',(snap)=>{
        this.notifications.unshift(snap.val());
        res(this.notifications);
      });
    });
  }

}
