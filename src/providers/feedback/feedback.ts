import { Injectable } from '@angular/core';
import { RouteEditorFeatures } from '../../providers/globalConstants/globalConstants';
import { Platform } from 'ionic-angular';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';//used for definig types

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

@Injectable()
export class FeedbackProvider {
  userId:string = null;

  constructor(public platform:Platform, public afAuth: AngularFireAuth,
  public afs: AngularFirestore) {
      this.platform.ready().then(()=>{
        this.afAuth.authState.subscribe( user => {
          if(user){
            this.userId = user.uid;
          }
        });
      });
  }

  //if the feedback document already exists, use .update to change it
  //the update cloud function will take the old value into account when computing
  //the new averages (for grade, and various features scores)
  //if feedback doc does not already exist, use .set to change it
  async submitFeedback(routeDocPath:string, feedback:RouteEditorFeatures){
    try{
      if(!this.userId){
        throw new Error('FeedBackProvider, submitFeedback: ERROR... no userId');
      }

      feedback['uid']= this.userId;

      const feedbackDocument = this.afs.doc(routeDocPath + `FEEDBACK/${this.userId}`);
      const oldFeedbackSnap = await  feedbackDocument.ref.get();
      if(oldFeedbackSnap.exists){
        console.log('updating feedback as:');
        console.log(feedback);
        await feedbackDocument.ref.update(feedback);
        return feedback;
      }else{
        console.log('setting feedback as:');
        console.log(feedback);
        await feedbackDocument.ref.set(feedback);
        return feedback;
      }
    }
    catch(error){
      console.log('FeedBackProvider, submitFeedback: ERROR...');
      console.lie(error);
      throw error;
    };
  }



}
