
import { Injectable } from '@angular/core';
import { Platform, Events } from 'ionic-angular';
import { File, FileEntry, Metadata } from '@ionic-native/file';

import { Observable } from 'rxjs/Observable';
import { combineLatest } from 'rxjs/operators/combineLatest';
// import { do } from 'rxjs/operators/do'

import { Subscription } from 'rxjs/Subscription';

import { CacheState, CacheTracker, LoadingObject, LoadedObject, emptyCacheState} from './cache-tracker';

import { ConnectionProvider } from '../../providers/connection/connection';
import { LocalStorageProvider} from '../local-storage/local-storage';
import { FileUtilitiesProvider } from '../file-utilities/file-utilities';
import { ImageProvider } from '../image/image';

import { feedCacheName, routeObject } from '../../providers/globalConstants/globalConstants';

import { betterConsole }  from '../globalConstants/globalConstants';
let console = betterConsole;

const B = 20; //the max number of images to cache before making more space
const tail:number = 5; //min num images to buffer/keep buffered "before" and "behind" selected route (where possible)
/*
TODO:
  code audit
    -error handling: transferObject.abort() throwing error that is properly handled?
*/

@Injectable()
export class FeedCache4Provider {

  private basePath:string = null; //path to cordova's root directory for this app's use
  private feedCachePath:string  = null;//path to and including 'FeedCache' directory

  cacheSubscription:Subscription;
  cacheTracker: CacheTracker = null;//instance of CacheTracker (see ./cache-tracker.ts)
  needOrphanCheck = true; //toggle this to force cacheManager$ to update the list of of orphans

  //downloads are spawned in serial, but can happen in parallel under certain circumstances
  //if number of active downloads exceeds maxSimultLoads, old downloads are aborted and cleaned up
  //to gurantee bandwidth for the highest priority image (see loadPriorityStack's use of abortTransfersAndCleanUpFiles)
  maxSimultLoads = 2;
  //TODO: figure out how to handle on pause event
  exitSubscription = null;

  onConnectPriorityLoader: (routes:routeObject[],i:number) => void = null;


  constructor(public storageProv: LocalStorageProvider,
    public fileUtilsProv: FileUtilitiesProvider,
    public image: ImageProvider, public platform:Platform,
    public connectionProv: ConnectionProvider, public events: Events ) {
  }

  async init(route$: Observable<routeObject[]>, index$:Observable<number>):Promise<any>{
      //set onConnect listener
    // if(!this.onConnectCallback){
    //   this.onConnectCallback = (route$, index$)=>{
    //       console.log('feed-cache4, onConnectCallback triggered. About to call buildTrackerAndCacheManager' );
    //       this.buildTrackerAndCacheManager(route$,index$);
    //   }
    //   this.events.subscribe('connected:true', this.onConnectCallback );
    // }

    await this.buildTrackerAndCacheManager(route$,index$);
    return true;
  }

  //Clean up after pre-existing cacheManagers (cancel subscriptions), then build cache tracker/cacheManager
  async buildTrackerAndCacheManager(route$: Observable<routeObject[]>, index$:Observable<number>):Promise<any>{
    try{
        //clean up after previous effects of feedCache (if it was initialized earlier)
        await this.prepForInit();

        if(!this.cacheTracker){
          this.cacheTracker = await this.initCacheTrackerAndFeedCache(); //'FeedCache' refers to the actual directory on the phone
        }

        //cacheManager coordinates logic that prioritizes image downloads and deletes and aborts (see initCacheManager for details)
        let cacheManager$ = await this.initCacheManager(this.cacheTracker,route$,index$);
        this.cacheSubscription = cacheManager$.subscribe();
        return true;
      }
      catch(error){
        // console.log('feed-cache init throwing error:');
        // console.log(error);
        throw error;
      }
  }

  //init was called because there is a new route$ observable (for ex. b/c filters changed)
  //set the cachePath if not already set
  //unsubscribe from the previous cacheSubscription if it exists and set it to null
  async prepForInit(){
    //QUESTION: abortTransfersAndCleanUpFiles ? ...I think if there are too many
    //downloads, the loadPriorityStack logic will abort transfers successfully but should test this

    try{
      if(!this.feedCachePath){
          this.feedCachePath = await this.getCachePath().catch(error=>{
            // console.log('Error getting cachePath:');
            // console.log(error);
            throw error;
          });
      }
      if(this.cacheSubscription){
        this.cacheSubscription.unsubscribe();
        this.cacheSubscription = null;
      }
      // console.log('prepForInit returning true');
      return true;
    }
    catch(error){
      // console.log('prepForInit throwing error:')
      console.lie(error);
      throw error;
    };
  }

  /**** initCacheTrackerAndFeedCache *****/
  async initCacheTrackerAndFeedCache():Promise<CacheTracker>{

    let feedCacheObj = await this.buildCacheTrackerFromFileNames();
    // console.log("1.) initCacheTrackerAndFeedCache, got feedCacheObj as:", feedCacheObj);
    if(feedCacheObj){
      // console.log("2.) about to init cache tracker from existing feedCache obj");
      this.cacheTracker = new CacheTracker(feedCacheObj);
      return this.cacheTracker;
    }else{
      // console.log(`3.) initCacheTrackerAndFeedCache -> in else. got feedCacheObj as ${feedCacheObj}. calling resetOrCreateCache...`);
      const cacheTracker = await this.resetOrCreateCache();
      return cacheTracker;
    }
  }

  /******** initCacheManager Overview:
  return an observable (the cacheManager$) which is responsible for prioritizing and managing downloads and file cleanup (deletes, download aborts)
  Notes: combineLatest fires every time there is a new route$ snapshot or index$ snapshot.
  route$ emits when routeObject[] changes in firstore or when new filters/settings are selected
  index$ changes as the user scrolls through the feed or taps the map to select a new route

  The cache tracker does the following:
    -if no data connection, do nothing (no need to try to download or make space if no data connection)
    -maintain an accurate array of orphans
    -make sure there is space in the cache on initialization (by calling deleteChunk)
    -build an array (priorityStack) of routeIds representing higest to lowest priority for download (see orderPriorities)
    -load any high priority images that were not already loaded
    -when each new image finishes loading
      --check that the download completed properly (file metadata.size matches the firestore metadata.size), if not, delete it
      --make space when images downloads are completed when the cache gets too full
    TODO:
     -implement download retry logic for images that do not load properly
     -test that the metadata.size checks are working properly (this should be obvious with app use)
  */
  initCacheManager(cacheTracker:CacheTracker,route$: Observable<any>,
    index$:Observable<number>):Observable<Promise<void>>{

      //NOTE: needOrphanCheck is set to true when initCacheManager is called (NOT when the combineLatest callback is called)
      //this is when we need to check for new orphans because the routes snapshot may be missing routes that are tracked
      //in the cacheTracker due to changes in filters, etc.
      this.needOrphanCheck = true;
      let cacheManager$ = route$.pipe(
        combineLatest(index$, async (routes:routeObject[],i:number) =>{
/*
          console.log('******** cacheManager combineLatest callback triggered *********');
          console.log('routes: ');
          console.log(routes);
          console.log('i: ',i);
*/
          //only try to load/delete routes if there is a data connection
          if(this.connectionProv.isConnected()){
            this.checkOrphansAndLoadPriorities(routes,i);
          }else{//unsubscribe from connected:true event, then resubscribe with new onConnectPriorityLoader, since priorities have prob changed
            if(this.onConnectPriorityLoader){
              this.events.unsubscribe('connected:true', this.onConnectPriorityLoader);
            }
            //when connected:true fires, wait 3 second, check if still connected and if so, try to load this route's image
            this.onConnectPriorityLoader = () => {
              setTimeout(()=>{
                if(this.connectionProv.isConnected()){
                  this.checkOrphansAndLoadPriorities(routes,i);
                }
              }, 3000);

            };
            this.events.subscribe('connected:true', this.onConnectPriorityLoader);
          }
        })
      );
      return cacheManager$;
    }

  async checkOrphansAndLoadPriorities(routes:routeObject[],i:number){
    if(this.needOrphanCheck){
      this.cacheTracker.updateOrphans(routes);
      this.needOrphanCheck = false;

      let deleteChunkResult = await this.deleteChunk(routes, i);
      // console.log('deleteChunkResult: ', deleteChunkResult);
    }

    let start = i - tail >= 0 ? i-tail:0;
    let stop = tail + i <= routes.length-1 ? tail + i :routes.length-1;

    let priorities = this.orderPriorities(routes.slice(start, stop+1), i);
    //filter out routes that have already loaded
    priorities =  priorities.filter(route => !this.cacheTracker.getLoaded(route.pushKey));

    // console.log('AFTER filter, priorities is:');
    // console.log(priorities);

    await this.loadPriorityStack(priorities, routes, i);
  }


  async buildCacheTrackerFromFileNames(){
    try{

      let cachedFileNames: string[] = [];
      let files = [];

      if(!this.basePath){

        this.basePath = await this.fileUtilsProv.getBasePath('cache').catch(error=>{
        }) as string;;

      }

      const cacheDirExists = await this.fileUtilsProv.dirExists('cache', feedCacheName).catch(error=>{
      });

      if(cacheDirExists){
        //get names of all files in the FeedCache directory from cordova file plugin

        files = await this.fileUtilsProv.file.listDir(this.basePath, feedCacheName);

        cachedFileNames = await this.fileUtilsProv.getFileNames('cache', feedCacheName);

        // await this.fileUtilsProv.displayContents('cache', feedCacheName);
      }

      let cacheState = emptyCacheState;

      if(files.length > 0){
        files.forEach( (file) =>{
          //if there's no current record of images for this id...
          // console.log('in cachedFileNames callback and name is: ', name);
          let id = file.name.split('_')[0];
          if( id ){
            cacheState.idToLoaded[id] = {name: file.name, fullPath: file.nativeURL};
          }
        });
      }

      // console.log('returning cacheState as:');
      // console.log(cacheState);

      return cacheState;
    }
    catch(error){

      // console.log('buildCacheTrackerFromFileNames catching error!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
      // console.log(error);
      return null;
    };
  }

  //i is the currently selected route's index: the one the user is looking at right now
  //get a stack of form [highestPriority ... lowestPriority]
  private orderPriorities(priorities:routeObject[], i:number):routeObject[]{
    let stack = [];
    for (let r_i = 0; r_i < priorities.length; r_i++ ){
      let delta = r_i - i;
      let position = delta >= 0 ?  2*delta   : -2* delta + -1;
      stack[position] = priorities[r_i];
    }

    stack = stack.filter( x=> !!x || x == 0);
    return stack;

  }

  private async loadPriorityStack(priorityStack: routeObject[], routes, i):Promise<boolean>{

    const cachePath = await this.getCachePath();
    const currentlyLoading = this.cacheTracker.getLoading() as {[key:string]:LoadingObject};
    const loadingKeys = Object.keys(currentlyLoading);

    //Prevent too many simultaneous downloads.
    if(loadingKeys.hasOwnProperty('length') && loadingKeys.length > this.maxSimultLoads ){
      // console.log('downloading this many..........', loadingKeys.length);
      await this.abortTransfersAndCleanUpFiles(loadingKeys,currentlyLoading,cachePath);
    }

    for(let route of priorityStack){
      try{
        //try to load image for this route NOTE: the loadImage handles retry logic
        //.then => if (not enough space){ deleteOneImage()}
        //.catch => try to load the next image
        //TODO: add retry logic to loadImage
        // console.log('loadPriorityStack: route is:');
        // console.log(route);
        const loadSuccessful:boolean = await this.loadImage(route).catch(error=>{
          // console.log('loadPriorityStack: error from loadImage');
          // console.lie(error);
          throw error;
        }) as boolean;
        // console.log('loadSuccessful: ', loadSuccessful);
        // console.log('numLoadedOrLoading: ', this.cacheTracker.getNumLoadedOrLoading());
        if(loadSuccessful && this.cacheTracker.getNumLoadedOrLoading() > B){
          // console.log('**** calling makeSpace ****' );
          await this.makeSpace(routes, i).catch(error=>{

            console.log('loadPriorityStack: error making space:');
            console.lie(error);
            throw error;
          });
        }
      }
      catch(error){//if abort() is called on route's transfer object, loadImage throws, breaking this loop
        console.log('error in load priority stack');
        console.lie(error);
        break;
      };
    }
    return true;
  }

  async abortTransfersAndCleanUpFiles(loadingKeys:string[]=null, currentlyLoading: {[key:string]:LoadingObject}=null, cachePath:string=null){
    // console.log('*** abortTransfersAndCleanUpFiles ***');
    if(!currentlyLoading){
      currentlyLoading = this.cacheTracker.getLoading() as {[key:string]:LoadingObject};
      // console.log('abortTransfersAndCleanUpFiles got currentlyLoading as:');
      // console.log(currentlyLoading);
    }

    if(!cachePath){
      cachePath = await cachePath;
    }

    const abortedIds = await this.cacheTracker.abortAllTransfers();
      // console.log('abortTransfersAndCleanUpFiles abortedIds:');
      // console.log(abortedIds);
    //TODO: improve this loop logic. Don't throw error (and break loop) because of failed removals: Keep trying to remove.

    if(loadingKeys && loadingKeys.hasOwnProperty('length') && loadingKeys.length > 0){
      for (let key of loadingKeys){
        // console.log('abortTransfersAndCleanUpFiles 2');
        try{
          // console.log('abortTransfersAndCleanUpFiles 3');
          const result = await this.fileUtilsProv.file.removeFile(cachePath, currentlyLoading[key].name);
        }
        catch(error){
          console.log('abortTransfersAndCleanUpFiles ERROR:');
          console.lie(error);
          throw error;
        };
      }
    }
    return true;
  }

  async deleteChunk(routes:routeObject[], i:number){
    let usedSpace = this.cacheTracker.getNumLoadedOrLoading();
    let makeSpaceErrors = false;

    // console.log('&&&&&&&& deleteChunk --before making space--, usedSpace: ', usedSpace);
    while (usedSpace >= B && !makeSpaceErrors){
      // console.log('usedSpace: ', usedSpace);
      await this.makeSpace(routes,i).catch(error=>{
        console.log('IN WHILE LOOP, makeSpace ERROR!!!:');
        console.lie(error);
        makeSpaceErrors= true;
      });
      usedSpace = this.cacheTracker.getNumLoadedOrLoading();
    }

    if(!makeSpaceErrors){//did not need to delete anything or completed deletes sucessfully
      // console.log('deleteChunk finished with NO errors;');
      // console.log('&&&&&&&& deleteChunk --after making space--, usedSpace: ', usedSpace);
      return true;
    }
    if(makeSpaceErrors){//DID need to delete images, but could not complete deltes
      // console.log('deleteChunk finished WITH errors;');
      // console.log('&&&&&&&& deleteChunk --after making space--, usedSpace: ', usedSpace);
      return false;
    }

    return this.makeSpace(routes,i);
  }

  async makeSpace(routes:routeObject[], i:number){
    try{
      // console.log('###########in makeSpace, before making space, feedCache looks like: ############');
      // await this.fileUtilsProv.displayContents('cache', feedCacheName);

      const deletedOrphanId = await this.deleteOrphan().catch(error=>{
        console.log('ERROR from deleteOrphan:');
        console.lie(error);
      });

      if(deletedOrphanId){
        // console.log('-%_%- successfully delted orphan: ', deletedOrphanId);
        // console.log('################ feedCache now looks like: ##############')
        // await this.fileUtilsProv.displayContents('cache', feedCacheName);

        return deletedOrphanId;
      }else{
        const deleted = await this.deleteFurthestImage(routes, i).catch(error=>{
          console.log('ERROR in deleteFurthestImage:');
          console.lie(error);
          throw error;
        });
        if(deleted){
          // console.log('################ feedCache now looks like: ##############')
          // await this.fileUtilsProv.displayContents('cache', feedCacheName);

          return deleted;
        }else{
          throw new Error('MakeSpace error: could not make space in FeedCache');
        }
      }
    }
    catch(error){
      throw error;
    };
  }

  //try to delete one orphan image then remove it's record from cache tracker
  //if cannot delete an orphan, return null
  async deleteOrphan(){
    let orphans = {};
    // console.log('*** fetching orphans ***');
    try{
      orphans = this.cacheTracker.getOrphans();
    }
    catch(error){
      console.log('ERROR from getOrphans');
      console.lie(error);
      throw(error);
    };

    let orphanIds = [];

    // console.log('orphans are:');
    // console.log(orphans);

    if(orphans){
      orphanIds = Object.keys(orphans);
    }

    // console.log('orphanIds computed as: ');
    // console.log(orphanIds);

    if( orphanIds.length < 1 ){
      // console.log('deleteOrphan returning null!!!!!!!!!!');
      return null;
    }

    if (orphanIds && orphanIds.hasOwnProperty('length') && orphanIds.length > 0){
      //QUESTION/TODO: should this loop through orphans until it finds one it

      //loop until able to delete an orphan
      for(let i=0; i< orphanIds.length; i++){
        let orphanId = orphanIds[i];
        if(!this.cacheTracker.getPendingRemovals(orphanId)){

          // console.log('.........trying to delete orphan: ', orphanId);
          const orphanName = orphans[orphanId];
          this.cacheTracker.setPendingRemoval(orphanId);
          const cachePath = await this.getCachePath();
          const result = this.fileUtilsProv.verifyRemoveFile(cachePath, orphanName).catch(error=>{
            console.log('***** ERROR in verifyRemoveFile, trying to delete ', orphanName);
            console.log('ERROR:');
            console.lie(error);
          });
          // console.log('deleted: ', orphanId, 'result was: ');
          // console.lie(result);
          this.cacheTracker.deletePendingRemoval(orphanId);
          this.cacheTracker.deleteLoaded(orphanId);
          this.cacheTracker.removeOrphan(orphanId);

          return orphanId;
        }
      }
    }

    return null;
  }

  //if l and r are eqidistant from route_i, populate 'cands' as [routes[l], routes[r] and try to delete them both
  //else populate cands with a single index (l xor r, whichever is further)
  async deleteFurthestImage(routes:routeObject[], i:number){
    // console.log('^^^ deleteFurthestImage' );
    const cachePath = await this.getCachePath();
    //pushKeys of the candidates for removal.
    let cands: string[] = [];

    //if there are fewer routes than fit inside of [...tail routes..., route_i, ...tail routes ....]
    //then do not delete anything
    if(routes.hasOwnProperty('length') && routes.length > 1 + 2*tail){
      // console.log('^^^ IN FIRST IF');
      let l = 0;
      let r = routes.length-1;
      // console.log('^^^ r is: ',r);
      let deleteComplete = false;//set to true when an image is sucessfully deleted from the cache
      //
      let tries= 0;
      const maxTries = routes.length - 2*tail;

      //generate candidate(s) pushKey(s)
      while(l + 2*tail + 1 < r  && !deleteComplete && tries <= maxTries){//have not traveresed routes nor deleted an image

        // console.log(`l: ${l} ... i: ${i} ... r: ${r}`);
        //dist from route_i to "left" cand is greater than dist from route_i to "right" cand.
        //OR r cannot move any closer to i, but l can.
        tries += 1;

        if(i - l > r - i || (r == i && l !== i)){
          // console.log('^^^ A ^^^');
          cands = [routes[l].pushKey];
          // console.log('^^^ *A* cands:');
          // console.log(cands);
          l = l < i ? l + 1:l; //move l toward i unless it equals i

        //dist to the right side was greater
        //OR l cannot move any closer to i, but r can
      }else if(r - i > i - l || (l == i && r !== i)){

          // console.log('^^^ B ^^^');
          cands = [routes[r].pushKey];
          // console.log('^^^ *B* cands:');
          // console.log(cands);
          r = r  > i ? r - 1 : r;//move r toward i unless it equals i
        }else if (r-i == i-l && l !== i && r !== i){//they are equidistant from i

          // console.log('^^^ C ^^^');
          cands = [routes[l].pushKey, routes[r].pushKey];
          // console.log('^^^ *C* cands:');
          // console.log(cands);
          l = l + 1 < i ? l + 1:l; //move l toward i unless it runs into i
          r = r - 1 < i ? r - 1 : r;//move r toward i unless it runs into i
        }

        for(let pushKey of cands){
          // console.log('^^^#@@@#@@@#...deleteFurthestImage loop. pushKey: ', pushKey);
          if( !this.cacheTracker.getPendingRemovals(pushKey)){
            //...and it is already loaded
            // console.log('^^^** pushKey: ', pushKey, ' not in pendingRemovals **');
            const loadedObject= this.cacheTracker.getLoaded(pushKey) as LoadedObject;
            // console.log('^^^** got loaded object as: ');
            // console.log(loadedObject);

            if(loadedObject){
              // console.log('^^^ inside the if ^^^');
              //...then delete it's image from the file system
              let result = await this.deleteLoadedImage(pushKey,cachePath,loadedObject.name);
              deleteComplete = result || deleteComplete; //deleteComplete might be true from other candidate
              // console.log('^^^*********deleted (furthest) route with pushKey: ', pushKey)
            }else{//if it wasn't already loaded, it might be loading now:

              // console.log('^^^ inside the else^^^');
              const loadingObject = this.cacheTracker.getLoading(pushKey) as LoadingObject;
              // console.log('^^^** got loadING object as: ');
              // console.log(loadingObject);

              if(loadingObject){
                let result = await this.deleteLoadingImage(pushKey, cachePath, loadingObject);
                deleteComplete = result || deleteComplete;//deleteComplete might be true from other candidate
                // console.log('^^^ *********deleted (furthest) route with pushKey: ', pushKey)
              }//it was not currently loading
            }//it was not already loaded
          }
        }

        if(deleteComplete){
          // console.log('^^^ deleteFurthestImage RETURNING: ', deleteComplete);
          return deleteComplete;
        }
      }//traversed the whole routes array and could not trigger any deletes.
    }//there aren't enough routes in the array to warrant any image deletes
    return null;
  }

  private async deleteLoadingImage(pushKey:string, cachePath:string, loadingObject: LoadingObject):Promise<boolean>{
    //abort the download
    this.cacheTracker.setPendingRemoval(pushKey);
    loadingObject.transferObject.abort();
    try{
      const flag = await this.fileUtilsProv.verifyRemoveFile(cachePath,loadingObject.name);
      this.cacheTracker.deleteLoading(pushKey);
      this.cacheTracker.deletePendingRemoval(pushKey);
      return true;
    }
    catch(error){
      throw(error);
    };
  }

  /*
  Assumptions: there IS a loaded file recorded in the cacheTracker with the given pushkey (and other parameters)
  Caution: this method does not check to make sure that the pushkey is in the cacheTracker's idToRecord object
  Returns true after the file (image) was removed from the file system and the record was removed from the cacheTracker
  */
  private async deleteLoadedImage(pushKey:string,cachePath:string,loadedName:string):Promise<boolean>{
    this.cacheTracker.setPendingRemoval(pushKey);
    try{
      let flag:string = await this.fileUtilsProv.verifyRemoveFile(cachePath,loadedName);
      //...then delete its record from the cacheTracker
      this.cacheTracker.deleteLoaded(pushKey);
      this.cacheTracker.deletePendingRemoval(pushKey);
      return true;
    }
    catch(error){
      //TODO If this throws, figure out why. It should only throw if it can't find the direcory (cache directory)
      throw(error);
    };
  }

  private async loadImage(route:routeObject):Promise<boolean>{
    try{
      const routeId = route.pushKey;
      const imgName = route.coverPhoto.split('/').slice(-2).join('_');
      const fStoreUrl = route.coverPhoto; //the storage url in firestore (not the download link!)
      const sizeFromFB = await this.image.getSizeFromFirebase(fStoreUrl);
      const transferObject = await this.image.getTransferObject(fStoreUrl, feedCacheName, "cache", imgName);

      // console.log('feed-cache4.ts, loadImage, loading route:');
      // console.log(route);

      // register new download with cacheTracker before initiating download
      this.cacheTracker.setLoading(routeId, imgName, transferObject.trans,  sizeFromFB);
      // console.log("is that route in this.cacheTracker.idToTransferObj and idToFileNameLoading?: ", this.cacheTracker);

      const cachePath = await this.getCachePath();
      const cacheDirectoryEntry = await this.fileUtilsProv.file.resolveDirectoryUrl(cachePath);

      //start download
      let newFileEntry= await transferObject.trans.download(transferObject.link, transferObject.destination);
      if(newFileEntry){//the download was allegedly successful. Check the image size and retry if necessary
        const fileEntry = await this.fileUtilsProv.file.getFile(cacheDirectoryEntry, imgName , {create:false});
        const metadata = await this.getMetadata(fileEntry);
        // console.log('*** in FileEntry.getMetadata callback and metadata are:');
        // console.log(metadata);
        // console.log('********** end metadata ************')
        if(metadata.size === sizeFromFB){
          this.cacheTracker.deleteLoading(routeId);
          this.cacheTracker.setLoaded(route.pushKey, imgName, newFileEntry.nativeURL);
          //QUESTION: should this return the path to and including the image instead of boolean?
          return true;
        }else{//incomplete download, delete image and try again
          const deleteResult = await this.deleteLoadedImage(route.pushKey, cachePath, imgName);
          if(deleteResult){
            console.log('*%*%*%*% ATTENTION: successfully deleted partial download!');
          }

          throw new Error('loadImage could not load file: size mismatch');
        }
      }
      //TODO: check for a partial download (is that possible if storage path didn't resolve?)
      //...if so, clean up after that download (delete file from directory)
      return false;
    }catch(error){
      //make sure to throw an error if abort is called on route's transfer object.
      //loadPriorityStack depends on this happening
      throw error;
    };

  }

  getMetadata(fileEntry:FileEntry):Promise<Metadata>{
    return new Promise((res,rej)=>{
      fileEntry.getMetadata((metadata:Metadata) =>{
        res(metadata);
      });
    });
  }

  private async getCachePath():Promise<string>{
    //if the full cache path has already been built, return it
    //returns full path to FeedCache, INCLUDING 'FeedCache'
    if(this.feedCachePath){
      return this.feedCachePath;
    }else{//construct the full cache path
      try{
        this.basePath = await this.fileUtilsProv.getBasePath('cache');
        // console.log('in getCachePath, feedCacheName is: ', feedCacheName);
        return this.basePath + feedCacheName;
      }catch(error){
        throw error;
      }
    }
  }
    /*********** Utilities ***********/
  private async resetOrCreateCache():Promise<CacheTracker>{
    //check if the cache folder (cacheName) exists
    console.log('...resetOrCreateCache');
    try{
      if(!this.basePath){
        this.basePath = await this.fileUtilsProv.getBasePath('cache');
      }

        // console.log('get basePath as: ', this.basePath);
      //we're about to recursively delete the FeedCache direcory. This will wreak havoc
      //on the cacheSubscription (if it exists)
      if(this.cacheSubscription){
        this.cacheSubscription.unsubscribe();
        this.cacheSubscription = null;
      }

      //abort any pending downloads
      // console.log('this.cacheTracker is:');
      // console.log(this.cacheTracker);
      if(this.cacheTracker){
        // console.log('trying to abort all transfers');
        this.cacheTracker.abortAllTransfers();
      }

      const cacheDirExists = await this.fileUtilsProv.file.checkDir(this.basePath, feedCacheName);
      // console.log('cacheDirExists:' , cacheDirExists);
      if(cacheDirExists){ //then delete it and it's contents, then recreate the folder
        //remove the cache's directory and it's contents
        // console.log('attempting to remove the directory');
        await this.fileUtilsProv.file.removeRecursively(this.basePath,feedCacheName);
        // console.log('after attempt to remove dir');

      }


      let exists = await this.fileUtilsProv.file.checkDir(this.basePath, feedCacheName);
      // console.log('removed directory recursively. This should be false: ', exists);
      //recreate the folder (since you just deleted it)
      await this.fileUtilsProv.checkOrBuildPath(feedCacheName, 'cache');

      //TODO: delete line. It's for debugging
      exists = await this.fileUtilsProv.file.checkDir(this.basePath, feedCacheName);
      // console.log('didWork: ', exists);

      this.cacheTracker = new CacheTracker();
      return this.cacheTracker;
    }catch(error){
      console.log('ERROR thrown in resetOrCreateCache:');
      console.lie(error);
      throw(error);
    };
  }

  async shutDown(){
    // if(this.onConnectCallback){
    //   this.events.unsubscribe('connected:true', this.onConnectCallback);
    // }

    if(this.cacheSubscription){
      this.cacheSubscription.unsubscribe();
      this.cacheSubscription = null;
    }

    if(this.onConnectPriorityLoader){
      this.events.unsubscribe('connected:true', this.onConnectPriorityLoader);
    }
    try{
      // console.log('feedCache4-> shutDown running, calling abortTransfersAndCleanUpFiles');
      await this.abortTransfersAndCleanUpFiles();

      let cacheState = this.cacheTracker.getCacheState();
      cacheState['priorityStack'] = [];

      //QUESTION: do we need to interact with storageProv any more? We're buildng cacheTracker directly from file names now.
      await this.storageProv.saveLocalCopy(feedCacheName, cacheState);
      return true;
    }
    catch(error){
      // console.log('error in shutDown: ');
      // console.log(error);
      throw error;
    };
  }
}
