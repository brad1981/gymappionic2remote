import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class LocalStorageProvider {
  //Every provider that has data to store permanently should register
  //a callback that stores its information using this array:
  shutdownCallbacks: Array<(...args: any[]) => Promise<any> >;

  dead_routeIds: Array<string>;

  constructor(public storage: Storage) {
    // console.log('Hello LocalStorageProvider Provider');
  }

  getData(key:string): Promise<any>{
    return new Promise<any>((res, rej)=>{
      this.storage.get(key).then((string) => {
        let obj = (JSON.parse(string));
        res(obj);
      }).catch((err) => {
        console.log(`local-storage provider, error in getData, could not get value for key: ${key}`)
        rej(err);
      });
    });
  }

  saveLocalCopy(name:string, data:any): Promise<any>{
    // console.log('SAVING LOCAL COPY WITH NAME '+ name+ " : and data 'is'");
    // console.log(data);

    return new Promise<any>((res, rej) => {
      this.storage.set(name,JSON.stringify(data)).then(() => {
        //NOTE (from dawson) it is not obvious to me why getData is called here.
        //NOTE: I'm guessing I did it so we could confirm that the data saved. I'd have to look
        //up the local storage docs to be sure. I might have been trying to make it throw when it doesn't work
        //either because it doesn't have that functionality, or because I misunderstood the docs.
          this.getData(name).then((gdata) => {
            res (gdata);
          }).catch((err) =>{
            rej(err);
          });
      }).catch((err) => {
        console.log("could not set data to local storage.");
        rej(err);
      });
    })
  }

  removeByKey(key:string):Promise<any>{
    return new Promise((res)=>{
      this.storage.remove(key);
    }).catch((err) => {
      console.log("err from remove by key:", err);
      throw err;
    });
  }

  // TODO - dawson will re-write this to be a recursive method
  // TODO: wait! I don't think you should rewrite this method. It's probably no longer useful,
  //since you already have the recursive route uploader. Instead of calling this method after
  //your recursive method completes, just use storageProv.removeByKey(key) on each key as you
  //learn that the upload succeeded. That way, if the app crashes or they turn off their phone at the wrong time
  //it minimizes the odds that local storage will have a very incorrect record of what uploads suddeeded or failed.
  //I'm not sure, but I think we might be able to just get rid of this entire method. I'm not sure if it's used anywhere else.
  //TODO: skips items if one of these rejects (because Promise.all).
  //use map or recursion to fix this issue
  removeByKeys(keys:string[]):Promise<any>{
    return new Promise((res)=>{
      let promises = [];
      if(!(!keys)&& keys.length>0){
        for(let key of keys){
          let promise = this.removeByKey(key);
          promises.push(promise);
        }
        Promise.all(promises)
        .then( ()=>{
          res();
        });
      }else{//there's nothing to remove
        res();
      }
    });
  }

  removeByKeyNoRejects(key:string){
    return this.storage.remove(key).catch(()=>{
      // console.log('tried to reject');
      Promise.resolve()});
  }

  registerShutdownCallBack(callback: (...args:any[]) => Promise<any>){
    this.shutdownCallbacks.push(callback);
  }

  //buildDeadRoutesArray(newRouteIds: Array<string>){}

}
