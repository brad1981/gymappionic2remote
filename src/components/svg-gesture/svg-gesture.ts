import { Component, ViewChild } from '@angular/core';
import { Platform, Events, Gesture, ViewController} from 'ionic-angular';
import {MapDimensions, ContainerDimensions} from '../../providers/globalConstants/globalConstants';
import * as svgPanZoom from 'svg-pan-zoom';

//TODO: unsubscribe from event: svgLoaded (does ionViewWillLeave fire in a component?)

/*
  NOTE:classes which extend SvgGestureComponent (such as SvgMapComponent and
       SvgRouteComponent) must call this.init() after the svg has fully loaded
       in their corresponding .html file. A good place to do this is in ngAfterViewInit
       note that ionViewDidLoad does not fire within component files
*/
@Component({
  selector: 'SvgGestureComponent',
  templateUrl: 'svg-gesture.html'
})
export class SvgGestureComponent {

  // SVG DOM elements
  @ViewChild('svgElement') svgElement ;


  //perform DOM manipulation on panZoomGroup after svgPanZoom obj is
  //initialized. It is the group to which THIS instance applied the class: svgPanZoom_viewport
  public panZoomGroup:HTMLElement = null;

  // gesture elements
  public options: any;

  public eventsHandler: any;

  //keep this stuff:

  public platformWidth: number;
  public platformHeight: number;
  public gesture: any = null;
  public svgPanZoom: any = null; //the svgPanZoom instance: https://github.com/ariutta/svg-pan-zoom

  public pannedX: number = 0;
  public pannedY: number = 0;
  public pinching: boolean = false; //used to determine if pinch event is starting or ongoing
  public initialScale: number = null;
  public minZoom: number = null;

  public defaultPinchCallback: (ev:any)=>void;
  public defaultPanCallback: (ev:any)=>void;

  constructor( public platform: Platform, public events: Events  ) {
    // console.log('In Gesture Component Constructor and svgElement is:');
    // console.log(JSON.parse(JSON.stringify(this.svgElement)));
    //QUESTION: should we do any checks to make sure the platform is ready?
    this.platformWidth = this.platform.width();
    this.platformHeight = this.platform.height();
  }//end constructor

  //contWidth is the width of the container,contHeight is the height of the container
  // svgWidth: sizes.viewBox.width;svgHeight:sizes.viewBox.height
  //contWidth: sizes.width; contHeight: sizes.height
  getMinZoom(svgWidth,svgHeight,contWidth,contHeight){
    const xRatio = contWidth / svgWidth;
    const yRatio = contHeight / svgHeight ;

    // console.log(`>>>>>>>>>>>>> xRatio: ${xRatio}, yRatio: ${yRatio} <<<<<<<<<<<`);

    return  xRatio >= yRatio ? xRatio : yRatio;
    // return  xRatio >= yRatio ? yRatio : xRatio; //<expriment: returning larger ratio

  }

  init(containerDimensions:ContainerDimensions = null, mapDimensions:MapDimensions = null){
      console.log(`******************** Class: ${this.constructor.name} ************************* `);
      console.log(">>>>>>>>>>>>> SvgGestureComponent, init , containerDimensions: <<<<<<<<<<<<<<<<");
      console.log(containerDimensions);
      //
      console.log('>>> svgPanZoom, init mapDimensions: <<<');
      console.log(mapDimensions);


      console.log(`END**************** Class: ${this.constructor.name} ************************* END`);
      //
      // const xRatio = containerDimensions.x / mapDimensions.width;
      // const yRatio = containerDimensions.y / mapDimensions.height ;

      // console.log(`>>>>>>>>>>>>> xRatio: ${xRatio}, yRatio: ${yRatio} <<<<<<<<<<<`);

      // const smallerRatio = xRatio < yRatio ? xRatio : yRatio;
      // this.minZoom = xRatio > yRatio ? xRatio : yRatio;


       // this.platform.ready().then(()=>{
        // Determine platform dimension s
        // console.log('Running svg-gesture init');

      this.initGesture().then((gesture)=>{
        //initialize panzoom
         // console.log('gesture initialized');
         if(mapDimensions && containerDimensions){
          this.minZoom = this.getMinZoom(mapDimensions.width,
                            mapDimensions.height, containerDimensions.x, containerDimensions.y)
         }

         this.initPanZoom(gesture,containerDimensions, mapDimensions).then((panZoom)=>{
          if(containerDimensions){

            // console.log('svg-gesture, initPanZoom.then callback, mapDimensions are...:');
            // console.log(containerDimensions);
          }

          this.setDefaultPinch(this.gesture);
          this.setDefaultPan(this.gesture);

          // console.log('panZoomInitialized');
          this.setChildClassHandlers();
          panZoom.updateBBox();
          // panZoom.fit();
          panZoom.zoom(this.minZoom);
        });
      });
  }

  initAsync(containerDimensions:ContainerDimensions = null,
                  mapDimensions:MapDimensions = null):Promise<any>{
      return new Promise((res,rej)=>{
        console.log(`****************************************** :${ "" } `);
        console.log(`**************initAsync, svg-gesture (comp), initAsync{}; containerDimensions, mapDimensions :`);
        console.log(containerDimensions);
        console.log(mapDimensions);
        console.log(`****************************************** :${ "" } `);

        this.initGesture().then((gesture)=>{
          //initialize panzoom
           // console.log('gesture initialized');
           if(mapDimensions && containerDimensions){
            this.minZoom = this.getMinZoom(mapDimensions.width,
                              mapDimensions.height,
                              containerDimensions.x, containerDimensions.y)
           }

           this.initPanZoom(gesture,containerDimensions, mapDimensions).then((panZoom)=>{
            if(containerDimensions){

              // console.log('svg-gesture, initPanZoom.then callback, mapDimensions are...:');
              // console.log(containerDimensions);
            }

            this.setDefaultPinch(this.gesture);
            this.setDefaultPan(this.gesture);

            // console.log('panZoomInitialized');
            this.setChildClassHandlers();
            panZoom.updateBBox();
            // panZoom.fit();
            panZoom.zoom(this.minZoom);
            res(panZoom);
          });
        }).catch(error=>{throw error});

      });

  }


  initGesture():Promise<any>{

    console.log(`******************** Class: ${this.constructor.name} ************************* `);
    console.log('** initGesture and svgElement is:');
    console.log(this.svgElement);

    return new Promise((res)=>{
      this.svgElement.nativeElement.addEventListener('touchmove', function(e){

            //console.log('*********** PREVENTING DEFUALT ************');
            e.preventDefault();
        });

      this.gesture = new Gesture(this.svgElement.nativeElement);
      this.gesture.listen();
      res(this.gesture);
    });
  }

  //derived classes should set custom listeners here
  setChildClassHandlers():Promise<any>{
    return new Promise((res)=>{
      res();
    });
  }

  initPanZoom(gesture,containerDimensions:ContainerDimensions = null, mapDimensions:MapDimensions = null):Promise<any>{
    var context = this;


    this.eventsHandler = {
        haltEventListeners: ['touchstart',  'touchmove', 'touchleave', 'touchcancel']
        , init: function(options) {
          // console.log('*********  INITIALIZING PAN ZOOM OBJECT!!!  ******');
          var instance = options.instance
            , initialScale = context.minZoom
            , pannedX = 0
            , pannedY = 0
            ,pinching = false;



          const sizes = instance.getSizes();
          // context.minZoom = context.getMinZoom(sizes.viewBox.width,
          //                   sizes.viewBox.height, sizes.width, sizes.height)
          //
          // context.minZoom = context.getMinZoom(mapDimensions.width,
          //                   mapDimensions.height, containerDimensions.x, containerDimensions.y)
          //
          // Init Hammer
          // Listen only for pointer and touch events
          this.hammer = gesture;//new Gesture(options.svgElement);//, {
          //this.hammer.listen();


          let defaultPanCallback = (ev)=>{
              // On pan start reset panned variables
            //console.log('*** Pan Event: ***');
            //console.log(ev);
            if (ev.type === 'panstart') {
              //console.log('**************Pan START event: ***********');
              //console.log(ev);
              pannedX = 0
              pannedY = 0
            }

            // Pan only the difference
            instance.panBy({x: ev.deltaX - pannedX, y: ev.deltaY - pannedY})
            pannedX = ev.deltaX
            pannedY = ev.deltaY
          };

          // // Handle pan
          // this.hammer.on('panstart panmove', function(ev){
          //
          // })

          //Handle pinch
          let defaultPinchCallback = (ev)=>{

            // const xRatio = containerDimensions.x / mapDimensions.width;
            // const yRatio = containerDimensions.y/mapDimensions.height;

            // console.log(`>>>>>>>>>>>>> xRatio: ${xRatio}, yRatio: ${yRatio} <<<<<<<<<<<`);

            // const xRatio =   mapDimensions.width / containerDimensions.x;
            // const yRatio = mapDimensions.height/containerDimensions.y;

            // const minZoom = xRatio > yRatio ? xRatio : yRatio; //set the minZoom to the greater ratio

            if (! pinching) {
              //console.log('*************pinch START event: *********');
              //console.log(ev);



              initialScale = instance.getZoom();

              pannedX =0;
              pannedY =0;

              pinching = true;
            }
            //console.log('*********************NEW ZOOM PAN:*****')


            // const tempScale = initialScale * ev.scale;
            // let newScale = tempScale > minZoom ? tempScale : minZoom ;
            const newScale = initialScale * ev.scale

            //console.log('pannedX BEFORE ZOOM: ' +pannedX);
            instance.zoomAtPoint(newScale,ev.center);//,ev.center)
            //console.log('pannedX AFTER ZOOM :' + pannedX);
            instance.panBy({x:ev.deltaX- pannedX,y:ev.deltaY-pannedY});
            // instance.zoom(initialScale * ev.scale);//, ev.center);
            // instance.panBy({x:ev.deltaX - pannedX, y:ev.deltaY -pannedY} );
            pannedX = ev.deltaX;
            pannedY = ev.deltaY;

          };
          //attach default gesture callbacks to SvgGestureComponent context
          context.defaultPinchCallback = defaultPinchCallback;
          context.defaultPanCallback = defaultPanCallback;

          //the svg element which should be manipulated by drawing on, etc.
          //CAUTION: do not querry DOM by this class name because there is often
          //more than one <g> tag with this class name!
          context.panZoomGroup = options.svgElement.getElementsByClassName('svg-pan-zoom_viewport')[0];

          // Prevent moving the page on some devices when panning over SVG
          options.svgElement.addEventListener('touchmove', function(e){
            //console.log('*********** PREVENTING DEFUALT ************');
            e.preventDefault();
        });

        options.svgElement.addEventListener('touchend',(ev)=>{
          //console.log('TOUCHEND TRIGGERED');
          pinching = false;
        });

      }//end init

      , destroy: function(){
          this.hammer.destroy()
        }
      }

      let beforePan = function(oldPan, newPan){
        var stopHorizontal = false
          , sizes = this.getSizes()
          , stopVertical = false
          , gutterWidth =0 //sizes.viewBox.width / 10
          , gutterHeight =0 // sizes.viewBox.height / 10

            // Computed variables
          // ,leftLimit = -sizes.viewBox.width*sizes.realZoom + sizes.width

          ,leftLimit = -mapDimensions.width*sizes.realZoom + containerDimensions.x
          // ,leftLimit = -847.86
          ,rightLimit = 0
          ,topLimit = -mapDimensions.height*sizes.realZoom + containerDimensions.y
          ,bottomLimit = 0

        let customPan = {x: 0, y:0}
        customPan.x = Math.max(leftLimit, Math.min(rightLimit, newPan.x))
        customPan.y = Math.max(topLimit, Math.min(bottomLimit, newPan.y))


        // console.log('beforePan, sizes.viewBox.x, sizes.viewBox.y: ');
        // console.log(sizes.viewBox.x, sizes.viewBox.y);

        return customPan
      }
      return new Promise((res)=>{

        console.log(">>>>>>>>>>>>> svgPanZoom , containerDimensions: <<<<<<<<<<<<<<<<");
        console.log(containerDimensions);

        console.log('>>> mapDimensions: <<<');
        console.log(mapDimensions);

        this.svgPanZoom = svgPanZoom(this.svgElement.nativeElement, {
          zoomEnabled: true
        , controlIconsEnabled: false
        , fit: false //IMPORTANT: if this is omitted, seems to default to true
        ,contain: false //IMPORTANT: if this is omitted, seems to default to true
        //, center:false
        ,minZoom: context.minZoom
        ,maxZoom:5
        , customEventsHandler: this.eventsHandler
        , beforePan: beforePan
        });
        res(this.svgPanZoom);

      });

  }

  setDefaultPinch(gesture){
    gesture.on('pinch',this.defaultPinchCallback);
  }

  setDefaultPan(gesture){
    gesture.on('panstart panmove',this.defaultPanCallback);
  }


  convertCoordinatesToSvg(screenX:number,screenY:number){
    //Assumes svg-gesture component's top left corner is at 0,0 (screen coords)
    //input: x,y pair in untransformed screen coordinates
    //output: point object in svg coordinate system
    //note: coordinates in the screen's ref frame start with 'screen'
    //...not really... looks like pan.x and pan.y are in screen coords
        let screenX_Offset = 0;
        let screenY_Offset = 0;
        //adjust

        let screenBoundingBox = this.svgElement.nativeElement
                                .getBoundingClientRect();

        if (screenBoundingBox){
          screenX_Offset = screenBoundingBox.left;
          screenY_Offset = screenBoundingBox.top;
        }
        console.log('svg-gesture, (component), convertCoordinatesToSvg, screenBoundingBox:' );
        console.log(screenBoundingBox);

        let sizes = this.svgPanZoom.getSizes();
        console.log('svg-gesture (component), sizes:' );
        console.log(sizes);
        let pan = this.svgPanZoom.getPan();

        let realZoom = sizes.realZoom;

        let svgNamespace = "http://www.w3.org/2000/svg";
        let circle = document.createElementNS(svgNamespace,'circle');
        let x = (screenX - pan.x)/realZoom -screenX_Offset/realZoom;
        let y = (screenY - pan.y)/realZoom - screenY_Offset/realZoom;

      return {x:x, y:y};
  }


}
