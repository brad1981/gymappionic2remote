# Plastick: Make up routes, share them with the climbing community.

## Motivation

When I started climbing, we made up routes (called "problems"). This began out of necessity (there were too few problems in our gym).
Making up routes was endlessly fun and sharing them with friends was even better. Kids these days don't make up routes and that is a 
shame. That is where Plastick (this app) comes in. 

When you make up a route in the real world, in your climbing gym, you can use the route editor in Plastick to document it and upload it to the 
feed for your gym. You get a chance to describe the route with tags like "powerful", "balancy", "reachy" etc and other users can both vote on these 
attributes and filter results based on these. You can also filter by difficulty and wall angle. 

All sorts of problems come up: 

*  How will users discover and then find routes?
*  How do you distinguish between routes created by users and the official routes, created by the gym's employees?
*  When someone flags a route for removal from the system (because the real, physical holds have been reset by the employess), what process is used to comfirm that
   the route is actually gone. In other words, how do you prevent vandalism?


Plastick solves all of these problems and more. 

## Technology

Backend:

*  Firestore
*  Google app engine
*  Google cloud functions

Front End:

*  Ionic and Angular
*  [SVG panzoom](https://github.com/bumbu/svg-pan-zoom)

## Features
Several features and some related challenges are described below.

### The Feed View

![](https://media.giphy.com/media/0yXsVdckFZPS7f4rN0/giphy.gif?cid=790b7611102c70f6ee250d624350bcf430449ff1f1ea2c3c&rid=giphy.gif&ct=g)

This view displays a map of the climbing gym in the top half and an Ionic "slides" component in the bottom half.
Each slide card represents a climbing route and displays the route's name, difficulty (...,V3,V4,V5,V6,...), traits ("Steep", "Pumpy", etc).
(I didn't have a climbing wall handy for testing, so many of the photos in the gif above depict whatever was near my work station.)

The map is implemented with a custom component, called svg-map.


The [svg-map component](https://bitbucket.org/brad1981/gymappionic2remote/src/master/src/components/svg-map/) extends the [svg-gesture component](https://bitbucket.org/brad1981/gymappionic2remote/src/development/src/components/svg-gesture/)  which wrapps the [svg-panzoom](https://github.com/bumbu/svg-pan-zoom) module. 
svg-gesture implements an intialization hook: setChildClassHandlers, implemented as a class method. Derived classes, such as svg-map override this method, which is called by the parent class's (svg-gesture)
initialization code after various asynchronous dependencies have resolved. This hook is used by svg-map to set custom hammer.js gesture-driven callbacks, which determine the 
behavior of the map when users tap it (map the tap into the svg's corrdinate system, project the tap to the nearest line with class="wall" in the svg, search for the route with 
nearest coordinates to that projected tap, emit the index of the route that was tapped so various observers can react to this event and scroll the slides to the appropriate route).
Other components extend the svg-gesture component and override setChildClassHandlers to set their own handlers for various user generated gestures.

At the bottom of this routes view is the slides component, showing the "cards" representing routes. These display the routes in order, from the climber's left to the climber's right.
This is accomplished by maintaining a dictionary order relation mapping each route to a spacially ordered position along the walls (stored as a property in the database, used for sorting
results). The images displayed on the cards are [downloaded, cached and purged intelligently](https://bitbucket.org/brad1981/gymappionic2remote/src/master/src/providers/feed-cache4/), 
based on which routes the user is likely to scroll to next, using the slides. When making the above gif, the cache was severly limited in it's size for development purposes. 

## Comments and Replies
![](https://media.giphy.com/media/jaPBo23kJxa0dZzdVu/giphy.gif?cid=790b761166f6c21187888521075f39b79a4608522ee1380b&rid=giphy.gif&ct=g)

Through the [route-comment component](https://bitbucket.org/brad1981/gymappionic2remote/src/master/src/components/route-comment/), users can leave feedback for
one another sharing encouragement, praise or strategies for climbing routes. This component is a basic CRUD component with pagenation. For the demo pictured above, 
comments were loaded in batches of two for the purpose of development and debugging. The avatar image is pulled from the user's facebook profile.
Normally, users cannot reply to their own comments, but this was enabled for development and debugging (fun note: the profile photo in the video is [me walking a highline](https://www.flickr.com/photos/bradleydean/4362426953/in/photostream/).

### Firestore and Storage Bucket Cleanup

Plastick is built on top of Firestore and Google's "Storage" for storing photos. The data are stored in somewhat complicated structures, because in addition to 
pairing users with the routes that they created, the database also stores various data for workout analytics (front end in development) associated with the users, their comments, replies and more.
Route images and the metadata representing which holds should be circled are stored seperately, rather than saving an altered image. This simplifies the 
problem of making future edits to routes, which would be useful for fixing mistakes or uploading variations (these features were anticipated, but are not yet implemented).
Embedded in route data are references to documents tracking route feedback and this must track who submitted the feedback to avoid double-voting. 
[Several google cloud functions](https://bitbucket.org/brad1981/cloudfunctions_gymapp_v2.0/src/master/) are used to handle all of the complicated ways that the interdependant data must change or 
be cleaned up (removed) based on various events (write, read, edit) at 
various paths in the database.

## Credits:
Thanks to the folks who helped make this happen:
Dawson Weehunt (Worked on front end).
Jean-Pierre Chery (Design work, mock-ups and talking sense into me).


## Installation Notes:

I wrote these notes to myself a long time ago. 

*Installations:
npm install -g ionic cordova
nmp install --save angularfire2 firebase

ionic cordova plugin add cordova-plugin-camera --save
npm install --save @ionic-native/camera

NOTE: .gitignore prevents these files from uploading.
Run "npm install" in the terminal after a git pull to automatically install these
dependencies (they seem to be listed in package.json)
*Installations:
npm install -g ionic cordova
nmp install --save angularfire2 firebase

ionic cordova plugin add cordova-plugin-camera --save
npm install --save @ionic-native/camera

NOTE: .gitignore prevents these files from uploading.
Run "npm install" in the terminal after a git pull to automatically install these
dependencies.

