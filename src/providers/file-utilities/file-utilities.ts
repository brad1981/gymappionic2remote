import { Injectable } from '@angular/core';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File, FileEntry } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';

import { LocalStorageProvider } from '../local-storage/local-storage';

//TODO: delete betterConsole stuffs (for development only)
import { betterConsole }  from '../globalConstants/globalConstants';
let console = betterConsole;
/*
*** Background Information ***
basePath: this is a native file system path that is used internally by cordova
to find resources. it looks something like ///file.ionic.etc...

there are multiple possible base paths:
1.) the 'cache' is subject to cleanup by the file system (might be
deleted without our consent). get a valid path to the the cache like this:

const cacheBasePath = await fileUtilsProv.getBasePath('cache');
2.) the 'data' cache (similar to #1 above) does NOT experience random cleanup
by the file system.

to reference a directory (that you know exists) at <baseCachePath>/maps/gymId/current
use the above method to get cacheBasePath and then concatenate with 'maps/gymId/current/'
NOTE: 'maps/gymId/current/' did NOT start with a '/' and it DID end with a '/'.
*/

@Injectable()
export class FileUtilitiesProvider {

  //used by this.getBasePath for input validation
  basePathFlags:string[] = ['cache',
                             'data'
                            ];

  constructor(public transfer: FileTransfer,
    public file: File, public filePath:FilePath,
    public storage: LocalStorageProvider) {
  }

  getBasePath(target:string):Promise<string>{
    //Helper function for getting base path for various built-in file locations
    //IMPORTANT: only call this method with an argument contained in this.basePathFlags
    return new Promise((res,rej)=>{
      let knownFlag = false;
      for (let flag of this.basePathFlags){
        if (target === flag){
          knownFlag = true;
          break;
        }
      }
      if (!knownFlag){
        rej (new Error(`Unknown flag! Expected one of : \r`
          + `${this.basePathFlags.join(', ')} \r`
          + `But got ${ target } instead.`));
      }else{
        switch(target){
          case 'cache':
            res(this.file.cacheDirectory);
          case 'data':
            res(this.file.dataDirectory);
        }
      }

    });
  }

  //TODO: rewrite everything so that there is only the blocking version of this function
  getBasePathNoPromise(target:string):string{
    //Helper function for getting base path for various built-in file locations
    //IMPORTANT: only call this method with an argument contained in this.basePathFlags
    //returns null if cannot find base path
      let knownFlag = false;
      for (let flag of this.basePathFlags){
        if (target === flag){
          knownFlag = true;
          break;
        }
      }
      if (!knownFlag){
        return null;
        // let err = new Error(`Unknown flag! Expected one of : \r`
        //   + `${this.basePathFlags.join(', ')} \r`
        //   + `But got ${ target } instead.`);
        //   return(err);
      }else{
        switch(target){
          case 'cache':
            return(this.file.cacheDirectory);
          case 'data':
            return(this.file.dataDirectory);
        }
      }
  }

  private verifyOrCreateDir(path,dirName):Promise<string>{
    //if the directory exists, at the path, resolve with directory's path
    //if does not, attempt to create directory at path/dirName
    //if cannot, reject with error
    return new Promise((res,rej)=>{
    // console.log('verify or create. path: '+path+ ' dirName: '+dirName);
      this.file.checkDir(path,dirName).then(( exists )=>{
        if(exists){
          // console.log('EXISTS: ' + path+dirName+'/');
          res(path+dirName+'/');
        }
      }).catch((error)=>{//directory does not exist
        this.file.createDir(path,dirName,false).then(( directoryObject )=>{
          // console.log('created:');
          // console.log(directoryObject.nativeURL);
          res(directoryObject.nativeURL);
        }).catch((error)=>{
          rej(error);
        });
      });
    });
  }

  private createDirRecursive(fullTarget:string, fullCurrent:string):Promise<string>{
      //TODO: Input Validation:
        //[] parameters are nonEmpty
        //[] enforce maximum folder nesting depth (decide on depth)

      //fullTarget: full path to target (desired) directory
      //fullCUrrent: currently existing sub-directory chain (must me contained
      // in the fist n-charaters of fullTarget)
    ///file/app/data/cache/gyms/sbp/topos/photos/0.jpg
    return new Promise((res,rej)=>{
      if(fullCurrent === fullTarget){
        res(fullCurrent);
      }else{
        if(fullCurrent.length >= fullTarget.length){ rej(new Error('Length Error!'))}
        //get remaining dir structure to create
        let remainder = fullTarget.slice(fullCurrent.length);
        //get next directory
        let endIndex = remainder.search('/');
        if (endIndex === -1){
          rej(new Error('Wrong input format. Got:'));
          // console.log("fullTarget: "+ fullTarget);
          // console.log('fullCurrent: '+ fullCurrent);
        }
        let nextDir = remainder.slice(0,endIndex);

        //pass current,nexDir into verifyOrCreateDir
        this.verifyOrCreateDir(fullCurrent,nextDir).then(( newPath )=>{
          //result-> createDirRecursive(fullTarget, result)
          this.createDirRecursive(fullTarget,newPath).then(( completePath )=>{
            // console.log('Resolving Recursive Method and completePath is:');
            // console.log(completePath);
            res(completePath);
          }).catch((error)=>{
            rej(error);
          });
        }).catch((error)=>{
          rej(error);
        });
      }
    });
  }

  checkOrBuildPath(pathFromParent:string, parentFlag:string):Promise<string>{
    //pathFromParent: path to desired directory from parentFlag's corresponding path
    //ex: if you want this path: pathFromParent-> gms/sbp/topos/photos in the cache dir
    //then parentFlag-> 'cache'
    //and the method resolves with: `${this.file.cacheDirectory}/gyms/sbp/topos/photos/`
    //...indicating that the path is now valid

    return new Promise((res,rej)=>{
      //check that pathFromParent is a string
      if(typeof(pathFromParent) !== typeof('')){
        rej(new Error("pathFromParent must be of type string. Got: "+ typeof(pathFromParent)));
      }
      //check that is does not start with a '/'
      if(pathFromParent[0] === '/'){
        pathFromParent = pathFromParent.slice(1, pathFromParent.length);
      }
      //check that it DOES end with a '/'
      if(pathFromParent[pathFromParent.length - 1] !== '/'){
        pathFromParent = pathFromParent + '/';
      }

      this.getBasePath(parentFlag).then(( base )=>{
          this.createDirRecursive(base+pathFromParent, base).then(( verifiedPath )=>{
              res(verifiedPath);
          }).catch((error)=>{ rej(error); }); }).catch((error)=>{ rej(error); });
    });


  }

  getPathAndFileName(pathToFile:string){
    //pathToFile: 'blah/foo/bar/filename.baz'
    //output: array of form ['blah/foo/bar/'', 'filename.baz']
    //TODO: input validation: deal with leading slash
    let i = pathToFile.lastIndexOf('/');
    return [pathToFile.substring(0,i+1),
           pathToFile.substring(i+1,pathToFile.length)];
  }

  getUseabePathFromNativePath(nativePath:string):Promise<string>{
    return this.filePath.resolveNativePath(nativePath);
  }

  validateNativePathStructure(nativePath:string){
    //check that nativePath is correctly formatted, but not that the directory actually exists
    //check that nativePath contains basePath corressponding to one of basePathFlags
    let basePaths = this.basePathFlags.filter((flag)=>{
      if(nativePath.indexOf(this.getBasePathNoPromise(flag)) === 0){
        return true;
      }
    });
    let basePath = '';
    if(basePaths.length === 0){
      return false;
    }else{
      let basePath = basePaths[0];
      let customPath = nativePath.slice(basePath.length + 1);
      //included leading "/" in customPath or did not end customPath with "/"
      if(customPath[0] === '/' || customPath[customPath.length - 1] !== "/" ){
        return false;
      }
    //it's probably properly formatted... probably
    return true;

    }

  }

  async displayContents(flag, dirName){
    // let basePath = await this.getBasePath(flag);
    let basePath = this.getBasePathNoPromise(flag);
    let files = await this.file.listDir(basePath,dirName);
     console.log(`##*******## CONTENTS OF ${basePath}/${dirName} ##***##`);
    files.forEach(file=>{
      console.log(file);})
  }

/*
dirName: the name of path to the directory from the basePath determined by the flag (for example: 'cache')
//NOTE: this only works if dirName is the direct descndant of the path generated by flag
//for example, if you want a list of files in <path_to_system_cache>/feedCahce
//...call this method with flag='cache', dirName = 'feedCache'
//but, if you want a list of files in <path_to_system_cache>/feedCahce/subDirectory
//...this method cannot help you (it was specifically made to help out the feedCache code)
*/
  async getFileNames(flag, dirName){
    try{
      // const basePath = await this.getBasePath(flag);
      const basePath = this.getBasePathNoPromise(flag);
        const fileObjects = await this.file.listDir(basePath,dirName);
        //just return the id

        //TODO check fileObj.name.split('_')[0] to make sure it exists, is the right length?
        //basically, some kind of validation
        return fileObjects.map(fileObj => fileObj.name);
    }catch(error){
      // console.log(`Error in fileUtilitiesProv, getFileNames :${ error } `);
      throw error;
    }

  }


  verifyRemoveFile(nativePath, fileName ):Promise<any>{
    //wraps file.removefile. resolve when file cannot be removed because it DNE (instead of throwing)
    //throws error if fileName's parent directory DNE
    //resolves 'r' if method removed it, resolves 'n' if the file was not present
    return new Promise((res,rej)=>{
      /***** File's parent directory exists?  ************/
      let path = "";
      //remove trailing "/" if present
      if(nativePath[nativePath.length-1]=="/"){
        //remove trailing "/"
        path = nativePath.slice(0,nativePath.length-1);
      }else{
        path = nativePath;
      }

      let i = path.lastIndexOf("/");
      if(i == -1){
        rej(new Error('Improperly formatted nativePath: '+nativePath ));
      }
      let dirName = path.slice(i+1);
      let parentDir = nativePath.slice(0,i+1);
      this.file.checkDir(parentDir, dirName).then(( exists )=>{
        //fileName's parent directory DOES exist
        if(exists){
          this.file.removeFile(nativePath,fileName).then(( result )=>{
            // console.log("File remove result: ", result);
            res('r');
          }).catch((error)=>{
            // console.log("Error trying to remove file at path: "+nativePath+" and name: "+ fileName);
            // console.log(error);
            res('n');//resolve: purppose of this method is to confirm the file is not there. it's not there, right?
          });
        }else{
          rej(new Error("Parent directory did not exist.... this should be unreachable code!"));
        }
      }).catch((error)=>{
        //we learned that we're looking in the wrong directory (since the directory we're looking in DNE)
        rej(error);
      });
    });
  }


  //wrapper for checkDir. This method returns false if a directory does not exist,
  //rather than (counterintuitively) throwing an error.
  async dirExists(flag:string,dirName:string ):Promise<boolean>{
    // const basePath = await this.getBasePath(flag);
    const basePath = this.getBasePathNoPromise(flag);
    let exists = false;
    exists = await this.file.checkDir(basePath, dirName).catch(error=>{
      // console.log('setting exists to false');
      exists = false;
      return exists;
    }) as boolean;
    // console.log('returning exists as: ', exists);
    return exists;
  }
//need to combine basePath with path to (but not including directory)
// async getFileNamesFromPathToDir(flag,pathWithDir){
//   //remove trailing '/' if present
//   let lastIndex = pathWithDir.length -1;
//   if(pathWithDir[lastIndex] === '/'){
//     pathWithDir = pathWithDir.slice(0,pathWithDir.length-1);
//   }
//   const dirName = pathWithDir.slice(pathWithDir.lastIndexOf('/') + 1);
//   console.log(`dirName is: ${  } `);
//   try{
//     const basePath = await this.getBasePath(flag);
//     lastIndex = pathWithDir.lastIndexOf('/');
//     const midPath = basePath.slice(0,lastIndex + 1);
//     // const fileObjects = await this.file.listDir(basePath+)
//
//   }
//   catch(error){
//       //handle error
//   };
//
//   }

}
