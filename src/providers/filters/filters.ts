import { Injectable } from '@angular/core';
import { Platform, Events} from 'ionic-angular';
import { AngularFireAuth } from '@angular/fire/auth';

import { RouteFilters } from '../../providers/globalConstants/globalConstants';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

import * as firebase from 'firebase/app';//used for definig types

// interface RangeFilter{
//   lower:number;
//   upper:number;
// }
//
// //properties for use with 'where' clause in Route documents
// export interface Filters{
//     slab?:boolean;
//     fingery?:boolean;
//     mantle?:boolean;
//     face?:boolean;
//     pumpy?:boolean;
//     sloper?:boolean;
//     overhang?:boolean;
//     balancy?:boolean;
//     pinchy?:boolean;
//
//     grades?: RangeFilter;
//
//     //possible range filters
//     moves?: RangeFilter;
//     stars?:RangeFilter;
//   }

@Injectable()
export class FiltersProvider {
  userId:string;
  filtersDocRef:any;
  minGrade = 0;
  maxGrade = 14;
  //TODO: dynamically set this in the template

  // filters: RouteFilters = {
  //   attributes:{
  //     slab:false,
  //     fingery:false,
  //     mantle:false,
  //     face:false,
  //     pumpy:false,
  //     sloper:false,
  //     overhang:false,
  //     balancy:false,
  //     pinchy: false,
  //   },
  //
  //   rangeFilters: {
  //     grades: {lower:0, upper:6}
  //   }
  // };

  filters:RouteFilters=null;

  constructor(
    public platform: Platform,
    public afAuth: AngularFireAuth,
  ) {
    this.platform.ready().then(()=>{
      const authListener = afAuth.authState.subscribe((user)=>{
        if (user){
          this.userId = user.uid;
          authListener.unsubscribe();
        }
      })
    });
  }

  async init(){
    //try to get filters from firestore
    let ref = await this.getFiltersDocRef();
    if(ref){
      this.filtersDocRef = ref;
      try{
        const filtersSnap = await this.filtersDocRef.get();
        if(filtersSnap.exists){
          //console.log('********* SETTING FILTERS FROM firestore snap');
          const source = filtersSnap.metadata.fromCache ? "local cache" : "server";
          //console.log('*** filters came from ', source);
          const tempFilters = filtersSnap.data();
          if(!tempFilters.hasOwnProperty('rangeFilters') || !tempFilters.hasOwnProperty('attributes')){
            await this.setDefaults();
          }else{
            this.filters = tempFilters;
          }

          // console.log(`filtersProv got filters from database as :`);
          // console.log(this.filters);

          return this.filters;
        }else{//there was no record of filters, so set defaults
          this.filters = await this.setDefaults();
          // console.log(`filtersProv set default filters as :`);
          // console.log(this.filters);
          return this.filters;
        }
      }catch(error){
        throw(error);
      }
    //could not get from firestore
    }else{//could not get a ref
      try{
        this.filters = await this.setDefaults();
        return this.filters;
      }catch(error){
        throw error;
      }
    }
  }

  getDefaultFilters():RouteFilters{
    return {
        setBy:"anyone",
        attributes:{
        },

        rangeFilters: {
          grade: {lower:0, upper:6},
          wallAngle:{lower:0, upper:5 }
        }
      }
  }

  async setDefaults():Promise<RouteFilters>{
    //set default filters (allow all routes from v0 to v6 )
    //set this.filters to default
    //try to save to firestore (should work offline too. Will just save to local firestore)
    //if saving rejects with error, resolve with this.filters anyway (the defaults)
    // const filters: RouteFilters = {
    //   rangeFilters: {
    //     //IMPORTANT: range filter keys mus MATCH the key used in RouteData (or FirestoreRouteData)
    //     //for example, use key "grade", NOT "grades"
    //     grade: {lower:0, upper:6}
    //   }
    // };
    //TODO: build this using filterCategoryObject exported from global constants

    //console.log('********* SETTING DEFAULT FILTERS ************' );
    this.filters = this.getDefaultFilters();

    //this.filters = filters;
    if(this.afAuth.auth.currentUser){//TODO check that this actually fails for a guest login
      try{
        await this.saveFilters(this.filters);
        return this.filters;
      }catch(error){
        return this.filters;
      }
    }else{//cannot save filters to db because user is not logged in
      return this.filters;
    }
  }

  async getFiltersDocRef(){
    if (this.afAuth.auth.currentUser){
      let db = firebase.firestore();
      let ref = db.collection('USERS').doc(this.afAuth.auth.currentUser.uid)
        .collection('FILTERS').doc('filters');
      // return  this.afs.collection('USERS').doc(this.afAuth.auth.currentUser.uid)
      //   .collection('filters').doc('filters');
      return ref;
    }else{
      return null;
    }
  }

  async saveFilters(filters:RouteFilters):Promise<any>{
    //TODO: check that user is logged in (not isAnonymous), filters is defined, else throw error
    const filtersRef = await this.getFiltersDocRef();
    let scrubbedAttributes = {};
    console.log('saveFilters: 1' );
    if(filters.hasOwnProperty('attributes')){
      Object.keys(filters.attributes).forEach(attrKey =>{
        if(filters.attributes[attrKey]){
          scrubbedAttributes[attrKey]=true;
        }
      });
    }
    this.filters = filters;
    filters['attributes']=scrubbedAttributes;

    console.log('filters.ts (provider), saveFilters, this.filters:');
    console.log(this.filters);

    try{
      console.log('saveFilters: about to set filters' );
      await filtersRef.set(filters);

      console.log('SUCCESS saving filters');
      return filtersRef;
    }catch(error){
      console.log('ERROR saving filters');
      throw( error );
    }

  }

  async getFilters(){
    // console.log('*** filtersProv-> getFilters and this.filters is:');
    // console.log(this.filters);
    if(!this.filters){
      try{
        // console.log('*** filtersProv-> getFilters... calling this.init():');
        this.filters = await this.init();
        return this.filters;
      }catch(error){
        throw error;
      }
    }else{//filters were already defined
      // console.log('*** filtersProv-> getFilters... in else. Filters were already defined as:');
      // console.log(this.filters);
      return this.filters;
    }
  }

  //TODO: when multiple range type keys become available,

async  getFiltersAndOrderBy(){
    //get object without the false values
    //this is the object that gets stored in firebase
    // console.log('In filtersProv, building queryObj');
    let rangeFieldKeys = [''];
    let queryObj = {};

    if(!this.filters){
      await this.init();
    }

    // console.log('**&&**&&**&& RETURNING query Object');

    return {filters: this.filters,
            orderBy: 'location.dist'};
  }

}
