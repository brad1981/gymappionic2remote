import { Component, Input } from '@angular/core';
import { Platform } from 'ionic-angular';
import { RouteFilters,
         RouteEditorFeatures, 
         RangeFilter,
         EditorOptions,
         filterCategoriesObject,
         orderedColorNames,
         colorNamesToColorCodes
       } from '../../providers/globalConstants/globalConstants';
import { FiltersProvider } from '../../providers/filters/filters';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;




@Component({
  selector: 'filter-selector',
  templateUrl: 'filter-selector.html'
})
export class FilterSelectorComponent {

  text: string;

  filters: RouteFilters | RouteEditorFeatures  = null;

  editorOptions:EditorOptions = null;

  filterCategoryObject:any = null;
  attributeCategories:string[] = null;
  rangeFilterCategories:string[]= null;

  orderedColorNames: string[] = null;
  colorNamesToColorCodes: {[key:string]: string} = null;

  categoriesReady: boolean = false;
  Object:any;//for using Object.keys in template

  showFeedbackModeMessages: boolean = false; //toggle whether to see feedback mode messages

  @Input() mode: string = null;//options: 'filter' xor 'edit'
          //'filter'-> sliders will have 2 vaues: one for lower and one for upper. used to filter feed results
          //'edit'-> sliders will have 1 value only. Used to describe new routes
  @Input() message: string = null;
  @Input() routeEditorFeatures: RouteEditorFeatures = null;


  /************** Overview **************************
  To use this component:
  pass mode: 'edit' xor 'filter' via @input

  NOTE: in 'edit' mode, this.filters will have type: RouteEditorFeatures
        in 'filter' mode, this.filters will have type: RouteFilters

  edit: for use in the route editor (sliders will have one value/knob)
  filter: for use in filtering views (sliders will have two values/knobs)

  To collect information from this component, select the component with
  @ViewChild('filterSelector') filterSelector: FilterSelectorComponent ;
  ...then call filterSelector.getFilters() as <Desired Type>
  ...where <Desired Type> is one of RouteFilters or RouteEditorFeatures

  ****************************************************/
  constructor(
    public filtersProv: FiltersProvider,
    public platform: Platform ) {

    this.platform.ready().then(async ()=>{
      this.Object = Object; //Important: allows use of Object.keys in template!

      this.filterCategoryObject = filterCategoriesObject;
      this.orderedColorNames = orderedColorNames;
      this.colorNamesToColorCodes = colorNamesToColorCodes;

      this.attributeCategories = Object.keys(filterCategoriesObject.attributes);
      this.rangeFilterCategories = Object.keys(filterCategoriesObject.rangeFilterCategories);
      this.categoriesReady = true;


    });

  }

  //in filter page, call this (use @viewchild) from ionViewCanLeave, to get the final state of the filters
  getFilters():RouteFilters | RouteEditorFeatures {
    //
    // let scrubbedAttributes = {};
    // console.log('saveFilters: 1' );
    // if(this.filters.hasOwnProperty('attributes')){
    //   Object.keys(this.filters.attributes).forEach(attrKey =>{
    //     if(this.filters.attributes[attrKey]){
    //       scrubbedAttributes[attrKey]=true;
    //     }
    //   });
    // }
    // this.filters['attributes']=scrubbedAttributes;
    console.log('filter-selector.ts, getFilters, returning this.filters:');
    console.log(this.filters);

    return this.filters;
  }

  ngAfterContentInit(){
    console.log('**&&**&&**&& After contentInit, calling initFilters:');
    this.initFilters();
  }

  async initFilters(modeOverride:string = null){
    console.log('checking mode! *******');
    console.log('in initFilters, mode is:');
    console.log(this.mode);
    console.log('modeOverride: ', modeOverride);

    let mode = modeOverride ? modeOverride : this.mode;

    switch(mode){
      case 'filter': {//in 'filter' mode, this.filters has type RouteFilters
        console.log('In filter-selector component, mode was filter **************');
        this.filters = await this.filtersProv.getFilters() as RouteFilters;
        console.log('filter-slector, initFilters: Got this.filters as');
        console.log(this.filters);
        return this.filters;
      }
      case 'edit':{//in 'editor' mode, this.filters has type RouteEditorFeatures

        if(this.routeEditorFeatures){
          console.log('setting this.filters from this.routeEditorFeatures: ');
          console.log(this.routeEditorFeatures);
          this.filters = this.routeEditorFeatures;
        }else{//there are no existing features, so we're setting the defaults

          console.log('setting filtersWithRanges from filtersProv: ');
          let filtersWithRanges = this.filtersProv.getDefaultFilters();
          console.log('got filtersWithRanges as:');
          console.log(filtersWithRanges);
          let filters:RouteEditorFeatures = {attributes: filtersWithRanges.attributes, rangeFilterVal: {} };
          //set the default value of each rangeFilter feature to be at the middle of the available range
          const rangeFilterKeys = Object.keys(filtersWithRanges.rangeFilters);

          console.log('rangeFilterKeys are:');
          console.log(rangeFilterKeys);

          console.log('before modifying filters, filters is:');
          console.log(filters);

          rangeFilterKeys.forEach(rangeKey => {
            const range:RangeFilter = filtersWithRanges.rangeFilters[rangeKey];
            console.log('range:');
            console.log(range);
            console.log("range.lower, range.upper : ", range.lower," , ", range.upper )
            filters.rangeFilterVal[rangeKey] = Math.round((range.lower + range.upper)/2);
            console.log('filters is now:');
            console.log(filters);
          });

          console.log('initFilters finishing. filters is:');
          console.log(filters);
          //filtersWithRanges doesn't have ranges anymore;
          this.filters = filters;

          console.log('about to return this.filters:');
          console.log(this.filters);

          return this.filters;
        }

      }
      case 'feedback':{
        //TODO:
        //1.) Define type for feedback object in global constants and import it (might be able to just use edit mode's)
        //...basically same type as edit mode uses, except does not have a description and title
        //2.) Copy and modify 'edit' case above to generate this.filters.
        //3.) Either:
        //    A: set a flag that hides description and title fields in template and just uses 'edit' mode html
        //    B: write html for 'ffedback' mode <--prob better idea because it will be easiest to modify later

        this.showFeedbackModeMessages = true;
        console.log('filter-selector-component, initFilters.... case feedback');
        return await this.initFilters('edit');

      }
    }
  }


}
