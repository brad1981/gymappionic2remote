//TODO: for storage, use the regular firebase SDK for not, not AngularFireStorageModule
//...because it does not appear to exist yet (9/4/2017)

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';

import { Facebook } from '@ionic-native/facebook';

//import {FirebaseRef} from 'angularfire2';


import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { AndroidFullScreen } from '@ionic-native/android-full-screen';

import { Camera } from '@ionic-native/camera';

import { MyApp } from './app.component';
import { AuthProvider } from '../providers/auth/auth';
import { UserDetailsProvider } from '../providers/user-details/user-details';
import { GymsProvider } from '../providers/gyms/gyms';

import {IonicImageCacheModule} from 'ionic3-image-cache';
import { IonicStorageModule } from '@ionic/storage';
import { LocalStorageProvider } from '../providers/local-storage/local-storage';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Network } from "@ionic-native/network";
import { ConnectionProvider } from '../providers/connection/connection';
import { SettingsProvider } from '../providers/settings/settings';
import { NotificationsProvider } from '../providers/notifications/notifications';
//import { RouteDetails3Provider } from '../providers/route-details3/route-details3';
//import { SyncatronProvider } from '../providers/syncatron/syncatron';
import { UrlProvider } from '../providers/url/url';
//import { Routes2Provider } from '../providers/routes2/routes2';
import { RouteDetails4Provider } from '../providers/route-details4/route-details4';
// import { FeedProvider } from '../providers/feed/feed';
import { NewRouteUtilitiesProvider } from '../providers/new-route-utilities/new-route-utilities';
import { ImageProvider } from '../providers/image/image';

import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { FileUtilitiesProvider } from '../providers/file-utilities/file-utilities';
import { FiltersProvider } from '../providers/filters/filters';
import { FilteredRoutesProvider } from '../providers/filtered-routes/filtered-routes';
import { MapsProvider } from '../providers/maps/maps';
//import { FeedBufferProvider } from '../providers/feed-buffer/feed-buffer';
//import { FeedCacheProvider } from '../providers/feed-cache/feed-cache';
// import { FeedCache2Provider } from '../providers/feed-cache2/feed-cache2';
// import { FeedCache3Provider } from '../providers/feed-cache3/feed-cache3';
import { FeedCache4Provider } from '../providers/feed-cache4/feed-cache4';
import { AppCenterAnalytics } from '@ionic-native/app-center-analytics';
import { AppCenterCrashes } from '@ionic-native/app-center-crashes';
import { ListsProvider } from '../providers/lists/lists';
import { FeedbackProvider } from '../providers/feedback/feedback';
import { SendProvider } from '../providers/send/send';
import { TimeProvider } from '../providers/time/time';
import { DeleteContestProvider } from '../providers/delete-contest/delete-contest';
// import { ListCacheProvider } from '../providers/list-cache/list-cache';

// Initialize Firebase
export const firebaseConfig = {
    apiKey: "AIzaSyC1Iz2jBBzHQ0YSaVNsvOeuOmHnLpsZYaw",
    authDomain: "gymionic2.firebaseapp.com",
    databaseURL: "https://gymionic2.firebaseio.com",
    projectId: "gymionic2",
    storageBucket: "gymionic2.appspot.com",
    messagingSenderId: "744303913157",
    timestampsInSnapshots:true
  };


@NgModule({
  declarations: [
    MyApp,
    //SvgMapComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),

    IonicImageCacheModule.forRoot(),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    //TODO: phase out AngularFireDatabase
    AngularFireDatabaseModule,
    //AngularFirestoreModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    UserDetailsProvider,
    GymsProvider,
    IonicStorageModule,
    File,
    //Transfer,
    FileTransfer,
    FileTransferObject,
    Camera,
    AndroidFullScreen,
    FilePath,
    LocalStorageProvider,
    Network,
    ConnectionProvider,
    SettingsProvider,
    NotificationsProvider,
    //RouteDetails3Provider,
    //SyncatronProvider,
    UrlProvider,
    //Routes2Provider,
    RouteDetails4Provider,
    // FeedProvider,
    //FeedBufferProvider,
    NewRouteUtilitiesProvider,
    ImageProvider,
    FileUtilitiesProvider,
    // FiltersProvider,
    FiltersProvider,
    FilteredRoutesProvider,
    MapsProvider,
    //FeedBufferProvider,
    //FeedCacheProvider,
    // FeedCache2Provider,
    // FeedCache3Provider,
    FeedCache4Provider,
    AppCenterAnalytics,
    AppCenterCrashes,
    Facebook,
    ListsProvider,
    FeedbackProvider,
    SendProvider,
    TimeProvider,
    DeleteContestProvider,
    // ListCacheProvider,
  ]
})
export class AppModule {}
