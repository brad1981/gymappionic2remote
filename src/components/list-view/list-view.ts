import { Component,
  ViewChild,
  Input,
  Output,
  EventEmitter } from '@angular/core';

import { ListsProvider } from '../../providers/lists/lists';
import { AlertController,
         LoadingController,
        reorderArray,
        List,
          } from 'ionic-angular';

import {IonicImageCacheConfig} from 'ionic3-image-cache';

import { FilteredRoutesProvider, FireStoreObservableRoute } from '../../providers/filtered-routes/filtered-routes';
import { FeedCache4Provider } from '../../providers/feed-cache4/feed-cache4';


import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { from } from 'rxjs'
import { BehaviorSubject} from 'rxjs/BehaviorSubject';

// import 'rxjs/operators/publishReplay';

import { betterConsole,
         // convertGradeToColorCode,
         // colorNamesToColorCodes,
         RouteObject }  from '../../providers/globalConstants/globalConstants';

let console = betterConsole;

export interface RouteSelectedData {
  route: RouteObject,
  imgLocation?: string
  cache: FeedCache4Provider
}


/*
USAGE:
Only include this component if the fsObs and feedCache (see @Input section)
have been fully initialized.
*/
@Component({
  selector: 'list-view',
  templateUrl: 'list-view.html'
})
export class ListViewComponent {

  // @ViewChild('#avatarCache') avatarCache: IonicImageCacheModule;

  text: string;
  listNameArr: string[];
  selectedList: string;
  currentListPushKeyArr: string[] = [];

  listArr$:Observable<RouteObject[]> = null;

  index$: BehaviorSubject<number> = new BehaviorSubject(0);

  listOptionsToggle:boolean = false;;
  showListOptions:boolean = false;
  hasReordered:boolean = false;
  canClick: boolean = true;

  //TODO: rename these: they're actually used for the item dimensions, not the list dimensions
  // localListDisplayWidth: number = null;
  // localListDisplayHeight: number = null;

  currentUnfilteredRoutes: RouteObject[] = null;
  unfilterdRoutesSubscrip: Subscription;

  //for debugging
  setListCounter:number = 0;
  //end debugging variables

  initComplete:boolean = false;
  index$UpdaterSubscrip:Subscription = null;
  lKey = "list";

  @Input() fsObs: FireStoreObservableRoute = null;
  @Input() feedCache: FeedCache4Provider = null;//try to use the instance of feedcache that was created in for the routes-feed page.
  @Input() visibility:boolean = true;
  @Input() displayDimensions:{height:number,width:number} = null;
  @Input() colorNamesToColorCodes:{[key:string]:string};
  @Input() filteredRoutes$: BehaviorSubject<RouteObject[]>;
  @Input() scrollTop$:BehaviorSubject<number>;

  @Output() routeSelected = new EventEmitter();

  constructor(public loadingCtrl: LoadingController, public listsProv:ListsProvider,
    public alertCtrl: AlertController, public ionicImageCacheConfig: IonicImageCacheConfig  ) {
      ionicImageCacheConfig.setCacheDirectoryName("avatar");
  }

  async init(){
    this.listArr$ = await this.listsInit();

    //TODO: unsubscribe from this...
    this.unfilterdRoutesSubscrip = this.fsObs.behaviorSubjRoute$.subscribe(routes=>{
      console.log('list-view.ts (componet), init, fsObsubjectRoutes emitting routes as:');
      console.log(routes);
      this.currentUnfilteredRoutes = routes;
    });

    await this.feedCache.init(this.listArr$, this.index$);
    // console.log('list-view (component), init: setting initComplete=true');

    this.initComplete = true; //controls inclusion of template components

    // this.testPrintFromRoutesObs(this.listArr$);

    // this.scrollTop$.subscribe(top=>{
    //   console.log(`********* SCROLL list-view (component), top: ${top} **********`);
    // });


    if(!this.index$UpdaterSubscrip){
      // const topOffset = this.getRouteListTopOffset();
      // const itemHeight = this.getRouteItemHeight();
      this.index$UpdaterSubscrip = this.scrollTop$.subscribe(top=>{
        console.log('list-view.ts (component), index$UpdaterSubscrip callback, top:');
        console.log(top);
        // const index = Math.floor((top)/130);

        const index = Math.floor((top)/this.displayDimensions.height);

        console.log(`******** list-view (component), index$UpdaterSubscrip: NEW INDEX: ${index} `);
        //IMPORTANT:only emit a new index$ value if it has changed
        if(this.index$.getValue() != index){
          this.index$.next(index);
        }
      });
    }
    return true;
  }


  ngAfterViewInit(){
    // console.log('NNNNNNNNN list-view (component), ngAfterViewInit: begin execution NNNNNNNNN');
    // console.log('NNNNNNNNNN visibility is:', this.visibility);
    console.log('list-view.ts (component), ngAfterViewInit: calling test print with all filtered routes passed in from routes-feed NNNNNNNN');
    this.testPrintFromRoutesObs(this.filteredRoutes$);
    
    this.init().then(()=>{
      console.log('GET NATIVE ELEMENT');
      const list = document.getElementById('ionList');

      // const list = this.ionList.getNativeElement;
      console.log('** native element list **');
      console.log(list);

    });
  }

  testPrintFromRoutesObs(routesObs:Observable<RouteObject[]>){
    console.log('list-view (component) TTTT test print all routes TTTT');
    if(routesObs){
      let listSubscrip = routesObs.subscribe(route=>{
        console.log(route);
      });
      // listSubscrip.unsubscribe();
    }
  }

  async listsInit():Promise<Observable<RouteObject[]>>{

    // console.log(`1...LLLLLLLL list-view (component) listsInit: BEGIN EXECUTION LLLLLLLLLLLL`);

    try{
      if(!this.fsObs){

        //TODO: try to set a default fsObs from the FeedCache4Provider
        throw new Error('list-view (component), listsInit: no this.fsObs');
      }

      if(!this.feedCache){
        throw new Error('list-view (component), listsInit: no this.feedCache');
      }

      //TODO: fetch the listNameArr, rather than using hard-coded array. Doing this
      //for now because custom lists are fetching routes for all maps, not just the current map

      // this.listNameArr = await this.listsProv.returnListNameArr();
      this.listNameArr = ["All Routes", "My Routes"];
      console.log("rf listsInit got listNameArr as:", this.listNameArr);
      //refresh list $ and ensure selected list still exists, if not default to "All Routes"
      // console.log("looking for : ", this.selectedList, " inside of: ", this.listNameArr);
      let exists = false;
      for(let i=0; i<this.listNameArr.length; i++){
        if(this.listNameArr[i] == this.selectedList){
          // console.log("calling set list w/ ", this.selectedList);

          // console.log(`SSSSSSSSSS list-view (component) listsInit: calling setList for the ${this.setListCounter}th time SSSSSSS`);
          // this.setListCounter += 1;

          console.log('2...LLLLLLLLL list-view (component), listsInit: calling setList');
          this.listArr$ = await this.setList2(this.selectedList, true);

          console.log('6... LLLLLL list-view (component), listsInit: calling feedCache.init ');


          await this.feedCache.init(this.listArr$, this.index$);

          console.log('7... LLLL list-view (component), listsInit: END execution ');

          exists = true;
          return this.listArr$;
        }
      }if(!exists){
        //QUESTION: doesn't set list automatically defualt to "ALL Routes"? What am I missing?
        //Does this block ever execute?

        // default to "All Routes"
        this.selectedList = "All Routes";
        // console.log("calling set list w/ All routes" );
        // console.log(`SSSSSSSSSS list-view (component), listsInit: Add Again callback: calling setList for the ${this.setListCounter}th time SSSSSSS`);
        // this.setListCounter += 1;
        console.log('list-view (component), listsInit: calling setList2 from if(!exists)');
        this.listArr$ = await this.setList2(this.selectedList, true);
        return this.listArr$;

      }

    }catch(err){
      console.log('list-view (component), listsInit... Error!');
      throw err;
    }
  }


  //list: list of routePushkeys (for the list we're setting)
  //toggleLO: TODO: figure out what this does and add documentation
  // async setList(list:string, toggleLO:boolean){
  //   console.log('SSSSSSSSS list-view (component), setList: begin execution');
  //   try{
  //     this.listNameArr = await this.listsProv.returnListNameArr();
  //     console.log("setLists, floorLists:", this.listNameArr);
  //     this.selectedList = null;
  //     this.listNameArr.forEach((item) => {
  //       if(item === list){
  //         // console.log("setting selectedList as:", list);
  //         this.selectedList = list;
  //       }
  //     });
  //     if(this.selectedList === null){
  //       // console.log("list did not exist in lnArr, setting selectedList to All Routes");
  //       this.selectedList = "All Routes";
  //     }
  //     if(this.selectedList === "All Routes"){
  //       // console.log("selected list is All ROutes!");
  //       this.listOptionsToggle = false;
  //       this.showListOptions = false;
  //       try{
  //
  //         // this.reloadRoutesOnDidEnter = true;
  //
  //         // console.log(`IIIIIIIIIIIIIII routes-feed (page), setList: calling this.init() for the ${this.initCounter}th time IIIIIIIIIIIIII`)
  //         // this.initCounter+= 1;
  //
  //         // await this.init();
  //         console.log('SSSSSSSSS list-view (component), setList: settings this.listArr$');
  //         // this.listArr$ = this.fsObs.filteredRoute$;
  //         this.listArr$ = this.filteredRoutes$;
  //
  //       }
  //       catch(err){
  //         throw err;
  //       }
  //     } else{//build the custom list
  //       try{
  //         this.showListOptions = this.listsProv.isListCustom(list);
  //         this.currentListPushKeyArr = await this.listsProv.returnListArr(list);
  //
  //
  //         console.log('SSSSSSSSS list-view (component), setList: settings this.listArr$, by calling getRouteObjObsFromPushKeys');
  //         this.listArr$ = this.getRouteObjObsFromPushKeys(this.currentListPushKeyArr);
  //         //
  //         // let listSubScrip = this.listArr$.subscribe(routes=>{
  //         //   console.log('listArr$ is:');
  //         //   console.log(routes);
  //         // });
  //         //
  //         // listSubScrip.unsubscribe();
  //
  //         // update cache to reflect list
  //
  //         // let nextIndexArr = [];
  //         // for(let i=0; i< listArr.length; i++){
  //         //   for(let i2=0; i2< this.currentUnfilteredRoutes.length; i2++){
  //         //     if(listArr[i].pushKey == this.currentUnfilteredRoutes[i2].pushKey){
  //         //       nextIndexArr.push(i2);
  //         //     }
  //         //   }
  //         // }
  //
  //         // for(let i=0; i<nextIndexArr.length; i++){
  //         //   this.index$.next(nextIndexArr[i]);
  //         // }
  //
  //
  //       }catch(err){
  //         console.log('list-view (component), setList... Error:');
  //         throw err;
  //       }
  //     }
  //
  //
  //   if(toggleLO){
  //     // console.log("toggleLO!");
  //     this.listOptionsToggle = false;
  //   }
  //   console.log('SSSSSSSSSSSSS list-view (component), setList: end execution SSSSSSSSSSSS');
  //   return true;
  //
  //   } catch(err){
  //     //catch
  //     throw err;
  //   }
  // }

  async setList2(listName:string, toggleLO:boolean):Promise<Observable<RouteObject[]>>{
    // console.log('3....SSSSSSSSS list-view (component), setList2: begin execution');
    try{

      // this.listNameArr = await this.listsProv.returnListNameArr();
      //TODO: fix this... it should actually fetch the list names from the array
      this.listNameArr = ["All Routes", "My Routes"];
      // console.log("setLists, floorLists:", this.listNameArr);

      // console.log('3A...SSSSSSSSSS checking selectedList: ');
      // console.log(listName);
      // console.log('3B...SSSSSSSSS is that in the array:')
      // console.log(this.listNameArr);

      this.selectedList = this.listNameArr.indexOf(listName) > -1 ? listName : "All Routes";

      // console.log('3C...SSSSSSSS this.selectedList is:');
      // console.log(this.selectedList);

      if (this.selectedList !== "All Routes"){
        //QUESTION: what should we do if it isn't a custom list?
        this.showListOptions = this.listsProv.isListCustom(listName);

        // console.log('4A....SSSSSSSSS list-view (component), setList: calling listsProv.returnListArr');
        this.currentListPushKeyArr = await this.listsProv.returnListArr(listName);
        // console.log('4A-I.... SSSSSS got this.currentListPushKeyArr as:');
        // console.log(this.currentListPushKeyArr);

        // console.log('5A....SSSSSSSSS list-view (component), setList: settings this.listArr$, by calling getRouteObjObsFromPushKeys');
        this.listArr$ = this.getRouteObjObsFromPushKeys(this.currentListPushKeyArr);

        if(toggleLO){
          // console.log("toggleLO!");
          this.listOptionsToggle = false;
        }

        return this.listArr$;

      }else{//Using all routes in list
        this.listOptionsToggle = false;
        this.showListOptions = false;
        // console.log('4B,5B.... SSSSSSSSS list-view (component), setList: settings this.listArr$');
        this.listArr$ = this.fsObs.behaviorSubjFilteredRoute$;
        return this.listArr$;
      }
    } catch(error){
      throw error;

    };

  }

  async changeList(newListName:string):Promise<boolean>{
    try{
      // console.log('CCCCC list-view (component), changeList called.');
      if(this.selectedList === newListName){
        // console.log('CCCCCCC list names DID NOT MATCH!!!');
        //TODO: verify that the selected list is valid (is this necessary? Wasn't the option pulled from firestore?)
        // console.log('CCCCC list-view (component), changeList: calling setList2');
        this.listArr$ = await this.setList2(newListName, false); //only give list options for custom lists
        this.index$.next(0);
        await this.feedCache.init(this.listArr$,this.index$);
        return true;
      }else{
        // console.log('CCCC list names DID MATCH');
      }
    }
    catch(error){
      throw error;
    };

  }
  /*********** DOM Helpers **********/
  //TODO: actually calculate this or grab elements from the dom and add up their height properties
  //...or grab the ion-list containing the routes and get the y-coord of it's top
  getRouteListTopOffset(){
    return 60;
  }

  //TODO: get the height from the actual DOM element.
  getRouteItemHeight(){
    if(this.displayDimensions){
      return this.displayDimensions.height;
    }
    else{
      throw new Error('list-view (component), getRouteItemHeight: ERROR: could not get height. this.displayDimensions was falsey');
    }
  }
  /******** END DOM Helpers ********/

  //routePushKeys <- the pushkeys of the routes from a particular list

  //TODO: replace this with an async function that querries the database, rather than searching the
  //currentUnfilteredRoutes array. Reason: if walls are ever shared across gyms, the currentUnfilteredRoutes
  //could get very large. Better to leverage the querrying optomizations in firestore ...that is, let
  //the google engineers solve the search problem: They're smarter than us!

  //PURPOSE: build array of RouteObjects corresponding to the pushkeys in routePushKeys
  getRouteObjObsFromPushKeys(routePushKeys:string[]):Observable<RouteObject[]>{
    // console.log('GGGGGGGGGGGGGGGG getRouteObjObsFromPushKeys: routePushKeys ');
    // console.log(routePushKeys);
    //
    // console.log('GGGGGGGGGGGGGGGG getRouteObjObsFromPushKeys: this.currentUnfilteredRoutes:');
    // console.log(this.currentUnfilteredRoutes);
    let routes : RouteObject[] = [] ;

    for(let i=0; i<routePushKeys.length; i++){
      for(let i2=0; i2<this.currentUnfilteredRoutes.length; i2++){
        if(routePushKeys[i] == this.currentUnfilteredRoutes[i2].pushKey){
          // console.log('pushing route:');
          // console.log(this.currentUnfilteredRoutes[i2]);
          routes.push(this.currentUnfilteredRoutes[i2] as RouteObject);
        }
      }
    }
    // console.log('getRouteObjObsFromPushKeys: built listArr as:')
    // console.log(routes);

    //NOTE that the routes array must be wrapped in another array!
    return from([routes]);
  }

  trackByPushKey(index, route){
    return route.pushKey;
  }

  //if route is tapped, emit data of selected route (using @Output)
  listTouch(route:RouteObject, touch:string){
    // Timeout to prevent touches on scrolls
    this.canClick = false;
    setTimeout(() => {
      this.canClick = true; }, 200);

    // scroll to route in background so that when you exit list view
    // the map view reflects what you were looking at.
    // if(index){
    //   this.scrollToRoute(index);
    // }else{
    //   this.scrollToRoute(0);
    // }

    if(touch == "tap"){
      // if the user is not editing the list and a user taps a route,
      // emit the routeSelected event
      if(!this.listOptionsToggle){
        if(this.feedCache.cacheTracker.cacheState.idToLoaded.hasOwnProperty(route.pushKey)){
          const imgLocation = this.feedCache.cacheTracker.cacheState.idToLoaded[route.pushKey].fullPath
          // this.routeSelected.emit({route:route, imgLocation: imgLocation} as RouteSelectedData);
          this.routeSelected.emit({route:route, cache: this.feedCache} as RouteSelectedData);
        }
        // console.log('TODO: toggleListView');
      }
    }
  }

  // listPress(pushKey, routeName, mapId){
  //   // if the user is not editing a list, and they long press a route,
  //   // show list options.
  //   if(!this.listOptionsToggle){
  //     this.routeListOptions(pushKey, routeName, mapId);
  //   }
  // }

  toggleListOptions(){
    // console.log('TTT list-view (component), toggleListOptions triggered!!!')
    this.listOptionsToggle = !this.listOptionsToggle;
    if(!this.listOptionsToggle){
      // if(this.hasReordered){
      //   this.listsProv.setReorder(this.selectedList).then(()=>{
      //   }).catch((error)=>{
      //   });
      // }
    }
  }

  //
  // routeListOptions(pushKey, routeName, mapId){
  //   let routeListOptionsAlert = this.alertCtrl.create({
  //     title: 'List Options for '+routeName,
  //     buttons: [
  //       {
  //         text: 'Add to a list',
  //         handler: ()  => {
  //           this.addRouteToListAlert(pushKey, routeName,mapId)
  //         }
  //       },
  //       {
  //         text: 'Create a new list',
  //         handler: () => {
  //             this.createNewCustomList(pushKey,mapId)
  //         }
  //       },
  //       {
  //         text: 'Cancel',
  //         handler: () => {
  //           // console.log('Cancel clicked');
  //         }
  //       }
  //     ]
  //   });
  //   routeListOptionsAlert.present();
  // }

  // async addRouteToListAlert(pushKey:string, routeName:string, mapId:string){
  //   try{
  //     // get all lists for the floor and the lists that the route already exists in
  //     let detailsListObj = await this.listsProv.returnRouteDetailsListObject(pushKey);
  //     let existsInLists = detailsListObj.existsInLists;
  //     let floorLists = detailsListObj.floorLists;
  //
  //     // console.log("floorLists:", floorLists);
  //     if(floorLists.length > 0){
  //       let addToListAlert = this.alertCtrl.create();
  //       addToListAlert.setTitle('Add '+routeName+' to list:');
  //       // add a radio option to the alert for each list on the floor
  //       floorLists.forEach((listName) =>{
  //         addToListAlert.addInput({
  //           type: 'radio',
  //           label: listName,
  //           value: listName,
  //           checked: false
  //         });
  //       })
  //       addToListAlert.addButton('Cancel');
  //       addToListAlert.addButton({
  //         text: 'Ok',
  //         handler: (listName: string) => {
  //           // console.log('Radio data:', listName);
  //           this.addRouteToListHandler(pushKey,mapId, listName, existsInLists);
  //         }
  //       });
  //       addToListAlert.present();
  //     }
  //     else{
  //       let noListsAlert = this.alertCtrl.create({
  //         title: 'No Existing Lists',
  //         message: 'After you create a custom list you can add routes to it.',
  //         buttons: [
  //           {
  //             text: 'OK',
  //             handler: () => {
  //               console.log('Cancel clicked');
  //             }
  //           }
  //         ]
  //       });
  //       noListsAlert.present();
  //     }
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }

  // async addRouteToListHandler(pushKey: string, mapId:string, selectedList: string, existsInLists: string[]){
  //   try{
  //     let loading = this.loadingCtrl.create({});
  //     loading.present();
  //     let duplicate:boolean = false;
  //     for(let i=0; i<existsInLists.length; i++){
  //       if(existsInLists[i] == selectedList){
  //         duplicate = true;
  //       }
  //     }
  //     if(duplicate){
  //       let duplicateListAlert = this.alertCtrl.create({
  //         title: 'Duplicate in List',
  //         message: 'This route already exists in this list. Are you sure you want to add it another time?',
  //         buttons: [
  //           {
  //             text: 'Cancel',
  //             handler: () => {
  //             }
  //           },
  //           {
  //             text: 'Add Again',
  //             handler: async ()  => {
  //               console.log('adding a second time...');
  //               try{
  //                 let pushKeyArr = [pushKey];
  //                 loading.present();
  //                 let result = await this.listsProv.addRouteToList(selectedList,mapId, pushKeyArr, false);
  //                 loading.dismiss();
  //                 if(result){
  //                   console.log("list added a second time...");
  //                   console.log(`SSSSSSSSSS route-feed (page), , Add Again callback: calling setList for the ${this.setListCounter}th time SSSSSSS`);
  //                   this.setListCounter += 1;
  //
  //                   await this.setList2(this.selectedList, true);
  //                   // await this.setList(this.selectedList, true);
  //                 }
  //                 else{
  //                   let listFailAlert = this.alertCtrl.create({
  //                     title: 'Error',
  //                     subTitle: 'There was an error.',
  //                     buttons: ['Dismiss']
  //                   });
  //                   listFailAlert.present();
  //                 }
  //               }
  //               catch(err){
  //                 loading.dismiss();
  //                 console.log("could not add route to list!");
  //                 throw err;
  //               }
  //             }
  //           }
  //         ]
  //       });
  //       duplicateListAlert.present();
  //     }
  //     else{
  //       try{
  //         let pushKeyArr = [pushKey];
  //         loading.present();
  //         let result = await this.listsProv.addRouteToList(selectedList, pushKeyArr, false);
  //         loading.dismiss();
  //         if(result){
  //           console.log("route added to list for the first time");
  //         }
  //         else{
  //           let listFailAlert = this.alertCtrl.create({
  //             title: 'Error',
  //             subTitle: 'There is already a list with this name, please choose a different one.',
  //             buttons: ['Dismiss']
  //           });
  //           listFailAlert.present();
  //         }
  //       }
  //       catch(err){
  //         loading.dismiss();
  //         console.log("could not add route to list!");
  //         throw err;
  //       }
  //     }
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }

  createNewCustomList(routeId, mapId){
    // console.log("createNewCustomList");
    let newListAlert = this.alertCtrl.create({
      title: 'Create New List',
      message: 'Enter a title for this new list',
      inputs: [
        {
          name: 'title',
          placeholder: 'New List'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: () => {
            // console.log('Cancel clicked');
          }
        },
        {
          text: 'Save',
          handler: (data)  => {
            console.log('Saved clicked, name is:  ', data.title);
            this.triggerAsyncNewList(data.title, routeId, mapId);
          }
        }
      ]
    });
    newListAlert.present();
  }

  async triggerAsyncNewList(title:string, pushKey:string, mapId){
    let loading = this.loadingCtrl.create();
    loading.present();
    try{
      let pushKeyArr = [pushKey]
      let result = await this.listsProv.addRouteToList(title,mapId, pushKeyArr, true);
      // console.log("got result as:", result);
      if(result){
        this.listsInit();
      }
      else{
        let listFailAlert = this.alertCtrl.create({
          title: 'Error',
          subTitle: 'There is already a list with this name, please choose a different one.',
          buttons: ['Dismiss']
        });
        listFailAlert.present();
      }
    }
    catch(err){
      console.log("could not call the createCustomList function from listProv in route-details2");
      throw err;
    }
    loading.dismiss();
  }
  //
  // renameList(){
  //   let renameListAlert = this.alertCtrl.create({
  //     title: 'Create New List',
  //     message: 'Enter a title for this new list',
  //     inputs: [
  //       {
  //         name: 'title',
  //         placeholder: 'New List'
  //       },
  //     ],
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Save',
  //         handler: (data)  => {
  //           console.log('Saved clicked, name is:  ', data.title);
  //           this.asyncRenameList(data.title);
  //         }
  //       }
  //     ]
  //   });
  //   renameListAlert.present();
  // }

  // async asyncRenameList(newName:string){
  //   try{
  //     let loading = this.loadingCtrl.create({});
  //     loading.present();
  //     await this.listsProv.renameList(this.selectedList, newName);
  //
  //     console.log(`SSSSSSSSSS route-feed (page), asyncRenameList : calling this.setList for the ${this.setListCounter}th time SSSSSSS`);
  //     this.setListCounter += 1;
  //
  //     await this.setList2(newName, false);
  //     // await this.setList(newName, false);
  //     await this.listsInit();
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }
  //
  // deleteList(){
  //   let deleteListAlert = this.alertCtrl.create({
  //     title: 'Delete List',
  //     message: 'Are you sure you want to delete this list?',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Delete',
  //         handler: ()  => {
  //           console.log('delete clicked!');
  //           this.asyncDeleteList();
  //         }
  //       }
  //     ]
  //   });
  //
  //   deleteListAlert.present();
  // }
  //
  // async asyncDeleteList(){
  //   try{
  //     let loading = this.loadingCtrl.create({});
  //     loading.present();
  //     await this.listsProv.deleteList(this.selectedList);
  //     await this.listsInit();
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }
  //
  // async reorderList(indexes: any) {
  //   // reorder lists locally, set a flag to indicate that once the user is done editing
  //   // comit changes to db.
  //   this.currentListPushKeyArr = reorderArray(this.currentListPushKeyArr, indexes);
  //   try{
  //     await this.listsProv.localReorderList(this.selectedList, this.currentListPushKeyArr);
  //     console.log("local reorder finished!");
  //
  //     console.log(`SSSSSSSSSS route-feed (page),reorderList: calling setList for the ${this.setListCounter}th time SSSSSSS`);
  //     this.setListCounter += 1;
  //
  //     await this.setList2(this.selectedList, false);
  //     // await this.setList(this.selectedList, false);
  //     this.hasReordered = true;
  //   }
  //   catch(err){
  //     //catch
  //     throw err;
  //   }
  // }
  //
  //
  // deleteRouteFromListAlert(routeId: string, indx: number){
  //   let deleteRFLAlert = this.alertCtrl.create({
  //     title: 'Delete List',
  //     message: 'Are you sure you want to delete this route from the list?',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         handler: () => {
  //           console.log('Cancel clicked');
  //         }
  //       },
  //       {
  //         text: 'Delete',
  //         handler: ()  => {
  //           console.log('delete clicked!');
  //           this.deleteRouteFromListHandler(routeId, indx);
  //         }
  //       }
  //     ]
  //   });
  //
  //   deleteRFLAlert.present();
  // }
  //
  // async deleteRouteFromListHandler(routeId: string, indx: number){
  //   console.log("remove from list clicked with ", this.selectedList, " and ", routeId);
  //   let loading = this.loadingCtrl.create();
  //   try{
  //     loading.present();
  //     await this.listsProv.removeRouteFromList(routeId, this.selectedList, indx);
  //
  //
  //       console.log(`SSSSSSSSSS route-feed (page), deleteRouteFromListHandler: calling this.setList for the ${this.setListCounter}th time SSSSSSS`);
  //       this.setListCounter += 1;
  //
  //
  //     await this.setList2(this.selectedList, false);
  //     // await this.setList(this.selectedList, false);
  //     loading.dismiss();
  //   }
  //   catch(err){
  //     loading.dismiss();
  //     console.log("err in calling removeRouteFromList");
  //     throw err;
  //   }
  // }
}
