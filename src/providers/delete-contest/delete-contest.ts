import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';//used for definig types

@Injectable()
export class DeleteContestProvider {
  constructor() {
  }

  //Try to delete the route directly (assume the component actually wants to do
  //this, rather than cast a delete vote)
  //return the routeId of the deleted route, if it is successfully deleted.
  async deleteRoute(gymId:string, mapsId:string, routeId:string):Promise<string|boolean>{
    try{
      const routeRef = this.getRouteRef(gymId, mapsId, routeId);
      await routeRef.delete();
      return (routeId);
    }
    catch(error){
      throw(error);
    };
  }

  /*
  Effect: update route document at gymId,mapsId, routeId
  voteType: 'delete' xor 'contest'
  existingDeleteVoters, existingContestVoters: userId[] ...who has already voted
  output:
   -false (if user already voted for this action)
   -routeId (if the record is successfully updated)
  Other responsibilites:
    -if userId already voted 'delete' and is now voting 'contest'=> remove their id
    from deleteVoterIds and append it to contestVoterIds in firestore. Similar if
    they vote 'contest' then switch their vote to 'delete'.
  */
  async handleVote(gymId:string, mapsId:string, routeId:string,
    userId:string,voteType:string, existingDeleteVoters:string[]=null,
    existingContestVoters:string[]=null){
      try{
        const alreadyVotedDel = existingDeleteVoters ?
                                existingDeleteVoters.indexOf(userId) >=0 : false;
        const alreadyVotedCont= existingContestVoters?
                                existingContestVoters.indexOf(userId) >=0 : false;

        if(alreadyVotedDel && voteType === 'delete'){
          // throw new Error(`Cannot append uid: ${userId} to existingDeleteVoters... uid is already present`);
          console.log('*****Already votred delete!!!******');
          return false;
        }
        if(alreadyVotedCont && voteType === 'contest'){
          // throw new Error(`Cannot append uid: ${userId} to existingContestVoters... uid is already present`);
          return false;
        }

        const routeRef = this.getRouteRef(gymId, mapsId, routeId);

        //NOTE: this assumes that if you try to use
        let voteUpdates = {};

        if(voteType == 'contest'){

          voteUpdates['contestVoterIds']=firebase.firestore.FieldValue.arrayUnion(userId);

          if(alreadyVotedDel){
            //they're trying to contest, but they aleady cast a vote to delete the route
            voteUpdates['deleteVoterIds'] = firebase.firestore.FieldValue.arrayRemove(userId);
          }

        }else{ // => voteType == 'delete'
          if(!existingDeleteVoters || existingDeleteVoters.length < 1 ){
            //this is the first delete vote, so create a timestamp for the delete cron job to read
            voteUpdates['firstDeleteDate'] = firebase.firestore.FieldValue.serverTimestamp();
          }

          voteUpdates['deleteVoterIds']=firebase.firestore.FieldValue.arrayUnion(userId);

          if(alreadyVotedCont){
            //they're trying to delete, but they aleady cast a vote to contest the route
            voteUpdates['contestVoterIds'] = firebase.firestore.FieldValue.arrayRemove(userId);
          }

        }
        await routeRef.update(voteUpdates);
        return routeId;
      }
      catch(error){
        throw error;
      };
    }

  getRouteRef(gymId:string,mapsId:string, routeId:string):firebase.firestore.DocumentReference{
    const db = firebase.firestore();
    return db.collection('GYMS').doc(gymId)
      .collection('MAPS').doc(mapsId)
      .collection('ROUTES').doc(routeId)
  }

}
