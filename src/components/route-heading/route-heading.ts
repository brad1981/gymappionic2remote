import { Component, Input } from '@angular/core';
// import { Input, ViewChild, SimpleChanges } from '@angular/core';
import {UserDetailsProvider} from '../../providers/user-details/user-details';
import { filterCategoriesObject, } from '../../providers/globalConstants/globalConstants';

@Component({
  selector: 'route-heading',
  templateUrl: 'route-heading.html'
})
export class RouteHeadingComponent {
  @Input() authorName: string;
  @Input() routeName: string;
  @Input() grade: number;

  //optional inputs
  @Input() authorMug: string;
  @Input()consensusGrade: number = null;
  @Input() features: string[] = null; //for now, select a random one to display
  @Input() wallAngle: number;

  //indicate style as "detail-view or routes-feed-map or editor"
  @Input() styleFlag: string; //determines which classes go into styleClasses

  // public styleClasses: string[]=[];

  public styleClasses: string = "";

  public filterCategoryObject = filterCategoriesObject;

  public style = null;
  // public styleClasses = ['rounded','eggshell','squat'];
  constructor(public userDetails:UserDetailsProvider) {
    // console.log('************* route-heading  constructor *************');
    // console.log(`inputs: authorName, routeName, grade, authorMug,
    //   consensusGrade: ${this.authorName},${this.routeName},${this.grade},${this.authorMug},
    //   ${this.consensusGrade}, styleFlag: ${this.styleFlag}`);
    //   console.log('*******************************************************');

  }

  ngAfterContentInit(){
    console.log(`%%%%%%%%&&&&&&&&&&%%%%%%%% :: route-heading, ngAfterContentInit{} ::: %%%%%%%%&&&&&&&&&&%%%%%%%% `);
    console.log("this.authorMug");
    console.log(this.authorMug)
    console.log('this.styleFlag');
    console.log(this.styleFlag);
    console.log(`%%%%%%%%&&&&&&&&&&%%%%%%%% :: ::::::  ::: %%%%%%%%&&&&&&&&&&%%%%%%%% `);

    if(!this.authorMug && this.styleFlag === 'editor'){
      this.userDetails.getUserDetails().then((currentUser)=>{
        this.authorMug = currentUser.photoURL;
        // console.log(`%%%%%%%%&&&&&&&&&&%%%%%%%% :: this.authorMug  ::: %%%%%%%%&&&&&&&&&&%%%%%%%% `);
        // console.log(this.authorMug)
        // console.log(`%%%%%%%%&&&&&&&&&&%%%%%%%% :: ::::::  ::: %%%%%%%%&&&&&&&&&&%%%%%%%% `);
      });
    }
      if(this.styleFlag== "detail-view" || this.styleFlag=="editor"){
        this.styleClasses = "squat eggshell rounded";
        // this.styleClasses.push("squat");
        // this.styleClasses.push("eggshell");
      }

      if(this.styleFlag == "routes-feed-map"){
        this.styleClasses = "eggshell rounded-bottom position-bottom";
      }

    if(this.features && this.features.length > 0){
      //set style to a randomly selected feature
      this.style = this.features[Math.floor(Math.random() * this.features.length)];
    }
  }

}
