import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoutesFeedPage } from './routes-feed';
import { ComponentsModule } from '../../components/components.module'
//import { SvgMapComponent } from '../../components/svg-map/svg-map';

@NgModule({
  declarations: [
    RoutesFeedPage,
  ],
  imports: [
    IonicPageModule.forChild(RoutesFeedPage),
    ComponentsModule
  ],
})
export class RoutesFeedPageModule {}
