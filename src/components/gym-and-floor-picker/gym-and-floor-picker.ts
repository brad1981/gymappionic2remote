import { Component } from '@angular/core';

import { Alert, AlertController, Platform } from 'ionic-angular';

import { ConnectionProvider } from '../../providers/connection/connection';
import { GymsProvider, GymData } from '../../providers/gyms/gyms';
import { MapsProvider, Maps } from '../../providers/maps/maps';
import { SettingsProvider, NotificationsSettings,SettingsObject  } from '../../providers/settings/settings';

import { AuthProvider } from '../../providers/auth/auth';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';
let console = betterConsole;

/******** Responsibilities **************
load gymData, which contails all possible gyms the user could select (including the various floors)
reset the maps provider's maps object if the selected gym changes


***********/
@Component({
  selector: 'gym-and-floor-picker',
  templateUrl: 'gym-and-floor-picker.html'
})
export class GymAndFloorPickerComponent {

  optionsLoaded:boolean = false;

  gymData:GymData[] = null;
  gymId:string = null;

  mapsObj:Maps = null;
  currentFloor:number;

  settings: SettingsObject= null;

  Object: any //for exposing 'Object to the template';

  constructor( public platform: Platform,

    public connection: ConnectionProvider,
    public gymsProv: GymsProvider,
    public authProv: AuthProvider,
    public mapsProv: MapsProvider,
    public settingsProv: SettingsProvider
  ) {
    console.log('************* Hello GymAndFloorPickerComponent Component **************');

    this.Object = Object;
  }

  async ngOnInit(){
    try{
      console.log('gym-and-floor-picker (component), ionViewDidEnter');
      await this.platform.ready();
      // const userInfo = this.authProv.getUser();
      await this.setGymOptions();
      this.settings = await this.settingsProv.getSettings();

      if(this.settings && this.settings.hasOwnProperty('gymId') && this.settings.gymId){
        try{
          //is the current gymId from settings still valid (in the gymOptions populating the template)?
          let settingsGymIdValid=false;
          this.gymData.forEach(gym=>{
            if (gym.gymId === this.settings.gymId){
              settingsGymIdValid = true;
            }
          });

          if(settingsGymIdValid){
            this.gymId = this.settings.gymId;
            let maps = await this.mapsProv.getMaps(this.settings.gymId);
            console.log('gym-and-floor-picker (compnent),: ionViewDidEnter ...got maps as:');
            console.log(maps);

            if(maps && maps.hasOwnProperty('mapByFloor')){
              console.log('setting this.mapsObj as');
              console.log(maps);
              this.mapsObj = maps;
            }
          }
          this.unpackSettings();
        }
        catch(error){
        };


      }else{
        console.log('gym-and-floor-picker (component), ionViewDidEnter, got this.settings as: ')  ;
        console.log(this.settings);
      }
      console.log('*******setting options loaded true ***************');
      this.optionsLoaded = true;
    }
    catch(error){
      console.log('gym-and-floor-picker (component), ionViewDidEnter, could not setGymOptions');
      throw error;
    };

  }

  async setGymOptions():Promise<GymData[]>{
    try{
      this.gymData = await this.gymsProv.setGymOptions();
      console.log('gym-and-floor-picker (component), setGymOptions, got this.gymData as:');
      console.log(this.gymData);
      return this.gymData;
    }
    catch(error){
      throw(error);
    };

  }

  async updateGymId(gym){
    //console.log('******UPDATE GYM ...and gym is:****** ', gym);

    this.gymId = gym.gymId;
    // this.settings.gymId = gym.gymId;
    await this.settingsProv.updateGymId(this.gymId);

    if(!this.settings.hasOwnProperty('currentFloor') || !Number.isInteger(this.settings.currentFloor)){
      this.settings.currentFloor = 0;
    }

    if(this.gymId){
      try{
        console.log('settingsPage: updateGymId. Trying to set this.mapsObj and this.gymId is:');
        console.log(this.gymId);
        let maps = await this.mapsProv.getMaps(this.gymId);
        console.log('settingsPage: updateGymId...got maps as:');
        console.log(maps);
        if(maps && maps.hasOwnProperty('mapByFloor')){
          console.log('settingsPage: updateGymId... settings this.mapsObj to:');
          console.log(maps);
          this.mapsObj = maps;
        }
      }
      catch(error){
        //this.mapsObj = null;
        console.log('settingsPage: Could not get maps object');
        console.log(error);
      };

      await this.settingsProv.updateGymId(gym.gymId);
    }
    return(null);

  }

  async updateFloor(floor){
    // console.log('updateFloor: type of floor is:');
    // console.log(typeof floor);
    this.currentFloor = floor;
    this.settings.currentFloor = floor;
    this.settingsProv.updateCurrentFloor(floor);
    await this.settingsProv.updateFloorOnFirestore(floor);
  }

  unpackSettings():Promise<any>{
    //console.log("unpackSettings called");
    return new Promise((res,rej)=>{
      if (this.settings){

        if(this.settings.gymId){
          this.gymId=this.settings.gymId;
        }
        if(this.settings.currentFloor){
          this.currentFloor = this.settings.currentFloor;
        }
        res();
      }else{
        let error = new Error('Cannot unpack settings. Settings are: '+ this.settings);
        //rej(error);
      }
    });
  }

  repackSettings():Promise<any>{
    console.log("repackSettings");
    return new Promise((res)=>{
      //HACK: hack alert:
      if(!this.settings){
        this.settings = {
          gymId: null,
          currentFloor:null
        };
      }
      this.settings['gymId'] = this.gymId;
      this.settings['currentFloor'] = this.currentFloor;
      /*
      this.settings['visibleMode'] = this.visibleMode;
      this.settings.notificationSettings['onComment']=this.onComment;
      this.settings.notificationSettings['onRouteDeleted']= this.onRouteDeleted;
      this.settings.notificationSettings['onNewSend'] = this.onNewSend;
      this.settings.notificationSettings['onNewAttempt']= this.onNewAttempt;
      this.settings.notificationSettings['onNewRouteByFollowed'] = this.onNewRouteByFollowed;
      */
      console.log('repackSettings: about to resolve');
      res();
    });
  }


}
