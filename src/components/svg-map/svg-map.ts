import { Component, Input,Output, EventEmitter, SimpleChanges,
          ViewChild, DoCheck, IterableDiffers,IterableDiffer, NgZone} from '@angular/core';
import { Platform, Events, Gesture, AlertController} from 'ionic-angular';
import { SvgGestureComponent } from '../svg-gesture/svg-gesture';
import {Location} from '../../providers/filtered-routes/filtered-routes';


import { RouteObject, ContainerDimensions, MapDimensions } from '../../providers/globalConstants/globalConstants';
import { BehaviorSubject} from 'rxjs/BehaviorSubject';
import { Subscription } from 'rxjs/Subscription';

import { betterConsole }  from '../../providers/globalConstants/globalConstants';

let console = betterConsole;

/*
NOTE: See extensive discussion of the concepts behind this component in svg-map.html,
      at the bottom of the page
*/

@Component({
  selector: 'SvgMapComponent',
  templateUrl: 'svg-map.html'
  // template: this.mapContents
})

export class SvgMapComponent extends SvgGestureComponent {
  @ViewChild('rotationFrame') rotationFrame;
  @ViewChild('svgElement') svgElement;

  pinchEnabled: boolean = true;
  text: string;
  walls: Wall[];
  viewReady: boolean = false;
  angle: number = 0;
  //the radius of circles, drawn on the map
  radius: number = 3;
  defaultColor: string = 'blue';
  selectedColor: string = 'red';
  lastLocation: Location = null; // last location selected by the user
  locationToEmit: Location = null; // the location which will be emited (either lastLocation or @input~newRoute)
  differ:IterableDiffer<{}>;

  selectedRoute$subsrip: Subscription = null; //when selectedRoute$ emits a route, this
                                //callback fires, triggering focusOnPoint

  @Input() newRoute:Location= null; // used for drawing a single route, input from the RouteEditorPage
  @Input() selectedRoute:RouteObject = null; //the data for the specific, selected route
  @Input() selectedRoute$:BehaviorSubject<RouteObject> = null;
  @Input() mode=null;//'edit' or 'navigate' or 'navigate-rotate' or 'view'
  @Input() routes: RouteObject[] = null; //all of the currently loaded routes
  @Input() containerDimensions: ContainerDimensions = null;
  @Input() mapDimensions: MapDimensions = null;
  @Input() mapContents = null;//the svg tags (as a string) to insert into the preexisting <svg> tags in svg-map.html.
                              //mapContents is the set of tags that actually get rendered as the map
  @Input() additionalClass = null; //for styling the div, containing the svg
  //the location of the route in format
  //... "wallCode"+"segmentIndex"+ "t_value" format
  @Output() locationSet = new EventEmitter();
  //the id of the selected route (selected by tapping map)
  @Output() routePinTapped= new EventEmitter();



  constructor(
    public platform: Platform,
    public events: Events,
    private differs: IterableDiffers,
    private alertCtrl: AlertController,
    private zone:NgZone
  ) {
    super(
      platform,
      events,
    );
    // console.log("************* svg-map (component), constructor*************");
    this.differ = differs.find([]).create(null);

    //console.log("containerDim:",this.containerDim);
  }

  ngOnChanges(changes: SimpleChanges){
    //NOTE: ngOnChanges ONLY fires when @Input properties are assigned or reassigned
    //CAUTION: Use ngDoCheck for mutation detection
    for (let propName in changes){
      if(propName =='selectedRoute' && this.viewReady
      && this.selectedRoute && this.selectedRoute .hasOwnProperty('location')
      && this.routes && this.routes.hasOwnProperty('length')){
        // console.log('svg-map (component), ngOnChanges, Calling focusOnPoint from if propName=selectedRoute');
        this.focusOnPoint(this.selectedRoute.location);
      }
      if(propName == 'routes' && this.mode=='navigate' && this.viewReady){

        // console.log(`***********************************************************************************************`);
        // console.log('svg-map (component), ngOnChanges, Calling drawRoutes from if propName = routes and navigate mode');
        // console.log(`***********************************************************************************************`);
        //TODO: test that this works... I don't think it does!
        //true-> do remove existing circles
        // console.log("ROUTES CHANGED!!!");
        this.drawRoutes(true);

      }

      if(propName == 'mapContents' && this.svgPanZoom){
        // console.log('*@*@*@*@*@*** mapContents change detected ***@*@*@*@*@* ');
        this.svgPanZoom.destroy();
        let svg = document.getElementsByClassName('map-component')[0];
        svg.innerHTML = this.mapContents;
        //TODO: might need to reinitialize (this.init() ...esp is map dimensions change)
        //this.svgElement = svg;

        // console.log('********** svg-map (component), calling this.init,  ****************');
        // console.log(this.containerDimensions);
        this.init(this.containerDimensions, this.mapDimensions);

      }
    }
  }

  // ngDoCheck(){
  //   let routeDataChanges = this.differ.diff(this.routeData);
  //   if(routeDataChanges && this.viewReady){
  //     // console.log('**********Ruote List change(s) detected**********');
  //     this.drawRoutes(true);
  //   }
  // }


  ngAfterViewInit(){
    let svg = document.getElementsByClassName('empty-map')[0];

    //TODO: dimensions <- use to create dimensions attributes of rendered map (currently hard-coded into the template)
    const innerHTML = this.mapContents;

    svg.innerHTML = innerHTML;
    svg.classList.remove('empty-map');
    this.init(this.containerDimensions, this.mapDimensions);
    // console.log("NNNNNNNNNNNNNNNN svg-map (component) ngAfterContentInit NNNNNNNNNNNNNNNNN");
    // console.log("children::", document.children);
    // console.log("svg.innerHTML:", svg.innerHTML);
  }

  ngAfterContentInit(){
    if(this.selectedRoute$){
      this.selectedRoute$subsrip = this.selectedRoute$.subscribe((route:RouteObject)=>{
        // console.log('********** svg-map (component), route:');
        // console.log(route);
        if(route){
          this.focusOnPoint(route.location);
        }
      });
    }
  }

  ionViewWillLeave(){
    if(this.selectedRoute$subsrip){
      this.selectedRoute$subsrip.unsubscribe();
    }

  }

  setChildClassHandlers():Promise<any>{
    // console.log('SSSSSSSSSSSSSSSSS  setChildClassHandlers SSSSSSSSSSSSSSSS');
    // console.log('SSSSSSSSSSSSSSSSS this.mode is:');
    // console.log(this.mode);
    return new Promise((res)=>{
        switch(this.mode){
          //'edit' mode: used to pick route location in the route editor
          case 'edit':
            this.initEditMode();
          case 'view':
            this.initViewMode();
            break;
          //'navigate' mode: used in the routes-feed page
          case 'navigate':
            // console.log('****************** svg-map, setChildClassHandlers, calling initNavigetMode***********');
            this.initNavigateMode();
            break;
          //TODO: implement map rotation
          case 'navigate-rotate':
          this.initNavigateRotate();
        }
        res();
      });
    }

  initNavigateRotate(){
    // console.log('Navigate Rotate Init!!!');
    //this.svgPanZoom.pan(0,0);
    //this.svgPanZoom.zoom(1);
    this.setRotateTapHandler();
  }

  setRotateTapHandler(){
    this.gesture.on('tap',(ev)=>{
      //Experimental rotation stuffs
      // console.log('calling rotateAboutBy' );
      this.rotateAboutBy(ev.center, 10);
    });
  }

  rotateAboutBy(screenPoint,deltaTheta){
    // console.log('rotating!!!');

    // console.log(screenPoint);

    let convertedPoint = this.convertCoordinatesToSvg(screenPoint.x, screenPoint.y);

    let panZoomGroup = document.getElementsByClassName('svg-pan-zoom_viewport')[0];

    let newAngle = (this.angle + deltaTheta)%360;

    let scale = this.svgPanZoom.getSizes().realZoom;
    // console.log('scale: '+scale);

    let magV = Math.sqrt(screenPoint.x *screenPoint.x + screenPoint.y*screenPoint.y);

    let deltaY = -Math.sin(newAngle*Math.PI/180)*magV;
    let deltaX = Math.cos(newAngle*Math.PI/180)*magV;

    let origin = "0 0";//point.x.toString()+"px; "+point.y.toString()+"px;";

    let location ={point:screenPoint};
    this.drawCircle(100,location, this.selectedColor);

    //this.rotationFrame.nativeElement.style['transform-origin'] = origin;
    panZoomGroup.setAttribute('transform-origin',origin);
    let angleString = (newAngle).toString();

    // let transformations = 'translate('+deltaX.toString()+"px,"+deltaY.toString()
    // +'px) '+'rotate('+angleString+'deg) ';

    //let transformations ='rotate('+angleString+'deg);';
    let transformations = 'rotate(30deg)';
    panZoomGroup.setAttribute('transform',transformations)

    //panZoomGroup.style['transform'] = transformations;

    this.angle=newAngle;
  }


/************************* NAVIGATE MODE ***************************/
  initNavigateMode(){
    // console.log('initNavigetMode');
    this.drawRoutes();
    this.initNavigateTapHandler();
  }

  initNavigateTapHandler(){
    // console.log('setting initTapHandler');
    this.gesture.on('tap',(ev)=>{

      //TODO: set this programatically
      //HACK: magic number!!!
      let headerHeight = 60;

      let sizes = this.svgPanZoom.getSizes();
      // console.log('***svg-map (component), tap handler, sizes is:**');
      // console.log(sizes);
      let scale = sizes.realZoom;

      let convertedTap= this.convertCoordinatesToSvg(ev.center.x, ev.center.y);
      // console.log("svg-map, tap this.gesture.on('tap'), ev:");
      // console.log(ev.center);

      // console.log("svg-map, tap this.gesture.on('tap'), convertedTap:");
      // console.log(convertedTap);

      //Search for closest point to tap
      //OPTIMIZE later: currently using linear scan of all routes
      //TODO: snap touch to closest wall, bisection search wall with t-values
      let minDist = null;
      let closestIndex = null;
      let sqDist = null;


      if (this.routes.hasOwnProperty('length')){
        // console.log('has length');
        for(let i=0; i<this.routes.length; i++){
          if (this.routes[i].hasOwnProperty('location')){
            // console.log('has location');
            let candPoint= {x:this.routes[i].location.point.x,
                            y:this.routes[i].location.point.y};

            let zoomedCand = {x:candPoint.x, y:candPoint.y};
            sqDist = Math.pow(convertedTap.x-zoomedCand.x,2) + Math.pow(convertedTap.y - zoomedCand.y,2);

            //TODO: if minDist is 0, this test evaluates to true. Thats a problem
            //TODO: scale minDist : note: minDist is quadratic, so... account for that
            if( !minDist  || sqDist < minDist){
              closestIndex = i;
              minDist = sqDist;
            }
          }
        }
      }
      //TODO: set sensitivity constant, insteac of the magic number, 240
      if(minDist && minDist < 240){
          this.routePinTapped.emit(closestIndex);
        }
    });
  }




/************** EDIT MODE ****************************************/

  initEditMode(shouldSetTapHandler = true){
    // if newRoute was passed something as input, set that as the standing location
    // and draw that on the map.
    // console.log('svg-map (component), initEditMode, this.newRoute: ');
    // console.log(this.newRoute);
    if(this.newRoute){
      this.locationToEmit = this.newRoute;
      this.clearCircles();
      this.drawCircle(this.radius, this.newRoute, this.defaultColor)
    }
    this.drawRoutes();
    this.initializeWalls().then(()=>{
      if (shouldSetTapHandler){
        this.setTapHandler();
      }
    });
  }

  initViewMode(){
    // console.log(')))))))))))))******* svgMap, initViewMode{}, this.newRoute:');
    // console.log(this.newRoute);
    this.initEditMode(false);
  }

  initViewMode2(){

  }


  initializeWalls():Promise<any>{
    return new Promise((res,rej)=>{
      let svgWallPaths = document.getElementsByClassName('wall');
      this.walls=[];
      let readyMethods = [];
      let lastPoint = null;

      for (let i = 0; i< svgWallPaths.length; i++){
        let path = svgWallPaths[i];

        let wall = new Wall(path,lastPoint);
        this.walls.push(wall);
        readyMethods.push(wall.ready);
      }

      Promise.all(readyMethods).then(()=>{
        // this.walls.map(x=>console.log(x));
        this.walls.map(w=>{});
        res();
      });
    });
  }

  setTapHandler(){
    // console.log(`svg-map (component), setTapHandler called! `);
    this.gesture.on('tap',(ev)=>{

    this.clearCircles();

    let point = this.convertCoordinatesToSvg(ev.center.x, ev.center.y);
    let smallestSqDist = null;
    let closestPoint = null;
    let bestProj = null;
    // console.log('walls are:');
    // console.log(this.walls);

    for (let i = 0; i<this.walls.length; i++){
      let proj = this.walls[i].getClosestPoint(point.x,point.y);
      if (proj && (!smallestSqDist || proj.sqDist < smallestSqDist)){
        closestPoint = proj.point;
        smallestSqDist = proj.sqDist;
        bestProj = proj;
        bestProj['wallCode']=this.walls[i].wallCode;
      }
    }

    if (closestPoint){


      let location:any = {};
      location.point = { x: closestPoint[0], y: closestPoint[1]};



      this.drawCircle(this.radius, location, this.defaultColor);
/*
deleted the below code and addapted the call to be able to use drawCircle()
      circle.setAttributeNS(null,'cx', point.x.toString() );
      circle.setAttributeNS(null,'cy', point.y.toString() );
      circle.setAttributeNS(null, 'r', r.toString());
      circle.setAttributeNS(null, 'style', 'fill: none; stroke: blue; stroke-width: 1px;')

      circle.setAttributeNS(null, 'class', 'CIRCLE');

      this.panZoomGroup.appendChild(circle);
*/
      // console.log('in setTap and bestProj:');
      // console.log(bestProj);

      this.lastLocation = {
        point: bestProj.point,
        wallCode: bestProj.wallCode,
        //segment: bestProj.segment,
        t:bestProj.segment + bestProj.t //in database, segment and t are summed
      }


      //disable collection of more taps
      //TODO: create hold nudge side to side interaction
      //this.gesture.off('tap');

    }

    });
  }

  focusOnPoint(svgFrameLocation){
    //only use this to focus on the selected point

    // console.log('focusOnPoint called with svgFrameLocation:');
    // console.log(svgFrameLocation);
      // console.log('svgFrameLocation:');
      // console.log('svgFrameLocation');

    if(svgFrameLocation.hasOwnProperty('point')){
      let selectedElements = Array.from(
        this.panZoomGroup.getElementsByClassName('selected'));
      //remove any previously selected element (should only be one, but just in case...)
      if (selectedElements){
        selectedElements.forEach((el)=>{
          this.panZoomGroup.removeChild(el);
        });
      }

      if(this.selectedRoute && this.selectedRoute.hasOwnProperty('location')){

        this.drawCircle(11,this.selectedRoute.location, this.selectedColor,true);

        let point = svgFrameLocation.point;
        let sizes = this.svgPanZoom.getSizes();
        let screenBoundingBox = this.svgElement.nativeElement
                                  .getBoundingClientRect();
        let realZoom = sizes.realZoom;

        // console.log('in focus on point and this.containerDim is:');
        // console.log(this.containerDimensions);
        //
        // console.log("point.x: ", point.x, " containerDim.x", this.containerDim.x);

        let pan = {x: -point.x*realZoom + this.containerDimensions.x/2,
                   y: -point.y*realZoom + this.containerDimensions.y/2 };

        this.svgPanZoom.pan(pan);
      }
      else{
        //this.drawCircle(11,svgFrameLocation.point, this.selectedColor,true);
        let point = svgFrameLocation.point;
        let sizes = this.svgPanZoom.getSizes();
        let screenBoundingBox = this.svgElement.nativeElement.getBoundingClientRect();
        let realZoom = sizes.realZoom;

        // console.log('in focus on point and this.containerDim is:');
        // console.log(this.containerDim);
        //
        // console.log("point.x: ", point.x, " containerDim.x", this.containerDim.x);

        //HACK magic numbers!!!
        let pan = {x: -point.x*realZoom + 300/2,
                   y: -point.y*realZoom + 300/2 };

        this.svgPanZoom.pan(pan);

      }
    }
  }

  drawCircle(r, location, color:string, selected=false){
    if(location.hasOwnProperty('point')&& location.point.hasOwnProperty('x')){

      // console.log('drawing circle at location:');
      // console.log(location);
      let svgNamespae = "http://www.w3.org/2000/svg";
      let circle = document.createElementNS(svgNamespae,'circle');

      //let point = location;//this.convertCoordinatesToSvg(location.point.x, location.point.y);

      circle.setAttributeNS(null,'cx', location.point.x.toString() );
      circle.setAttributeNS(null,'cy', location.point.y.toString() );
      circle.setAttributeNS(null, 'r', r.toString());
      circle.setAttributeNS(null, 'style', 'fill: none; stroke: '+color+'; stroke-width: 1px;')

      if(selected){
        circle.setAttributeNS(null,'class','CIRCLE selected');//use to access and remove
      }else{
        circle.setAttributeNS(null, 'class', 'CIRCLE');
      }


      this.panZoomGroup.appendChild(circle);
    }

  }


  clearCircles(){
    //let oldCircles= document.getElementsByClassName('CIRCLE');
    let oldCircles= this.panZoomGroup.getElementsByClassName('CIRCLE');

    if(oldCircles){
      for (let i = 0; i< oldCircles.length; i++){
        let circle = oldCircles[i];
        //remove the circle from the DOM
        // this.panZoomGroup.removeChild(circle);
        circle.parentNode.removeChild(circle);
      }
    }
  }

  drawRoutes(removeFirst = false){
    if(removeFirst){
      this.clearCircles();
    }

    // console.log('svg-map (component), drawRoutes and routes is:');
    // console.log(this.routes);

    if (this.routes && this.routes.hasOwnProperty('length')){
      for (let i=0; i < this.routes.length; i++){
        let route = this.routes[i];
        if (route.hasOwnProperty('location')){
          this.drawCircle(this.radius, this.routes[i].location, this.defaultColor);
        }
      }
    }

    //initial, selected hold
    // if(this.selectedRoute == null && this.newRoute !== null){
    //   this.selectedRoute = this.newRoute;
    // }
    if (this.selectedRoute){
      if(this.selectedRoute.hasOwnProperty('location')){
        this.drawCircle(11,this.selectedRoute.location, this.selectedColor, true);
        // console.log('svg-map (component), drawRoutes, Calling focusOnPoint from if and this.selectedRoute is:');
        // console.log(this.selectedRoute);

        this.focusOnPoint(this.selectedRoute.location);
      }
      // else if(this.selectedRoute.hasOwnProperty('point')){
      //   this.drawCircle(11, this.selectedRoute, this.selectedColor, true);
      //   console.log('svg-map (component), drawRoutes, Calling focusOnPoint from ELSE if');
      //   this.focusOnPoint(this.selectedRoute)
      // }
    }
    this.viewReady = true;
  }

  // fired from toolbar
  selectLocation(){

    // if a point was selected, then use the last location they selected.
    if(this.lastLocation !== null){
      this.locationToEmit  = {
        point:{x:parseFloat(this.lastLocation.point[0].toFixed(3)),
               y: parseFloat(this.lastLocation.point[1].toFixed(3))},
        t: parseFloat(this.lastLocation.t.toFixed(3)),
        wallCode: this.lastLocation.wallCode,
        floor: parseInt(this.lastLocation.wallCode[0]),
        //use dist to in orderBy query to get routes in order by location from left to right
        dist: this.lastLocation.wallCode + this.lastLocation.t.toString()
      };

      this.locationSet.emit(this.locationToEmit);
    }

    // if they did not select a location, but the component got location info from @input newRoute
    // (meaning they had seleceted a location, left, and returned to the component), then use that
    else if (this.locationToEmit !== null){
      this.locationSet.emit(this.locationToEmit);
    }

    // if they did not select anything and also have not previously selected anything, make them.
    else{
      let alert = this.alertCtrl.create({
        title: 'No Location Selected',
        subTitle: 'Please select a location representing where your route begins on the map.',
        buttons: ['Dismiss']
      });

      alert.present();

    }


  }

}



interface Projection{
  point:[number, number];
  sqDist:number;
  t:number; //the parameter, t, in P = P0 + At
}

//TODO: replace hitBoxPadding (magic number) with parameter
class Wall {
  hitBoxPadding: number = 20;
  wallCode:string;
  vertices:[number,number][];
  ready: Promise<any>;
  name: string; //the name of wall (North Wall, The Prow, etc)

  constructor(svgPath:any,previousPoint = null){ //TODO: get proper type for an svg path
    this.ready = new Promise((res)=>{
      this.init(svgPath,previousPoint).then(()=>{
        res(undefined);
      });


    });
  }

  init(svgPath:any, prevPoint=null):Promise<any>{
    return new Promise((res,rej)=>{

      let svgPathString = svgPath.getAttribute("d");
      this.wallCode = svgPath.getAttribute('id');
      this.name = svgPath.getAttribute('name');
      this.getVertexRepresentation(svgPathString).then((vertices)=>{
        this.vertices = vertices;
        res();
      });

    });

  }

  getVertexRepresentation(pathString:string,previousPoint = null):Promise<any>{
    //NOTE: absoute path coordinates are assumed. Does not work with relative
    //TODO: error handling: rej code is not implemented
    return new Promise((res,rej)=>{

      pathString = pathString.trim(); //remove whitespace from beginning and end

      let pathType = pathString[0]; //for line paths either 'm' or 'M
      let pathEnd = pathString[pathString.length -1];
      let pathStringArray = pathString.replace('M','').replace('z','')
                   .replace('Z','').trim().split(' ');

      //get vertices into form: [[x1,y1],[x2,y2],...]
      let pathVertices = pathStringArray.map(x => x.split(",").map(x=>Number(x)));

      if (pathEnd == 'z' || pathEnd == 'Z'){
        //close the path
        pathVertices.push(pathVertices[0]);
      }

      res(pathVertices);

    });
  }

  //TODO: define return interface
  getOrthProjAndDist(target:[number,number]){
    //input: point data (probably from user input)
    //returns: {point:[number,number],dist:number}
    //point: the coordinates of the target projected onto the walls
    //dist: the distance from the target to the projected point
    //TODO: maybe make this also find the dist along the wall (for database indexing)
    //note, t represents the vector from the initial point of the segment to the target
    //...s represents the vector from the initial point of the segment to the terminal point of the segment

    //build arrays of s and t vectors
    let sVectors = this.vertices.slice(0,this.vertices.length-1)
                   .map((p,i)=>{
                     //each segment vector is terminal point - initial point
                     return [this.vertices[i+1][0]-p[0], this.vertices[i+1][1]-p[1] ];
                   });

    let tVectors = this.vertices
                  .map((p)=>{
                    return[ target[0] - p[0], target[1]-p[1] ];
                  });
    //for each segment
    for (let i = 0; i<sVectors.length; i++){
      //project target onto the segment

      //is the projection on the segment?
      //yes-> get distance from target to segment
      //no -> rej with error "target not on segment"
    }

  }

  magnitude(dx,dy){
    return Math.sqrt(dx**dx + dy*dy);
  }

  dotProd(v1:[number,number],v2:[number,number]){
    return v1[0]*v2[0]+v1[1]*v1[1];
  }

  getClosestPoint(tx,ty){
    let smallestDist:number = null;
    let closestPoint: [number, number] = null;
    let segment = null;
    let t:number  = null;

    for (let i = 0; i<this.vertices.length-1; i++){
      let x1 = this.vertices[i][0];
      let y1 = this.vertices[i][1];
      let x2 = this.vertices[i+1][0];
      let y2 = this.vertices[i+1][1];

      let result = this.getClosestPointOnSegment(x1,y1,x2,y2,tx,ty);

      if (result && (!smallestDist ||  result.sqDist < smallestDist)){
        smallestDist = result.sqDist;
        closestPoint = result.point;
        t = result.t;
        segment = i;
      }
    }
    if(closestPoint){
      return {point:closestPoint, sqDist:smallestDist, segment: segment, t:t};
    }else{
      return null;
    }

  }

  getClosestPointOnSegment(x1,y1,x2,y2,tx,ty):Projection{

    //build hitbox
    let maxX = x1>x2? x1:x2 + this.hitBoxPadding ;
    let minX = x1<x2? x1:x2  - this.hitBoxPadding;
    let maxY = y1>y2? y1:y2 + this.hitBoxPadding;
    let minY = y1<y2? y1:y2 - this.hitBoxPadding;

    //is tx,ty outside of hitbox?
    if (tx < minX || tx > maxX || ty < minY || ty > maxY){
      return null;
    }


    if(x1 === x2){//the segment is horizontal
      return this.getProjToVertSeg(x1,y1,x2,y2,tx,ty);
    }else{
      if(y1 === y2){//the segment is vertical
        return this.getProjToHorizSeg(x1,y1,x2,y2,tx,ty);
      }else{//the segment is neither horizontal nor vertical => it is skew
        return(this.getProjToSkewSeg(x1,y1,x2,y2,tx,ty));
      }
    }
  }

  getProjToSkewSeg(x1,y1,x2,y2,tx,ty):Projection{
    //proj the tap (tx,ty) to a segment that is neight horizontal nor vertical

    let dx = x2-x1;
    let dy = y2-y1;
    let dx2 = dx*dx;
    let dy2 = dy*dy;

    let px:number = (tx*dx2 + x1*dy2 + dx*dy*(ty-y1)) /(dx2 + dy2);
    let isOnSegObj = this.skewProjIsOnSeg(px,x1,dx);
    if(isOnSegObj.isOnseg){

      let py:number = ((y2-y1)/(x2-x1))*(px-x1)+y1;
      let point: [number,number] = [px,py];
      let t = isOnSegObj.t;

      let sqrDist = Math.pow(tx - px, 2)+ Math.pow(ty - py,2);

      return { point:point, sqDist: sqrDist, t:t};
    }else{
      return null;
    }
  }

  skewProjIsOnSeg(ox,x1,dx){
    //input:ox is the x-coordinate of the orthog proj of the target
    //onto the segment with initial x-coordinate x1.
    //dx is the delta x for the segment
    //note, this method is based on the parametric equation (or vector eqn)
    //for a line: O = P0+At (pg. 691, Cohen, Precalculus, 6th edition)
    let to = (ox-x1)/dx;
    let isOnSeg =  (0 <= to && to <= 1);
    return {isOnseg: isOnSeg, t: to}
  }

  getProjToHorizSeg(x1,y1,x2,y2,tx,ty):Projection{
    //project the tap (tx,ty) to a horizontal segment

    let maxX = x1>x2? x1:x2;
    let minX = x1<x2? x1:x2;

    if(minX < tx && tx < maxX){//tap's proj is on the segment
      let p:[number,number] = [tx,y1]; //y-coordinates are all the same for the segment
      //for horiz seg, dist from tap to projection is len of vert seg
      let sqDist = Math.pow(y1 - ty,2);
      let t = Math.abs((tx - x1)/(x2-x1)); //pseudo t-value for ordering routes along horiz segment
      return {point: p, sqDist:sqDist, t:t};
    }else{
      return null;
    }
  }

  getProjToVertSeg(x1,y1,x2,y2,tx,ty):Projection{
    //project the tap (tx,ty) to a vertical segment
    let maxY = y1>y2? y1:y2;
    let minY = y1<y2? y1:y2;

    if (minY < ty && ty < maxY){
      let p:[number,number] = [x1,ty];

      //dist is just horiz dist to projected point:
      let sqDist= Math.pow(x1 - tx,2);
      let t = Math.abs((ty - y1)/(y2-y1)); //pseudo t-value for ordering routes along horiz segment
      return {point:p, sqDist:sqDist, t:t};
    }
  }



}
