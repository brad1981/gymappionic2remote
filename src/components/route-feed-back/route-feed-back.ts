import { Component, ViewChild, Input, EventEmitter, Output } from '@angular/core';
import { FilterSelectorComponent } from '../../components/filter-selector/filter-selector';
import { RouteEditorFeatures } from '../../providers/globalConstants/globalConstants';

import { Platform, Loading, LoadingController} from 'ionic-angular';

import {FeedbackProvider} from '../../providers/feedback/feedback';

@Component({
  selector: 'route-feed-back',
  templateUrl: 'route-feed-back.html'
})
export class RouteFeedBackComponent {
  @ViewChild('filterSelector') filterSelector: FilterSelectorComponent ;
  //the firestore path to the current route's doc
  //looks like /GYMS/{gymId}/MAPS/{mapsId}/ROUTES/{routeId}
  @Input() fStoreRouteDocPath: string = null;

  @Output() gotFeedback = new EventEmitter();

  templateContentReady:boolean = false;
  mode = 'feedback';
  feedBack: RouteEditorFeatures = null;
  postingFeedback: boolean = null;
  platformReady: boolean = false;

  constructor(public platform: Platform, public feedBackProv: FeedbackProvider,
    public loadingCtrl: LoadingController) {
    platform.ready().then(()=>{
      this.platformReady = true;
    });
  }

  ngAfterContentInit(){
    this.templateContentReady = true;
  }

  //the button that triggers this method is only visible if platform.ready() has resolved
  //TODO: loader is currently dismissed when upload succeds or fails.
  //... change so that user is alerted to failed upload
  async submitFeedback():Promise<RouteEditorFeatures>{

    this.postingFeedback = true;
    this.feedBack = this.filterSelector.getFilters() as RouteEditorFeatures;

    if(this.feedBack){
      let loader = this.loadingCtrl.create({
        content: "uploading your feedback..."
      });
      loader.present();
      try{
        const feedback = await this.feedBackProv.submitFeedback(this.fStoreRouteDocPath, this.feedBack);
        this.gotFeedback.emit(true);
        loader.dismiss();
        return feedback;

      }
      catch(error){
        loader.dismiss();
      };

    }
    else{
      throw new Error('route-feed-back component, submitFeedback: ERROR... could not get feedback from FilterSelectorComponent');
    }



  }

}
